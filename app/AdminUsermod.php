<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUsermod extends Model{
	protected $table 	= 'admins';
	protected $fillable = ['name', 'email', 'username', 'password', 'privilege', 'picture'];
	protected $hidden = [
			'password', 'remember_token',
	];
}
