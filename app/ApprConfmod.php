<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprConfmod extends Model{
	protected $table 	= 'approver_conf';
	protected $fillable = ['modul', 'adminid', 'priority'];
}
