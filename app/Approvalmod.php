<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Approvalmod extends Model{
	protected $table 	= 'approval';
	protected $fillable = ['modulid', 'adminid', 'status','priority','modul'];
}
