<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchieveCompanymod extends Model{
	
	protected $table 	= 'archieve_company';
	protected $fillable = ['typedoc','typefile','realname','randomname','idcompany'];
	
}