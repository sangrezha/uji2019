<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Archieveopsmod extends Model{
	protected $table 	= 'archieve_operation';
	protected $fillable = ['idizin','typedoc','typefile','idcompany','realname','randomname','category'];
}