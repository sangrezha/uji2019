<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Bumod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'badan_usaha';

    protected $fillable = [
        'name', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
