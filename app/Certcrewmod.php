<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Certcrewmod extends Model{
	
	protected $table 	= 'certcrew';
	protected $fillable = ['uniqdocument','idcompany','namacrew','nomorskill','unitkerja','categoryskill','jenis_uji','berlaku','keterangan','typefile','realname','randomname'];
	
}