<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Certmaintainmod extends Model{
	
	protected $table 	= 'certcrewmaintaince';
	protected $fillable = ['uniqdocument','idcompany','namacrew','nomorcert','tempat','jabatan','jenislatihan','tglterbit','keterangan','typefile','realname','randomname'];
	
}