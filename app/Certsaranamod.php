<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Certsaranamod extends Model{
	
	protected $table 	= 'certsarana';
	protected $fillable = ['uniqdocument','idcompany','nomorcert','nomorbadan','milik','jenis_sarana','jenis_uji','keterangan','typefile','realname','randomname','berlaku'];
	
}