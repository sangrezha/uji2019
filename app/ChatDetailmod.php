<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ChatDetailmod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'chat_detail';

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'starttime', 'nama', 'email', 'endtime', 'subject', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
