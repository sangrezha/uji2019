<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class ChatNewmod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'chatnew';

    protected $dates = [
        'created_at', 'updated_at', 'starttime', 'endtime'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
