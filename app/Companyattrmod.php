<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Companyattrmod extends Model{
	
	protected $table 	= 'company_attribute';
	protected $fillable = ['name','uniqid','npwp','is_permit'];
}