<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Eventmod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'event';

    protected $fillable = [
        'title', 'lead', 'content', 'publish_date', 'thumbnail', 'picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
