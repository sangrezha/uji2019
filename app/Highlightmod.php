<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Highlightmod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'highlight';

    protected $fillable = [
        'title', 'highlight', 'picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
