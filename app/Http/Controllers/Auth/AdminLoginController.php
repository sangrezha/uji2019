<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Captcha;
use DB;
use App\AdminUsermod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage, Mail; // use AdminUsermod;


class AdminLoginController extends Controller
{

    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = '';
      $this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function index()
    {
        return view('webmin.admin_template')->with('codepage',$this->codepage);
    }

    public function home()
    {
        return view('webmin.admin_template')->with('codepage',$this->codepage);
    }

    public function showLoginForm()
    {
      # code...
      // echo $this->variable1;exit;
      // print_r(config('app.halamanWeb'));
      // print_r(config('app.admPriv'));exit;
      return view('auth.webmin-login')->with('codepage',$this->codepage);
    }

    public function showForgotPassword()
    {
      return view('auth.webmin-forgotPassword')->with('codepage',$this->codepage);
    }

    public function forgotSent(Request $request)
    {
      # code...
      $rules = array(
        'email'   => 'required|email',
        'captcha' => 'required|captcha'
      );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
              return Redirect::route('admin.forgot')
                  ->withErrors($validator)
                  ->withInput($request->only('email'));
          }else {
              // store
              // print_r(Input::get('privilege'));exit;
              $adminCek = DB::table('admins')
                         ->where('email','=',$request->email)
                         ->first();
            //  print_r($adminCek->id);exit;
              if (isset($adminCek->id))
              {
                $admin = AdminUsermod::find($adminCek->id);
                $password_baru = str_random(6);
                $admin->password = Hash::make($password_baru);
                $admin->updated_by      = 'web system request';
                // redirect

                $data = array('email'=>$admin->email, 'name'=>$admin->name,'password_baru'=>$password_baru);
                Mail::send('auth.emails.forgotPassword', $data, function($message) use ($admin) {
                   $message->to($admin->email, $admin->name)->subject
                      ('Reset Password untuk Balai Pengujian Perkeretaapian CMS');
                   $message->from('no-reply@arieswdd.com','Aries Widodo');
                });
                $admin->save();

                Session::flash('message', 'Password sudah direset, silahkan cek email untuk mengetahui password baru anda! ');
                return Redirect::route('admin.login');

              } else {
                Session::flash('message', 'Email tidak terdaftar!');
                return Redirect::route('admin.forgot');

              }

      //return redirect()->back()->withInput($request->only('email'));
        }
      }

    public function logout(Request $request)
    {
      # code...
      Auth::guard('admin')->logout();
      // $request->session()->flush();
      // $request->session()->regenerate();
      return redirect()->intended(route('admin.login'));
    }

    public function login(Request $request)
    {
      # code...
      $this->validate($request, [
        'username' => 'required|min:6',
        'password' => 'required|min:6',
        'captcha' => 'required|captcha'
        ]);

        if (Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password])) {
          return redirect()->intended(route('admin.dashboard'));
        } else {
          Session::flash('message', 'username/password salah!');
        }

      return redirect()->back()->withInput($request->only('username'));
    }
}
