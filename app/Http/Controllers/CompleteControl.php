<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Certsaranamod;
use App\Certcrewmod;
use App\Certmaintainmod;
use App\Izinopmod;
use Auth;

class CompleteControl extends Controller
{
    public function operasisarana($iddoc)
	{
		$condition = [
			'idcompany' 	=> Auth::user()->idcompany,
			'uniqdocument'	=> $iddoc
		];
		$i=0;
		
		$rawx = Certsaranamod::where($condition);
		if($rawx->count() > 0){
			++$i;
		}
		
		$rawx = Certcrewmod::where($condition);
		if($rawx->count() > 0){
			++$i;
		}
		
		$rawx = Certmaintainmod::where($condition);
		if($rawx->count() > 0){
			++$i;
		}
		
		if($i == 3){
			Izinopmod::where('uniqid',$iddoc)->update(['is_complete' => 1]);
		}
	}
}
