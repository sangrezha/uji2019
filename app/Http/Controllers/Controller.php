<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        date_default_timezone_set("Asia/Bangkok");

        DB::statement("SET SESSION SQL_MODE = '' ");
    }

    public function getFilePathAttribute($value){
        $img = Image::make(public_path($value)); //your image I assume you have in public directory
        $img->insert(public_path('/images/watermark.png'), 'bottom-right'); //insert watermark in (also from public_directory)
        $img->save(public_path($value)); //save created image (will override old image)
        return $value; //return value
    }

}
