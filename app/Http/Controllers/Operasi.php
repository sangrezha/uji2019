<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Linpelmod;
use App\Izintmpmod;
use App\Izinopsmod;
use App\Archieveopsmod;
use App\Companyattrmod;
use Auth;
use File;
use Mail;

class Operasi extends Controller
{
    public function index(Request $req)
	{
		for($it=1;$it <= 12;$it++){
			$doc[$it] = $req->session()->get('sessizinops'.$it);
			$req->session()->forget('sessizinops'.$it);
		}
		
		
		if(!empty($doc)){
			foreach($doc as $docsess){
				$where = [
					'idcompany'		=> Auth::user()->idcompany,
					'randomname'	=> $docsess
				];
				
				$pathtemp = base_path('/storage/app/public/temp/'.$docsess);
				@unlink($pathtemp);
				Izintmpmod::where($where)->delete();
			}
		}
		
		$data['linpel'] = Linpelmod::orderBy('lintas');
		return view('contents.izinsaranaoperasi',$data);
	}
	
	public function frekuensi(Request $req)
	{
		$id = $req->input('idne');
		$rawfrek = Linpelmod::where('id',$id);
		$json['frek'] = $rawfrek->first()->frekuensi;
		
		return response()->json($json);
	}
	
	public function upload(Request $req)
	{
		$error = $sizefile = $typedoc = 0;
		$filecap = null;
		if($req->hasFile('userfile1')){
			$filecap = $req->file('userfile1');
			$typedoc = 1;
		} else if($req->hasFile('userfile2')){
			$filecap = $req->file('userfile2');
			$typedoc = 2;
		} else if($req->hasFile('userfile3')){
			$filecap = $req->file('userfile3');
			$typedoc = 3;
		} else if($req->hasFile('userfile4')){
			$filecap = $req->file('userfile4');
			$typedoc = 4;
		} else if($req->hasFile('userfile5')){
			$filecap = $req->file('userfile5');
			$typedoc = 5;
		} else if($req->hasFile('userfile6')){
			$filecap = $req->file('userfile6');
			$typedoc = 6;
		} else if($req->hasFile('userfile7')){
			$filecap = $req->file('userfile7');
			$typedoc = 7;
		} else if($req->hasFile('userfile8')){
			$filecap = $req->file('userfile8');
			$typedoc = 8;
		} else if($req->hasFile('userfile9')){
			$filecap = $req->file('userfile9');
			$typedoc = 9;
		} else if($req->hasFile('userfile10')){
			$filecap = $req->file('userfile10');
			$typedoc = 10;
		} else if($req->hasFile('userfile11')){
			$filecap = $req->file('userfile11');
			$typedoc = 11;
		} else if($req->hasFile('userfile12')){
			$filecap = $req->file('userfile12');
			$typedoc = 12;
		} else{
			$error = 1;
		}
		
		if($error == 0){
			if($filecap->isValid()){
				$arr_mime = [
                    'application/pdf',
					'image/jpeg'
                ];
				$mime = $filecap->getMimeType();
				if(in_array($mime,$arr_mime)){
					$typefile = null;
					if($mime == 'application/pdf'){
						$typefile = 'pdf';
					} elseif($mime == 'image/jpeg'){
						$typefile = 'jpeg';
					} else{
						$typefile = 'none';
					}
					
					$sizefile = $filecap->getSize();
					if($sizefile < 10485760){
						$namefile 	= $filecap->getClientOriginalName();
						$randomname = null;
						$rawExe = explode(".",$namefile);
						do{
							$randomname = md5($namefile.time().uniqid());
							$rawname 	= Izintmpmod::where('randomname',$randomname);
						}while($rawname->count() > 0);
						$randomname = $randomname.'.'.$rawExe[1];
						$data_insert = [
							'idcompany'	=> Auth::user()->idcompany,
							'typedoc' 	=> $typedoc,
							'typefile'	=> $typefile,
							'realname' 	=> $namefile,
							'randomname'=> $randomname
						];
						$folder		= base_path('storage/app/public/temp');
						$filecap->move($folder,$randomname);
						Izintmpmod::create($data_insert);
						
						$req->session()->put('sessizinops'.$typedoc, $randomname);
					} else{
						$error = 4;
					}
				} else{
					$error = 3;
				}
			} else{
				$error = 2;
			}
		}
		
		if($error == 0){
			$json = [
				'error' 	=> $error,
				'typedoc'	=> $typedoc,
				'random'	=> $randomname,
				'real'		=> $namefile
			];
		} else{
			$json['error'] = $error;
		}
		return response()->json($json);
	}
	
	public function remove(Request $req, $id)
	{
		$pathfile = base_path('storage/app/public/temp/'.$id);
		@unlink($pathfile);
		$gettypedoc = Izintmpmod::where('randomname',$id)->first()->typedoc;
		$req->session()->forget("sessizinops".$gettypedoc);
		Izintmpmod::where('randomname',$id)->delete();
		return;
	}
	
	public function submit(Request $req)
	{
		$arrdata = $req->input('datasend');
		$error = 0;
		$opsi_izin = $arrdata['opsi_izin'];
		if($arrdata['opsi_izin'] == 3){
			
			$doc = null;
			for($i = 1;$i <= 5; ++$i){
				if(!empty($req->session()->get('sessizinops'.$i))){
					$doc[$i] = $req->session()->get('sessizinops'.$i);
				}
			}
			
			//--- document not allow all empty ---
			if(count($doc) < 5){
				$error = 1;
			} else{
				$uniqid = null;
				//--- generate uniqid ---
				do{
					$uniqid = uniqid();
					$rawID = Izinopsmod::where('uniqid',$uniqid);
				} while($rawID->count() > 0);
				//---- save in table ---
				$complete = 0;
				if(sizeof($doc) == 5){$complete = 1;}
				$insdata = [
					'uniqid'	=> $uniqid,
					'idcompany' => Auth::user()->idcompany,
					'idlinpel'	=> $arrdata['linpel'],
					'freqreq'	=> $arrdata['minta'],
					'is_complete'=> $complete,
					'perihal'	=> $arrdata['perihal'],
					'opsi'		=> $opsi_izin
				];
				
				Izinopsmod::create($insdata);
				
				//--- move to folder izin operator ---
				
				foreach($doc as $d){
					$wherefiletmp = [
						'idcompany' 	=> Auth::user()->idcompany,
						'randomname'	=> $d
					];
					
					$rawfile = Izintmpmod::where($wherefiletmp)->first();
					$name_random = $rawfile->randomname;
					do{
						$condwhere = [
							'idcompany' 	=> Auth::user()->idcompany,
							'randomname'	=> $name_random
						];
						$check_name = Archieveopsmod::where($condwhere);
						$name_random = md5($name_random.time().uniqid()).'.'.$rawfile->typefile;
					} while($check_name->count() > 0);
					
					$for_archieve = [
						'idcompany'	=> $rawfile->idcompany, 
						'typedoc'	=> $rawfile->typedoc,
						'typefile'	=> $rawfile->typefile,
						'realname' 	=> $rawfile->realname,
						'randomname'=> $name_random,
						'idizin'	=> $uniqid
					];
					
					Archieveopsmod::create($for_archieve);
					$doc_old = base_path('/storage/app/public/temp/'.$rawfile->randomname);
					$doc_new = base_path('/storage/app/public/izinoperasi/'.$name_random);
					
					File::move($doc_old,$doc_new);
					
					$conddel = [
						'idcompany'		=> Auth::user()->idcompany,
						'randomname'	=> $rawfile->randomname
					];
					
					Izintmpmod::where($conddel)->delete();
					@unlink($doc_old);
					
					Companyattrmod::where('uniqid',Auth::user()->idcompany)->update(['is_permit' => 1]);
				}
				
				for($i=1;$i<=5;$i++){
					$req->session()->forget('sessizinops'.$i);
				}
				$bu = Companyattrmod::where('uniqid',Auth::user()->idcompany)->first()->name;
				$datamail = ['bu' => $bu];
				Mail::send('panels.maildoc',$datamail,function($message){
					$message->to('llaka.kspu@dephub.go.id','Admin Perijinan Online')
						->bcc('dessy@teamwork-design.com','Dessy')
						->from('yonie@teamwork-design.com', 'System Admin Perijinan Online')
						->subject('Notifikasi Dokumen Izin Baru');
				});
			}
		} else if($arrdata['opsi_izin'] == 2){ // --- Penambahan Frekuensi ----
			for($j = 6;$j <= 12;$j++){
				if(!empty($req->session()->get('sessizinops'.$j))){
					$doc[$j] = $req->session()->get('sessizinops'.$j);
				}
			}
			
			if(count($doc) < 6){
				$error = 1;
			} else{
				$uniqid = null;
				//--- generate uniqid ---
				do{
					$uniqid = uniqid();
					$rawID = Izinopsmod::where('uniqid',$uniqid);
				} while($rawID->count() > 0);
				//---- save in table ---
				$complete = 0;
				if(sizeof($doc) == 7){$complete = 1;}
				$insdata = [
					'uniqid'	=> $uniqid,
					'idcompany' => Auth::user()->idcompany,
					'is_complete'=> $complete,
					'perihal'	=> $arrdata['perihal'],
					'opsi'		=> $opsi_izin
				];
				
				Izinopsmod::create($insdata);
				
				//--- move to folder izin operator ---
				
				foreach($doc as $d){
					$wherefiletmp = [
						'idcompany' 	=> Auth::user()->idcompany,
						'randomname'	=> $d
					];
					
					$rawfile = Izintmpmod::where($wherefiletmp)->first();
					$name_random = $rawfile->randomname;
					do{
						$condwhere = [
							'idcompany' 	=> Auth::user()->idcompany,
							'randomname'	=> $name_random
						];
						$check_name = Archieveopsmod::where($condwhere);
						$name_random = md5($name_random.time().uniqid()).'.'.$rawfile->typefile;
					} while($check_name->count() > 0);
					
					$for_archieve = [
						'idcompany'	=> $rawfile->idcompany, 
						'typedoc'	=> $rawfile->typedoc,
						'typefile'	=> $rawfile->typefile,
						'realname' 	=> $rawfile->realname,
						'randomname'=> $name_random,
						'idizin'	=> $uniqid
					];
					
					Archieveopsmod::create($for_archieve);
					$doc_old = base_path('/storage/app/public/temp/'.$rawfile->randomname);
					$doc_new = base_path('/storage/app/public/izinoperasi/'.$name_random);
					
					File::move($doc_old,$doc_new);
					
					$conddel = [
						'idcompany'		=> Auth::user()->idcompany,
						'randomname'	=> $rawfile->randomname
					];
					
					Izintmpmod::where($conddel)->delete();
					@unlink($doc_old);
					
					Companyattrmod::where('uniqid',Auth::user()->idcompany)->update(['is_permit' => 1]);
				}
				
				for($i=6;$i<=12;$i++){
					$req->session()->forget('sessizinops'.$i);
				}
				
				$bu = Companyattrmod::where('uniqid',Auth::user()->idcompany)->first()->name;
				$datamail = ['bu' => $bu];
				Mail::send('panels.maildoc',$datamail,function($message){
					$message->to('llaka.kspu@dephub.go.id','Admin Perijinan Online')
						->bcc('dessy@teamwork-design.com','Dessy')
						->from('yonie@teamwork-design.com', 'System Admin Perijinan Online')
						->subject('Notifikasi Dokumen Frekuensi Baru');
				});
			}
		}
		$json['error'] 	= $error;
		$json['url']	= url('/member/profile');
		return response()->json($json);
	}
	
	public function tolinpel()
	{
		$tanggal = date("Y-m-d H:i:s");
		$security = base64_encode(md5($tanggal).'linpel');
		$url = 'http://180.250.60.27/linpel/login-pos';
		$data = [
			'nama'		=> Auth::user()->name,
			'userid' 	=> Auth::user()->uniqid,
			'email'		=> Auth::user()->email,
			'tanggal'	=> $tanggal,
			'security'	=> $security
		];
		
		$header = [
			"cache-control: no-cache",
			"content-type: multipart/form-data;",
		];
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_TIMEOUT,60);
		//curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$res = curl_exec($ch);
		curl_close($ch);
		//$data = json_decode($res);
		$json['data'] = $res;
		$json['nama'] = Auth::user()->name;
		$json['userid'] = Auth::user()->uniqid;
		$json['email'] = Auth::user()->email;
		$json['tanggal'] = $tanggal;
		$json['security'] = $security;
		return response()->json($json);
	}
}
