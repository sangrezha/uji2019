<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Companyattrmod;
use App\ArchieveCompanymod;
use App\Izinopsmod;

class Profile extends Controller
{
    public function index()
	{
		$data['permit'] = Companyattrmod::where('uniqid',Auth::user()->idcompany)->first()->is_permit;
		$data['menu1'] 	= 1;
		$data['docs'] 	= ArchieveCompanymod::where('idcompany', Auth::user()->idcompany);
		return view('contents.profilview',$data);
	}
	
	public function getdata()
	{
		$temp = $doc = null;
		$rawProfile = Companyattrmod::where('uniqid',Auth::user()->idcompany);
		if($rawProfile->count() > 0){
			$profile = $rawProfile->first();
			$temp = [
				'company'	=> $profile->name,
				'alamat'	=> (isset($profile->alamat)) ? $profile->alamat : '',
				'pemimpin'	=> (isset($profile->pemimpin)) ? $profile->pemimpin : '',
				'npwp'		=> $profile->npwp
			];
			
			$rawDoc = ArchieveCompanymod::where('idcompany', Auth::user()->idcompany);
			if($rawDoc->count() > 0){
				$i = 0;
				foreach($rawDoc->get() as $rowdoc){
					switch($rowdoc->typedoc){
						case 1: $arsip = 'SKDP';break;
						case 2: $arsip = 'NPWP';break;
						case 3: $arsip = 'AKTA';break;
						case 4: $arsip = 'KTP';break;
					};
					$doc[] = [
						'no' 	=> ++$i,
						'arsip'	=> $arsip,
						'id'	=> $rowdoc->id,
						'url'	=> url('/member/download',['id' => $rowdoc->realname])
					];
				}
			} else{
				$doc = 0;
			}
			
		} else{
			$temp 	= 0;
			$doc 	= 0;
		}
		
		$json['data'] 	= $temp;
		$json['doc']	= $doc;
		return response()->json($json);
	}
	
	public function getcompany()
	{
		$rawInfo = Companyattrmod::where('uniqid',Auth::user()->idcompany)->first();
		$json['alamat'] 	= $rawInfo->alamat;
		$json['perusahaan'] = $rawInfo->name;
		$json['pemimpin'] 	= $rawInfo->pemimpin;
		
		return response()->json($json);
	}
	
	public function getupdate(Request $req)
	{
		$arrdata = $req->input('datasend');
		
		$data = [
			'alamat'	=> $arrdata['alamat'],
			'pemimpin'	=> $arrdata['pemimpin']
		];
		
		Companyattrmod::where('uniqid',Auth::user()->idcompany)->update($data);
		$raw = Companyattrmod::where('uniqid',Auth::user()->idcompany)->first();
		return response()->json($raw);
	}
	
	public function allgetpermit()
	{
		$temp = null;
		//--- For Monitoring ---
		
		$whmonitor = [
			'idcompany' 	=> Auth::user()->idcompany,
			'is_done'		=> 0
		];
		
		$rawSaranaOps = Izinopsmod::where($whmonitor);
		if($rawSaranaOps->count() > 0){
			$i = 0;
			foreach($rawSaranaOps->get() as $opsSarana){
				$status = null;
				if($opsSarana->is_complete == 0){
					$status = 'Dokumen tidak lengkap';
				} else{
					$status = 'Proses';
				}
				$temp[] = [
					'no'		=> ++$i,
					'izin'		=> 'Sarana',
					'perihal'	=> $opsSarana->perihal,
					'jenis' 	=> 'Operasi',
					'status'	=> $status,
					'iddoc'		=> $opsSarana->uniqid
				];
			}
		}
		
		$json['data'] = $temp;
		return response()->json($json);
	}
	
	public function download($id)
	{
		$where = [
			'realname' 	=> $id,
			'idcompany'	=> Auth::user()->idcompany
		];
		$namefile = ArchieveCompanymod::where($where)->first()->randomname;
		$folder = base_path('/storage/app/public/company');
		$path = $folder.'/'.$namefile;
		
		return response()->file($path);
	}
}
