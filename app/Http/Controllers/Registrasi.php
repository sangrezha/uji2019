<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\Tempfilemod;
use App\Companyattrmod;
use App\ArchieveCompanymod;
use Mail;
use App\User;
use Excel;

class Registrasi extends Controller
{
    public function verify()
    {
    	
		$data['menu1'] 	= 1;
		return view('contents.verifyregister',$data);
    }
	
	public function getdata()
	{
		$where = [
			'level'		=> 8,
			'active'	=> 0
		];
		
		$rawdata = User::where($where);
		$temp = null;
		if($rawdata->count() > 0){
			$i = 0;
			foreach($rawdata->get() as $raw){
				$company = Companyattrmod::where('uniqid', $raw->idcompany);
				$temp[] = [
					'no' 	=> ++$i,
					'nama'	=> $raw->name,
					'bu'	=> ($company->first()->name != null) ? $company->first()->name : '',
					'email'	=> $raw->email,
					'id'	=> $raw->uniqid
				];
			}
		} else{
			$temp = 0;
		}
		
		$json['data'] = $temp;
		return response()->json($json);
	}
	
	public function terima($id)
	{
		User::where('uniqid',$id)->update(['active' => 1]);
		$email = User::where('uniqid',$id)->first()->email;
		$datamail = [
				'pesan' => 'Terima kasih telah menunggu proses verifikasi akun untuk perizinan online anda telah aktif. Silakan login kembali'
			];
		Mail::send('panels.mailtolakreg',$datamail,function($message) use($email){
				$message->to($email,'Perijinan Online User')
					->bcc('dessy@teamwork-design.com','Dessy')
					->from('llaka.kspu@dephub.go.id', 'Admin Perijinan Online')
					->subject('Notifikasi Diterima Pengguna Baru');
			});
		return;
	}
	
	public function tolak($id)
	{
		$rawInfo = User::where('uniqid',$id)->first();
		$email = $rawInfo->email;
		$company = $rawInfo->idcompany;
		$arsip = ArchieveCompanymod::where('idcompany',$company);
		if($arsip->count() > 0){
			foreach($arsip->get() as $ll){
				$pathdel = base_path('/storage/app/public/company/'.$ll->randomname);
				@unlink($pathdel);
				ArchieveCompanymod::where('randomname',$ll->randomname)->delete();
			}
		}
		Companyattrmod::where('uniqid',$company)->delete();
		User::where('uniqid',$id)->delete();
		$datamail = [
				'pesan' => 'Permohonan perizinan Operasi Anda ditolak karena (data tidak sesuai/data tidak lengkap) silakan anda melakukan pengkinian/edit data pendukung Izin Operasi sesuai dengan persyaratan.'
			];
		Mail::send('panels.mailtolakreg',$datamail,function($message) use($email){
				$message->to($email,'Perijinan Online User')
					->bcc('dessy@teamwork-design.com','Dessy')
					->from('llaka.kspu@dephub.go.id', 'Admin Perijinan Online')
					->subject('Notifikasi Ditolak Pengguna Baru');
			});
		
		return;
	}
	
	public function formregis(Request $req)
	{
		$doc[1] = $req->session()->forget('documentsess1');
		$doc[2] = $req->session()->forget('documentsess2');
		$doc[1] = $req->session()->forget('documentsess3');
		$doc[1] = $req->session()->forget('documentsess4');
		
		if(!empty($doc)){
			foreach($doc as $l){
				$pathtemp = base_path('/storage/app/public/temp/'.$l);
				@unlink($pathtemp);
				Tempfilemod::where('randomname',$l)->delete();
			}
		}
		return view('contents.formregister');
	}
	
	public function upload(Request $req)
	{
		$error = $sizefile = $typedoc = 0;
		$filecap = null;
		if($req->hasFile('userfile1')){
			$filecap = $req->file('userfile1');
			$typedoc = 1;
		} elseif($req->hasFile('userfile2')){
			$filecap = $req->file('userfile2');
			$typedoc = 2;
		} elseif($req->hasFile('userfile3')){
			$filecap = $req->file('userfile3');
			$typedoc = 3;
		} elseif($req->hasFile('userfile4')){
			$filecap = $req->file('userfile4');
			$typedoc = 4;
		} else{
			$error = 1;
		}
		
		if($error == 0){
			if($filecap->isValid()){
				$arr_mime = [
                    'application/pdf',
					'image/jpeg'
                ];
				$mime = $filecap->getMimeType();
				if(in_array($mime,$arr_mime)){
					$typefile = null;
					if($mime == 'application/pdf'){
						$typefile = 'pdf';
					} elseif($mime == 'image/jpeg'){
						$typefile = 'jpeg';
					} else{
						$typefile = 'none';
					}
					
					$sizefile = $filecap->getSize();
					if($sizefile < 5000000){
						$namefile 	= $filecap->getClientOriginalName();
						$randomname = null;
						$rawExe = explode(".",$namefile);
						do{
							$randomname = md5($namefile.time().uniqid());
							$rawname 	= Tempfilemod::where('randomname',$randomname);
						}while($rawname->count() > 0);
						$randomname = $randomname.'.'.$rawExe[1];
						$data_insert = [
							'typedoc' 	=> $typedoc,
							'typefile'	=> $typefile,
							'realname' 	=> $namefile,
							'randomname'=> $randomname
						];
						$folder		= base_path('storage/app/public/temp');
						$filecap->move($folder,$randomname);
						Tempfilemod::create($data_insert);
						
						$req->session()->put('documentsess'.$typedoc, $randomname);
					} else{
						$error = 4;
					}
				} else{
					$error = 3;
				}
			} else{
				$error = 2;
			}
		}
		
		if($error == 0){
			$json = [
				'error' 	=> $error,
				'typedoc'	=> $typedoc,
				'random'	=> $randomname,
				'real'		=> $namefile
			];
		} else{
			$json['error'] = $error;
		}
		return response()->json($json);
	}
	
	public function remove(Request $req, $id)
	{
		$pathfile = base_path('storage/app/public/temp/'.$id);
		@unlink($pathfile);
		$gettypedoc = Tempfilemod::where('randomname',$id)->first()->typedoc;
		$req->session()->forget("documentsess".$gettypedoc);
		Tempfilemod::where('randomname',$id)->delete();
		return;
	}
	
	public function save(Request $req)
	{
		$error = 0;
		$companyname = null;
		$doc1 = $req->session()->get('documentsess1');
		$doc2 = $req->session()->get('documentsess2');
		$doc3 = $req->session()->get('documentsess3');
		$doc4 = $req->session()->get('documentsess4');
		$arrdata = $req->input('datasend');
		
		if(empty($doc1)){
			$error = 1;
		} else if(empty($doc2)){
			$error = 2;
		} else if(empty($doc3)){
			$error = 3;
		}  else if(empty($doc4)){
			$error = 6;
		} else{
			$companyname 	= strtoupper($arrdata['company']);
			$check_company 	= Companyattrmod::where('name',$companyname);
			$check_username = User::where('username',$arrdata['username']);
			$check_email 	= User::where('email',$arrdata['email']);
			
			if($check_company->count() > 0){
				$error = 4;
			} else if($check_username->count() > 0){
				$error = 5;
			} else if($check_email->count() > 0){
				$error = 7;
			}
		}
		
		if($error == 0){
			$username = $arrdata['username'];
			$password = bcrypt($arrdata['password']);
			$datamail = [
				'username' => $username,
				'password' => $arrdata['password']
			];
			$email = $arrdata['email'];
		
			Mail::send('panels.mailnewuser',$datamail,function($message) use($email){
				$message->to($email,'Perijinan Online User')
					->bcc('dessy@teamwork-design.com','Dessy')
					->from('llaka.kspu@dephub.go.id', 'Admin Perijinan Online')
					->subject('Notifikasi Pengguna Baru');
			});
			if(sizeof(Mail::failures()) > 0){ $error == 8;}
			
			
		}
		if($error == 0){
			$uniq_company = $uniq_account = null;
			do{
				$uniq_company 	= uniqid();
				$raw_uniq_comp 	= Companyattrmod::where('uniqid',$uniq_company);
			} while($raw_uniq_comp->count() > 0);
			
			do{
				$uniq_account = uniqid();
				$raw_uniq_acoount = User::where('uniqid',$uniq_account);
			} while($raw_uniq_comp-> count() > 0);
			
			$company_data = [
				'uniqid' 	=> $uniq_company,
				'name'		=> $companyname,
				'npwp'		=> $arrdata['npwp']
			];
			
			$account_data = [
				'uniqid' 	=> $uniq_account,
				'idcompany'	=> $uniq_company,
				'username'  => $username,
				'password'	=> $password,
				'level'		=> 8,
				'name'		=> $arrdata['name'],
				'email'		=> $arrdata['email']
			];
			
			if($error == 0){
				Companyattrmod::create($company_data);
				User::create($account_data);
				
				//---- Save archieve document SIUP ----
				$siup = $req->session()->get('documentsess1');
				$parse_siup = explode(".",$siup);
				$name_siup 	= $parse_siup[0];
				$ext_siup 	= $parse_siup[1];
				$check_siup = ArchieveCompanymod::where('randomname',$siup);
				
				//--- if the same name file ---
				if($check_siup->count() > 0){
					do{
						$siup = md5($name_siup.uniqid());
						$siup = $siup.'.'.$ext_siup;
						$check_siup = ArchieveCompanymod::where('randomname',$siup);
					} while($check_siup->count() > 0);
				}
				$old_siup = $req->session()->get('documentsess1');
				$nama_nyata = Tempfilemod::where('randomname',$old_siup)->first();
				$siup_data = [
					'typedoc'	=> 1,
					'typefile' 	=> $ext_siup,
					'randomname'=> $siup,
					'realname'	=> $nama_nyata->realname,
					'idcompany' => $uniq_company
				];
				ArchieveCompanymod::create($siup_data);
				
				$siup_old = base_path('/storage/app/public/temp/'.$old_siup);
				$siup_new = base_path('/storage/app/public/company/'.$siup);
				File::move($siup_old,$siup_new);
				@unlink($siup_old);
				Tempfilemod::where('randomname',$old_siup)->delete();
				$req->session()->forget("documentsess1");
				//---- end save SIUP ----
				
				//--- Save archieve document NPWP ---
				$npwp = $req->session()->get('documentsess2');
				$parse_npwp = explode(".",$npwp);
				$name_npwp 	= $parse_npwp[0];
				$ext_npwp 	= $parse_npwp[1];
				$check_npwp = ArchieveCompanymod::where('randomname', $npwp);
				
				if($check_npwp->count() > 0){
					do{
						$npwp = md5($name_npwp.uniqid());
						$npwp = $npwp.'.'.$ext_npwp;
						$check_npwp = ArchieveCompanymod::where('randomname',$npwp);
					} while($check_npwp->count() > 0);
				}
				$old_npwp = $req->session()->get('documentsess2');
				$nama_nyata = Tempfilemod::where('randomname',$old_npwp)->first();
				$npwp_data = [
					'typedoc'	=> 2,
					'typefile' 	=> $ext_npwp,
					'randomname'=> $npwp,
					'realname'	=> $nama_nyata->realname,
					'idcompany' => $uniq_company
				];
				ArchieveCompanymod::create($npwp_data);
				$old_npwp = $req->session()->get('documentsess2');
				$npwp_old = base_path('/storage/app/public/temp/'.$old_npwp);
				$npwp_new = base_path('/storage/app/public/company/'.$npwp);
				File::move($npwp_old,$npwp_new);
				@unlink($npwp_old);
				Tempfilemod::where('randomname',$old_npwp)->delete();
				$req->session()->forget("documentsess2");
				//---- end save NPWP ----
				
				//---- Save archieve document AKTA ---
				$akta = $req->session()->get('documentsess3');
				$parse_akta = explode(".",$akta);
				$name_akta 	= $parse_akta[0];
				$ext_akta 	= $parse_akta[1];
				$check_akta = ArchieveCompanymod::where('randomname',$akta);
				
				if($check_akta->count() > 0){
					do{
						$akta = md5($name_akta.uniqid());
						$akta = $akta.'.'.$ext_akta;
						$check_akta = ArchieveCompanymod::where('randomname',$akta);
					} while($check_akta->count() > 0);
				}
				$old_akta = $req->session()->get('documentsess3');
				$nama_nyata = Tempfilemod::where('randomname',$old_akta)->first();
				$akta_data = [
					'typedoc'	=> 3,
					'typefile' 	=> $ext_akta,
					'randomname'=> $akta,
					'realname'	=> $nama_nyata->realname,
					'idcompany' => $uniq_company
				];
				ArchieveCompanymod::create($akta_data);
				
				$akta_old = base_path('/storage/app/public/temp/'.$old_akta);
				$akta_new = base_path('/storage/app/public/company/'.$akta);
				File::move($akta_old,$akta_new);
				@unlink($akta_old);
				Tempfilemod::where('randomname',$old_akta)->delete();
				$req->session()->forget("documentsess3");
				
				//---- end save AKTA
				
				
				//---- Save archieve document KTP ---
				$ktp = $req->session()->get('documentsess4');
				$parse_ktp = explode(".",$ktp);
				$name_ktp 	= $parse_ktp[0];
				$ext_ktp 	= $parse_ktp[1];
				$check_ktp = ArchieveCompanymod::where('randomname',$ktp);
				
				if($check_ktp->count() > 0){
					do{
						$ktp = md5($name_ktp.uniqid());
						$ktp = $ktp.'.'.$ext_ktp;
						$check_ktp = ArchieveCompanymod::where('randomname',$ktp);
					} while($check_ktp->count() > 0);
				}
				$old_ktp = $req->session()->get('documentsess4');
				$nama_nyata = Tempfilemod::where('randomname',$old_ktp)->first();
				$ktp_data = [
					'typedoc'	=> 4,
					'typefile' 	=> $ext_ktp,
					'randomname'=> $ktp,
					'realname'	=> $nama_nyata->realname,
					'idcompany' => $uniq_company
				];
				ArchieveCompanymod::create($ktp_data);
				
				$ktp_old = base_path('/storage/app/public/temp/'.$old_ktp);
				$ktp_new = base_path('/storage/app/public/company/'.$ktp);
				File::move($ktp_old,$ktp_new);
				@unlink($ktp_old);
				Tempfilemod::where('randomname',$old_ktp)->delete();
				$req->session()->forget("documentsess4");
				
				//---- end save KTP ----
				$datamail = [
					'tgl'	=> date("d/m/Y"),
					'nama'	=> $arrdata['name'],
					'bu'	=> $companyname
				];
				Mail::send('panels.mailkspualert',$datamail,function($message){
					$message->to('llaka.kspu@dephub.go.id','Admin Perijinan Online')
						->bcc('dessy@teamwork-design.com','Dessy')
						->from('yonie@teamwork-design.com', 'System Admin Perijinan Online')
						->subject('Notifikasi Registrasi Pengguna Baru');
				});
			}
			
		}
		$json = ['error' => $error];
		return response()->json($json);
	}
	
	public function detail($id)
	{
		
	}
	
	public function linpel()
	{
		$urlget = 'http://posko.djka.dephub.go.id/pusdatin/masterstasiun';
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$urlget);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		$res = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($res);
		
		$tgljam = date("dmY_Hi");
		Excel::create('master_stasiun_'.$tgljam, function($excel) use($data){
			$rawdata = $data;
			$excel->sheet('Master Stasiun', function($sheet) use($data){
				$sheet->cell('A3:C3',function($cell){
                    $cell->setFontWeight('bold');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
				
				$sheet->setCellValue('A3','Daop/Divre');
                $sheet->setCellValue('B3','Singkatan');
                $sheet->setCellValue('C3','Stasiun');
				
				$startrow = 4;
				
				foreach($data as $rowdata){
					$sheet->setCellValue('A'.$startrow,$rowdata->daopdivre);
					$sheet->setCellValue('B'.$startrow,$rowdata->singkatan);
					$sheet->setCellValue('C'.$startrow,$rowdata->stasiun);
					++$startrow;
				}
			});
		})->export('xlsx');
		
	}
}