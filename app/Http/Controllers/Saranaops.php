<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Linpelmod;
use App\Izinopsmod;
use App\Izintmpmod;
use Auth;
use File;

class Saranaops extends Controller
{
    public function index(Request $req)
	{
		$surat = $req->session()->get('sesssurat');
		if(!empty($surat)){
			$where = [
				'idcompany'		=> Auth::user()->idcompany,
				'randomname'	=> $surat
			];
			$pathtemp = base_path('/storage/app/public/temp/'.$surat);
			@unlink($pathtemp);
			Izintmpmod::where($where)->delete();
			$req->session()->forget('sesssurat');
		}
		$data['linpel'] = Linpelmod::orderBy('lintas');
		return view('contents.sarana_ops',$data);
	}
	
	public function add(Request $req)
	{
		$surat = $req->session()->get('sesssurat');
		$error = 0;
		if(!empty($surat)){
			$arrdata = $req->input('datasend');
			$perihal = $arrdata['perihal'];
			$checkperihal = Izinopsmod::where('perihal',$perihal);
			
			if($checkperihal->count() < 1){
				
				$uniq = null;
				
				do{
					$uniq = uniqid();
					$check = Izinopsmod::where('uniqid',$uniq);
				} while($check->count() > 0);
				
				if($arrdata['opsi_izin'] == 3){
					$cond = [
						'idcompany'	=> Auth::user()->idcompany,
						'randomname'=> $surat
					];
					
					$sm = Izintmpmod::where($cond)->first();
					$random = $sm->randomname;
					$extfile = $sm->typefile;
					do{
						$cmfile = Izinopsmod::where('randomname',$random);
						$random = md5($random.time().uniqid()).'.'.$extfile;
					}while($cmfile->count() > 0);
					
					$tanggal = str_replace("/","-",$arrdata['tanggal']);
					$tanggal = date("Y-m-d",strtotime($tanggal));
					$data_izin = [
						'idlinpel'	=> $arrdata['linpel'],
						'freqreq'	=> $arrdata['minta'],
						'perihal'	=> $perihal,
						'uniqid'	=> $uniq,
						'idcompany'	=> Auth::user()->idcompany,
						'tanggal'	=> $tanggal,
						'nomorsurat'=> $arrdata['surat'],
						'typefile'	=> $extfile,
						'realname'	=> $sm->realname,
						'randomname'=> $random
					];
					
					Izinopsmod::create($data_izin);
					
					$doc_old = base_path('/storage/app/public/temp/'.$sm->randomname);
					$doc_new = base_path('/storage/app/public/suratoperasi/'.$random);
					File::move($doc_old,$doc_new);
					$conddel = [
						'idcompany'		=> Auth::user()->idcompany,
						'randomname'	=> $sm->randomname
					];
					
					Izintmpmod::where($conddel)->delete();
					@unlink($doc_old);
					
					$json['url'] 	= url('/izin/operasi/sertifikatsarana',['id' => $uniq]);
				}
			} else{
				$error = 2;
			}
		} else{
			$error = 1;
		}
		$json['error'] = $error;
		return response()->json($json);
	}
	
	public function uploadsurat(Request $req)
	{
		$error = $sizefile = $typedoc = 0;
		$filecap = null;
		if($req->hasFile('userfile')){
			$filecap = $req->file('userfile');
			if($filecap->isValid()){
				
				$arr_mime = [
                    'application/pdf',
					'image/jpeg'
                ];
				$mime = $filecap->getMimeType();
				
				if(in_array($mime,$arr_mime)){
					$typefile = null;
					if($mime == 'application/pdf'){
						$typefile = 'pdf';
					} elseif($mime == 'image/jpeg'){
						$typefile = 'jpeg';
					} else{
						$typefile = 'none';
					}
					
					$sizefile = $filecap->getSize();
					if($sizefile < 1000000){
						$namefile 	= $filecap->getClientOriginalName();
						$randomname = null;
						$rawExe = explode(".",$namefile);
						do{
							$randomname = md5($namefile.time().uniqid());
							$rawname 	= Izintmpmod::where('randomname',$randomname);
						}while($rawname->count() > 0);
						$randomname = $randomname.'.'.$rawExe[1];
						$data_insert = [
							'idcompany'	=> Auth::user()->idcompany,
							'typefile'	=> $typefile,
							'realname' 	=> $namefile,
							'randomname'=> $randomname
						];
						$folder		= base_path('storage/app/public/temp');
						$filecap->move($folder,$randomname);
						Izintmpmod::create($data_insert);
						
						$req->session()->put('sesssurat', $randomname);
					} else{
						$error = 4;
					}
				} else{
					$error = 3;
				}
				
			} else{
				$error = 2;
			}
		} else{
			$error = 1;
		}
		
		if($error == 0){
			$json = [
				'error' 	=> $error,
				'typedoc'	=> 1,
				'random'	=> $randomname,
				'real'		=> $namefile
			];
		} else{
			$json['error'] = $error;
		}
		return response()->json($json);
	}
	
	public function remove(Request $req, $id)
	{
		$pathfile = base_path('storage/app/public/temp/'.$id);
		@unlink($pathfile);
		$gettypedoc = Izintmpmod::where('randomname',$id)->first()->typedoc;
		$req->session()->forget("sesssurat".$gettypedoc);
		Izintmpmod::where('randomname',$id)->delete();
		return;
	}
	
	public function tolinpel()
	{
		$tanggal = date("Y-m-d H:i:s");
		$security = base64_encode(md5($tanggal).'linpel');
		$url = 'http://posko.djka.dephub.go.id/linpel/login-pos';
		$data = [
			'nama'		=> Auth::user()->name,
			'userid' 	=> Auth::user()->uniqid,
			'email'		=> Auth::user()->email,
			'tanggal'	=> $tanggal,
			'security'	=> $security
		];
		
		$header = [
			"cache-control: no-cache",
			"content-type: multipart/form-data;",
		];
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_TIMEOUT,60);
		//curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$res = curl_exec($ch);
		curl_close($ch);
		//$data = json_decode($res);
		$json['data'] = $res;
		$json['nama'] = Auth::user()->name;
		$json['userid'] = Auth::user()->username;
		$json['email'] = Auth::user()->email;
		$json['tanggal'] = $tanggal;
		$json['security'] = $security;
		return response()->json($json);
	}
}
