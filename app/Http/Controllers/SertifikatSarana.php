<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Izintmpmod;
use App\Certsaranamod;
use Auth;
use File;
use App\Http\Controllers\CompleteControl;

class SertifikatSarana extends Controller
{
    public function formcertsarana(Request $req, $id)
	{
		$doc = $req->session()->get('sesscertsarana');
		if(!empty($doc)){
			$where = [
				'idcompany'		=> Auth::user()->idcompany,
				'randomname'	=> $doc
			];
			$pathtemp = base_path('/storage/app/public/temp/'.$doc);
			@unlink($pathtemp);
			Izintmpmod::where($where)->delete();
			$req->session()->forget('sesscertsarana');
		}
		$data['iddoc'] = $id;
		$data['menu1'] = 1;
		return view('contents.formcertsarana',$data);
	}
	
	public function uploadsarana(Request $req)
	{
		$error = $sizefile = $typedoc = 0;
		$filecap = null;
		if($req->hasFile('userfile')){
			$filecap = $req->file('userfile');
			if($filecap->isValid()){
				
				$arr_mime = [
                    'application/pdf',
					'image/jpeg'
                ];
				$mime = $filecap->getMimeType();
				
				if(in_array($mime,$arr_mime)){
					$typefile = null;
					if($mime == 'application/pdf'){
						$typefile = 'pdf';
					} elseif($mime == 'image/jpeg'){
						$typefile = 'jpeg';
					} else{
						$typefile = 'none';
					}
					
					$sizefile = $filecap->getSize();
					if($sizefile < 1048576){
						$namefile 	= $filecap->getClientOriginalName();
						$randomname = null;
						$rawExe = explode(".",$namefile);
						do{
							$randomname = md5($namefile.time().uniqid());
							$rawname 	= Izintmpmod::where('randomname',$randomname);
						}while($rawname->count() > 0);
						$randomname = $randomname.'.'.$rawExe[1];
						$data_insert = [
							'idcompany'	=> Auth::user()->idcompany,
							'typedoc' 	=> 1,
							'typefile'	=> $typefile,
							'realname' 	=> $namefile,
							'randomname'=> $randomname
						];
						$folder		= base_path('storage/app/public/temp');
						$filecap->move($folder,$randomname);
						Izintmpmod::create($data_insert);
						
						$req->session()->put('sesscertsarana', $randomname);
					} else{
						$error = 4;
					}
				} else{
					$error = 3;
				}
				
			} else{
				$error = 2;
			}
		} else{
			$error = 1;
		}
		
		if($error == 0){
			$json = [
				'error' 	=> $error,
				'typedoc'	=> 1,
				'random'	=> $randomname,
				'real'		=> $namefile
			];
		} else{
			$json['error'] = $error;
		}
		return response()->json($json);
	}
	
	public function remove(Request $req, $id)
	{
		$pathfile = base_path('storage/app/public/temp/'.$id);
		@unlink($pathfile);
		$gettypedoc = Izintmpmod::where('randomname',$id)->first()->typedoc;
		$req->session()->forget("sesscertsarana".$gettypedoc);
		Izintmpmod::where('randomname',$id)->delete();
		return;
	}
	
	public function submit(CompleteControl $complete,Request $req)
	{
		$document = $req->session()->get('sesscertsarana');
		$error = 0;
		if(!empty($document)){
			$arrdata 	= $req->input('datasend');
			$company 	= Auth::user()->idcompany;
			$wtmp 		= [
				'idcompany'	=> $company,
				'randomname'=> $document,
				'typedoc'	=> 1
			];
			
			$rawtemp 	= Izintmpmod::where($wtmp)->first();
			$random 	= $rawtemp->randomname;
			$ext 		= $rawtemp->typefile;
			
			do{
				$cmfile = Certsaranamod::where('randomname',$random);
				$random = md5($random.time().uniqid()).'.'.$ext;
				
			} while($cmfile->count() > 0);
			$berlaku = str_replace("/","-",$arrdata['berlaku']);
			$berlaku = date("Y-m-d",strtotime($berlaku));
			$datacert = [
				'uniqdocument' 	=> $arrdata['iddocument'],
				'idcompany'		=> $company,
				'nomorcert'		=> $arrdata['nocert'],
				'nomorbadan'	=> $arrdata['nobadan'],
				'milik'			=> $arrdata['milik'],
				'jenis_sarana'	=> $arrdata['jenissarana'],
				'jenis_uji'		=> $arrdata['jenisuji'],
				'typefile'		=> $rawtemp->typefile,
				'realname'		=> $rawtemp->realname,
				'randomname'	=> $random,
				'berlaku'		=> $berlaku
			];
			
			if(!empty($arrdata['keterangan'])){
				$datacert['keterangan'] = $arrdata['keterangan'];
			}
			
			Certsaranamod::create($datacert);
			$doc_old = base_path('/storage/app/public/temp/'.$rawtemp->randomname);
			$doc_new = base_path('/storage/app/public/sertifikatsarana/'.$random);
			
			File::move($doc_old,$doc_new);
			$conddel = [
				'idcompany'		=> $company,
				'randomname'	=> $rawtemp->randomname
			];
			
			Izintmpmod::where($conddel)->delete();
			@unlink($doc_old);
			$complete->operasisarana($arrdata['iddocument']);
			$req->session()->forget('sesscertsarana');
		} else{
			$error = 1;
		}
		
		$json['error'] = $error;
		return response()->json($json);
	}
	
	public function edit(Request $req)
	{
		$document 	= $req->session()->get('sesscertsarana');
		$arrdata 	= $req->input('datasend');
		$datacert 	= null;
		
		if(!empty($document)){
			$cond = [
				'idcompany'		=> Auth::user()->idcompany,
				'randomname'	=> $document,
				'typedoc'		=> 1
			];
			
			$rawtemp = Izintmpmod::where($cond)->first();
			$random 	= $rawtemp->randomname;
			$ext 		= $rawtemp->typefile;
			
			do{
				$cmfile = Certsaranamod::where('randomname',$random);
				$random = md5($random.time().uniqid()).'.'.$ext;
				
			} while($cmfile->count() > 0);
			
			$datacert['typefile']	= $rawtemp->typefile;
			$datacert['randomname']	= $random;
			$datacert['realname'] 	= $rawtemp->realname;
			
			$doc_old = base_path('/storage/app/public/temp/'.$rawtemp->randomname);
			$doc_new = base_path('/storage/app/public/sertifikatsarana/'.$random);
			
			File::move($doc_old,$doc_new);
			$conddel = [
				'idcompany'		=> Auth::user()->idcompany,
				'randomname'	=> $rawtemp->randomname
			];
			
			Izintmpmod::where($conddel)->delete();
			@unlink($doc_old);
			$req->session()->forget('sesscertsarana');
			
			//---- Delete Old File ---
			
			$randomOld = Certsaranamod::where('id',$arrdata['id'])->first()->randomname;
			$olddel = base_path('/storage/app/public/sertifikatsarana/'.$randomOld);
			@unlink($olddel);
		}
		$berlaku = str_replace("/","-",$arrdata['berlaku']);
		$datacert['berlaku'] = date("Y-m-d", strtotime($berlaku));
		$datacert['jenis_sarana'] 	= $arrdata['jenissarana'];
		$datacert['nomorcert']		= $arrdata['nocert'];
		$datacert['nomorbadan']		= $arrdata['nobadan'];
		$datacert['milik']			= $arrdata['milik'];
		$datacert['jenis_uji']		= $arrdata['jenisuji'];
		$datacert['jenis_uji']		= $arrdata['jenisuji'];
		
		if(!empty($arrdata['keterangan'])){
			$datacert['keterangan'] = $arrdata['keterangan'];
		}
		
		Certsaranamod::where('id', $arrdata['id'])->update($datacert);
		return response()->json(['error' => 0]);
	}
	
	public function getdata($id)
	{
		$temp = null;
		$where = [
			'uniqdocument'	=> $id,
			'idcompany'		=> Auth::user()->idcompany
		];
		
		$rcert = Certsaranamod::where($where);
		if($rcert->count() > 0){
			foreach($rcert->get() as $rowdata){
				$temp[] = [
					'jenissarana'	=> $rowdata->jenis_sarana,
					'nocert'		=> $rowdata->nomorcert,
					'nobadan'		=> $rowdata->nomorbadan,
					'milik'			=> $rowdata->milik,
					'jenisuji'		=> $rowdata->jenis_uji,
					'berlaku'		=> date("d/m/Y", strtotime($rowdata->berlaku)),
					'id'			=> $rowdata->id
				];
			}
		} else{
			$temp = 0;
		}
		
		$json['data'] = $temp;
		return response()->json($json);
	}
	
	public function getdetail($id)
	{
		$tmp = null;
		
		$rawInfo = Certsaranamod::where('id',$id);
		if($rawInfo->count() > 0){
			$rawInfo = $rawInfo->first();
			$tmp = [
				'jenissarana' 	=> $rawInfo->jenis_sarana,
				'nomorcert'		=> $rawInfo->nomorcert,
				'nomorbadan'	=> $rawInfo->nomorbadan,
				'milik'			=> $rawInfo->milik,
				'jenisuji'		=> $rawInfo->jenis_uji,
				'berlaku'		=> date("d/m/Y",strtotime($rawInfo->berlaku)),
				'id'			=> $rawInfo->id,
				'keterangan'	=> $rawInfo->keterangan
			];
		} else{$tmp = 0;}
		
		$json['data'] = $tmp;
		return response()->json($json);
	}
	
	public function removedoc($id)
	{
		
	}
}
