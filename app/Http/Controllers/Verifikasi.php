<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Izinopsmod;
use App\Companyattrmod;
use App\Archieveopsmod;
use App\Verifikatormod;
use Auth;
use App\User;
use Mail;

class Verifikasi extends Controller
{
    public function index()
	{
		$data['menu2'] = 1;
		return view('contents.verifikasiview',$data);
	}
	
	public function getall()
	{
		$level = Auth::user()->level;
		//--- set to kspu if admin ---
		if($level == 1){$level = 2;}
		
		$rawIzin = Izinopsmod::where('is_done',0);
		$tmp = null;
		if($rawIzin->count() > 0){
			$i = 0;
			foreach($rawIzin->get() as $row){
				
				$condv = [
					'level'	=> $level,
					'uniqdocument' => $row->uniqid
				];
				
				$verifikator_check = Verifikatormod::where($condv);
				if($verifikator_check->count() < 1){
					switch($row->opsi){
						case 1: $opsi = 'Penambahan Lintas Pelayanan'; break;
						case 2: $opsi = 'Penambahan Frekuensi'; break;
						case 3: $opsi = 'Izin Baru'; break;
						case 4: $opsi = 'Izin Perpanjangan'; break;
					}
					$bu = Companyattrmod::where('uniqid',$row->idcompany)->first()->name;
					
					$tmp[] = [
						'no'		=> ++$i,
						'bu'		=> $bu,
						'perihal'	=> $row->perihal,
						'jenis'		=> $opsi,
						'url'		=> url('/verifikasi/izin/operasi/detail',['id' => $row->uniqid])
					];
				}	
			}
		} else{
			$tmp = 0;
		}
		
		$json['data'] = $tmp;
		return response()->json($json);
	}
	
	public function detail($iddoc)
	{
		$rawIzin = Izinopsmod::where('uniqid',$iddoc)->first();
		$company = Companyattrmod::where('uniqid',$rawIzin->idcompany)->first()->name;
		$data['company'] = $company;
		
		$temp = null;
		
		$Arsip = Archieveopsmod::where('idizin',$iddoc);
		
		if($Arsip->count() > 0){
			$i = 0;
			foreach($Arsip->get() as $row){
				$doc = null;
				if($rawIzin->opsi == 3){
					switch($row->typedoc){
						case 1: $doc = 'Studi Kelayakan'; break;
						case 2: $doc = 'Sertifikasi Rangkaian'; break;
						case 3: $doc = 'Sertifikasi Sarana'; break;
						case 4: $doc = 'Sertifikasi Awak'; break;
						case 5: $doc = 'Fasilitas Perawatan'; break;
					}
				} else if($rawIzin->opsi == 2){
					switch($row->typedoc){
						case 6: $doc = 'Izin Operasi'; break;
						case 7: $doc = 'SOP Pengoperasian'; break;
						case 8: $doc = 'Sertifikasi Uji Sarana'; break;
						case 9: $doc = 'Sertifikasi Awak Sarana'; break;
						case 10: $doc = 'Rencana Kerja'; break;
						case 11: $doc = 'Jumlah Penambahan'; break;
						case 12: $doc = 'Perjanjian Kerja'; break;
					}
				} else{
					$doc = 'none';
				}
				$url = url('/verifikasi/izin/operasi/download',['id' => $row->randomname]);
				$temp[] = [
					'no'	=> ++$i,
					'doc'	=> $doc,
					'url'	=> $url
				];
			}
		}
		
		$data['data'] 		= $temp;
		$data['company'] 	= $company;
		$data['urlacc']		= url('/verifikasi/izin/operasi/terima',['id' => $iddoc]);
		$data['urlrej']		= url('/verifikasi/izin/operasi/tolak',['id' => $iddoc]);
		return view('contents.izindetail',$data);
	}
	
	public function download($id)
	{
		$folder = base_path('/storage/app/public/izinoperasi');
		$path = $folder.'/'.$id;
		
		return response()->file($path);
	}
	
	public function terima($id)
	{
		$level = Auth::user()->level;
		if($level == 1){$level = 2;}
		$updateverifikator = [
			'level'			=> $level,
			'idverikator'	=> Auth::user()->uniqid,
			'uniqdocument'	=> $id,
			'is_verify'		=> 1
		];
		
		Verifikatormod::create($updateverifikator);
		$rawInfo = Izinopsmod::where('uniqid',$id);
		$idcompany = $rawInfo->first()->idcompany;
		$rawUser = User::where('idcompany',$idcompany);
		$email = $rawUser->first()->email;
		
		$datamail['pesan'] = 'Terima kasih telah mengajukan pembuatan Izin Operasi. Silakan melampirkan dokumen Asli/legalisir ke Staf KSPU untuk 
		melanjutkan proses Perizinan Anda. Dokumen Asli/legaisir diterima maksimum 3x24 jam setelah 
		Anda melakukan upload dokumen melalui system perizinan online ini.';
		Mail::send('panels.mailtolakreg',$datamail,function($message) use($email){
			$message->to($email,'Perijinan Online User')
				->bcc('dessy@teamwork-design.com','Dessy')
				->from('llaka.kspu@dephub.go.id', 'Admin Perijinan Online')
				->subject('Notifikasi Izin diterima');
		});
		
		return redirect('/verifikasi/izin/operasi');
	}
	
	public function tolak($id)
	{
		$level = Auth::user()->level;
		if($level == 1){$level = 2;}
		$updateverifikator = [
			'level'			=> $level,
			'idverikator'	=> Auth::user()->uniqid,
			'uniqdocument'	=> $id,
			'is_reject'		=> 1
		];
		
		Verifikatormod::create($updateverifikator);
		$rawInfo = Izinopsmod::where('uniqid',$id);
		$idcompany = $rawInfo->first()->idcompany;
		$rawUser = User::where('idcompany',$idcompany);
		$email = $rawUser->first()->email;
		
		$datamail['pesan'] = 'Permohonan perizinan Operasi Anda ditolak karena (data tidak sesuai/data tidak lengkap) 
		silakan anda melakukan pengkinian/edit data pendukung Izin Operasi sesuai dengan persyaratan.';
		Mail::send('panels.mailtolakreg',$datamail,function($message) use($email){
			$message->to($email,'Perijinan Online User')
				->bcc('dessy@teamwork-design.com','Dessy')
				->from('llaka.kspu@dephub.go.id', 'Admin Perijinan Online')
				->subject('Notifikasi izin ditolak');
		});
		return redirect('/verifikasi/izin/operasi');
	}
}
