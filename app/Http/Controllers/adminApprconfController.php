<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use Carbon\Carbon;
use App\ApprConfmod;
use App\AdminUsermod;
use App\Approvalmod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage, Mail; // use AdminUsermod;

class adminApprconfController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P13';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        //$dataList = ApprConfmod::all();
        $dataList = DB::table('approver_conf')
                    ->selectRaw('approver_conf.*, admins.name as admin')
                    ->leftjoin('admins','approver_conf.adminid','admins.id')
                   ->orderBy('approver_conf.id', 'desc')
                   ->orderBy('approver_conf.priority', 'asc')
                   ->get();
        return view('webmin.apprconfadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      $data =  new \stdClass();
        $data->admin = AdminUsermod::get();
        return view('webmin.apprconfadmin_add')->with('data', $data)->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      // dd($request);
      $rules = array(
          'modul'       => 'required',
          'admin'       => 'required',
          'priority'       => 'required',
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }else {
            $admin = new ApprConfmod;
            $admin->adminid   = $request->admin;
            $admin->modul   = $request->modul;
            $admin->priority   = $request->priority;
            $admin->save();
            // redirect
            Session::flash('message', 'Approver telah berhasil dibuat!');
            return Redirect::route('apprconf.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = ApprConfmod::find($id);
      //echo $data->picture;exit;
      // Storage::delete(public_path('/images/news/'.$data->thumbnail));
      // Storage::delete(public_path('/images/news/'.$data->picture));

      $data->delete();

      // redirect
      Session::flash('message', 'Berita telah berhasil dihapus!');
      return Redirect::route('apprconf.dashboard');
    }

    public static function setAppr($request)
    {
      Approvalmod::where('modulid',$request->modulid)->where('modul',$request->modul)->delete();
      if($request->status != "draft")
      {
        $appr = ApprConfmod::where('modul',$request->modul)->orderby('priority','asc')->orderby('created_at','asc')->get();
        $x = 1;
        foreach ($appr as $key => $value) 
        {
          $admin = new Approvalmod;
          $admin->adminid   = $value->adminid;
          $admin->modulid   = $request->modulid;
          $admin->modul   = $request->modul;
          $admin->priority   = $x;
          $admin->status   = 0;
          $admin->save();
          $x++;

          // if($x == 1){
            $user = AdminUsermod::where('id',$value->adminid)->first();
            if(isset($user->email)){
              $data = array('email'=>$user->email, 'name'=>$user->name, 'modul'=>$request->modul, 'modulid'=>$request->modulid);
              Mail::send('webmin.emails.notifApproval', $data, function($message) use ($user) {
                 $message->to($user->email, $user->name)->subject
                    ('Approval konten untuk Balai Pengujian Perkeretaapian CMS');
                 $message->from('no-reply@arieswdd.com','Aries Widodo');
              });
            }
          // }
        }
      }
    }


}
