<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use Carbon\Carbon;
use App\ApprConfmod;
use App\AdminUsermod;
use App\Approvalmod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminApprovalController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P14';

      $this->middleware('auth:admin');
    }

    public function index()
    {

      $appr = DB::table('approval')
      ->selectRaw('min(priority), id')
      ->where('status',0)
      ->groupBy('modulid','modul');
        $dataList = DB::table('approval')
                  ->selectRaw('approval.id, IF(news.title != "",news.title,IF(wikiuji.title != "",wikiuji.title,pengumuman.title)) as title, approval.status,  IF(news.title != "",news.created_at,IF(wikiuji.title != "",wikiuji.created_at,pengumuman.created_at)) as created_at,  IF(news.title != "",news.created_by,IF(wikiuji.title != "",wikiuji.created_by,pengumuman.created_by)) as created_by, approval.modul, approval.adminid')
                  ->leftjoin('news',function ($join) {
                    $join->on('approval.modulid','=','news.id')->where('approval.modul','news');
                  })
                  ->leftjoin('pengumuman',function ($join) {
                    $join->on('approval.modulid','=','pengumuman.id')->where('approval.modul','pengumuman');
                  })
                  ->leftjoin('wikiuji',function ($join) {
                    $join->on('approval.modulid','=','wikiuji.id')->where('approval.modul','wikiuji');
                  })
                  ->joinSub($appr, 'appr', function ($join) {
                    $join->on('approval.id', '=', 'appr.id');
                  })
                  ->where('approval.adminid',Auth::guard('admin')->user()->id)
                  ->where('approval.status',0)
                   ->orderBy('approval.created_at', 'asc')
                   ->orderBy('approval.status', 'asc')
                   ->get();
        //            ->toSql();
        // dd($dataList);
        return view('webmin.approvaladmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }


    public function detail($id)
    {
        $appr = Approvalmod::find($id);
        $data = DB::table($appr->modul)->find($appr->modulid);
        $data->modul = $appr->modul;
        $data->approvalid = $appr->id;
        return view('webmin.approvaladmin_detail')->with('data', $data)->with('codepage',$this->codepage);;

    }

    public function update(Request $request, $id)
    {
      $appr         = Approvalmod::find($id);
      $appr->status   = ($request->status == 'approve'?1:-1);
      $appr->note   = $request->note;
      $appr->save();

      $total = Approvalmod::where('modulid', $appr->modulid)->get()->count();
      $done = Approvalmod::where('modulid', $appr->modulid)->where('status', '!=', 0)->get()->count();
      $approve = Approvalmod::where('modulid', $appr->modulid)->where('status', '>', 0)->get()->count();
      $reject = Approvalmod::where('modulid', $appr->modulid)->where('status', '<', 0)->get()->count();
      
      if($total == $done)
      {
        if($total == $approve)
        {
          $modul  = DB::table($appr->modul)->where('id', $appr->modulid)->update([
            'status' => 'publish'
          ]);
        }else{
          $modul  = DB::table($appr->modul)->where('id', $appr->modulid)->update([
            'status' => 'reject'
          ]);
        }
      }
      return Redirect::route('approval.dashboard');
    }


}
