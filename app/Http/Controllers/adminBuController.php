<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\Bumod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminBuController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P04';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        $dataList = Bumod::all();
        return view('webmin.badanusahaadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.badanusahaadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'name'       => 'required',
          'address'       => 'required|min:6'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::route('bu.add')
                ->withErrors($validator)
                ->withInput();
        }else {
            $admin = new Bumod;
            $admin->name   = Input::get('name');
            $admin->address   = Input::get('address');
            $admin->uniqid = uniqid();
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Successfully created Badan Usaha!');
            return Redirect::route('bu.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Bumod::find($id);
        return view('webmin.badanusahaadmin_edit')->with('data', $data)->with('codepage',$this->codepage);;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'name'       => 'required',
          'address'       => 'required|min:6'
      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::route('bu.add')
                  ->withErrors($validator)
                  ->withInput();
          }else {
              $admin = Bumod::find($id);
              $admin->name   = Input::get('name');
              $admin->address   = Input::get('address');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Successfully updated Badan Usaha!');
              return Redirect::route('bu.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = Bumod::find($id);
      $data->delete();

      // redirect
      Session::flash('message', 'Successfully deleted Badan Usaha!');
      return Redirect::route('bu.dashboard');
    }


}
