<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\Chatmod;
use App\ChatNewmod;
use App\ChatDetailmod;
use App\AdminUsermod;

use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminChatController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P10';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        //$dataList = pegawaimod::all();
        DB::enableQueryLog();

        $kat = str_replace("|", ",", Auth::guard('admin')->user()->chatkategori);
        // $dataList = DB::table('chat');

        $cats = explode("|", Auth::guard('admin')->user()->chatkategori);
        $where = '';
        foreach ($cats as $k=>$v){
          $where .= "subject like '%$v%' or ";
          // echo $v;
          // $dataList->orwhere('subject', 'like', '%'.$v.'%');
        }
        $where .= ' 1=1';
        // $dataList->get();

        $dataList = Chatmod::where(DB::raw($where))->orderby('created_at', 'desc')->get();

        // dd(
        //     DB::getQueryLog()
        // );
        return view('webmin.chatadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    public function chatnotif()
    {
      $kat = str_replace("|", ",", Auth::guard('admin')->user()->chatkategori);
      $cats = explode("|", Auth::guard('admin')->user()->chatkategori);
      $where = '';
      foreach ($cats as $k=>$v){
        $where .= "subject like '%$v%' or ";
      }
      // $rest = substr("abcdef", 0, -1);
      $where .= '1=1';

      $dataList = ChatNewmod::where(DB::raw($where))->orderby('created_at', 'desc')->get();

      $textnya = "";
      foreach ($dataList as $data) {
        $textnya .= '
        <li>
          <a href="'.route('chat.lihat', ['id' => $data->id]).'">
            <i class="fa fa-users text-aqua"></i> '.$data->nama.'
          </a>
        </li>
        ';
      }

      return $textnya;
    }
    public function chatcount()
    {
      $kat = str_replace("|", ",", Auth::guard('admin')->user()->chatkategori);
      $cats = explode("|", Auth::guard('admin')->user()->chatkategori);
      $where = '';
      foreach ($cats as $k=>$v){
        $where .= "subject like '%$v%' or ";
      }
      // $rest = substr("abcdef", 0, -1);
      $where .= '1=1';

      $dataList = ChatNewmod::where(DB::raw($where))->orderby('created_at', 'desc')->get();

      return $dataList->count();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $dataList = DB::table('jabatan')
                 ->orderBy('priority', 'asc')
                 ->get();
        return view('webmin.chatadmin_add')->with('codepage',$this->codepage)->with('dataList', $dataList);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'nama'       => 'required|min:2',
          // 'nip'       => 'required',
          'picture' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::route('pegawai.add')
                ->withErrors($validator)
                ->withInput();
        }else {
            // store
            // print_r(Input::get('privilege'));exit;
            $maxPriority = DB::table('pegawai')
                       ->orderBy('priority', 'desc')
                       ->first();
            //print_r($maxPriority);exit;
            //foreach ($maxPriority as $k => $v){

            //}
            $valMax = (@$maxPriority->priority) ? @$maxPriority->priority : 0;
            // echo  $valMax = $valMax+1;exit;
            $image = $request->file('picture');
            $input['imagename'] = '';

            if ($image) {
              $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

              $destinationPath = public_path('/images/pegawai/');

              $image->move($destinationPath, $input['imagename']);
            }
            //echo $priv;exit;
            $admin = new Pegawaimod;
            $admin->nama   = Input::get('nama');
            $admin->nip   = Input::get('nip');
            $admin->divisi   = Input::get('divisi_jabatan');
            $admin->pimpinan   = Input::get('pimpinan');
            $admin->golongan   = Input::get('golongan');
            $admin->pangkat   = Input::get('pangkat');
            $admin->jabatan   = Input::get('jabatan');
            $admin->pendidikan   = Input::get('pendidikan');
            $admin->uniqid = uniqid();
            $admin->priority = $valMax+1;
            $admin->picture = $input['imagename'];
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Successfully created pegawai!');
            return Redirect::route('pegawai.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function lihat($id)
    {
      // echo "abde";exit;
      $adminnya = AdminUsermod::get();
      foreach ($adminnya as $admin) {
        $listadmin[$admin->uniqid] = $admin;
      }

      $data = Chatmod::find($id);

      $data->status = 1;
      $data->save();

      $listchat = ChatDetailmod::join('chat', 'chat_detail.chatid', '=', 'chat.appsessionid')
                  ->select('chat_detail.*', 'chat.nama')
                  ->where("chatid", $data->appsessionid)
                  ->where('chat_detail.statusadmin', 1)
                  ->orderby('chat_detail.created_at')->get();

      return view('webmin.chatadmin_lihat')
              ->with('data', $data)
              ->with('listchat', $listchat)
              ->with('listadmin', $listadmin)
              ->with('codepage',$this->codepage);

    }

    public function listchat($sessid)
    {
      $this->middleware('guest:admin');
      // $dataChat = ChatDetailmod::where('chatid', $sessid)->where('status', 0)->get();
      $dataChat = ChatDetailmod::join('chat', 'chat_detail.chatid', '=', 'chat.appsessionid')
                  ->select('chat_detail.*', 'chat.nama')
                  ->where("chatid", $sessid)
                  ->where('chat_detail.statusadmin', 0)
                  ->orderby('chat_detail.created_at')->get();

      $adminnya = AdminUsermod::get();

      foreach ($adminnya as $admin) {
        $listadmin[$admin->uniqid] = $admin;
      }
      // print_r($dataChat);exit;
      $chatnya = '';
      foreach ($dataChat as $chat) {
        if ($chat->userid){
          $chatnya .= '
          <div class="direct-chat-msg right">
            <div class="direct-chat-info clearfix">
              <span class="direct-chat-name pull-right">'.$listadmin[$chat->userid]->name.'</span>
              <span class="direct-chat-timestamp pull-left">'.$chat->created_at->format("d F Y H:i:s").'</span>
            </div>
            <div class="direct-chat-text">
              '.$chat->message.'
            </div>
          </div>
          ';
        } else {
          $chatnya .= '
          <div class="direct-chat-msg">
            <div class="direct-chat-info clearfix">
              <span class="direct-chat-name pull-left">'.$chat->nama.'</span>
              <span class="direct-chat-timestamp pull-right">'.$chat->created_at->format("d F Y H:i:s").'</span>
            </div>
            <div class="direct-chat-text">
              '.$chat->message.'
            </div>
          </div>
          ';
        }
        $data = ChatDetailmod::where('uniqid', $chat->uniqid)->first();
        $data->statusadmin = 1;
        $data->save();
      }

  		return $chatnya;

    }

    public function postchat(Request $request){
      $callback = array("error"=>0, "message"=>array(), "data"=>"");

      $data = Chatmod::where('appsessionid', Input::get('sessionid'))->first();
      $data->status = 2;
      $data->save();

      $admin = new ChatDetailmod;
      $admin->message   = Input::get('msg');
      $admin->chatid   = Input::get('sessionid');
      $admin->uniqid = uniqid();
      $admin->userid = Auth::guard('admin')->user()->uniqid;
      $admin->save();
      $callback['message'] = "Ok";

  		return json_encode($callback);
  	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Pegawaimod::find($id);
        $dataList = DB::table('jabatan')
                 ->orderBy('priority', 'asc')
                 ->get();
        return view('webmin.chatadmin_edit')->with('dataList', $dataList)->with('data', $data)->with('codepage',$this->codepage);;

    }

    public function sortsave(Request $request)
    {
      # code...
      //print_r($request);exit;
      $ctr=0;
      foreach ($request->item as $k => $v) {
        $ctr++;
        $data = Pegawaimod::find($v);
        $data->priority = $ctr;
        $data->save();
        # code...
      }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'nama'       => 'required|min:2',
      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::route('pegawai.add')
                  ->withErrors($validator)
                  ->withInput();
          }else {
              // store
              // print_r(Input::get('privilege'));exit;
              $admin = pegawaimod::find($id);


              if ($request->file('picture')){
                $image = $request->file('picture');

                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

                $destinationPath = public_path('/images/pegawai/');

                $image->move($destinationPath, $input['imagename']);
                $admin->picture = $input['imagename'];
              }

              if (Input::get('picture_del')){
                $admin->picture = '';
              }

              //echo $priv;exit;
            //  $admin = new AdminUsermod;
              $admin->nama   = Input::get('nama');
              $admin->nip   = Input::get('nip');
              $admin->divisi   = Input::get('divisi_jabatan');
              $admin->pimpinan   = Input::get('pimpinan');
              $admin->golongan   = Input::get('golongan');
              $admin->pangkat   = Input::get('pangkat');
              $admin->jabatan   = Input::get('jabatan');
              $admin->pendidikan   = Input::get('pendidikan');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Successfully updated pegawai!');
              return Redirect::route('pegawai.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = Chatmod::find($id);
      $datadet = ChatDetailmod::where("chatid", $data->appsessionid);
      //echo $data->picture;exit;
      // Storage::delete(public_path('/images/pegawai/'.$data->picture));

      $datadet->delete();
      $data->delete();

      // redirect
      Session::flash('message', 'Chat telah sukses dihapus!');
      return Redirect::route('chat.dashboard');
    }


}
