<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use Carbon\Carbon;
use App\Eventmod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminEventController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P05';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        //$dataList = Eventmod::all();
        $dataList = DB::table('event')
                   ->orderBy('id', 'desc')
                   ->get();
        return view('webmin.eventadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.eventadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'publish_date'       => 'required',
          'title'       => 'required|min:6',
          'lead'       => 'required|min:6',
          'content'      => 'required',
          'thumbnail' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::route('event.add')
                ->withErrors($validator)
                ->withInput();
        }else {
            // store
            // print_r(Input::get('privilege'));exit;

            $thumb = $request->file('thumbnail');
            $input['thumbname'] = '';

            if ($thumb) {
              $input['thumbname'] = time().'thumb.'.$thumb->getClientOriginalExtension();

              $tdestinationPath = public_path('/images/event/');

              $thumb->move($tdestinationPath, $input['thumbname']);
            }

            $image = $request->file('picture');
            $input['imagename'] = '';

            if ($image) {
              $input['imagename'] = time().'pic.'.$image->getClientOriginalExtension();

              $destinationPath = public_path('/images/event/');

              $image->move($destinationPath, $input['imagename']);
            }
            //echo $priv;exit;
            $admin = new Eventmod;
            $admin->publish_date   = Input::get('publish_date');
            $admin->title   = Input::get('title');
            $admin->lead   = Input::get('lead');
            $admin->content      = Input::get('content');
            $admin->slug   = str_slug(Input::get('title'), '-');
            $admin->uniqid = uniqid();
            $admin->thumbnail = $input['thumbname'];
            $admin->picture = $input['imagename'];
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Successfully created Event!');
            return Redirect::route('event.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Eventmod::find($id);
        return view('webmin.eventadmin_edit')->with('data', $data)->with('codepage',$this->codepage);;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'publish_date'       => 'required',
          'title'       => 'required|min:6',
          'lead'       => 'required|min:6',
          'content'      => 'required',
      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::route('event.edit')
                  ->withErrors($validator)
                  ->withInput();
          }else {
              // store
              // print_r(Input::get('privilege'));exit;
              $admin = Eventmod::find($id);


              if ($request->file('thumbnail')){
                $thumb = $request->file('thumbnail');

                $input['thumbname'] = time().'thumb.'.$thumb->getClientOriginalExtension();

                $tdestinationPath = public_path('/images/event/');

                $thumb->move($tdestinationPath, $input['thumbname']);
                $admin->thumbnail = $input['thumbname'];
              }

              if (Input::get('thumbnail_del')){
                $admin->thumbnail = '';
              }

              if ($request->file('picture')){
                $image = $request->file('picture');

                $input['imagename'] = time().'pic.'.$image->getClientOriginalExtension();

                $destinationPath = public_path('/images/event/');

                $image->move($destinationPath, $input['imagename']);
                $admin->picture = $input['imagename'];
              }

              if (Input::get('picture_del')){
                $admin->picture = '';
              }

              //echo $priv;exit;
            //  $tgl_publish = Carbon::createFromFormat('d-m-Y', Input::get('publish_date'))->format('Y-m-d');
            //  $admin = new AdminUsermod;
              $admin->publish_date   = Carbon::parse(Input::get('publish_date'));
              $admin->title   = Input::get('title');
              $admin->lead   = Input::get('lead');
              $admin->slug   = str_slug(Input::get('title'), '-');
              $admin->content      = Input::get('content');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Successfully updated Event!');
              return Redirect::route('event.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = Eventmod::find($id);
      //echo $data->picture;exit;
      Storage::delete(public_path('/images/event/'.$data->thumbnail));
      Storage::delete(public_path('/images/event/'.$data->picture));

      $data->delete();

      // redirect
      Session::flash('message', 'Successfully deleted the Event!');
      return Redirect::route('event.dashboard');
    }


}
