<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use Carbon\Carbon;
use App\gallerymod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminGalleryController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P11';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        //$dataList = gallerymod::all();
        Session::forget('image_images');
        $dataList = gallerymod::orderBy('id', 'desc')
                   ->get();
        return view('webmin.galleryadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        Session::forget('image_images');
        return view('webmin.galleryadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
        'judul'       => 'required|min:6',
        'picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }else {
            if (!Session::get('image_images')){
              $validator->getMessageBag()->add('thumbnail', 'anda harus upload gambar');
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
            }

            $image = $request->file('thumbnail');
            $picture = $request->file('picture');
            $input['imagename'] = '';
            $input['picturename'] = '';

            // if ($image) {
            //   $input['imagename'] = time().uniqid().'.'.$image->getClientOriginalExtension();
            //
            //   $destinationPath = public_path('/images/gallery/');
            //
            //   $image->move($destinationPath, $input['imagename']);
            // }

            if (Session::get('image_images')){
              $input['imagename'] = Session::get('image_images');
              // $input['imagename'] = Session::get('higlight_images');
            }

            if ($picture) {
              $input['picturename'] = time().uniqid().'.'.$picture->getClientOriginalExtension();

              $destinationPath = public_path('/images/gallery/');

              $picture->move($destinationPath, $input['picturename']);
              $this->getFilePathAttribute('/images/gallery/'.$input['picturename']);
            }
            //echo $priv;exit;
            $admin = new gallerymod;
            $admin->category   = Input::get('halaman');
            $admin->title   = Input::get('judul');
            $admin->uniqid = uniqid();
            $admin->thumbnail = $input['imagename'];
            $admin->picture = $input['picturename'];
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();

            // redirect
            Session::flash('message', 'Gallery telah berhasil dibuat!');
            return Redirect::route('gal.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        Session::forget('image_images');
        $data = gallerymod::find($id);
        return view('webmin.galleryadmin_edit')->with('data', $data)->with('codepage',$this->codepage);;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
        'judul'       => 'required|min:6',
        'picture' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
          }else {
              // store
              // print_r(Input::get('privilege'));exit;

              $admin = gallerymod::find($id);

              if (Session::get('image_images')){
                $admin->thumbnail = Session::get('image_images');
              }
              if (Input::get('thumbnail_del')){
                $admin->thumbnail = '';
              }

              if ($request->file('picture')){
                $picture = $request->file('picture');

                $input['picname'] = time().uniqid().'.'.$picture->getClientOriginalExtension();

                $destinationPath = public_path('/images/gallery/');

                $picture->move($destinationPath, $input['picname']);
                $admin->picture = $input['picname'];
                $this->getFilePathAttribute('/images/gallery/'.$input['picname']);
              }

              if (Input::get('picture_del')){
                $admin->picture = '';
              }

              $admin->category   = Input::get('halaman');
              $admin->title   = Input::get('judul');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Gallery telah berhasil diubah!');
              return Redirect::route('gal.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = gallerymod::find($id);
      Storage::delete(public_path('/images/gallery/'.$data->thumbnail));
      Storage::delete(public_path('/images/gallery/'.$data->picture));

      $data->delete();

      // redirect
      Session::flash('message', 'Gallery telah berhasil dihapus!');
      return Redirect::route('gal.dashboard');
    }

    public function postImage() {
			//echo "HIIIIII";

			$data = $_POST['image'];

			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);

			$data = base64_decode($data);
			$imageName = time().'.png';
			file_put_contents(public_path('/images/gallery/').$imageName, $data);
      $this->getFilePathAttribute('/images/gallery/'.$imageName);

			Session::put('image_images', $imageName);

			echo "Image Uploaded";
		}


}
