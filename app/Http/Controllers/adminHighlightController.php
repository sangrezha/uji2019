<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\Highlightmod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminHighlightController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P03';
      
      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        //$dataList = Highlightmod::all();
        $dataList = DB::table('highlight')
                   ->orderBy('priority', 'asc')
                   ->get();
        return view('webmin.highlightadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    public function sort()
    {
        //
      //echo "abcde";exit;
        //$dataList = Highlightmod::all();
        $dataList = DB::table('highlight')
                   ->orderBy('priority', 'asc')
                   ->get();
        return view('webmin.highlightadmin_sort')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.highlightadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'judul'       => 'required|min:6',
          // 'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:max_width=1366,max_height=584'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }else {
            if (!Session::get('image_images')){
              $validator->getMessageBag()->add('gambar', 'anda harus upload gambar');
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
            }
            // store
            // print_r(Input::get('privilege'));exit;
            $maxPriority = DB::table('highlight')
                       ->orderBy('priority', 'desc')
                       ->first();
            //print_r($maxPriority);exit;
            //foreach ($maxPriority as $k => $v){

            //}
            $valMax = $maxPriority->priority;
            // echo  $valMax = $valMax+1;exit;
            // $image = $request->file('gambar');
            $input['imagename'] = '';

            if (Session::get('image_images')){
              $input['imagename'] = Session::get('image_images');
            }
            // if ($image) {
            //   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            //   $destinationPath = public_path('/images/highlight/');

            //   $image->move($destinationPath, $input['imagename']);
            // }
            //echo $priv;exit;
            $admin = new Highlightmod;
            $admin->title   = Input::get('judul');
            $admin->highlight   = Input::get('highlight');
            $admin->uniqid = uniqid();
            $admin->priority = $valMax+1;
            $admin->picture = $input['imagename'];
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Highlight telah sukses dibuat!');
            return Redirect::route('highlight.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Highlightmod::find($id);
        return view('webmin.highlightadmin_edit')->with('data', $data)->with('codepage',$this->codepage);;

    }

    public function sortsave(Request $request)
    {
      # code...
      //print_r($request);exit;
      $ctr=0;
      foreach ($request->item as $k => $v) {
        $ctr++;
        $data = Highlightmod::find($v);
        $data->priority = $ctr;
        $data->save();
        # code...
      }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'judul'       => 'required|min:6',
          // 'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:max_width=1366,max_height=584'

      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
          }else {
              // store
              // print_r(Input::get('privilege'));exit;
              $admin = Highlightmod::find($id);


              // if ($request->file('gambar')){
              //   $image = $request->file('gambar');

              //   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

              //   $destinationPath = public_path('/images/highlight/');

              //   $image->move($destinationPath, $input['imagename']);
              //   $admin->picture = $input['imagename'];
              // }
              if (Session::get('image_images')){
                $admin->picture = $input['imagename'] = Session::get('image_images');
              }


              if (Input::get('picture_del')){
                $admin->picture = '';
              }

              //echo $priv;exit;
            //  $admin = new AdminUsermod;
              $admin->title   = Input::get('judul');
              $admin->highlight   = Input::get('highlight');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Highlight telah berhasil diubah!');
              return Redirect::route('highlight.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = Highlightmod::find($id);
      //echo $data->picture;exit;
      Storage::delete(public_path('/images/highlight/'.$data->picture));

      $data->delete();

      // redirect
      Session::flash('message', 'Highlight berhasil dihapus!');
      return Redirect::route('highlight.dashboard');
    }

    public function postImage() {
			//echo "HIIIIII";

			$data = $_POST['image'];

			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);

			$data = base64_decode($data);
			$imageName = time().'.png';
			file_put_contents(public_path('/images/highlight/').$imageName, $data);
      $this->getFilePathAttribute('/images/highlight/'.$imageName);

			Session::put('image_images', $imageName);

			echo "Image Uploaded";
		}

}
