<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\RegIzin;
use App\RegCat;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminIzinController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P062';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        // $dataList = RegIzin::all();
        $dataList = DB::table('izin_regulasi')
            ->join('izin_kategori', 'izin_regulasi.category_id', '=', 'izin_kategori.id')
            ->select('izin_regulasi.*', 'izin_kategori.name')
            ->orderBy('izin_regulasi.created_at', 'desc')
            ->get();
        //$dataCat = RegCat::all();
        return view('webmin.izinadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $dataList = RegCat::all();
        return view('webmin.izinadmin_add')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'kategori'       => 'required',
          'judul'       => 'required|min:3',
          'tahun'      => 'required',
          'tentang'      => 'required',
          'attachfile' => 'required|mimes:pdf|max:10240'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }else {
            // store
            // print_r(Input::get('privilege'));exit;

            $image = $request->file('attachfile');

            $input['attachfile'] = $image->getClientOriginalName();

            $destinationPath = base_path('/storage/app/public/website');

            $image->move($destinationPath, $input['attachfile']);
            //echo $priv;exit;
            $admin = new RegIzin;
            $admin->category_id   = Input::get('kategori');
            $admin->title   = Input::get('judul');
            $admin->tentang      = Input::get('tentang');
            $admin->tahun      = Input::get('tahun');
            $admin->uniqid = uniqid();
            $admin->attachfile = $input['attachfile'];
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Regulasi telah sukses ditambah!');
            return Redirect::route('izin.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = RegIzin::find($id);
        $dataList = RegCat::all();
        //echo "abcdef";exit;
        return view('webmin.izinadmin_edit')->with('dataList', $dataList)->with('data', $data)->with('codepage',$this->codepage);
        // return view('webmin.izinadmin_edit')->with('data', $data)->with('codepage',$this->codepage);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'kategori'       => 'required',
          'judul'       => 'required|min:6',
          'tahun'      => 'required',
          'tentang'      => 'required',
          'attachfile' => 'mimes:pdf|max:10240'

      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
          }else {
              // store
              //print_r(Input::get('tentang'));exit;
              $admin = RegIzin::find($id);


              if ($request->file('attachfile')){
                $image = $request->file('attachfile');

                $input['attachfile'] = $image->getClientOriginalName();

                $destinationPath = base_path('/storage/app/public/website');;

                $image->move($destinationPath, $input['attachfile']);
                $admin->attachfile = $input['attachfile'];
              }

              if (Input::get('attachfile_del')){
                $admin->attachfile = '';
              }

              //echo $priv;exit;
            //  $admin = new AdminUsermod;
              $admin->category_id   = Input::get('kategori');
              $admin->title   = Input::get('judul');
              $admin->tahun      = Input::get('tahun');
              $admin->tentang      = Input::get('tentang');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Regulasi telah berhasil diubah!');
              return Redirect::route('izin.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = RegIzin::find($id);
      //echo $data->picture;exit;
      Storage::delete(base_path('/storage/app/public/website/').$data->attachfile);

      $data->delete();

      // redirect
      Session::flash('message', 'Regulasi telah berhasil dihapus!');
      return Redirect::route('izin.dashboard');
    }


}
