<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\Jabatanmod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminJabatanController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P091';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        //$dataList = Jabatanmod::all();
        $dataList = DB::table('jabatan')
                   ->orderBy('priority', 'asc')
                   ->get();
        return view('webmin.jabatanadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    public function sort()
    {
        //
      //echo "abcde";exit;
        //$dataList = Jabatanmod::all();
        $dataList = DB::table('jabatan')
                   ->orderBy('priority', 'asc')
                   ->get();
        return view('webmin.jabatanadmin_sort')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.jabatanadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'nama'       => 'required|min:2|unique:jabatan,title',
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }else {
            // store
            // print_r(Input::get('privilege'));exit;
            $maxPriority = DB::table('jabatan')
                       ->orderBy('priority', 'desc')
                       ->first();
            //print_r($maxPriority);exit;
            //foreach ($maxPriority as $k => $v){

            //}
            $valMax = 0;
            if ($maxPriority) {
              $valMax = $maxPriority->priority;
            }
            // echo  $valMax = $valMax+1;exit;
            // $image = $request->file('gambar');
            // $input['imagename'] = '';
            //
            // if ($image) {
            //   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            //
            //   $destinationPath = public_path('/images/jabatan/');
            //
            //   $image->move($destinationPath, $input['imagename']);
            // }
            //echo $priv;exit;
            $admin = new Jabatanmod;
            $admin->title   = Input::get('nama');
            // $admin->jabatan   = Input::get('jabatan');
            $admin->uniqid = uniqid();
            $admin->priority = $valMax+1;
            // $admin->picture = $input['imagename'];
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Jabatan telah sukses dibuat!');
            return Redirect::route('jabatan.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Jabatanmod::find($id);
        return view('webmin.jabatanadmin_edit')->with('data', $data)->with('codepage',$this->codepage);;

    }

    public function sortsave(Request $request)
    {
      # code...
      //print_r($request);exit;
      $ctr=0;
      foreach ($request->item as $k => $v) {
        $ctr++;
        $data = Jabatanmod::find($v);
        $data->priority = $ctr;
        $data->save();
        # code...
      }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'nama'       => 'required|min:2',
          // 'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:max_width=1366,max_height=584'

      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
          }else {
              // store
              // print_r(Input::get('privilege'));exit;
              $admin = Jabatanmod::find($id);


              // if ($request->file('gambar')){
              //   $image = $request->file('gambar');
              //
              //   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
              //
              //   $destinationPath = public_path('/images/jabatan/');
              //
              //   $image->move($destinationPath, $input['imagename']);
              //   $admin->picture = $input['imagename'];
              // }
              //
              // if (Input::get('picture_del')){
              //   $admin->picture = '';
              // }

              //echo $priv;exit;
            //  $admin = new AdminUsermod;
              $admin->title   = Input::get('nama');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Jabatan telah berhasil diubah!');
              return Redirect::route('jabatan.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = Jabatanmod::find($id);
      //echo $data->picture;exit;
      // Storage::delete(public_path('/images/jabatan/'.$data->picture));

      $data->delete();

      // redirect
      Session::flash('message', 'Jabatan telah berhasil dihapus!');
      return Redirect::route('jabatan.dashboard');
    }


}
