<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\adminApprconfController;
use File;
use DB;
use Carbon\Carbon;
use App\ApprConfmod;
use App\Newsmod;
use App\Approvalmod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminNewsController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P04';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
      // $approver = ApprConfmod::where('modul','news')->get()->count();
      // if($approver == 0){
      //   return Redirect::back()->withErrors(['approver'=>'Approver belum diatur'])
      //   ->withInput();
      //   exit;
      // }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        //$dataList = Newsmod::all();
        $dataList = DB::table('news')
                   ->orderBy('id', 'desc')
                   ->get();
        return view('webmin.newsadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.newsadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
     
      // dd($request);
      $rules = array(
          'tanggal_publish'       => 'required',
          'judul'       => 'required|min:6',
          'lead'       => 'required|min:6',
          'konten'      => 'required',
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }else {
            if (!Session::get('image_images')){
              $validator->getMessageBag()->add('thumbnail', 'anda harus upload gambar');
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
            }
            // store
            // print_r(Input::get('privilege'));exit;

            // $thumb = $request->file('thumbnail');
            // $input['thumbname'] = '';
            //
            // if ($thumb) {
            //   $input['thumbname'] = time().'thumb.'.$thumb->getClientOriginalExtension();
            //
            //   $tdestinationPath = public_path('/images/news/');
            //
            //   $thumb->move($tdestinationPath, $input['thumbname']);
            // }
            //
            // $image = $request->file('picture');
            // $input['imagename'] = '';
            //
            // if ($image) {
            //   $input['imagename'] = time().'pic.'.$image->getClientOriginalExtension();
            //
            //   $destinationPath = public_path('/images/news/');
            //
            //   $image->move($destinationPath, $input['imagename']);
            // }
            //echo $priv;exit;
            if (Session::get('image_images')){
              $input['thumbname'] = Session::get('image_images');
            }

            $admin = new Newsmod;
            $admin->publish_date   = Input::get('tanggal_publish');
            $admin->title   = Input::get('judul');
            $admin->lead   = Input::get('lead');
            $admin->content      = Input::get('konten');
            $admin->uniqid = uniqid();
            $admin->slug   = str_slug(Input::get('title'), '-');
            $admin->status   = $request->input('status','approval');
            $admin->thumbnail = $input['thumbname'];
            // $admin->picture = $input['imagename'];
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            
            // if($request->status != "draft"){
            //   $data =  new \stdClass();
            //   $data->modul = 'news';
            //   $data->modulid = $admin->id;
            //   $appr = adminApprconfController::setAppr($data);
            // }
            $data =  new \stdClass();
            $data->status = $request->input('status','approval');
            $data->modul = 'news';
            $data->modulid =  $admin->id;
            $appr = adminApprconfController::setAppr($data);
            
            // redirect
            if($request->status == 'draft'){
              Session::flash('message', 'Draft berita berhasil disimpan, silakan di update kembali dan diajukan approvalnya');
            }else{
              Session::flash('message', 'Berita telah berhasil dibuat! dan akan segera di tinjau');
            }
            return Redirect::route('news.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Newsmod::find($id);
        $note = Approvalmod::select('admins.name','approval.note')->where('approval.modulid', $id)->join('admins','approval.adminid','admins.id')->where('approval.modul','news')->whereNotNull('approval.note')->get();
        return view('webmin.newsadmin_edit')->with('data', $data)->with('codepage',$this->codepage)->with('note',$note);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;
      // dd($id);
      $rules = array(
          'tanggal_publish'       => 'required',
          'judul'       => 'required|min:6',
          'lead'       => 'required|min:6',
          'konten'      => 'required',
      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
          }else {
              // store
              // print_r(Input::get('privilege'));exit;
              $admin = Newsmod::find($id);
              if (Session::get('image_images')){
                $admin->thumbnail = Session::get('image_images');
              }
              //
              // if ($request->file('thumbnail')){
              //   $thumb = $request->file('thumbnail');
              //
              //   $input['thumbname'] = time().'thumb.'.$thumb->getClientOriginalExtension();
              //
              //   $tdestinationPath = public_path('/images/news/');
              //
              //   $thumb->move($tdestinationPath, $input['thumbname']);
              //   $admin->thumbnail = $input['thumbname'];
              // }
              //
              // if (Input::get('thumbnail_del')){
              //   $admin->thumbnail = '';
              // }
              //
              // if ($request->file('picture')){
              //   $image = $request->file('picture');
              //
              //   $input['imagename'] = time().'pic.'.$image->getClientOriginalExtension();
              //
              //   $destinationPath = public_path('/images/news/');
              //
              //   $image->move($destinationPath, $input['imagename']);
              //   $admin->picture = $input['imagename'];
              // }
              //
              // if (Input::get('picture_del')){
              //   $admin->picture = '';
              // }

              //echo $priv;exit;
            //  $tgl_publish = Carbon::createFromFormat('d-m-Y', Input::get('publish_date'))->format('Y-m-d');
            //  $admin = new AdminUsermod;
              $admin->publish_date   = Carbon::parse(Input::get('tanggal_publish'));
              $admin->title   = Input::get('judul');
              $admin->lead   = Input::get('lead');
              $admin->slug   = str_slug(Input::get('judul'), '-');
              $admin->content      = Input::get('konten');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->status   = $request->input('status','approval');
              $admin->save();
              // if($request->status != "draft"){
              //   $data =  new \stdClass();
              //   $data->modul = 'news';
              //   $data->modulid = $id;
              //   $appr = adminApprconfController::setAppr($data);
              // }

              $data =  new \stdClass();
              $data->status = $request->input('status','approval');
              $data->modul = 'news';
              $data->modulid =  $id;
              $appr = adminApprconfController::setAppr($data);
              
              // redirect
              Session::flash('message', 'Berita telah berhasil dibuat! dan akan segera di tinjau');
              return Redirect::route('news.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = Newsmod::find($id);
      //echo $data->picture;exit;
      Storage::delete(public_path('/images/news/'.$data->thumbnail));
      // Storage::delete(public_path('/images/news/'.$data->picture));

      $data->delete();

      Approvalmod::where('modulid',$id)->where('modul','news')->delete();

      // redirect
      Session::flash('message', 'Berita telah berhasil dihapus!');
      return Redirect::route('news.dashboard');
    }

    public function postImage() {
			//echo "HIIIIII";

			$data = $_POST['image'];

			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);

			$data = base64_decode($data);
			$imageName = time().'thumb.png';
      file_put_contents(public_path('/images/news/').$imageName, $data);
      $this->getFilePathAttribute('/images/news/'.$imageName);

			Session::put('image_images', $imageName);

			echo "Image Uploaded";
		}

}
