<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\Pagemod;
use Carbon\Carbon;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminPageController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P02';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        // $dataList = Pagemod::all();
        $dataList = DB::table('page')
                   ->orderBy('id', 'desc')
                   ->get();
        return view('webmin.pageadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.pageadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'halaman'       => 'required',
          'judul'       => 'required|min:3',
          'konten'      => 'required',
          // 'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:max_width=1366,max_height=376',
          'excel1' => 'mimes:xls,xlsx|max:2048',
          'excel2' => 'mimes:xls,xlsx|max:2048'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }else {
          if (!Session::get('image_images')){
            $validator->getMessageBag()->add('gambar', 'anda harus upload gambar');
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
          }
            // $image = $request->file('gambar');
            $input['imagename'] = '';
            $input['excel1'] = '';
            $input['excel2'] = '';

            if ($request->file('excel1')){
              $fileExcel = $request->file('excel1')->getRealPath();
              $dataxcl = \Excel::load($fileExcel)->get();
              if($dataxcl->count()){
                foreach ($dataxcl as $datas) {
                   $arr[] = $datas->toArray();
                   // print_r($data);
                }
                $input['excel1'] = json_encode($arr);
              }
            }

            if ($request->file('excel2')){
              $fileExcel2 = $request->file('excel2')->getRealPath();
              $dataxcl2 = \Excel::load($fileExcel2)->get();
              if($dataxcl2->count()){
                foreach ($dataxcl2 as $datas) {
                   $arr2[] = $datas->toArray();
                   // print_r($data);
                }
                $input['excel1'] = json_encode($arr2);
              }
            }


            if (Session::get('image_images')){
              $input['imagename'] = Session::get('image_images');
            }
            // if ($image) {
            //   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            //   $destinationPath = public_path('/images/page/');

            //   $image->move($destinationPath, $input['imagename']);
            // }
            //echo $priv;exit;
            $admin = new Pagemod;
            $admin->codepage   = Input::get('halaman');
            $admin->title   = Input::get('judul');
            $admin->highlight   = Input::get('highlight');
            $admin->content      = Input::get('konten');
            $admin->judul1   = Input::get('judul1');
            $admin->excel1   = $input['excel1'];
            $admin->judul2   = Input::get('judul2');
            $admin->excel2   = $input['excel2'];
            $admin->uniqid = uniqid();
            $admin->picture = $input['imagename'];
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Halaman telah sukses ditambahkan!');
            return Redirect::route('page.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function stats()
    {

      $this->codepage = 'P07';
      // $statsDay = DB::table('page_stat')
      //            ->where('email','=',$request->email)
      //            ->first();

      $statsDay = DB::table('page_stat')->select(
	            DB::raw('DATE_FORMAT(tanggal, "%Y-%m-%d") as tgl')
	            ,DB::raw('count(DATE_FORMAT(tanggal, "%Y-%m-%d")) as total'))
	        ->groupBy('tgl')
          ->orderBy('tgl', 'asc')
	        ->get();
      $statsBrowser = DB::table('page_stat')->select(
              'browser'
              ,DB::raw('count(browser) as total'))
          ->groupBy('browser')
          ->get();

      $statsOS = DB::table('page_stat')->select(
              'os'
              ,DB::raw('count(os) as total'))
          ->groupBy('os')
          ->get();
      $statsPage = DB::table('page_stat')->select(
              'codepage'
              ,DB::raw('count(codepage) as total'))
          ->groupBy('codepage')
          ->orderBy('codepage', 'asc')
          ->get();
      $statsTot = DB::table('page_stat')->select(
                  DB::raw('count(created_at) as total'))
                  ->first();
      $skrg = Carbon::now();
      $hariIni = $skrg->timezone('Asia/Jakarta')->toDateString();
      $awalMinggu = Carbon::now()->timezone('Asia/Jakarta')->startOfWeek()->format('Y/m/d');
      $akhirMinggu = Carbon::now()->timezone('Asia/Jakarta')->endOfWeek()->format('Y/m/d');
      $bulanIni =  Carbon::now()->timezone('Asia/Jakarta')->month;
      $tahunIni =  Carbon::now()->timezone('Asia/Jakarta')->year;
      //print_r($skrg->timezone('Asia/Jakarta')->toDateString());exit;
      //echo Carbon::createFromFormat('Y-m-d', $skrg);exit;
      $statsHariIni = DB::table('page_stat')->select(
                      DB::raw('count(DATE_FORMAT(tanggal, "%Y-%m-%d")) as total'))
                      ->whereDate('tanggal','=', $hariIni)
                      ->first();

      $statsMingguIni = DB::table('page_stat')->select(
                        DB::raw('count(DATE_FORMAT(tanggal, "%Y-%m-%d")) as total'))
                        ->whereDate('tanggal','>=', $awalMinggu)
                        ->whereDate('tanggal','<=', $akhirMinggu)
                        ->first();

      $statsBulanIni = DB::table('page_stat')->select(
                        DB::raw('count(DATE_FORMAT(tanggal, "%Y-%m-%d")) as total'))
                        ->whereMonth('tanggal','=', $bulanIni)
                        ->first();

      $statsTahunIni = DB::table('page_stat')->select(
                        DB::raw('count(DATE_FORMAT(tanggal, "%Y-%m-%d")) as total'))
                        ->whereYear('tanggal','=', $tahunIni)
                        ->first();
      // print_r($statsBulanIni);exit;
      //var_dump($statsTahunIni);exit;
      return view('webmin.statsadmin')
              ->with('statsHariIni', $statsHariIni)
              ->with('statsMingguIni', $statsMingguIni)
              ->with('statsBulanIni', $statsBulanIni)
              ->with('statsTahunIni', $statsTahunIni)
              ->with('statsDay', $statsDay)
              ->with('statsBrowser', $statsBrowser)
              ->with('statsOS', $statsOS)
              ->with('statsTot', $statsTot)
              ->with('statsPage', $statsPage)
              ->with('codepage',$this->codepage);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Pagemod::find($id);
        return view('webmin.pageadmin_edit')->with('data', $data)->with('codepage',$this->codepage);;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'halaman'       => 'required',
          'judul'       => 'required|min:3',
          'konten'      => 'required',
          // 'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:max_width=1366,max_height=376',
          'excel1' => 'mimes:xls,xlsx|max:2048',
          'excel2' => 'mimes:xls,xlsx|max:2048'

      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
          }else {
            // if (!Session::get('image_images')){
            //   $validator->getMessageBag()->add('gambar', 'anda harus upload gambar');
            //   return Redirect::back()
            //       ->withErrors($validator)
            //       ->withInput();
            // }
              // store
              // print_r(Input::get('privilege'));exit;
              $admin = Pagemod::find($id);

              if ($request->file('excel1')){
                $fileExcel = $request->file('excel1')->getRealPath();
                $dataxcl = \Excel::load($fileExcel)->get();
                if($dataxcl->count()){
                  foreach ($dataxcl as $datas) {
                    // print_r($datas);
                     $arr[] = $datas->toArray();
                     // print_r($data);
                  }
                  $admin->excel1 = json_encode($arr);
                }
              }

              $admin->excel1 = str_replace(',[]', '', $admin->excel1);

              if ($request->file('excel2')){
                $fileExcel2 = $request->file('excel2')->getRealPath();
                $dataxcl2 = \Excel::load($fileExcel2)->get();
                if($dataxcl2->count()){
                  foreach ($dataxcl2 as $datas) {
                    // print_r($datas);
                     $arr2[] = $datas->toArray();
                     // print_r($data);
                  }
                  $admin->excel2 = json_encode($arr2);
                }
              }

              $admin->excel2 = str_replace(',[]', '', $admin->excel2);

              // exit;

              // print_r(json_encode($arr));exit;


              // if ($request->file('gambar')){
              //   $image = $request->file('gambar');

              //   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

              //   $destinationPath = public_path('/images/page/');

              //   $image->move($destinationPath, $input['imagename']);
              //   $admin->picture = $input['imagename'];
              // }

              if (Session::get('image_images')){
                $admin->picture = $input['imagename'] = Session::get('image_images');
              }

              if (Input::get('picture_del')){
                $admin->picture = '';
              }

              //echo $priv;exit;
            //  $admin = new AdminUsermod;
              $admin->codepage   = Input::get('halaman');
              $admin->title   = Input::get('judul');
              $admin->highlight   = Input::get('highlight');
              $admin->content      = Input::get('konten');
              $admin->judul1   = Input::get('judul1');
              $admin->judul2   = Input::get('judul2');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Halaman telah sukses diubah!');
              return Redirect::route('page.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = Pagemod::find($id);
      //echo $data->picture;exit;
      Storage::delete(public_path('/images/page/'.$data->picture));

      $data->delete();

      // redirect
      Session::flash('message', 'Halaman sukses dihapus!');
      return Redirect::route('page.dashboard');
    }

    public function postImage() {
			//echo "HIIIIII";

			$data = $_POST['image'];

			list($type, $data) = explode(';', $data);
			list(, $data)      = explode(',', $data);

			$data = base64_decode($data);
			$imageName = time().'.png';
			file_put_contents(public_path('/images/page/').$imageName, $data);
      $this->getFilePathAttribute('/images/page/'.$imageName);

			Session::put('image_images', $imageName);

			echo "Image Uploaded";
		}
}
