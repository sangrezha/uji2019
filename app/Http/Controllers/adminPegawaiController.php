<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\Pegawaimod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminPegawaiController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P09';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        //$dataList = pegawaimod::all();
        $dataList = DB::table('pegawai')
                   ->orderBy('priority', 'asc')
                   ->get();
        return view('webmin.pegawaiadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    public function sort()
    {
        //
      //echo "abcde";exit;
        //$dataList = pegawaimod::all();
        $dataList = DB::table('pegawai')
                   ->orderBy('priority', 'asc')
                   ->get();
        return view('webmin.pegawaiadmin_sort')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $dataList = DB::table('jabatan')
                 ->orderBy('priority', 'asc')
                 ->get();
        return view('webmin.pegawaiadmin_add')->with('codepage',$this->codepage)->with('dataList', $dataList);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'nama'       => 'required|min:2',
          // 'nip'       => 'required',
          'divisi_jabatan'       => 'required',
          'picture' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::route('pegawai.add')
                ->withErrors($validator)
                ->withInput();
        }else {
            // store
            // print_r(Input::get('privilege'));exit;
            $maxPriority = DB::table('pegawai')
                       ->orderBy('priority', 'desc')
                       ->first();
            //print_r($maxPriority);exit;
            //foreach ($maxPriority as $k => $v){

            //}
            $valMax = (@$maxPriority->priority) ? @$maxPriority->priority : 0;
            // echo  $valMax = $valMax+1;exit;
            $image = $request->file('picture');
            $input['imagename'] = '';

            if ($image) {
              $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

              $destinationPath = public_path('/images/pegawai/');

              $image->move($destinationPath, $input['imagename']);
            }
            //echo $priv;exit;
            $admin = new Pegawaimod;
            $admin->nama   = Input::get('nama');
            $admin->nip   = Input::get('nip');
            $admin->divisi   = Input::get('divisi_jabatan');
            $admin->pimpinan   = Input::get('pimpinan');
            $admin->golongan   = Input::get('golongan');
            $admin->pangkat   = Input::get('pangkat');
            $admin->jabatan   = Input::get('jabatan');
            $admin->staff   = Input::get('staff');
            $admin->pendidikan   = Input::get('pendidikan');
            $admin->uniqid = uniqid();
            $admin->priority = $valMax+1;
            $admin->picture = $input['imagename'];
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Pegawai telah sukses dibuat!');
            return Redirect::route('pegawai.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Pegawaimod::find($id);
        $dataList = DB::table('jabatan')
                 ->orderBy('priority', 'asc')
                 ->get();
        return view('webmin.pegawaiadmin_edit')->with('dataList', $dataList)->with('data', $data)->with('codepage',$this->codepage);;

    }

    public function sortsave(Request $request)
    {
      # code...
      //print_r($request);exit;
      $ctr=0;
      foreach ($request->item as $k => $v) {
        $ctr++;
        $data = Pegawaimod::find($v);
        $data->priority = $ctr;
        $data->save();
        # code...
      }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'nama'       => 'required|min:2',
          'divisi_jabatan'       => 'required',
      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
          }else {
              // store
              // print_r(Input::get('privilege'));exit;
              $admin = pegawaimod::find($id);


              if ($request->file('picture')){
                $image = $request->file('picture');

                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

                $destinationPath = public_path('/images/pegawai/');

                $image->move($destinationPath, $input['imagename']);
                $admin->picture = $input['imagename'];
              }

              if (Input::get('picture_del')){
                $admin->picture = '';
              }

              //echo $priv;exit;
            //  $admin = new AdminUsermod;
              $admin->nama   = Input::get('nama');
              $admin->nip   = Input::get('nip');
              $admin->divisi   = Input::get('divisi_jabatan');
              $admin->pimpinan   = Input::get('pimpinan');
              $admin->golongan   = Input::get('golongan');
              $admin->pangkat   = Input::get('pangkat');
              $admin->jabatan   = Input::get('jabatan');
              $admin->staff   = Input::get('staff');
              $admin->pendidikan   = Input::get('pendidikan');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Pegawai telah sukses diubah!');
              return Redirect::route('pegawai.dashboard');
          }


    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = pegawaimod::find($id);
      //echo $data->picture;exit;
      Storage::delete(public_path('/images/pegawai/'.$data->picture));

      $data->delete();

      // redirect
      Session::flash('message', 'Pegawai telah sukses dihapus!');
      return Redirect::route('pegawai.dashboard');
    }


}
