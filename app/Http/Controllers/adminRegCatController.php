<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\RegCat;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminRegCatController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P061';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
      //  $dataList = RegCat::all();
        $dataList = DB::table('izin_kategori')
                   ->orderBy('id', 'desc')
                   ->get();
        return view('webmin.regcatadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.regcatadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'nama'       => 'required|min:3|unique:izin_kategori,name'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }else {
            $admin = new RegCat;
            $admin->name   = Input::get('nama');
            $admin->uniqid = uniqid();
            $admin->created_by = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Jenis Peraturan telah berhasil ditambah!');
            return Redirect::route('rc.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = RegCat::find($id);
        return view('webmin.regcatadmin_edit')->with('data', $data)->with('codepage',$this->codepage);;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'nama'       => 'required|min:3'
      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
          }else {
              $admin = RegCat::find($id);
              $admin->name   = Input::get('nama');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Jenis Peraturan telah berhasil diubah!');
              return Redirect::route('rc.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = RegCat::find($id);
      $data->delete();

      // redirect
      Session::flash('message', 'Jenis Peraturan telah berhasil dihapus!');
      return Redirect::route('rc.dashboard');
    }


}
