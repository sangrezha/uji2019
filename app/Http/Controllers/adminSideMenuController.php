<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\adminApprconfController;
use File;
use DB;
use Carbon\Carbon;
use App\ApprConfmod;
use App\SideMenumod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminSideMenuController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P16';
      $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        //$dataList = SideMenumod::all();
        $dataList = DB::table('sidemenu')
                   ->orderBy('id', 'desc')
                   ->get();
        return view('webmin.sidemenuadmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.sidemenuadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'title'       => 'required|min:6',
          'link'       => 'required|min:6',
          'window'       => 'required',
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }else {
            $admin = new SideMenumod;
            $admin->title   = Input::get('title');
            $admin->link   = Input::get('link');
            $admin->window   = Input::get('window');
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->priority      = 'max(priority)+1';
            $admin->save();
            Session::flash('message', 'Menu telah berhasil dibuat!');
            return Redirect::route('sidemenu.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = SideMenumod::find($id);
        return view('webmin.sidemenuadmin_edit')->with('data', $data)->with('codepage',$this->codepage);;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
      $rules = array(
          'title'       => 'required|min:6',
          'link'       => 'required|min:6',
          'window'       => 'required',
      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput();
          }else {
              $admin = SideMenumod::find($id);
              $admin->title   = Input::get('title');
              $admin->link   = Input::get('link');
              $admin->window   = Input::get('window');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
             
              Session::flash('message', 'Menu telah berhasil diubah!');
              return Redirect::route('sidemenu.dashboard');
          }


    }
    public function setstatus($id, $status)
    {
     
      $admin = SideMenumod::find($id);
      $admin->status   = ($status=='active'?'inactive':'active');
      $admin->updated_by      = Auth::guard('admin')->user()->name;
      $admin->save();
      
      Session::flash('message', 'Menu telah berhasil diubah!');
      return Redirect::route('sidemenu.dashboard');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = SideMenumod::find($id);
      //echo $data->picture;exit;
      // Storage::delete(public_path('/images/sidemenu/'.$data->thumbnail));
      // Storage::delete(public_path('/images/sidemenu/'.$data->picture));

      $data->delete();

      // redirect
      Session::flash('message', 'Menu telah berhasil dihapus!');
      return Redirect::route('sidemenu.dashboard');
    }


}
