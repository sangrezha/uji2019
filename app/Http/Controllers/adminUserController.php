<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\AdminUsermod;
use App\PreferencesMod;
use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class adminUserController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P01';

      $this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        // $dataList = AdminUsermod::all();
        $dataList = DB::table('admins')
                   ->orderBy('id', 'desc')
                   ->get();
        return view('webmin.useradmin_template')->with('dataList', $dataList)->with('codepage',$this->codepage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.useradmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'name'       => 'required',
          'username'       => 'required|min:6|unique:admins',
          'email'      => 'required|email|unique:admins',
          'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
          'password' => 'required|min:6',
          'privilege' => 'required'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::route('user.add')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else {
            // store
            // print_r(Input::get('privilege'));exit;
            $priv = implode('|', $request->privilege);
            $chat = "";
            if(isset($request->chatkategori)){
              $chat = implode('|', $request->chatkategori);
            }

            $image = $request->file('gambar');

            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/images/user/');

            $image->move($destinationPath, $input['imagename']);
            //echo $priv;exit;
            $admin = new AdminUsermod;
            $admin->username   = Input::get('username');
            $admin->name   = Input::get('name');
            $admin->email      = Input::get('email');
            $admin->privilege = $priv;
            $admin->chatkategori = $chat;
            $admin->active = 1;
            $admin->uniqid = uniqid();
            $admin->picture = $input['imagename'];
            $admin->password = Hash::make(Input::get('password'));
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Admin telah sukses dibuat!');
            return Redirect::route('useradmin.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    public function changePassword()
    {
        //
//        $data = AdminUsermod::find($id);
        $codehalaman = 'P1';
        return view('webmin.useradmin_chgpasswd')->with('codepage',$this->codepage);
    }

    public function webSetting()
    {
        //
//        $data = AdminUsermod::find($id);
        $codehalaman = 'P00';
        $preferences = PreferencesMod::where('id', '1')->first();

        return view('webmin.useradmin_preferences')
                ->with('data', $preferences)
                ->with('codepage',$this->codepage);
    }


    public function webSettingSave(Request $request)
    {
        //
//        $data = AdminUsermod::find($id);
      $rules = array(
          'title'       => 'required|min:6|',
      );
      $validator = Validator::make(Input::all(), $rules);



      if ($validator->fails()) {
            return Redirect::route('user.chgpasswd')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else {
            // store
            // print_r(Input::get('privilege'));exit;
            $admin = PreferencesMod::where('id', '1')->first();

            $harichat = implode(',', Input::get('chat_hari'));

            if ($request->file('gambar')){
              $image = $request->file('gambar');

              $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

              $destinationPath = public_path('/images/popup/');

              $image->move($destinationPath, $input['imagename']);
              $admin->popup_image = $input['imagename'];
            }
            // $admin = AdminUsermod::find($id);
              $admin->title = Input::get('title');
              $admin->popup_url = Input::get('popup_url');
              $admin->popup_active = Input::get('popup_active');
              $admin->meta_desc = Input::get('meta_description');
              $admin->meta_keyword = Input::get('meta_keyword');
              $admin->konten_head = Input::get('konten_head');
              $admin->konten_body_t = Input::get('konten_body_atas');
              $admin->konten_body_b = Input::get('konten_body_bawah');
              $admin->chat_hari = $harichat;
              $admin->chat_active = Input::get('chat_active');
              $admin->chat_jam1 = Input::get('chat_jam1');
              $admin->chat_jam2 = Input::get('chat_jam2');
              $admin->teks1 = Input::get('teks_sdm');
              $admin->teks2 = Input::get('teks_prasarana');
              $admin->teks3 = Input::get('teks_sarana');
              $admin->footer = Input::get('footer');
              $admin->save();
              // redirect
              Session::flash('message', 'Website Setting sukses diubah!');

            return Redirect::route('admin.setting');


            //echo $priv;exit;
          //  $admin = new AdminUsermod;

        }
    }

    public function changePasswordSave($id, Request $request)
    {
        //
//        $data = AdminUsermod::find($id);
      $rules = array(
          'password_lama'       => 'required|min:6',
          'password_baru'       => 'required|min:6|different:password_lama',
      );
      $validator = Validator::make(Input::all(), $rules);



      if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        }else {
            // store
            // print_r(Input::get('privilege'));exit;
            $admin = AdminUsermod::find($id);
            if (Hash::check($request->password_lama, Auth::guard('admin')->user()->password))
            {
              if (Input::get('password_baru')) {
                $admin->password = Hash::make(Input::get('password_baru'));
              }
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Password telah berhasil diubah!');
            } else {
              Session::flash('message', 'Password lama tidak sama! Password gagal diubah');
            }
            return Redirect::route('user.chgpasswd');


            //echo $priv;exit;
          //  $admin = new AdminUsermod;

        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = AdminUsermod::find($id);
        return view('webmin.useradmin_edit')->with('data', $data)->with('codepage',$this->codepage);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

        $rules = array(
            'name'       => 'required',
            'username'       => 'required|min:6',
            'email'      => 'required|email',
            'gambar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'privilege' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
              return Redirect::back()
                  ->withErrors($validator)
                  ->withInput(Input::except('password'));
          }else {
              // store
              // print_r(Input::get('privilege'));exit;
              $admin = AdminUsermod::find($id);


              $priv = implode('|', $request->privilege);
              $chat = "";
              if(isset($request->chatkategori)){
                $chat = implode('|', $request->chatkategori);
              }
              if ($request->file('gambar')){
                $image = $request->file('gambar');

                $input['imagename'] = time().'.'.$image->getClientOriginalExtension();

                $destinationPath = public_path('/images/user/');

                $image->move($destinationPath, $input['imagename']);
                $admin->picture = $input['imagename'];
              }

              if (Input::get('picture_del')){
                $admin->picture = '';
              }

              //echo $priv;exit;
            //  $admin = new AdminUsermod;
              $admin->username   = Input::get('username');
              $admin->name   = Input::get('name');
              $admin->email      = Input::get('email');
              $admin->privilege = $priv;
              $admin->chatkategori = $chat;
              $admin->active = 1;
              if (Input::get('password')) {
                $admin->password = Hash::make(Input::get('password'));
              }
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Data Admin telah sukses diubah!');
              return Redirect::route('useradmin.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = AdminUsermod::find($id);
      //echo $data->picture;exit;
      Storage::delete(public_path('/images/user/'.$data->picture));

      $data->delete();

      // redirect
      Session::flash('message', 'Admin telah sukses dihapus!');
      return Redirect::route('useradmin.dashboard');
    }

    public function status($id)
    {
      $data = AdminUsermod::find($id);
      //echo $data->picture;exit;
      if ($data->active) {
        $data->active = 0;
      } else {
        $data->active = 1;
      }

      $data->save();

      // redirect
      Session::flash('message', 'Status Admin sukses diubah!');
      return Redirect::route('useradmin.dashboard');//
    }
}
