<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\Bumod;
use App\Pagemod;
use App\RegIzin;
use App\RegCat;
use App\Companyattrmod;
use App\PreferencesMod;

use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class webControllersss extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P04';

      //$this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
      date_default_timezone_set("Asia/Jakarta");

    if (trim ($timestamp) == '')
          {
                  $timestamp = time ();
          }
          elseif (!ctype_digit ($timestamp))
          {
              $timestamp = strtotime ($timestamp);
          }
          # remove S (st,nd,rd,th) there are no such things in indonesia :p
          $date_format = preg_replace ("/S/", "", $date_format);
          $pattern = array (
              '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
              '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
              '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
              '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
              '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
              '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
              '/April/','/June/','/July/','/August/','/September/','/October/',
              '/November/','/December/',
          );
          $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
              'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
              'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
              'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
              'Oktober','November','Desember',
          );
          $date = date ($date_format, $timestamp);
          $date = preg_replace ($pattern, $replace, $date);
          $date = "{$date} ";
          return $date;
      }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      echo "abcsssde";exit;
        $this->codepage = 'I01';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.home_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function profil()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I02';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.profile_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function regulasi()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I03';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $dataList = DB::table('izin_regulasi')
            ->join('izin_kategori', 'izin_regulasi.category_id', '=', 'izin_kategori.id')
            ->select('izin_regulasi.*', 'izin_kategori.name')
            ->get();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.regulasi_template')
        ->with('dataList', $dataList)
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function izinumum()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I041';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function izinkhusus()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I042';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranakhusus_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function prasaranaumum()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I0411';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function prasaranaumumtrase()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04111';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function prasaranaumumusaha()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04112';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function prasaranaumumbangun()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04113';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function prasaranaumumops()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04114';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function prasaranaumumops1()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I041141';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function prasaranaumumops2()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I041142';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function prasaranaumumops3()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I041143';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function prasaranaumumops4()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I041144';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranaumum()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I0412';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranaumumusaha()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04121';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranaumumlintas()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04122';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranaumum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranaumumops()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04123';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranaumumops1()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I041231';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranaumumops2()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I041232';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranaumumops3()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I041233';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranaumumops4()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I041234';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranaumum_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranakhusus1()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I0421';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranakhusus_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranakhusus2()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I0422';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.prasaranakhusus_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranakhusus3()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I0423';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranakhusus_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranakhususops1()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04231';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranakhusus_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranakhususops2()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04232';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranakhusus_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranakhususops3()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04233';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranakhusus_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function saranakhususops4()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I04234';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.saranakhusus_operasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    public function badanusaha()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I05';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        //$dataList = Bumod::all();
        $dataList = Companyattrmod::all();
        $preferences = PreferencesMod::where('id', '1')->first();
        $ctr=1;
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        return view('web.badanusaha_template')
        ->with('dataList', $dataList)
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('ctr', $ctr)
        ->with('hariini', $hariini);
    }

    public function kontak()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I06';
        $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        return view('web.kontak_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('codepage',$this->codepage)
        ->with('hariini', $hariini);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('webmin.badanusahaadmin_add')->with('codepage',$this->codepage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
      $rules = array(
          'name'       => 'required',
          'address'       => 'required|min:6'
      );
      $validator = Validator::make(Input::all(), $rules);

      if ($validator->fails()) {
            return Redirect::route('bu.add')
                ->withErrors($validator)
                ->withInput();
        }else {
            $admin = new Bumod;
            $admin->name   = Input::get('name');
            $admin->address   = Input::get('address');
            $admin->uniqid = uniqid();
            $admin->created_by      = Auth::guard('admin')->user()->name;
            $admin->save();
            // redirect
            Session::flash('message', 'Successfully created Badan Usaha!');
            return Redirect::route('bu.dashboard');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Bumod::find($id);
        return view('webmin.badanusahaadmin_edit')->with('data', $data)->with('codepage',$this->codepage);;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {

      //  echo $data->name;exit;

      $rules = array(
          'name'       => 'required',
          'address'       => 'required|min:6'
      );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
              return Redirect::route('bu.add')
                  ->withErrors($validator)
                  ->withInput();
          }else {
              $admin = Bumod::find($id);
              $admin->name   = Input::get('name');
              $admin->address   = Input::get('address');
              $admin->updated_by      = Auth::guard('admin')->user()->name;
              $admin->save();
              // redirect
              Session::flash('message', 'Successfully updated Badan Usaha!');
              return Redirect::route('bu.dashboard');
          }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = Bumod::find($id);
      $data->delete();

      // redirect
      Session::flash('message', 'Successfully deleted Badan Usaha!');
      return Redirect::route('bu.dashboard');
    }


}
