<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use DB;
use App\Newsmod;
use App\WikiUjimod;
use App\SideMenumod;
use App\Mitramod;
use App\Eventmod;
use App\Pagemod;
use App\Chatmod;
use App\ChatDetailmod;
use App\PengumumanMod;
use App\Pegawaimod;
use App\Jabatanmod;
use App\Pagestatmod;
use App\AdminUsermod;
use App\gallerymod;
use App\RegIzin;
use App\RegCat;
use Carbon\Carbon;
use App\Companyattrmod;
use App\PreferencesMod;
use App\chartPiemod;
use App\chartBarmod;
use Jenssegers\Agent\Agent;
use LRedis;
use GeoIP;


use Validator, Input, Redirect, Auth, Session, Hash, Storage; // use AdminUsermod;

class webController extends Controller
{
    //
    public function __construct()
    {
      # code...
      parent::__construct();
      $this->codepage = 'P04';
      date_default_timezone_set("Asia/Bangkok");

      //$this->middleware('auth:admin');
      //$this->middleware('guest:admin', ['except' => ['logout']]);
    }

    public static function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
      date_default_timezone_set("Asia/Jakarta");

    if (trim ($timestamp) == '')
          {
                  $timestamp = time ();
          }
          elseif (!ctype_digit ($timestamp))
          {
              $timestamp = strtotime ($timestamp);
          }
          # remove S (st,nd,rd,th) there are no such things in indonesia :p
          $date_format = preg_replace ("/S/", "", $date_format);
          $pattern = array (
              '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
              '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
              '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
              '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
              '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
              '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
              '/April/','/June/','/July/','/August/','/September/','/October/',
              '/November/','/December/',
          );
          $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
              'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
              'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
              'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
              'Oktober','November','Desember',
          );
          $date = date ($date_format, $timestamp);
          $date = preg_replace ($pattern, $replace, $date);
          $date = "{$date} ";
          return $date;
      }

      public static function log_stat($codepage){

        $agent = new Agent();
        //$geo = GeoIP::getLocation();

        //print_r($geo);exit;
        $stat = new Pagestatmod;

        //$stat->negara = $geo->country;
      //  $stat->lokasi = $geo->city;
        /* cek jenis nya, desktop, mobile atau desktop */
        if ($agent->isMobile()){
          $stat->jenis = 'Mobile';
        } elseif ($agent->isTablet()) {
          $stat->jenis = 'Tablet';
        } elseif ($agent->isRobot()) {
          $stat->jenis = 'Robot';
        } else {
          $stat->jenis = 'Desktop';
        }

        $stat->codepage = $codepage;
        $stat->browser = $agent->browser();
        $stat->os = $agent->platform();
        $stat->devicename = $agent->device();
        // $stat->tanggal = Carbon::now();

        $stat->save();

      }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I01';
        // $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        //View::make('view')->withModel($model);
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        $highlight = DB::table('highlight')
                   ->orderBy('priority', 'asc')
                   ->get();
         $news1 = Newsmod::orderBy('publish_date', 'desc')
                    ->where('publish_date','<=', Carbon::now())
                    ->where('status','publish')
                    ->first();
         $pengumuman1 = Pengumumanmod::orderBy('publish_date', 'desc')
                   ->where('publish_date','<=', Carbon::now())
                   ->where('status','publish')
                   ->first();
        $active[$news1->uniqid] = 'active';
        $active[$pengumuman1->uniqid] = 'active';
        $news = Newsmod::orderBy('publish_date', 'desc')
                   ->where('publish_date','<=', Carbon::now())
                   ->where('status','publish')
                   ->limit(9)
                   ->get();
        $pengumuman = PengumumanMod::orderBy('publish_date', 'desc')
                       ->where('publish_date','<=', Carbon::now())
                       ->limit(9)
                       ->get();
        $mitra = Mitramod::orderBy('priority', 'asc')->get();
        //print_r($preferences);exit;
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.home_template')
        ->with('page', $page)
        ->with('highlight', $highlight)
        ->with('active', $active)
        ->with('news', $news)
        ->with('mitra', $mitra)
        ->with('pengumuman', $pengumuman)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }


/* -------------------profil---------------------------------------*/

    public function struktur()
    {
        $this->codepage = 'I021';
        $this->log_stat($this->codepage);

        $preferences = PreferencesMod::where('id', '1')->first();
        $chartBar = chartBarmod::orderby('priority')->get();
        $chartPie = chartPiemod::orderby('priority')->get();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        $jabatan1 = Jabatanmod::where("priority",1)->first();
        $pegawai1 = Pegawaimod::where('divisi', $jabatan1->uniqid)->first();
        $pegawai1child = Pegawaimod::where('divisi', $jabatan1->uniqid)->whereNull('pimpinan')->get();
        $jabatan2 = Jabatanmod::where("priority", '>', 1)->orderby('priority')->get();
        $baris = Jabatanmod::where("priority", '>', 1)->select('baris')->distinct('baris')->orderby('baris')->get();
        // print_r($baris);exit;
        $jabatannya = array();
        if($baris->count() > 0){

          foreach ($baris as $barisnya){
            $jabatan3 = Jabatanmod::where("baris", $barisnya->baris)->orderby('priority')->get();
            $ctr = 0;
            foreach ($jabatan3 as $detjabatan) {
              $ctr++;
              $jabatannya[$barisnya->baris][$ctr]['title'] = $detjabatan->title;
              $jabatannya[$barisnya->baris][$ctr]['id'] = $detjabatan->id;
              $jabatannya[$barisnya->baris][$ctr]['uniqid'] = $detjabatan->uniqid;
              $jabatannya[$barisnya->baris][$ctr]['priority'] = $detjabatan->priority;
            }
          }
        }

        // print_r($jabatannya);exit;
        $pegawai = array();
        foreach ($jabatan2 as $jabatan){
            $ctr = 0;
            $peg = Pegawaimod::where('divisi', $jabatan->uniqid)->where('pimpinan', 1)->orderby('priority')->get();
            foreach ($peg as $pegawais){
              $ctr++;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['nama'] = $pegawais->nama;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['posisi'] = $jabatan->nama;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['jabatan'] = $pegawais->jabatan;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['picture'] = $pegawais->picture;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['pimpinan'] = $pegawais->pimpinan;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['nip'] = $pegawais->nip;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['golongan'] = $pegawais->golongan;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['pangkat'] = $pegawais->pangkat;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['pendidikan'] = $pegawais->pendidikan;
              $pegawai[$jabatan->baris][$jabatan->uniqid][$ctr]['staff'] = $pegawais->staff;
            }
            $ctr = 0;
            $peg = Pegawaimod::where('divisi', $jabatan->uniqid)->whereNull('pimpinan')->orderby('priority')->get();
            foreach ($peg as $pegawais){
              $ctr++;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['nama'] = $pegawais->nama;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['posisi'] = $jabatan->nama;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['jabatan'] = $pegawais->jabatan;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['picture'] = $pegawais->picture;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['pimpinan'] = $pegawais->pimpinan;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['nip'] = $pegawais->nip;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['golongan'] = $pegawais->golongan;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['pangkat'] = $pegawais->pangkat;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['pendidikan'] = $pegawais->pendidikan;
              $pegawaichild[$jabatan->baris][$jabatan->uniqid][$ctr]['staff'] = $pegawais->staff;
            }
        }



        // print_r($pegawai);
        // exit;
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.struktur_template')
                ->with('page', $page)
                ->with('jabatan1', $jabatan1)
                ->with('jabatan2', $jabatan2)
                ->with('jabatannya', $jabatannya)
                ->with('baris', $baris)
                ->with('chartBar', $chartBar)
                ->with('chartPie', $chartPie)
                ->with('pegawai1', $pegawai1)
                ->with('pegawai1child', $pegawai1child)
                ->with('pegawai', $pegawai)
                ->with('pegawaichild', $pegawaichild)
                ->with('pref', $preferences)
                ->with('sidemenu', $sidemenu)
                ->with('codepage',$this->codepage);
    }

    public function datapegawai()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I022';
        $this->log_stat($this->codepage);
        // $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $pegawai = DB::table('pegawai')
                   ->orderBy('priority', 'asc')
                   ->get();
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($page);exit;
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.datapegawai_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('pegawai', $pegawai)
        ->with('codepage',$this->codepage);
    }

    public function tupoksi()
    {
        $this->codepage = 'I023';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.tupoksi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function event()
    {
        //
        $this->codepage = 'I024';
        $this->log_stat($this->codepage);
        $berita = DB::table('event')
                   ->orderBy('publish_date', 'desc')
                   ->where('publish_date','<=', Carbon::now())
                   ->get();
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.kegiatan_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('berita', $berita)
        ->with('codepage',$this->codepage);
    }

    public function detilevent($slug)
    {
        //
        $this->codepage = 'I024';
        $this->log_stat($this->codepage);
        $news = Eventmod::where('slug', $slug)->first();
        $ctr = (@$news->counter) ? $news->counter : 0;
        $ctr++;
        $news->counter = $ctr;
        $news->save();
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.kegiatandetil_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('news', $news)
        ->with('codepage',$this->codepage);
    }

    public function denah()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I025';
        $this->log_stat($this->codepage);
        // $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($page);exit;
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.denah_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function regulasidown($filenya)
    {
      $downfile = RegIzin::where('attachfile', $filenya)->first();
      $downfile->counter = $downfile->counter+1;
      $downfile->save();
      $folder = base_path('/storage/app/public/website');
    	$path = $folder.'/'.$filenya;

    	return response()->file($path);
      // print_r($downfile);
      exit;

    }

    public function pengumumandown($filenya)
    {
      $downfile = PengumumanMod::where('attachfile', $filenya)->first();
      $downfile->counter = $downfile->counter+1;
      $downfile->save();
      $folder = base_path('/storage/app/public/pengumuman');
    	$path = $folder.'/'.$filenya;

    	return response()->file($path);
      // print_r($downfile);
      exit;

    }

    public function pengumuman(Request $request)
    {
        //

        $this->codepage = 'I09';
        $this->log_stat($this->codepage);
        $cat = (isset($request->cat)?$request->cat:1);
        // $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
//         $preferences = PreferencesMod::where('id', '1')->first();
//         $page = Pagemod::where('codepage', $this->codepage)->first();
//
// //        $preferences = PreferencesMod::where('id', '1')->first();
//         $dataList = DB::table('pengumuman')
//             ->orderBy('id', 'desc')
//             ->get();
//         $page = Pagemod::where('codepage', $this->codepage)->first();
//         //print_r($preferences);exit;
//         ->with('dataList', $dataList)
//         ->with('page', $page)
//         ->with('pref', $preferences)
//         ->with('codepage',$this->codepage);

          if(\Auth::guard('admin')->user()){
            $berita = DB::table('pengumuman')
                        ->orderBy('publish_date', 'desc')
                        ->where('publish_date','<=', Carbon::now())
                        ->whereIn('status',['publish','approval','draft'])
                        ->where('category',$cat)
                        ->paginate(15);
          }else{
            $berita = DB::table('pengumuman')
                        ->orderBy('publish_date', 'desc')
                        ->where('publish_date','<=', Carbon::now())
                        ->where('status','publish')
                        ->where('category',$cat)
                        ->paginate(15);
          }
          $preferences = PreferencesMod::where('id', '1')->first();
          $page = Pagemod::where('codepage', $this->codepage)->first();
          
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
          return view('web.pengumuman_template')
          ->with('page', $page)
          ->with('pref', $preferences)
          ->with('sidemenu', $sidemenu)
          ->with('berita', $berita)
          ->with('codepage',$this->codepage);
    }

    public function detilpengumuman($slug)
    {
        //
        $this->codepage = 'I09';
        $this->log_stat($this->codepage);
        if(\Auth::guard('admin')->user()){
          $news = PengumumanMod::where('slug', $slug)->first();
        }else{
          $news = PengumumanMod::where('slug', $slug)->where('status','publish')->first();
        }
        $ctr = (@$news->counter) ? $news->counter : 0;
        $ctr++;
        $news->counter = $ctr;
        $news->save();
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.detilpengumuman_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('news', $news)
        ->with('codepage',$this->codepage);
    }

    public function getKey($json){
      $varnya = json_decode($json);
      $keynya = array_keys((array)$varnya[0]);
      return $keynya;
    }
    public function regulasi()
    {
        //

        $this->codepage = 'I024';
        $this->log_stat($this->codepage);
        // $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();

//        $preferences = PreferencesMod::where('id', '1')->first();
        $dataList = DB::table('izin_regulasi')
            ->join('izin_kategori', 'izin_regulasi.category_id', '=', 'izin_kategori.id')
            ->select('izin_regulasi.*', 'izin_kategori.name')
            ->orderBy('tahun', 'asc')
            ->get();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($preferences);exit;
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.regulasi_template')
        ->with('dataList', $dataList)
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function pembinaan()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I03';
        $this->log_stat($this->codepage);
        // $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($page);exit;
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.pembinaan_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

/* ------------------- pengujian sdm ---------------------*/
    public function ujisdmUmum()
    {
        $this->codepage = 'I040';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisdmUmum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisdmInfo()
    {
        $this->codepage = 'I041';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisdmInfo_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisdmSOP()
    {
        $this->codepage = 'I042';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisdmSOP_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisdmJadwal()
    {
        $this->codepage = 'I043';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        $dat['key1'] = $this->getKey($page->excel1);
        $dat['excel1']  = json_decode($page->excel1, true);
        $dat['key2'] = $this->getKey($page->excel2);
        $dat['excel2']  = json_decode($page->excel2, true);
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisdmJadwal_template')
        ->with('page', $page)
        ->with('dat', $dat)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisdmPeserta()
    {
        $this->codepage = 'I044';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisdmPeserta_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisdmLokasi()
    {
        $this->codepage = 'I045';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisdmLokasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisdmHasil()
    {
        $this->codepage = 'I046';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->orderBy('created_at','DESC')->first();
        $dat['key1'] = $this->getKey($page->excel1);
        $dat['excel1']  = json_decode($page->excel1, true);
        $dat['key2'] = $this->getKey($page->excel2);
        $dat['excel2']  = json_decode($page->excel2, true);
        // dd($page->excel1);
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisdmHasil_template')
        ->with('page', $page)
        ->with('dat', $dat)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisdmSarana()
    {
        $this->codepage = 'I047';
        $this->log_stat($this->codepage);
        $dataList = gallerymod::where('category', 1)->orderby('created_at', 'desc')->get();
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisdmSarana_template')
        ->with('page', $page)
        ->with('dataList', $dataList)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

/* ------------------- pengujian prasarana ---------------------*/

    public function ujiprasaranaUmum()
    {
        $this->codepage = 'I050';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujiprasaranaUmum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujiprasaranaInfo()
    {
        $this->codepage = 'I051';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujiprasaranaInfo_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujiprasaranaSOP()
    {
        $this->codepage = 'I052';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujiprasaranaSOP_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujiprasaranaJadwal()
    {
        $this->codepage = 'I053';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        $dat['key1'] = $this->getKey($page->excel1);
        $dat['excel1']  = json_decode($page->excel1, true);
        $dat['key2'] = $this->getKey($page->excel2);
        $dat['excel2']  = json_decode($page->excel2, true);

        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujiprasaranaJadwal_template')
        ->with('page', $page)
        ->with('dat', $dat)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujiprasaranaPeserta()
    {
        $this->codepage = 'I054';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujiprasaranaPeserta_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujiprasaranaLokasi()
    {
        $this->codepage = 'I055';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujiprasaranaLokasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujiprasaranaHasil()
    {
        $this->codepage = 'I056';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        $dat['key1'] = $this->getKey($page->excel1);
        $dat['excel1']  = json_decode($page->excel1, true);
        $dat['key2'] = $this->getKey($page->excel2);
        $dat['excel2']  = json_decode($page->excel2, true);

        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujiprasaranaHasil_template')
        ->with('page', $page)
        ->with('dat', $dat)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujiprasaranaData()
    {
        $this->codepage = 'I057';
        $this->log_stat($this->codepage);
        $dataList = gallerymod::where('category', 2)->orderby('created_at', 'desc')->get();
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujiprasaranaData_template')
        ->with('page', $page)
        ->with('dataList', $dataList)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujiprasaranaPermohonan()
    {
        $this->codepage = 'I058';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        // $dat['key1'] = $this->getKey($page->excel1);
        // $dat['excel1']  = json_decode($page->excel1, true);
        // $dat['key2'] = $this->getKey($page->excel2);
        // $dat['excel2']  = json_decode($page->excel2, true);

        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujiprasaranaPermohonan_template')
        ->with('page', $page)
        // ->with('dat', $dat)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

/* ------------------- pengujian sarana ----------------*/

    public function ujisaranaUmum()
    {

        $this->codepage = 'I060';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisaranaUmum_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisaranaInfo()
    {

        $this->codepage = 'I061';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisaranaInfo_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisaranaSOP()
    {

        $this->codepage = 'I062';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisaranaSOP_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisaranaJadwal()
    {

        $this->codepage = 'I063';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        $dat['key1'] = $this->getKey($page->excel1);
        $dat['excel1']  = json_decode($page->excel1, true);
        $dat['key2'] = $this->getKey($page->excel2);
        $dat['excel2']  = json_decode($page->excel2, true);
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisaranaJadwal_template')
        ->with('dat', $dat)
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisaranaLokasi()
    {

        $this->codepage = 'I064';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisaranaLokasi_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisaranaHasil()
    {

        $this->codepage = 'I065';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        $dat['key1'] = $this->getKey($page->excel1);
        $dat['excel1']  = json_decode($page->excel1, true);
        $dat['key2'] = $this->getKey($page->excel2);
        $dat['excel2']  = json_decode($page->excel2, true);
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisaranaHasil_template')
        ->with('dat', $dat)
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function ujisaranaPeralatan()
    {

        $this->codepage = 'I066';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $dataList = gallerymod::where('category', 3)->orderby('created_at', 'desc')->get();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.ujisaranaPeralatan_template')
        ->with('page', $page)
        ->with('dataList', $dataList)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function berita()
    {
        //
        $this->codepage = 'I07';
        $this->log_stat($this->codepage);
        if(\Auth::guard('admin')->user()){
          $berita = DB::table('news')
                     ->orderBy('publish_date', 'desc')
                     ->where('publish_date','<=', Carbon::now())
                     ->whereIn('status',['publish','approval','draft'])
                     ->paginate(15);
        }else{
          $berita = DB::table('news')
                     ->orderBy('publish_date', 'desc')
                     ->where('publish_date','<=', Carbon::now())
                     ->where('status','publish')
                     ->paginate(15);
        }

        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.berita_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('berita', $berita)
        ->with('codepage',$this->codepage);
    }

    public function detilberita($slug)
    {
        //
        $this->codepage = 'I07';
        $this->log_stat($this->codepage);
        if(\Auth::guard('admin')->user()){
          $news = Newsmod::where('slug', $slug)->first();
        }else{
          $news = Newsmod::where('slug', $slug)->where('status','publish')->first();
        }
        if(!$news){
          abort(404);
        }
        $ctr = (@$news->counter) ? $news->counter : 0;
        $ctr++;
        $news->counter = $ctr;
        $news->save();
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.detilberita_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('news', $news)
        ->with('codepage',$this->codepage);
    }
    public function wikiuji()
    {
        //
        $this->codepage = 'I12';
        $this->log_stat($this->codepage);
        if(\Auth::guard('admin')->user()){
          $berita = DB::table('wikiuji')
                     ->orderBy('publish_date', 'desc')
                     ->where('publish_date','<=', Carbon::now())
                     ->whereIn('status',['publish','approval','draft'])
                     ->paginate(15);
        }else{
          $berita = DB::table('wikiuji')
                     ->orderBy('publish_date', 'desc')
                     ->where('publish_date','<=', Carbon::now())
                     ->where('status','publish')
                     ->paginate(15);
        }

        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.wikiuji_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('berita', $berita)
        ->with('codepage',$this->codepage);
    }

    public function detilwikiuji($slug)
    {
        //
        $this->codepage = 'I12';
        $this->log_stat($this->codepage);
        if(\Auth::guard('admin')->user()){
          $news = WikiUjimod::where('slug', $slug)->first();
        }else{
          $news = WikiUjimod::where('slug', $slug)->where('status','publish')->first();
        }
        if(!$news){
          abort(404);
        }
        $ctr = (@$news->counter) ? $news->counter : 0;
        $ctr++;
        $news->counter = $ctr;
        $news->save();
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.detilwikiuji_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('news', $news)
        ->with('codepage',$this->codepage);
    }

    public function mitrakerja()
    {
        //
        $this->codepage = 'I11';
        $this->log_stat($this->codepage);
        $berita = DB::table('mitra')
                   ->orderBy('priority')
                   ->paginate(15);

        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.mitrakerja_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('berita', $berita)
        ->with('codepage',$this->codepage);
    }

    public function detilmitrakerja($slug)
    {
        //
        $this->codepage = 'I11';
        $this->log_stat($this->codepage);
        $news = Mitramod::where('slug', $slug)->first();
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.detilmitrakerja_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('news', $news)
        ->with('codepage',$this->codepage);
    }

    public function sendMessage(){
  		$redis = LRedis::connection();
  		$data = ['message' => Request::input('message'), 'user' => Request::input('user')];
      print_r($data);
  		$redis->publish('message', json_encode($data));
  		return response()->json([]);
  	}

    public function chat1(Request $request){
      $callback = array("error"=>0, "message"=>array(), "data"=>"");
      $admin = new Chatmod;
      $admin->subject   = Input::get('subject');
      $admin->nama   = Input::get('nama');
      $admin->email   = Input::get('email');
      $admin->starttime   = Carbon::now();
      $admin->status   = 0;
      $admin->appsessionid   = Input::get('sessionid');
      $admin->uniqid = uniqid();
      $admin->save();

      $callback['message'] = "Ok";

  		return json_encode($callback);
  	}

    public function chat2(Request $request){
      $callback = array("error"=>0, "message"=>array(), "data"=>"");
      $admin = new ChatDetailmod;
      $admin->message   = Input::get('msg');
      $admin->chatid   = Input::get('sessionid');
      $admin->uniqid = uniqid();
      $admin->userid = '';
      $admin->save();
      $callback['message'] = "Ok";

  		return json_encode($callback);
  	}

    public function chat3($sessid){
      // $dataChat = ChatDetailmod::where('chatid', $sessid)->where('status', 0)->get();
      $dataChat = ChatDetailmod::join('chat', 'chat_detail.chatid', '=', 'chat.appsessionid')
                  ->select('chat_detail.*', 'chat.nama')
                  ->where("chatid", $sessid)
                  ->where('chat_detail.status', 0)
                  ->orderby('chat_detail.created_at')->get();
      // print_r($dataChat);exit;

      $adminnya = AdminUsermod::get();

      foreach ($adminnya as $admin) {
        $listadmin[$admin->uniqid] = $admin;
      }

      $chatnya = '';
      foreach ($dataChat as $chat) {
        if ($chat->userid){
          $chatnya .= '<div class="djka-chat-msg">'.$chat->message.'</div>';
          // $chatnya .= '
          // <div class="direct-chat-msg right">
          //   <div class="direct-chat-info clearfix">
          //     <span class="direct-chat-name pull-right"><b>Admin</b></span>
          //     <span class="direct-chat-timestamp pull-left">'.$chat->created_at->format("d F Y H:i:s").'</span>
          //   </div>
          //   <div class="direct-chat-text text-danger">
          //     <p class="text-danger">'.$chat->message.'</p>
          //   </div>
          // </div>
          // ';
        } else{
          $chatnya .= '<div class="djka-chat-msg is-user">'.$chat->message.'</div>';
          // $chatnya .= '
          // <div class="direct-chat-msg">
          //   <div class="direct-chat-info clearfix">
          //     <span class="direct-chat-name pull-left">'.$chat->nama.'</span>
          //     <span class="direct-chat-timestamp pull-right">'.$chat->created_at->format("d F Y H:i:s").'</span>
          //   </div>
          //   <div class="direct-chat-text text-primary">
          //     <p class="text-primary">'.$chat->message.'</p>
          //   </div>
          // </div>
          // ';
        }
        // $chatnya .= '<p>'.$chat->message.'</p>';
        $data = ChatDetailmod::where('uniqid', $chat->uniqid)->first();
        $data->status = 1;
        $data->save();
      }

  		return $chatnya;
  	}

    public function kontak()
    {
        //
      //echo "abcde";exit;
        $this->codepage = 'I08';
        $this->log_stat($this->codepage);
      // $hariini = $this->indonesian_date (time() , 'l, j F  Y');
        //echo $hariini;exit;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        //print_r($page);exit;
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.kontak_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('codepage',$this->codepage);
    }

    public function cari(Request $request)
    {
        //
        $this->codepage = 'I09';
        $this->log_stat($this->codepage);
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();

        $rules = array(
            'keyword'  => 'required|min:3',
        );
        $validator = Validator::make(Input::all(), $rules);

        $keyword = Input::get("keyword");


        if ($validator->fails()) {
              // return Redirect::back()
              //     ->withErrors($validator)
              //     ->withInput();
              $pesan = "Kata kunci minimal 3 huruf";
              
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
              return view('web.search_template')
              ->with('page', $page)
              ->with('keyword', $keyword)
              ->with('pesan', $pesan)
              ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
              ->with('codepage',$this->codepage);

          }else {


            $hasilpage = DB::table('page')
                       ->where('title','like', '%'.$keyword.'%')
                       ->orwhere('highlight','like', '%'.$keyword.'%')
                       ->orwhere('content','like', '%'.$keyword.'%')
                       ->get();
             $hasilnews = DB::table('news')
                        ->where('title','like', '%'.$keyword.'%')
                        ->orwhere('lead','like', '%'.$keyword.'%')
                        ->orwhere('content','like', '%'.$keyword.'%')
                        ->get();
              $hasilevent = DB::table('event')
                         ->where('title','like', '%'.$keyword.'%')
                         ->orwhere('lead','like', '%'.$keyword.'%')
                         ->orwhere('content','like', '%'.$keyword.'%')
                         ->get();
          //  $hasilpage = DB::table('page')
          //             ->where('title','like', '%'.$keyword.'%')
          //             ->orwhere('highlight','like', '%'.$keyword.'%')
          //             ->orwhere('content','like', '%'.$keyword.'%')
          //             ->get();
            //print_r($hasil1);exit;
            
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
            return view('web.search_template')
            ->with('page', $page)
            ->with('keyword', $keyword)
            ->with('hasilpage', $hasilpage)
            ->with('hasilnews', $hasilnews)
            ->with('hasilevent', $hasilevent)
            ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
            ->with('codepage',$this->codepage);
          }
    }

    public function preview(Request $request){
      if($request->modul == "news")
      {
        $this->codepage = 'I07';
        $news = new \stdClass();
        $news->title = $request->judul;
        $news->lead = $request->lead;
        $news->publish_date = $request->tanggal_publish;
        $news->content = $request->konten;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.detilberita_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('news', $news)
        ->with('codepage',$this->codepage);
      }
      if($request->modul == "news")
      {
        $this->codepage = 'I09';
        $news = new \stdClass();
        $news->title = $request->judul;
        $news->lead = $request->lead;
        $news->publish_date = $request->tanggal_publish;
        $news->content = $request->konten;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.detilpengumuman_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('news', $news)
        ->with('codepage',$this->codepage);
      }
      if($request->modul == "wikiuji")
      {
        $this->codepage = 'I12';
        $news = new \stdClass();
        $news->title = $request->judul;
        $news->lead = $request->lead;
        $news->publish_date = $request->tanggal_publish;
        $news->content = $request->konten;
        $preferences = PreferencesMod::where('id', '1')->first();
        $page = Pagemod::where('codepage', $this->codepage)->first();
        
        $sidemenu = SideMenumod::orderBy('priority', 'asc')->where('status','active')->get();
        return view('web.detilwikiuji_template')
        ->with('page', $page)
        ->with('pref', $preferences)
        ->with('sidemenu', $sidemenu)
        ->with('news', $news)
        ->with('codepage',$this->codepage);
      }
    }
}
