<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Izinopsmod extends Model{
	protected $table 	= 'izinsaranaops';
	protected $fillable = ['uniqid','idcompany','is_complete','opsi','date_expired','idlinpel','freqreq','is_done','reason','perihal','tanggal','nomorsurat','typefile','realname','randomname'];
}