<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Izintmpmod extends Model{
	protected $table 	= 'tmpizin';
	protected $fillable = ['idcompany','typedoc','typefile','realname','randomname','subtypedoc'];
}