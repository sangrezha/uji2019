<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Linpeldocmod extends Model{
	
	protected $table 	= 'linpeldoc';
	protected $fillable = ['uniqdocument','idcompany','typefile','realname','randomname'];
	
}