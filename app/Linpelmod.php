<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Linpelmod extends Model{
	protected $table 	= 'lintaspelayanan';
	protected $fillable = ['lintas','via','frekuensi'];
}