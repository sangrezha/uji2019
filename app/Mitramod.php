<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Mitramod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'mitra';

    protected $fillable = [
        'title', 'picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
