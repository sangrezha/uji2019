<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Pagemod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'page';

    protected $fillable = [
        'title', 'content', 'codepage', 'picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
