<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Pagestatmod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'page_stat';

    protected $fillable = [
        'codepage', 'browser', 'os', 'jenis', 'ip', 'lokasi', 'devicename'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
