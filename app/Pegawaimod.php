<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Pegawaimod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'pegawai';

    protected $fillable = [
        'nama', 'nip', 'golongan', 'pangkat', 'jabatan', 'pendidikan', 'picture', 'staff'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
