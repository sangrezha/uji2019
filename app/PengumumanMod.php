<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class PengumumanMod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'pengumuman';

    protected $fillable = [
        'title', 'deskripsi','category'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];

}
