<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class PreferencesMod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'preferences';

    protected $fillable = [
        'title', 'meta_desc', 'meta_keyword', 'konten_head', 'konten_body_t', 'konten_body_b', 'footer'
    ];

    
}
