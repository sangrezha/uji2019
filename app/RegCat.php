<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class RegCat extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'izin_kategori';

    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
