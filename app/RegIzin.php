<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class RegIzin extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'izin_regulasi';

    protected $fillable = [
        'category', 'tahun', 'title', 'tentang'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];

    public function cat()
    {
        return $this->belongsTo('App\RegCat', 'id', 'category_id');
    }
}
