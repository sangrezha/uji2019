<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class SideMenumod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'sidemenu';

    protected $fillable = [
        'title', 'link','status','window','priority'
    ];
}
