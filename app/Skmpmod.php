<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Smkpmod extends Model{
	
	protected $table 	= 'smkp';
	protected $fillable = ['uniqdocument','idcompany','typefile','realname','randomname'];
	
}