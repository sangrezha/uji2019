<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Sopsaranamod extends Model{
	
	protected $table 	= 'sopsarana';
	protected $fillable = ['typesop','uniqdocument','idcompany','typefile','realname','randomname','keterangan'];
	
}