<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Studylayakmod extends Model{
	
	protected $table 	= 'studylayak';
	protected $fillable = ['typestudy','uniqdocument','idcompany','typefile','realname','randomname','keterangan'];
	
}