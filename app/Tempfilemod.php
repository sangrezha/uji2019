<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tempfilemod extends Model{
	
	protected $table 	= 'tempfile';
	protected $fillable = ['typedoc','realname','randomname','typefile'];
}