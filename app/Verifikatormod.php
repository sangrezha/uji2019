<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Verifikatormod extends Model{
	
	protected $table 	= 'verifikator';
	protected $fillable = ['uniqdocument','level','idverifikator','is_verify','is_reject','reason'];
	
}