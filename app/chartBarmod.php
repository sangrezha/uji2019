<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class chartBarmod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'chart_bar';

    protected $fillable = [
        'title', 'nilai', 'warna'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
}
