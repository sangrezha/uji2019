<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class gallerymod extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'gallery';

    protected $fillable = [
        'title', 'category', 'thumbnail', 'picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'uniqid',
    ];
  
}
