function sendout_error(msg)
{
    var template = '<div class="alert alert-warning alert-dismissable">';
    template = template +'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-warning"></i>Peringatan !</h4>'+msg+'</div>';
    return template;
}

function sendout_info(msg)
{
    var template = '<div class="alert alert-info alert-dismissable">';
    template = template +'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-info"></i>Informasi !</h4>'+msg+'</div>';
    return template;
}

function ValidateEmail(inputText)  
{  
	var mailformat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(inputText.match(mailformat)){ return true;}  
	else{return false;}
}