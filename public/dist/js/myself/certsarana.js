$(document).ready(function(){
	var iddoc = $("#iddocument").val();
	loaddata(iddoc);
	
	$("#berlaku").datepicker({
		todayHighlight : true
	});
	
	var ket = CKEDITOR.replace('keterangan');
	
	$("#izinpilih").change(function(){
		var pilih = $("#izinpilih").val();
		
		if(pilih == 1){
			$("#form-lintas").hide();
		} else if(pilih == 2){
			$("#form-lintas").hide();
		} else if(pilih == 3){
			$("#form-lintas").show();
		} else if(pilih == 4){
			$("#form-lintas").hide();
		} else{
			$("#form-lintas").hide();
		}
	});
	
	$("#klikbatal").click(function(){
		var filerand = $("#namefile").val();
		var cancelurl = $("#cancelurl").val();
		var fullcancel = cancelurl+"/"+filerand;
		$.get(fullcancel, function(response){
			$("#formdoc").show();
			$("#hasil").empty();
			$("#namefile").val('');
			$("#klikbatal").hide();
		});
	});
	
	$("#btsimpan").click(function(){
		$("#error_msg").empty();
		
		
		if($("#jenissarana").val() == ''){
			var htm = sendout_error('Jenis Sarana harus diisi');
			$("#error_msg").html(htm);
			$("#jenissarana").focus();
			return false;
		} else if($("#nocert").val() == ''){
			var htm = sendout_error('No Sertifikat Sarana harus diisi');
			$("#error_msg").html(htm);
			$("#nocert").focus();
			return false;
		} else if($("#nobadan").val() == ''){
			var htm = sendout_error('Nomor Badan harus diisi');
			$("#error_msg").html(htm);
			$("#nobadan").focus();
			return false;
		} else if($("#milik").val() == ''){
			var htm = sendout_error('Kepemilikan harus diisi');
			$("#error_msg").html(htm);
			$("#milik").focus();
			return false;
		} else if($("#jenisuji").val() == ''){
			var htm = sendout_error('Jenis Pengujian harus diisi');
			$("#error_msg").html(htm);
			$("#jenisuji").focus();
			return false;
		} else if($("#berlaku").val() == ''){
			var htm = sendout_error('Masa berlaku harus diisi');
			$("#error_msg").html(htm);
			$("#berlaku").focus();
			return false;
		} else{
			
			$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
				}
			});
			if($("#idcertsarana").val() == ''){
				var submit_url = $("#submiturl").val();
				var datacert = {
					jenissarana : $("#jenissarana").val(),
					nocert 		: $("#nocert").val(),
					nobadan 	: $("#nobadan").val(),
					milik 		: $("#milik").val(),
					jenisuji 	: $("#jenisuji").val(),
					berlaku 	: $("#berlaku").val(),
					keterangan 	: ket.getData(),
					iddocument 	: $("#iddocument").val()
				};
			} else{
				var submit_url = $("#submitediturl").val();
				var datacert = {
					jenissarana : $("#jenissarana").val(),
					nocert 		: $("#nocert").val(),
					nobadan 	: $("#nobadan").val(),
					milik 		: $("#milik").val(),
					jenisuji 	: $("#jenisuji").val(),
					berlaku 	: $("#berlaku").val(),
					keterangan 	: ket.getData(),
					id 			: $("#idcertsarana").val(),
				};
			}
			
			
			$.post(submit_url,{datasend: datacert},function(r){
				if(r.error == 1){
					var htm = sendout_error("File document belum diupload");
					$("#error_msg").html(htm);
					return false;
				} else{
					$("#uploadarsip")[0].reset();
					$("#formdoc").show();
					$("#hasil").empty();
					$("#namefile").val('');
					$("#klikbatal").hide();
					
					$("#jenissarana").val('');
					$("#nocert").val('');
					$("#nobadan").val('');
					$("#milik").val('');
					$("#jenisuji").val('');
					$("#berlaku").val('');
					$("#idcertsarana").val('');
					ket.setData('');
					loaddata($("#iddocument").val());
				}
			});
		}
	});
	
	$("#btclear").click(function(){
		$("#jenissarana").val('');
		$("#nocert").val('');
		$("#nobadan").val('');
		$("#milik").val('');
		$("#jenisuji").val('');
		$("#berlaku").val('');
		$("#idcertsarana").val('');
		ket.setData('');
		$("#klikbatal").click();
	});
});

function loaddata(iddoc)
{
	$("#table-view").find("tr:gt(1)").empty();
	var urlget = $("#urltable").val();
	$.get(urlget+'/'+iddoc, function(r){
		var data = r.data;
		
		if(data != 0){
			$("#view-sertifikasi").show();
			var view = Handlebars.compile($("#table-view tr").eq(1).html());
			data.forEach(function(raw){
				var dataview = {
					hb_jenissarana 	: raw.jenissarana,
					hb_nocert		: raw.nocert,
					hb_nobadan 		: raw.nobadan,
					hb_milik 		: raw.milik,
					hb_jenisuji		: raw.jenisuji,
					hb_berlaku		: raw.berlaku,
					hb_id 			: raw.id
				};
				
				$("#table-view tr:last").after("<tr>"+view(dataview)+"</tr>");
			});
		}
	});	
}

function subform()
{
	document.getElementById("btupload").click();
}

function uploaddocument()
{
	var userfile 	= $('#userfile').val();
	var urlpost 	= $("#urlupload").val();
	
	if(userfile){
		$.ajaxSetup({
			headers : {
				'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
			}
		});
		$("#error_msg").empty();
		$('#uploadarsip').ajaxForm({
            url: urlpost,
            type: 'post',
            beforeSend: function() {
                $("#progress").css("width","0%");
            },
            uploadProgress: function(event, position, total, percentComplete) {
                $("#prosesform").show();
				$("#progress").css('width',percentComplete+'%');
            },
            beforeSubmit: function() {
                $("#progress").css("width","0%");
            },
            complete: function(xhr) {
                $("#prosesform").hide();
				$("#progress").css('width','0%');
            },
            success: function(result) {
				var msg = result.error;
                if(msg == 1){
					alert("File kosong");
					$("#uploadarsip")[0].reset();
					return false;
				} else if(msg == 2){
					alert("File tidak valid");
					$("#uploadarsip")[0].reset();
					return false;
				} else if(msg == 3){
					alert("File bukan PDF atau Image JPEG");
					$("#uploadarsip")[0].reset();
					return false;
				} else if(msg == 4){
					alert("File melebihi 1MB");
					$("#uploadarsip")[0].reset();
					return false;
				} else{
					$("#error_msg").empty();
					var valname = result.random;
					var typedoc = result.typedoc;
					
					$("#formdoc").hide();
					$("#klikbatal").show();
					$("#hasil").html(result.real);
					$("#namefile").val(result.random);
					
					$("#uploadarsip")[0].reset();
					$("#progress").css("width","0%");

				}
            },
        });
	}
}

function editing(id)
{
	var urledit = $("#urldetail").val();
	$("#idcertsarana").val(id);
	$.get(urledit+'/'+id,function(response){
		var dt = response.data;
		
		$("#jenissarana").val(dt.jenissarana);
		$("#nocert").val(dt.nomorcert);
		$("#nobadan").val(dt.nomorbadan);
		$("#milik").val(dt.milik);
		$("#jenisuji").val(dt.jenisuji);
		$("#berlaku").val(dt.berlaku);
		CKEDITOR.instances['keterangan'].setData(dt.keterangan);
		
		$('html,body').scrollTop(0);
	});
}

function removedoc(id)
{
	var url = $("#urlremove").val();
	
	$.get(url+'/'+id, function(r){
		var iddocument = $("#iddocument").val();
		loaddata(iddocument);
	});
}