$(document).ready(function(){
	var remove_url = $("#removeurl").val();
	$("#tanggalsurat").datepicker({
		todayHighlight : true
	});
	
	$("#linpel").change(function(){
		var value = $("#linpel").val();
		
		if(value == ""){
			$("#linpel").val('0');
			$("#form-kapasitas-lintas").hide();
			$("#kapasitas").val('');
			$(".form-upload-baru").hide();
			$("#form-kapasitas-request").hide();
			
			var urllinpel = $("#urllinpel").val();
			$.get(urllinpel, function(r){
				window.open(r.data);
			});
			
			/*alert("Dibutuhkan Izin Lintas Pelayanan");
			*/
			return false;
		} else if(value == 0){
			$("#form-kapasitas-lintas").hide();
			$("#kapasitas").val('');
			$(".form-upload-baru").hide();
			$("#form-kapasitas-request").hide();
		} else{
			$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
				}
			});
			var getfrek = $("#frekuensi").val();
			$.post(getfrek,{idne:value}, function(response){
				$("#form-kapasitas-lintas").show();
				$("#kapasitas").val(response.frek);
				$(".form-upload-baru").show();
				$("#form-kapasitas-request").show();
			});
		}
	});
	
	$("#izinpilih").change(function(){
		var pilih = $("#izinpilih").val();
		
		if(pilih == 1){
			$(".form-upload-baru").hide();
			$("#form-lintas").hide();
			$(".form-frekuensi-baru").hide();
		} else if(pilih == 2){
			$(".form-upload-baru").hide();
			$("#form-lintas").hide();
			$(".form-frekuensi-baru").show();
		} else if(pilih == 3){
			$(".form-upload-baru").show();
			$("#form-lintas").show();
			$(".form-frekuensi-baru").hide();
		} else if(pilih == 4){
			$(".form-upload-baru").hide();
			$("#form-lintas").hide();
			$(".form-frekuensi-baru").hide();
		} else{
			$(".form-upload-baru").hide();
			$("#form-lintas").hide();
			$(".form-frekuensi-baru").show();
		}
	});
	
	$("#klikbatal1").click(function(){
		var filerand = $("#namefile1").val();
		
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc1").show();
			$("#hasil1").empty();
			$("#namefile1").val('');
			$("#klikbatal1").hide();
		});
	});
	
	$("#klikbatal2").click(function(){
		var filerand = $("#namefile2").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc2").show();
			$("#hasil2").empty();
			$("#namefile2").val('');
			$("#klikbatal2").hide();
		});
	});
	
	$("#klikbatal3").click(function(){
		var filerand = $("#namefile3").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc3").show();
			$("#hasil3").empty();
			$("#namefile3").val('');
			$("#klikbatal3").hide();
			
		});
	});
	
	$("#klikbatal4").click(function(){
		var filerand = $("#namefile4").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc4").show();
			$("#hasil4").empty();
			$("#namefile4").val('');
			$("#klikbatal4").hide();
		});
	});
	
	$("#klikbatal5").click(function(){
		var filerand = $("#namefile5").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc5").show();
			$("#hasil5").empty();
			$("#namefile5").val('');
			$("#klikbatal5").hide();
		});
	});
	
	$("#klikbatal6").click(function(){
		var filerand = $("#namefile6").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc6").show();
			$("#hasil6").empty();
			$("#namefile6").val('');
			$("#klikbatal6").hide();
		});
	});
	
	$("#klikbatal7").click(function(){
		var filerand = $("#namefile7").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc7").show();
			$("#hasil7").empty();
			$("#namefile7").val('');
			$("#klikbatal7").hide();
		});
	});
	
	$("#klikbatal8").click(function(){
		var filerand = $("#namefile8").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc8").show();
			$("#hasil8").empty();
			$("#namefile8").val('');
			$("#klikbatal8").hide();
		});
	});
	
	$("#klikbatal9").click(function(){
		var filerand = $("#namefile9").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc9").show();
			$("#hasil9").empty();
			$("#namefile9").val('');
			$("#klikbatal9").hide();
		});
	});
	
	$("#klikbatal10").click(function(){
		var filerand = $("#namefile10").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc10").show();
			$("#hasil10").empty();
			$("#namefile10").val('');
			$("#klikbatal10").hide();
		});
	});
	
	$("#klikbatal11").click(function(){
		var filerand = $("#namefile11").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc11").show();
			$("#hasil11").empty();
			$("#namefile11").val('');
			$("#klikbatal11").hide();
		});
	});
	
	$("#klikbatal12").click(function(){
		var filerand = $("#namefile12").val();
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc12").show();
			$("#hasil12").empty();
			$("#namefile12").val('');
			$("#klikbatal12").hide();
		});
	});
	
	$("#btsimpan").click(function(){
		$("#error_msg").empty();
		var pilih = $("#izinpilih").val();
		var submit_url = $("#urlsubmit").val();
		
		$.ajaxSetup({
			headers : {
				'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		if(pilih == 0){
			var htm = sendout_error('Perizinan belum dipilih');
			$("#error_msg").html(htm);
			$("#izinpilih").focus();
			return false;
		} else if($("#perihal").val() == ''){
			var htm = sendout_error('Perihal perizinan harus diisi');
			$("#error_msg").html(htm);
			$("#perihal").focus();
			return false;
		} else if(pilih == 3){
			
			if($("#kapasitasreq").val() == ''){
				var htm = sendout_error("Frekuensi yang diminta belum diisi");
				$("#kapasitasreq").focus();
				$("#error_msg").html(htm);
			} else{
				
				
				var data = {
					opsi_izin 	: pilih,
					linpel 		: $("#linpel").val(),
					minta 		: $("#kapasitasreq").val(),
					perihal 	: $("#perihal").val()
				};
				
				$.post(submit_url,{datasend : data}, function(response){
					if(response.error == 1){
						var htm = sendout_error("Dokumen belum lengkap diupload");
						$("#error_msg").html(htm);
					} else if(response.error == 2){
						var htm = sendout_error("Perihal sudah ada");
						$("#error_msg").html(htm);
						$("#perihal").focus();
					} else{
						window.location.replace(response.url);
					}
				});
			}
			
		} else if(pilih == 2){
			
			var data = {
				opsi_izin 	: pilih,
				perihal		: $("#perihal").val()
			}
			
			$.post(submit_url,{datasend: data}, function(response){
				if(response.error == 1){
					var htm = sendout_error("Dokumen belum diupload");
					$("#error_msg").html(htm);
				} else if(response.error == 2){
					var htm = sendout_error("Perihal sudah ada");
					$("#error_msg").html(htm);
					$("#perihal").focus();
				} else{
					window.location.replace(response.url);
				}
			});
			
		} else if(pilih == 1){
			var urllinpel = $("#urllinpel").val();
			$.get(urllinpel, function(r){
				window.open(r.data);
			});
		}
	});
});

function subform(arsip)
{
	document.getElementById("btupload"+arsip).click();
}

function uploadmanual(arsip)
{
	var userfile 	= $('#userfile'+arsip).val();
	var urlpost 	= $("#urlupload").val();
	
	if(userfile){
		$.ajaxSetup({
			headers : {
				'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
			}
		});
		$("#error_msg").empty();
		$('#uploadarsip'+arsip).ajaxForm({
            url: urlpost,
            type: 'post',
            beforeSend: function() {
                $("#progress").css("width","0%");
            },
            uploadProgress: function(event, position, total, percentComplete) {
                $("#prosesform").show();
				$("#progress").css('width',percentComplete+'%');
            },
            beforeSubmit: function() {
                $("#progress").css("width","0%");
            },
            complete: function(xhr) {
                $("#prosesform").hide();
				$("#progress").css('width','0%');
            },
            success: function(result) {
				var msg = result.error;
                if(msg == 1){
					alert("File kosong");
					$("#uploadarsip"+arsip)[0].reset();
					return false;
				} else if(msg == 2){
					alert("File tidak valid");
					$("#uploadarsip"+arsip)[0].reset();
					return false;
				} else if(msg == 3){
					alert("File bukan PDF atau Image JPEG");
					$("#uploadarsip"+arsip)[0].reset();
					return false;
				} else if(msg == 4){
					alert("File melebihi 15MB");
					$("#uploadarsip"+arsip)[0].reset();
					return false;
				} else{
					$("#error_msg").empty();
					var valname = result.random;
					var typedoc = result.typedoc;
					
					$("#formdoc"+arsip).hide();
					$("#klikbatal"+arsip).show();
					$("#hasil"+arsip).html(result.real);
					$("#namefile"+arsip).val(result.random);
					
					$("#uploadarsip"+arsip)[0].reset();
					$("#progress").css("width","0%");

				}
            },
        });
	}
}