$(document).ready(function(){
	var remove_url = $("#removeurl").val();
	$("#tanggalsurat").datepicker({
		todayHighlight : true
	});
	
	$("#linpel").change(function(){
		var value = $("#linpel").val();
		
		if(value == ""){
			alert("Dibutuhkan Izin Lintas Pelayanan");
			$("#linpel").val('0');
			$("#form-kapasitas-request").hide();
			return false;
		} else if(value == 0){
			$("#form-kapasitas-request").hide();
		} else{
			$("#form-kapasitas-request").show();
		}
	});
	
	$("#izinpilih").change(function(){
		var pilih = $("#izinpilih").val();
		
		if(pilih == 1){
			$("#form-lintas").hide();
		} else if(pilih == 2){
			$("#form-lintas").hide();
		} else if(pilih == 3){
			$("#form-lintas").show();
		} else if(pilih == 4){
			$("#form-lintas").hide();
		} else{
			$("#form-lintas").hide();
		}
	});
	
	$("#klikbatal").click(function(){
		var filerand = $("#namefile").val();
		
		$.get(remove_url+"/"+filerand, function(response){
			$("#formdoc").show();
			$("#hasil").empty();
			$("#namefile").val('');
			$("#klikbatal").hide();
		});
	});
	
	$("#btsimpan").click(function(){
		$("#error_msg").empty();
		var pilih = $("#izinpilih").val();
		var submit_url = $("#urlsubmit").val();
		
		if(pilih == 0){
			var htm = sendout_error('Perizinan belum dipilih');
			$("#error_msg").html(htm);
			$("#izinpilih").focus();
			return false;
		} else if($("#perihal").val() == ''){
			var htm = sendout_error('Perihal perizinan harus diisi');
			$("#error_msg").html(htm);
			$("#perihal").focus();
			return false;
		} else if(pilih == 3){
			var request 	= $("#kapasitasreq").val();
			var capacity 	= $("#kapasitas").val();
			if(parseInt(request) > parseInt(capacity)){
				var htm = sendout_info("Permintaan melebihi kapasitas");
				$("#error_msg").html(htm);
			} else{
				$.ajaxSetup({
					headers : {
						'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				var data = {
					opsi_izin 	: pilih,
					linpel 		: $("#linpel").val(),
					minta 		: $("#kapasitasreq").val(),
					perihal 	: $("#perihal").val(),
					tanggal 	: $("#tanggalsurat").val(),
					surat 		: $("#surat").val()
					
				};
				
				$.post(submit_url,{datasend : data}, function(response){
					if(response.error == 1){
						var htm = sendout_error("Surat belum diupload");
						$("#error_msg").html(htm);
					} else if(response.error == 2){
						var htm = sendout_error("Perihal sudah ada");
						$("#error_msg").html(htm);
						$("#perihal").focus();
						
					} else{
						window.location.replace(response.url);
					}
				});
			}
			
		} else if(pilih == 1){
			var urllinpel = $("#urllinpel").val();
			$.get(urllinpel, function(r){
				window.open(r.data);
			});
		}
	});
});

function subform()
{
	document.getElementById("btupload").click();
}

function uploaddocument()
{
	var userfile 	= $('#userfile').val();
	var urlpost 	= $("#urlupload").val();
	
	if(userfile){
		$.ajaxSetup({
			headers : {
				'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
			}
		});
		$("#error_msg").empty();
		$('#uploadarsip').ajaxForm({
            url: urlpost,
            type: 'post',
            beforeSend: function() {
                $("#progress").css("width","0%");
            },
            uploadProgress: function(event, position, total, percentComplete) {
                $("#prosesform").show();
				$("#progress").css('width',percentComplete+'%');
            },
            beforeSubmit: function() {
                $("#progress").css("width","0%");
            },
            complete: function(xhr) {
                $("#prosesform").hide();
				$("#progress").css('width','0%');
            },
            success: function(result) {
				var msg = result.error;
                if(msg == 1){
					alert("File kosong");
					$("#uploadarsip")[0].reset();
					return false;
				} else if(msg == 2){
					alert("File tidak valid");
					$("#uploadarsip")[0].reset();
					return false;
				} else if(msg == 3){
					alert("File bukan PDF atau Image JPEG");
					$("#uploadarsip")[0].reset();
					return false;
				} else if(msg == 4){
					alert("File melebihi 1MB");
					$("#uploadarsip")[0].reset();
					return false;
				} else{
					$("#error_msg").empty();
					var valname = result.random;
					var typedoc = result.typedoc;
					
					$("#formdoc").hide();
					$("#klikbatal").show();
					$("#hasil").html(result.real);
					$("#namefile").val(result.random);
					
					$("#uploadarsip")[0].reset();
					$("#progress").css("width","0%");

				}
            },
        });
	}
}