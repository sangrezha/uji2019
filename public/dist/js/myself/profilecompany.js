$(document).ready(function(){
	loadprofile();
	loadizin();
	
	$("#btn-edit").click(function(){
		var url = $("#url-doc-ops").val();
		var document = $("#docselect").val();
		var iddoc = $("#id-edit").val();
		
		if(document == 1){
			var tofrom = url+'/sertifikatsarana/'+iddoc;
			window.location.replace(tofrom);
		}
	});
	
	$("#editcomp").click(function(){
		var url = $("#urlcompany").val();
		
		$.get(url,function(r){
			$("#edalamat").val(r.alamat);
			$("#edpemimpin").val(r.pemimpin);
		});
	});
	
	$("#btn-upcomp").click(function(){
		if($("#edalamat").val() == ''){
			var htm = sendout_error("Alamat harus diisi");
			$("#error_msg").html(htm);
			$("#edalamat").focus();
			$("#modalcompany").modal('hide');
			return false;
		} else if($("#edpemimpin").val() == ''){
			var htm = sendout_error("Nama Pemimpin Perusahaan harus diisi");
			$("#error_msg").html(htm);
			$("#edpemimpin").focus();
			$("#modalcompany").modal('hide');
			return false;
		} else{
			
			$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
				}
			});
			
			var datacomp = {
				alamat 	: $("#edalamat").val(),
				pemimpin: $("#edpemimpin").val()
			};
			var urlupdate = $("#urlupdater").val();
			$("#modalcompany").modal('hide');
			$.post(urlupdate,{datasend: datacomp}, function(response){
				$("#alamat").val(response.alamat);
				$("#pemimpin").val(response.pemimpin);
				$("#error_msg").empty();
			});
			
		}
	});
});

function loadprofile()
{
	$("#table-arsip tr:gt(1)").empty();
	$("#table-arsip").hide();
	var urlprofile = $("#urlprofile").val();
	$.get(urlprofile,function(rprofile){
		var data = rprofile.data;
		
		if(data != 0){
			$("#company").val(data.company);
			$("#alamat").val(data.alamat);
			$("#pemimpin").val(data.pemimpin);
			$("#npwp").val(data.npwp);
			
			var dokumen = rprofile.doc;
			
			if(dokumen != 0){
				$("#table-arsip").show();
				var view = Handlebars.compile($("#table-arsip tr").eq(1).html());
				dokumen.forEach(function(raw){
					var data_arsip = {
						hb_no 		: raw.no,
						hb_arsip 	: raw.arsip,
						hb_id 		: raw.id,
						hb_url		: raw.url
					};
					
					$("#table-arsip tr:last").after("<tr>"+view(data_arsip)+"</tr>");
				});
			}
		}
	});
}

function loadizin()
{
	$("#table-monitor tr:gt(1)").empty();
	$("#table-monitor").hide();
	var urlpermit = $("#urlizin").val();
	
	$.get(urlpermit, function(rpermit){
		var data = rpermit.data;
		
		if(!data == 0){
			$("#table-monitor").show();
			var view = Handlebars.compile($("#table-monitor tr").eq(1).html());
			data.forEach(function(raw){
				var dataizin = {
					hbi_no 		: raw.no,
					hbi_izin	: raw.izin,
					hbi_jenis	: raw.jenis,
					hbi_status 	: raw.status,
					hbi_perihal : raw.perihal,
					hbi_iddoc	: raw.iddoc
				};
				$("#table-monitor tr:last").after("<tr>"+view(dataizin)+"</tr>");
			});
		}
	});
}

function editdocument(iddoc,perihal)
{
	
	var ques = confirm("Dokumen perihal "+perihal+" akan diedit ?");
	if(ques == true){
		$("#modaledit").modal('show');
		$("#id-edit").val(iddoc);
		$("#perihal").val(perihal);
	}
}