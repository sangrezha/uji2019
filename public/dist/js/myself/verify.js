$(document).ready(function(){
	loaddata();
});

function loaddata()
{
	$("#table-verify").find("tr:gt(1)").empty();
	var urlget = $("#urltable").val();
	$.get(urlget, function(r){
		var data = r.data;
		
		if(data != 0){
			
			var view = Handlebars.compile($("#table-verify tr").eq(1).html());
			data.forEach(function(raw){
				var dataview = {
					hbno 		: raw.no,
					hbnama		: raw.nama,
					hbbu 		: raw.bu,
					hbmail 		: raw.email,
					hbid 		: raw.id
				};
				
				$("#table-verify tr:last").after("<tr>"+view(dataview)+"</tr>");
			});
		}
	});	
}

function detail(id,nama)
{
	var url = $("#urldetail").val();
	window.location.replace(url+"/"+id);
}

function accepted(id,nama)
{
	var ff = confirm("Akan mengaktifkan "+nama+" ?");
	if(ff == true){
		var url = $("#urlterima").val();
		$.get(url+"/"+id, function(){
			loaddata();
		})
	}
}

function rejected(id,nama)
{
	var ff = confirm("Akan menghapus "+nama+" ?");
	if(ff == true){
		var url = $("#urltolak").val();
		$.get(url+"/"+id, function(){
			loaddata();
		})
	}
}