'use strict';
(function(){
var totalCurves = document.getElementsByClassName("curve").length;
var loadedCurves = 0;
var generateCurvedImage = function(img){
    img.style.display="none";
    var canvas = img.canvas;
    var parent = img.parentElement;
    parent.style['position']='relative';
    var paddingTop = parseInt(getComputedStyle(parent,null).paddingTop);
    var paddingBottom = parseInt(getComputedStyle(parent,null).paddingBottom); 
    var imageWidth = parent.clientWidth;
    var imageHeight = parent.clientHeight;
    if(img.src!==""){
        imageWidth=img.width;
        imageHeight=img.height;
    }

    var calculatedHeight = parent.clientHeight / parent.clientWidth * imageWidth;
    var hoffset = imageWidth/4;
    var typeTop = img.dataset.top;
    var typeBottom = img.dataset.bottom;
    var fillColor = img.dataset.fill || "magenta";
    var ctx = canvas.getContext("2d");
    canvas.width=imageWidth;
    canvas.height=calculatedHeight;

    var left = 0;
    var right = imageWidth;
    var hcenter = canvas.width/2;
    var top = 0;
    var bottom = calculatedHeight;    
    var topStart = 0;     

    var calculatedPaddingTop = paddingTop * (imageWidth/parent.clientWidth);
    var calculatedPaddingBottom = paddingBottom * (imageWidth/parent.clientWidth);

    ctx.beginPath();
    //Create top curve
    if(typeTop==="emboss"){
        topStart=top;
        ctx.moveTo(left,top);
        ctx.quadraticCurveTo(left+hoffset,calculatedPaddingTop, hcenter,calculatedPaddingTop);
        ctx.quadraticCurveTo(right-hoffset,calculatedPaddingTop, right,top);
    }
    else if(typeTop==="bevel"){
        topStart=calculatedPaddingTop;
        ctx.moveTo(left,top+calculatedPaddingTop);
        ctx.quadraticCurveTo(left+hoffset,top, hcenter,top);
        ctx.quadraticCurveTo(right-hoffset,top, right,calculatedPaddingTop);   
    }
    else{
        topStart=top;
        ctx.moveTo(left,top);
        ctx.lineTo(right,top)
    }
    
    //Create bottom curve
    if(typeBottom==="emboss"){
        ctx.lineTo(right,bottom);
        ctx.quadraticCurveTo(right-hoffset,bottom-calculatedPaddingBottom,hcenter,bottom-calculatedPaddingBottom)
        ctx.quadraticCurveTo(left+hoffset,bottom-calculatedPaddingBottom,left,bottom)
    }
    else if(typeBottom==="bevel"){
        ctx.lineTo(right,bottom-calculatedPaddingBottom);
        ctx.quadraticCurveTo(right-hoffset,bottom,hcenter,bottom)
        ctx.quadraticCurveTo(left+hoffset,bottom,left,bottom-calculatedPaddingBottom)            
    }
    else{
         ctx.lineTo(right,bottom)
         ctx.lineTo(left,bottom)
    }
    
    ctx.lineTo(left,topStart); //Back to start
    ctx.fillStyle = fillColor;
    ctx.fill();
    if(img.src!==""){
        ctx.clip();
        ctx.drawImage(img, 0, 0);        
    }
}
var createCanvases = function(){
    var images = document.getElementsByClassName("curve");
    var i =0;
    var img;
    for(i=0;i<images.length;i++){
        img = images[i];
        img.canvas = document.createElement("CANVAS");  
        img.canvas.className = "curve-canvas";
        img.parentNode.insertBefore(img.canvas, img);        
    }
}

var curveRedrawTimeout = null;
var redrawCurves = function(){
    var images = document.getElementsByClassName("curve");
    var i =0;
    var img;    
    for(i=0;i<images.length;i++){
        img = images[i];
        if(img.complete){
            generateCurvedImage(img);
        }              
    }
    curveRedrawTimeout = window.setTimeout(function(){
        redrawCurves();
    },400);
}

createCanvases();
redrawCurves();

var activateDropdown = function(){
    var menu = $('[data-djka-menu]');
    if(menu.data('is-activated')) return;
    menu.data('is-activated',true);

    menu.find('ul').each(function(i){
        var ulSub = $(this);
        var a = $(this).prev();
        //Add caret for menu who has children
        a.append('<i class="fa fa-caret-down"></i>');

        //Menu item with dropdown should not go anywhere
        a.on('click',function(e){
            e.preventDefault();
            menu.find('.show-sub').removeClass('show-sub');
            a.parent().addClass('show-sub');
            e.stopPropagation();
        })
    });

    
}

var activateMenu = function(){
    var toggle = $('[data-djka-menutoggle]');
    var menuArea = $('[data-djka-menu-area]');
    toggle.on('click',function(e){
        e.preventDefault();
        menuArea.toggleClass('is-opened');
    });
}

var activateSearchbox = function(){
    var box = $('[data-djka-searchbox]');
    var toggle = $('[data-djka-searchbox-toggle]');
    var form = $('[data-djka-searchbox-form]');
    var field = form.find('.djka-searchbox-field');
    toggle.on('click',function(e){
        e.preventDefault();
        e.stopPropagation();
        box.addClass('show-form');
        field.focus();
    });
    box.on('click',function(e){
        e.stopPropagation();
    });
}


activateDropdown();
activateMenu();
activateSearchbox();

//Click on body cancels toggle
$('body').on('click',function(){
    $('[data-djka-menu]').find('.show-sub').removeClass('show-sub');
    $('[data-djka-searchbox]').removeClass('show-form');
    $('[data-djka-floating-menu]').removeClass('is-opened');
});

// RESPONSIVE SLIDES FOR HOME HERO
if( $("[data-djka-home-slides]").responsiveSlides ){
    $("[data-djka-home-slides]").responsiveSlides({
        auto:true,
        timeout:5000,
        namespace:'homeslides'
    });
}

//OWL CAROUSEL
if($("[data-carousel-1]").owlCarousel){
    $("[data-carousel-1]").owlCarousel({
        responsiveClass:true,
        margin:20,
        autoplay:false,
        responsive:{
            0:{items:1, nav:true },
            320:{items:1, nav:true },
            600:{items:2, nav:true },
            800:{items:3, nav:true, loop:true },
            1400:{items:5, nav:true, loop:false }
        }
    });
}

//OWL CAROUSEL/*
if($("[data-djka-partner-carousel]").owlCarousel){
    $("[data-djka-partner-carousel]").owlCarousel({
        responsiveClass:true,
        margin:20,
        autoplay:false,
        responsive:{
            0:{items:1, nav:true },
            320:{items:2, nav:true },
            600:{items:3, nav:true },
            800:{items:4, nav:true, loop:true },
            1400:{items:5, nav:true, loop:false }
        }
    });
}

//TOGGLE FUNKY
$('[data-djka-funky-menu').each(function(i){
    var menu = $(this);
    var toggle = $(this).find('[data-djka-funky-toggle]');
    toggle.on('click',function(e){
        e.preventDefault();
        menu.toggleClass('is-opened');
    });
});

//RESTRUCTURE THE CAROUSEL
if($("[data-carousel-pengumuman]").owlCarousel){
    $("[data-carousel-pengumuman]").owlCarousel({
        responsiveClass:true,
        margin:20,
        autoplay:false,
        responsive:{
            0:{items:1, nav:true },
            320:{items:1, nav:true },
            600:{items:1, nav:true },
            800:{items:1, nav:true },
            1400:{items:1, nav:true }
        }
    });
}

var activateFloatingMenu = function(){
    var menu = $('[data-djka-floating-menu]');
    var toggle = $('[data-djka-floating-menu-toggle] span');
    var close = $('[data-djka-floating-menu-toggle] i');

    menu.on('click',function(e){
        e.stopPropagation();
    });

    close.on('click',function(){
        menu.removeClass('is-opened')
    });

    toggle.on('click',function(){
        menu.toggleClass('is-opened')
    });
}
activateFloatingMenu();

//CARD 2 EXPAND COLLAPSE
$('[data-djka-card2]').each(function(i){
    var card = $(this);
    card.on('click',function(e){
        e.preventDefault();
        card.siblings().removeClass('is-active')
        card.toggleClass('is-active');
    });
});

var activateChatBox = function(){
    var chatbox = $('[data-djka-chat]');
    var toggle = $('[data-djka-chat-toggle]');
    var close = $('[data-djka-chat-close]');

    toggle.on('click',function(e){
        e.preventDefault();
        chatbox.addClass('is-opened');
    })
    close.on('click',function(e){
        e.preventDefault();
        chatbox.removeClass('is-opened');
    })
}
activateChatBox();


var activateStickyMenu = function(){
    var offsetHeight = -0;
    var doc = $(document);
    var yScroll;
    var menu = $('[data-djka-floating-menu]');

    if(menu.length<1) return;

    var parent = menu.parent('div');

    if(parent.length<1) return;

    doc.on('scroll',function(){
        if(doc.width() < 1000) return; //Only for screen > 1000px
        yScroll = doc.scrollTop();

        if( yScroll>parent.offset().top + offsetHeight){
            menu.addClass('is-floating')
        }
        else{
            menu.removeClass('is-floating')
        }
    });
}



})();
