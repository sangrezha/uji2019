<p>Dear {{ $name }},</p>
<br><br>
<p>Kami menerima request untuk mereset password anda. Password anda telah sukses kami ganti dan bisa dilihat dibawah ini.</p>
<p>Sebagai catatan, simpan password dibawah ini dan segera hapus email ini untuk alasan keamanan. Jika dirasa masih error, segera hubungi administator kami.</p><br><br>
<p>Password baru anda: {{ $password_baru }}</p>
<br><br>
<p>Hormat Kami,</p>
<p>Tim Support Balai Pengujian Perkeretaapian</p>
