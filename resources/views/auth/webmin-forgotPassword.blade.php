<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  @include('webmin.head')
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition login-page">
<div class="wrapper">

  <div class="login-box">
    <div class="login-logo">
      <a href="{{ route('admin.login') }}"><img src="{{asset('/public/images/logo-login.png')}}" /></a>
    </div>
    @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Forgot Password</p>

      <form action="{{ route('admin.forgot.submit') }}" method="post">
        {{ csrf_field() }}
        <div class="form-group has-feedback {{ ($errors->has('email')) ? 'has-error' : '' }}">
          <input type="text" name="email" class="form-control" placeholder="Email terdaftar">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          @if ($errors->has('email'))
                  <span class="help-block">{{ $errors->first('email') }}</span>
          @endif
        </div>
        <div class="form-group has-feedback {{ ($errors->has('captcha')) ? 'has-error' : '' }}">
          {!! Captcha::img('flat') !!}  <input type="captcha" name="captcha" class="form-control" placeholder="Captcha Code">
          @if ($errors->has('captcha'))
                  <span class="help-block">{{ $errors->first('captcha') }}</span>
          @endif
        </div>
        <div class="row">
          <div class="col-xs-8">
          <div class="checkbox icheck">
            <a href="{{ route('admin.login')}}">Kembali ke halaman login</a><br>
          </div>
        </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
          </div>

          <!-- /.col -->
        </div>
      </form>

    </div>
  </div>
    <!-- /.login-box-body -->
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset("/public/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset("/public/bootstrap/js/bootstrap.min.js") }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset("/public/dist/js/app.min.js") }}"></script>

<script src="{{ asset("/public/plugins/fastclick/fastclick.min.js") }}"></script>

<script src="{{ asset("/public/plugins/slimscroll/slimscroll.min.js") }}"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>
