<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Webmin DIREKTORAT JENDERAL PERKERETAAPIAN | Kementrian Perhubungan Republik Indonesia</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('/public/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/public/dist/css/AdminLTE.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('/public/plugins/iCheck/square/blue.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('/public/assets/mystyle.css') }}">
  <link rel="shortcut icon" href="{{ asset('/public/assets/images/icon_logo.png')}}"/>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<style type="text/css">
	.member_kiri { width:755px; height:695px; background:url({{ asset('/public/assets/images/member_kiri_img.jpg') }}); float:left; position:relative;}
	.member_login { width:508px; height:695px; background:url({{ asset('/public/assets/images/member_kanan.jpg') }} ); float:left; position:relative;}
	.kotak-form { width:359px; height:338px; margin-top:107px; margin-left:30px; float:left; position:relative;}
	.kotak-form #username { width:290px; height:38px; margin-top:7px; border:none; }
	.kotak-form #password { width:290px; height:38px; margin-top:25px; border:none; }
	.kotak-form #captcha { width:160px; height:40px; margin-top:55px; margin-left:160px; border:none;}
	.kotak-form #button { width:206px; height:34px; background:url({{asset('/public/assets/images/login_member.jpg')}}); margin-top:100px; margin-left:-10px; border:none; }
	.bawa_button { width:226px; height:34px; margin-top:-50px; margin-left:20px; position:relative;}
	#wrapper{ width: 100%; margin-left: 20%;}
</style>

<body>
	<div id="wrapper">
		<div class="member_kiri" ></div>
		<div class="member_login">
			<form class="kotak-form" action="{{ route('admin.login.submit') }}" method="post">
				{{ csrf_field() }}

				<input id="username" name="username" type="text" placeholder="Username" value="{{ old('username') }}">

				<input id="password" name="password" type="password" placeholder="Password">

				<input id="captcha" name="captcha" type="text" placeholder="TYPE THE TEXT HERE">
				<button type="submit" style="display: none;" class="btn btn-primary btn-block btn-flat" id="btsubmit">Log In</button>
			</form>
			<a href="javascript:void(0)" onclick="submit()"><img src="{{asset('public/assets/images/login_member.jpg')}}" width="206" height="34" alt="login" class="bawa_button"></a>
		</div>
	</div>

@if ($errors->has('username'))
				<span class="help-block">
					<strong>{{ $errors->first('username') }}</strong>
				</span>
@endif

@if ($errors->has('password'))
				<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
@endif
<script type="text/javascript" src="{{ asset('public/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
function submit()
{
	document.getElementById("btsubmit").click();
}
</script>
</body>
</html>
