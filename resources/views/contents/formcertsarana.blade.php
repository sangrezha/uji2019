@extends('layouts.template')

@section('navigation')
	@include('navigations.member')
@endsection

@section('embedscript')
<link rel="stylesheet" href="{{ asset('public/plugins/datepicker/datepicker3.css') }}">

<script type="text/javascript" src="{{ asset('public/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/plugins/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/information.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/handlebars.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/jquery.form.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/myself/certsarana.js') }}"></script>
@endsection

@section('contentheader')
<h1>
    Form Izin
    <small>Sarana Operasi</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-building"></i> Profil</a></li>
   <li><a href="#">Edit</a></li>
   
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12" id="error_msg"></div>
</div>
<input type="hidden" id="iddocument" value="<?php echo isset($iddoc) ? $iddoc : null;?>"/>
<input type="hidden" id="urlupload" value="{{url('/izin/operasi/sertifikatsarana/uploadsarana')}}"/>
<input type="hidden" id="cancelurl" value="{{url('/izin/operasi/sertifikatsarana/cancel')}}"/>
<input type="hidden" id="submiturl" value="{{url('/izin/operasi/sertifikatsarana/submit')}}"/>
<input type="hidden" id="submitediturl" value="{{url('/izin/operasi/sertifikatsarana/edit')}}"/>
<input type="hidden" id="urltable" value="{{url('/izin/operasi/sertifikatsarana/getdata')}}"/>
<input type="hidden" id="urldetail" value="{{url('/izin/operasi/sertifikatsarana/getdetail')}}"/>
<input type="hidden" id="urlremove" value="{{url('/izin/operasi/sertifikatsarana/removedoc')}}"/>
<input type="hidden" id="idcertsarana"/>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header bg-blue">
				<h3 class="box-title">Form Sertifikasi Sarana</h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body">
					
					<div class="form-group">
						<label for="jenissarana" class="col-sm-2 control-label">Jenis Sarana</label>
						<div class="col-sm-6">
							<input type="text" id="jenissarana" class="form-control" placeholder="Jenis Sarana">
						</div>
					</div>
					
					<div class="form-group">
						<label for="nocert" class="col-sm-2 control-label">No Sertifikat</label>
						<div class="col-sm-6">
							<input type="text" id="nocert" class="form-control" placeholder="No Sertifikat">
						</div>
					</div>
					
					<div class="form-group">
						<label for="nobadan" class="col-sm-2 control-label">Nomor Badan</label>
						<div class="col-sm-6">
							<input type="text" id="nobadan" class="form-control" placeholder="Nomor Badan">
						</div>
					</div>
					
					<div class="form-group">
						<label for="milik" class="col-sm-2 control-label">Kepemilikan</label>
						<div class="col-sm-6">
							<input type="text" id="milik" class="form-control" placeholder="Kepemilikan">
						</div>
					</div>
					
					<div class="form-group">
						<label for="jenisuji" class="col-sm-2 control-label">Jenis Pengujian</label>
						<div class="col-sm-6">
							<input type="text" id="jenisuji" class="form-control" placeholder="Jenis Pengujian">
						</div>
					</div>
					
					<div class="form-group">
						<label for="berlaku" class="col-sm-2 control-label">Masa Berlaku</label>
						<div class="col-sm-6">
							<input type="text" id="berlaku" class="form-control" data-date-format="dd/mm/yyyy" placeholder="Masa berlaku sertifikasi sarana">
						</div>
					</div>
					
					<div class="form-group">
						<label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
						<div class="col-sm-6">
							<textarea id="keterangan" rows="10" cols="80"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<form role="form" action="javascript:uploaddocument();" method="post" enctype="multipart/form-data"  name="uploadarsip" id="uploadarsip">
							<label for="userfile" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Upload Dokumen Sarana 
								<input id="userfile" name="userfile" type="file" onchange="subform()">(Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal" style="display: none;"><i class="fa fa-close"></i> <label id="hasil">&nbsp;</label><input type="hidden" id="namefile"/></a>
							<button type="submit" class="btn btn-primary" id="btupload" onclick="javascript:uploaddocument();" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group" id="prosesform" style="display: none;">
						<label for="inputEmail3" class="col-sm-2 control-label">&nbsp;</label>
						<div class="progress">
							<div id="progress" class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
								<span class="sr-only">50% Complete (success)</span>
							</div>
						</div>					
					</div>
					
					
				</div>
				<div class="box-footer">
					<div class="col-md-6 pull-right">
						<button type="submit" class="btn btn-warning" id="btclear"><i class="fa fa-times"></i> Batal</button>
						<button type="submit" class="btn btn-primary" id="btsimpan"><i class="fa fa-save"></i> Simpan </button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row" id="view-sertifikasi" style="display: none;">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header bg-blue">
				<h3 class="box-title">Daftar Sertifikasi Sarana</h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body table-responsive">
					<table class="table table-hover table-striped" id="table-view">
						<tr>
							<th>Jenis Sarana</th>
							<th>No Sertifikat</th>
							<th>No Badan</th>
							<th>Kepemilikan</th>
							<th>Jenis Pengujian</th>
							<th>Masa Berlaku</th>
							<th colspan="3" style="text-align: center;">Tindakan</th>
						</tr>
						<tr style="display: none;">
							<td>@{{hb_jenissarana}}</td>
							<td>@{{hb_nocert}}</td>
							<td>@{{hb_nobadan}}</td>
							<td>@{{hb_milik}}</td>
							<td>@{{hb_jenisuji}}</td>
							<td>@{{hb_berlaku}}</td>
							<td><a href="javascript:void(0)" onclick="editing('@{{hb_id}}')" class="pull-right"><i class="fa fa-edit"></i></a></td>
							<td style="text-align: center;"><a href="javascript:void(0)" onclick="seepdf('@{{hb_id}}')"><i class="fa fa-file-pdf-o"></i></a></td>
							<td><a href="javascript:void(0)" onclick="removedoc('@{{hb_id}}')"><i class="fa fa-trash-o"></i></a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection