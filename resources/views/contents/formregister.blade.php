<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Perizinan Online | Form Registrasi</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('/public/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/public/dist/css/AdminLTE.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('/public/plugins/iCheck/square/blue.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition register-page">
<div class="register-box">
	<div class="register-logo">
		<a href="#"><b>Registrasi</b>Perizinan</a>
	</div>

	<div class="register-box-body">
		<p class="login-box-msg">Akun Baru</p>
		<div class="form-group has-feedback">
			<input type="text" class="form-control" placeholder="Username" id="username">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" class="form-control" placeholder="Password" id="password">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" class="form-control" placeholder="Konfirmasi Password" id="konfirm">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
    
		<div class="form-group has-feedback">
			<input type="text" class="form-control" placeholder="Nama" id="nama"/>
			<span class="glyphicon glyphicon-user form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="text" class="form-control" placeholder="Nama Perusahaan" id="company">
			<span class="glyphicon glyphicon-home form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="email" class="form-control" placeholder="Email" id="email">
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		
		<div class="form-group has-feedback">
			<input type="text" class="form-control" placeholder="NPWP Perusahaan" id="npwp">
			<span class="glyphicon glyphicon-home form-control-feedback"></span>
		</div>
		
		<div class="form-group has-feedback">
			<form role="form" action="javascript:uploadmanual(1);" method="post" enctype="multipart/form-data"  name="uploadarsip1" id="uploadarsip1">
				<div class="form-group">
					<div class="btn btn-default btn-file" id="formdoc1">
						<i class="fa fa-paperclip"></i> SKDP
						<input id="userfile1" name="userfile1" type="file" onchange="subform(1)"> (Type file PDF/JPG max 1MB)
					</div> 
					<a href="javascript:void(0)" id="klikbatal1" style="display: none;"><i class="fa fa-close"></i> <label id="hasil1">&nbsp;</label><input type="hidden" id="namefile1"/></a>
				</div>
				
				<button type="submit" class="btn btn-primary" id="btupload1" onclick="javascript:uploadmanual(1);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
			</form>
		</div>
		
		<div class="form-group has-feedback">
			<form role="form" action="javascript:uploadmanual(2);" method="post" enctype="multipart/form-data"  name="uploadarsip2" id="uploadarsip2">
				<div class="form-group">
					<div class="btn btn-default btn-file" id="formdoc2">
						<i class="fa fa-paperclip"></i> NPWP
						<input id="userfile2" name="userfile2" type="file" onchange="subform(2)"> (Type file PDF/JPG max 1MB)
					</div>
					<a href="javascript:void(0)" id="klikbatal2" style="display: none;"><i class="fa fa-close"></i> <label id="hasil2">&nbsp;</label><input type="hidden" id="namefile2"/></a>
				</div>
				
				<button type="submit" class="btn btn-primary" id="btupload2" onclick="javascript:uploadmanual(2);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
			</form>
		</div>
		
		<div class="form-group has-feedback">
			<form role="form" action="javascript:uploadmanual(3);" method="post" enctype="multipart/form-data"  name="uploadarsip3" id="uploadarsip3">
				<div class="form-group">
					<div class="btn btn-default btn-file" id="formdoc3">
						<i class="fa fa-paperclip"></i> AKTA
						<input id="userfile3" name="userfile3" type="file" onchange="subform(3)"> (Type file PDF/JPG max 1MB)
					</div>
					<a href="javascript:void(0)" id="klikbatal3" style="display: none;"><i class="fa fa-close"></i> <label id="hasil3">&nbsp;</label><input type="hidden" id="namefile3"/></a>
				</div>
				
				<button type="submit" class="btn btn-primary" id="btupload3" onclick="javascript:uploadmanual(3);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
			</form>
		</div>

		<div class="form-group has-feedback">
			<form role="form" action="javascript:uploadmanual(4);" method="post" enctype="multipart/form-data"  name="uploadarsip4" id="uploadarsip4">
				<div class="form-group">
					<div class="btn btn-default btn-file" id="formdoc4">
						<i class="fa fa-paperclip"></i> KTP
						<input id="userfile4" name="userfile4" type="file" onchange="subform(4)"> (Type file PDF/JPG max 1MB)
					</div>
					<a href="javascript:void(0)" id="klikbatal4" style="display: none;"><i class="fa fa-close"></i> <label id="hasil4">&nbsp;</label><input type="hidden" id="namefile4"/></a>
				</div>
				
				<button type="submit" class="btn btn-primary" id="btupload4" onclick="javascript:uploadmanual(4);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
			</form>
		</div>
		
		<div class="form-group col-md-6 control-label" id="prosesform" style="display: none;">
			<label for="inputEmail3" class="col-sm-2 control-label">&nbsp;</label>
			<div class="progress">
				<div id="progress" class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
					<span class="sr-only">50% Complete (success)</span>
				</div>
			</div>					
		</div>
		<div class="row">
			<div class="col-xs-8">
			  &nbsp;
			</div>
        <!-- /.col -->
			<div class="col-xs-4">
			  <button type="button" class="btn btn-primary btn-block btn-flat" id="btreg">Register</button>
			</div>
        <!-- /.col -->
		</div>
    
	</div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
<div class="modal fade" id="moddisclamer" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-file"></i> Persetujuan</h4>
            </div>
            <div class="modal-body">
                <p>
					Anda melakukan registrasi ke system perizinan online perkeretaapian. 
					Dengan ini Anda memberikan persetujuan untuk mengikuti peraturan yang berlaku di web ini dan memberikan 
					informasi/meng upload data yang akurat sesuai dengan persyaratan yang ada di web ini untuk mengajukan perizinan perkeretaapian.
				</p>
				<p>
					Jika Anda sudah melakukan registrasi maksimum 7 hari Anda dapat mengajukan permintaan perizinan yang ada di web aplikasi ini, jika dalam 7 hari tidak melakukan pengajuan izin maka data registrasi Anda akan di hapus dari system dan silakan mengulang proses registrasi dari awal.  
Terkait Frekuensi yang di minta, akan ada pemberitahuan terkait kesesuaian GAPEKA melalui email Anda.
				</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-agree"><i class="fa fa-hand-o-right"></i> Setuju</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>
<!-- jQuery 2.2.3 -->
<script src="{{ asset('/public/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('/public/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/jquery.form.js') }}"></script>
<script type="text/javascript">

$(document).ready(function(){
	$("#klikbatal1").click(function(){
		var filerand = $("#namefile1").val();
		$.get("{{url('registrasi/remove/')}}"+"/"+filerand, function(response){
			$("#formdoc1").show();
			$("#hasil1").empty();
			$("#namefile1").val('');
			$("#klikbatal1").hide();
		});
	});
	
	$("#klikbatal2").click(function(){
		var filerand = $("#namefile2").val();
		$.get("{{url('registrasi/remove/')}}"+"/"+filerand, function(response){
			$("#formdoc2").show();
			$("#hasil2").empty();
			$("#namefile2").val('');
			$("#klikbatal2").hide();
		});
	});
	
	$("#klikbatal3").click(function(){
		var filerand = $("#namefile3").val();
		$.get("{{url('registrasi/remove/')}}"+"/"+filerand, function(response){
			$("#formdoc3").show();
			$("#hasil3").empty();
			$("#namefile3").val('');
			$("#klikbatal3").hide();
		});
	});

	$("#klikbatal4").click(function(){
		var filerand = $("#namefile4").val();
		$.get("{{url('registrasi/remove/')}}"+"/"+filerand, function(response){
			$("#formdoc4").show();
			$("#hasil4").empty();
			$("#namefile4").val('');
			$("#klikbatal4").hide();
		});
	});
	
	$("#btreg").click(function(){
		if($("#username").val() == ''){
			alert("Username harus diisi");
			$("#username").focus();
			return false;
		} else if($("#password").val() == ''){
			alert("Password harus diisi");
			$("#password").focus();
			return false;
		} else if($("#konfirm").val() == ''){
			alert("Konfirmasi Password harus diisi");
			$("#konfirm").focus();
			return false;
		} else if( $("#password").val() != $("#konfirm").val()){
			alert("Password tidak sama dengan Konfirmasi");
			return false;
		} else if($("#nama").val() == ''){
			alert("Nama harus diisi");
			$("#nama").focus();
			return false;
		} else if($("#company").val() == ''){
			alert("Nama perusahaan harus diisi");
			$("#company").focus();
			return false;
		} else if($("#npwp").val() == ''){
			alert("NPWP harus diisi");
			$("#npwp").focus();
			return false;
		} else if(ValidateEmail($("#email").val()) == false){
			alert("Email tidak valid");
			$("#email").focus();
			return false;
		} else{
			$("#moddisclamer").modal('show');
		}
	});
	
	$("#btn-agree").click(function(){
		$("#moddisclamer").modal('hide');
		$("#btreg").hide();
		$.ajaxSetup({
				headers : {
					'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
				}
			});
			
			var datareg = {
				name 		: $("#nama").val(),
				company 	: $("#company").val(),
				username	: $("#username").val(),
				password 	: $("#password").val(),
				email 		: $("#email").val(),
				npwp		: $("#npwp").val()
			};
			
			$.post("{{url('/registrasi/save')}}", {datasend:datareg}, function(respon){
				var errormsg = respon.error;
				
				if(errormsg == 1){
					alert("SKDP belum diupload");
					$("#btreg").show();
					return false;
				} else if(errormsg == 2){
					alert("NPWP belum diupload");
					$("#btreg").show();
					return false;
				} else if(errormsg == 3){
					alert("Akta belum diupload");
					$("#btreg").show();
					return false;
				} else if(errormsg == 4){
					alert("Nama perusahaan sudah ada");
					$("#btreg").show();
					$("#company").focus();
					return false;
				} else if(errormsg == 5){
					alert("Username sudah ada");
					$("#btreg").show();
					$("#username").focus();
					return false;
				} else if(errormsg == 6){
					alert("KTP belum upload");
					$("#btreg").show();
					return false;
				} else if(errormsg == 7){
					alert("Email sudah ada");
					$("#btreg").show();
					$("#email").focus();
					return false;
				} else if(errormsg == 8){
					alert("Gagal kirim email, pastikan alamat email Anda benar");
					$("#btreg").show();
					$("#email").focus();
					return false;
				} else{
					alert("Terima kasih sudah melakukan registrasi ke system Perizinan Online saat ini sedang proses verifikasi (maks. 2x 24 jam), silakan mengecek email anda.");
					$("#btreg").show();
					window.location.replace("{{url('/login')}}");
				}
				
				
			});
	});
});

function subform(arsip)
{
	document.getElementById("btupload"+arsip).click();
}

function uploadmanual(arsip)
{
	var userfile = $('#userfile'+arsip).val();
	
	if(userfile){
		$.ajaxSetup({
			headers : {
				'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
			}
		});
		$("#error_msg").empty();
		$('#uploadarsip'+arsip).ajaxForm({
            url:"{{ url('/registrasi/upload') }}",
            type: 'post',
            beforeSend: function() {
                $("#progress").css("width","0%");
            },
            uploadProgress: function(event, position, total, percentComplete) {
                $("#prosesform").show();
				$("#progress").css('width',percentComplete+'%');
            },
            beforeSubmit: function() {
                $("#progress").css("width","0%");
            },
            complete: function(xhr) {
                $("#prosesform").hide();
				$("#progress").css('width','0%');
            },
            success: function(result) {
				var msg = result.error;
                if(msg == 1){
					alert("File kosong");
					$("#uploadarsip"+arsip)[0].reset();
					return false;
				} else if(msg == 2){
					alert("File tidak valid");
					$("#uploadarsip"+arsip)[0].reset();
					return false;
				} else if(msg == 3){
					alert("File bukan PDF atau Image JPEG");
					$("#uploadarsip"+arsip)[0].reset();
					return false;
				} else if(msg == 4){
					alert("File melebihi 5MB");
					$("#uploadarsip"+arsip)[0].reset();
					return false;
				} else{
					$("#error_msg").empty();
					var valname = result.random;
					var typedoc = result.typedoc;
					
					$("#formdoc"+typedoc).hide();
					$("#klikbatal"+typedoc).show();
					$("#hasil"+typedoc).html(result.real);
					$("#namefile"+typedoc).val(result.random);
					
					$("#uploadarsip"+typedoc)[0].reset();
					$("#progress").css("width","0%");

				}
            },
        });
	}
}

function ValidateEmail(inputText)  
{  
	var mailformat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(inputText.match(mailformat)){ return true;}  
	else{return false;}
}
</script>
</body>
</html>
