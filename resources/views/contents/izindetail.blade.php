@extends('layouts.template')

@section('navigation')
	@include('navigations.internal')
@endsection

@section('embedscript')
<script type="text/javascript" src="{{ asset('public/dist/js/information.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/handlebars.js') }}"></script>
@endsection

@section('contentheader')
<h1>
    Verifikasi
    <small>Izin</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-file"></i> Verifikasi</a></li>
   <li><a href="#"> Detail</a></li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header bg-blue">
				<h3 class="box-title"><?php echo isset($company) ? $company : '' ?></h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body">
@if(!empty($data))
					<table class="table" id="table-arsip">
						<tr>
							<th>#</th>
							<th>Dokumen</th>
							<th>Detil</th>
						</tr>
	@foreach($data as $rowdata)					
						<tr>
							<td>{{$rowdata['no']}}</td>
							<td>{{$rowdata['doc']}}</td>
							<td><a href="{{$rowdata['url']}}" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a></td>
						</tr>
	@endforeach
					</table>
@endif
				</div>
			</div>
			<div class="box-footer">
				<div class="pull-right">
				<a href="{{$urlacc}}" class="btn btn-primary" id="btaccepted"><i class="fa fa-save"></i> Terima </a>
				<a href="{{$urlrej}}" class="btn btn-warning" id="btrejected"><i class="fa fa-times"></i> Tolak</a>
				</div>
				
			</div>
		</div>
		
	</div>
</div>


@endsection