@extends('layouts.template')

@section('navigation')
	@include('navigations.member')
@endsection

@section('embedscript')
<link rel="stylesheet" href="{{ asset('public/plugins/datepicker/datepicker3.css') }}">

<script type="text/javascript" src="{{ asset('public/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/information.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/handlebars.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/jquery.form.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/myself/operationsarana.js') }}"></script>
@endsection

@section('contentheader')
<h1>
    Form Izin
    <small>Sarana Operasi</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-building"></i> Umum</a></li>
   <li><a href="#">Sarana</a></li>
   <li><a href="#">Operasi</a></li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12" id="error_msg"></div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header bg-blue">
				<h3 class="box-title">Form Izin Operasi</h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body">
					
					<div class="form-group">
						<label for="perihal" class="col-sm-2 control-label">Perihal</label>
						<div class="col-sm-6">
							<input type="text" id="perihal" class="form-control" >
						</div>
					</div>
					
					<div class="form-group">
						<label for="izinpilih" class="col-sm-2 control-label">Izin yang diajukan</label>
						<div class="col-sm-6">
							<select id="izinpilih" class="form-control">
								<option value="0" selected="selected">--- Pilih Izin --- </option>
								<option value="1">Penambahan Lintas Pelayanan</option>
								<option value="2">Penambahan Frekuensi</option>
								<option value="3">Izin Baru</option>
								<option value="4">Izin Perpanjangan</option>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="form-lintas" style="display: none;">
						<label for="linpel" class="col-sm-2 control-label">Lintas Pelayanan</label>
						<div class="col-sm-6">
							<select id="linpel" class="form-control">
								<option value="0" selected="selected">--- Pilih Satu ---</option>
@if($linpel->count() > 0)
	@foreach($linpel->get() as $option)
								<option value="{{$option->id}}">{{$option->lintas}} (via: {{$option->via}} )</option>
	@endforeach
@endif
								<option value="">Lainnya</option>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="form-kapasitas-request" style="display: none;">
						<label for="kapasitasreq" class="col-sm-2 control-label">Frekuensi yang diminta</label>
						<div class="col-sm-6">
							<input type="text" id="kapasitasreq" class="form-control" onkeyup="if(/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
						</div>
					</div>
					
					<div class="form-group form-upload-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(1);" method="post" enctype="multipart/form-data"  name="uploadarsip1" id="uploadarsip1">
							<label for="userfile" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc1" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Studi Kelayakan 
								<input id="userfile1" name="userfile1" type="file" onchange="subform(1)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal1" style="display: none;"><i class="fa fa-close"></i> <label id="hasil1">&nbsp;</label><input type="hidden" id="namefile1"/></a>
							<button type="submit" class="btn btn-primary" id="btupload1" onclick="javascript:uploadmanual(1);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-upload-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(2);" method="post" enctype="multipart/form-data"  name="uploadarsip2" id="uploadarsip2">
							<label for="userfile2" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc2" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Sertifikasi Rangkaian 
								<input id="userfile2" name="userfile2" type="file" onchange="subform(2)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal2" style="display: none;"><i class="fa fa-close"></i> <label id="hasil2">&nbsp;</label><input type="hidden" id="namefile2"/></a>
							<button type="submit" class="btn btn-primary" id="btupload2" onclick="javascript:uploadmanual(2);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-upload-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(3);" method="post" enctype="multipart/form-data"  name="uploadarsip3" id="uploadarsip3">
							<label for="userfile2" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc3" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Sertifikasi Sarana 
								<input id="userfile3" name="userfile3" type="file" onchange="subform(3)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal3" style="display: none;"><i class="fa fa-close"></i> <label id="hasil3">&nbsp;</label><input type="hidden" id="namefile3"/></a>
							<button type="submit" class="btn btn-primary" id="btupload3" onclick="javascript:uploadmanual(3);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-upload-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(4);" method="post" enctype="multipart/form-data"  name="uploadarsip4" id="uploadarsip4">
							<label for="userfile2" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc4" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Sertifikasi Awak 
								<input id="userfile4" name="userfile4" type="file" onchange="subform(4)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal4" style="display: none;"><i class="fa fa-close"></i> <label id="hasil4">&nbsp;</label><input type="hidden" id="namefile4"/></a>
							<button type="submit" class="btn btn-primary" id="btupload4" onclick="javascript:uploadmanual(4);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-upload-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(5);" method="post" enctype="multipart/form-data"  name="uploadarsip5" id="uploadarsip5">
							<label for="userfile5" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc5" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Bukti Fasilitas Perawatan
								<input id="userfile5" name="userfile5" type="file" onchange="subform(5)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal5" style="display: none;"><i class="fa fa-close"></i> <label id="hasil5">&nbsp;</label><input type="hidden" id="namefile5"/></a>
							<button type="submit" class="btn btn-primary" id="btupload5" onclick="javascript:uploadmanual(5);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-frekuensi-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(6);" method="post" enctype="multipart/form-data"  name="uploadarsip6" id="uploadarsip6">
							<label for="userfile6" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc6" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Izin Operasi
								<input id="userfile6" name="userfile6" type="file" onchange="subform(6)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal6" style="display: none;"><i class="fa fa-close"></i> <label id="hasil6">&nbsp;</label><input type="hidden" id="namefile6"/></a>
							<button type="submit" class="btn btn-primary" id="btupload6" onclick="javascript:uploadmanual(6);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-frekuensi-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(7);" method="post" enctype="multipart/form-data"  name="uploadarsip7" id="uploadarsip7">
							<label for="userfile7" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc7" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> SOP Pengoperasian
								<input id="userfile7" name="userfile7" type="file" onchange="subform(7)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal7" style="display: none;"><i class="fa fa-close"></i> <label id="hasil7">&nbsp;</label><input type="hidden" id="namefile7"/></a>
							<button type="submit" class="btn btn-primary" id="btupload7" onclick="javascript:uploadmanual(7);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-frekuensi-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(8);" method="post" enctype="multipart/form-data"  name="uploadarsip8" id="uploadarsip8">
							<label for="userfile8" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc8" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Sertifikasi Uji Sarana
								<input id="userfile8" name="userfile8" type="file" onchange="subform(8)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal8" style="display: none;"><i class="fa fa-close"></i> <label id="hasil8">&nbsp;</label><input type="hidden" id="namefile8"/></a>
							<button type="submit" class="btn btn-primary" id="btupload8" onclick="javascript:uploadmanual(8);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-frekuensi-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(9);" method="post" enctype="multipart/form-data"  name="uploadarsip9" id="uploadarsip9">
							<label for="userfile9" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc9" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Sertifikasi Awak Sarana
								<input id="userfile9" name="userfile9" type="file" onchange="subform(9)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal9" style="display: none;"><i class="fa fa-close"></i> <label id="hasil9">&nbsp;</label><input type="hidden" id="namefile9"/></a>
							<button type="submit" class="btn btn-primary" id="btupload9" onclick="javascript:uploadmanual(9);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-frekuensi-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(10);" method="post" enctype="multipart/form-data"  name="uploadarsip10" id="uploadarsip10">
							<label for="userfile10" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc10" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Rencana Kerja
								<input id="userfile10" name="userfile10" type="file" onchange="subform(10)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal10" style="display: none;"><i class="fa fa-close"></i> <label id="hasil10">&nbsp;</label><input type="hidden" id="namefile10"/></a>
							<button type="submit" class="btn btn-primary" id="btupload10" onclick="javascript:uploadmanual(10);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-frekuensi-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(11);" method="post" enctype="multipart/form-data"  name="uploadarsip11" id="uploadarsip11">
							<label for="userfile11" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc11" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Jumlah Penambahan
								<input id="userfile11" name="userfile11" type="file" onchange="subform(11)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal11" style="display: none;"><i class="fa fa-close"></i> <label id="hasil11">&nbsp;</label><input type="hidden" id="namefile11"/></a>
							<button type="submit" class="btn btn-primary" id="btupload11" onclick="javascript:uploadmanual(11);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group form-frekuensi-baru" style="display: none;">
						<form role="form" action="javascript:uploadmanual(12);" method="post" enctype="multipart/form-data"  name="uploadarsip12" id="uploadarsip12">
							<label for="userfile12" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc12" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Perjanjian Kerja
								<input id="userfile12" name="userfile12" type="file" onchange="subform(12)"> (Type file PDF/JPG, max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal12" style="display: none;"><i class="fa fa-close"></i> <label id="hasil12">&nbsp;</label><input type="hidden" id="namefile12"/></a>
							<button type="submit" class="btn btn-primary" id="btupload12" onclick="javascript:uploadmanual(12);" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group" id="prosesform" style="display: none;">
						<label for="inputEmail3" class="col-sm-2 control-label">&nbsp;</label>
						<div class="progress">
							<div id="progress" class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
								<span class="sr-only">50% Complete (success)</span>
							</div>
						</div>					
					</div>
					
					<div class="form-group" id="btn-view">
						<label for="denom" class="col-sm-2 control-label">&nbsp;</label>
						<div class="col-sm-6">
							<button type="submit" class="btn btn-primary" id="btsimpan"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="moddisclamer" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-file"></i> Persetujuan</h4>
            </div>
            <div class="modal-body">
                <p>
					Terima kasih anda telah melakukan proses registrasi Izin Operasi, dengan ini Pemohon memberikan persetujuan untuk mengikuti peraturan yang berlaku di web ini dan memberikan informasi/meng upload data yang akurat sesuai dengan persyaratan yang ada di web ini untuk mengajukan perizinan perkeretaapian.
				</p>
				<p>
					Pemohon mengetahui dengan jelas bahwa untuk melanjutkan proses ini akan terdapat biaya yang harus di bayarkan sesuai dengan jenis yang di pilih dan sesuai dengan aturan yang berlaku.
				</p>
				<p>
					Pemohon akan mengirimkan dokumen asli untuk proses verifikasi ke staf KSPU maksimal 3 hari setelah dokumen di upload, selebihnya dokumen akan dihapus dari system daan pemohon akan mengulangi proses perizinan dari awal.
				</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-agree"><i class="fa fa-hand-o-right"></i> Setuju</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="removeurl" value="{{url('/izin/operasi/remove/')}}"/>
<input type="hidden" id="frekuensi" value="{{url('/izin/operasi/getfrekuensi')}}"/>
<input type="hidden" id="urlupload" value="{{url('/izin/operasi/upload')}}"/>
<input type="hidden" id="urlsubmit" value="{{url('/izin/operasi/submit')}}"/>
<input type="hidden" id="urllinpel" value="{{url('/izin/operasi/request/linpel')}}"/>
@endsection