@extends('layouts.template')

@section('navigation')
	@include('navigations.member')
@endsection

@section('embedscript')
<script type="text/javascript" src="{{ asset('public/dist/js/information.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/handlebars.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/myself/profilecompany.js') }}"></script>
@endsection

@section('contentheader')
<h1>
    Profil
    <small>Perusahaan</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-file"></i> Perusahaan</a></li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12" id="error_msg"></div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header bg-blue">
				<h3 class="box-title">Profil Perusahaan</h3>
				<div class="btn-group pull-right">
					<a class="btn btn-danger btn-sm" href="#" data-toggle="modal" data-target="#modalcompany" id="editcomp"><i class="fa fa-edit"></i></a>
				</div>
			</div>
			<div class="form-horizontal">
				<div class="box-body">
					
					<div class="form-group">
						<label for="tanggal" class="col-sm-2 control-label">Nama Perusahaan</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="company" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="tanggal" class="col-sm-2 control-label">Alamat</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="alamat" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="tanggal" class="col-sm-2 control-label">Nama Pemimpin</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="pemimpin" disabled>
						</div>
					</div>
					
					<div class="form-group">
						<label for="tanggal" class="col-sm-2 control-label">NPWP</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="npwp" disabled>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header bg-blue">
				<h3 class="box-title">Arsip Perusahaan</h3>
				<div class="btn-group pull-right">
					<!--<a class="btn btn-info btn-sm" href="#" data-toggle="modal" data-target="#modalsearch"><i class="fa fa-edit"></i></a>-->&nbsp;
				</div>
			</div>
			<div class="form-horizontal">
				<div class="box-body">
					<table class="table" syle="display: none;" id="table-arsip">
						<tr>
							<th>#</th>
							<th>Arsip/Dokumen</th>
							<th>Lihat</th>
						</tr>
						<tr style="display: none;">
							<td>@{{hb_no}}</td>
							<td>@{{hb_arsip}}</td>
							<td><a href="@{{hb_url}}" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header bg-blue">
				<h3 class="box-title">Monitor Proses Izin</h3>
				
			</div>
			
			<div class="form-horizontal">
				<div class="box-body">
					<table class="table" syle="display: none;" id="table-monitor">
						<tr>
							<th>#</th>
							<th>Perizinan</th>
							<th>Jenis</th>
							<th>Perihal</th>
							<th>Status</th>
							<!--<th>Edit</th>-->
						</tr>
						<tr style="display: none;">
							<td>@{{hbi_no}}</td>
							<td>@{{hbi_izin}}</td>
							<td>@{{hbi_jenis}}</td>
							<td>@{{hbi_perihal}}</td>
							<td>@{{hbi_status}}</td>
							<!--<td><a href="javascript:void(0)" onclick="editdocument('@{{hbi_iddoc}}','@{{hbi_perihal}}')"><i class="fa fa-edit"></i></a></td>-->
						</tr>
					</table>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="modal fade" id="modaledit" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-file"></i> Edit Dokumen</h4>
            </div>
            <div class="modal-body">
                
                <div class="form-group">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="width:70px">Perihal</span>
                        <input type="text" id="perihal" name="perihal" class="form-control" disabled />
						<span class="input-group-addon"><i class="fa fa-file"></i></span>
                    </div>
                </div>
                
				<div class="form-group">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="width:70px">Pilih Dokumen</span>
                        <select class="form-control" id="docselect" name="docselect">
							<option value="0" selected="selected">--- Pilih Salah Satu ---</option>
							<option value="1">Sertifikat Sarana</option>
							<option value="2">Sertifikat Awak</option>
							<option value="3">Sertifikat Tenaga Perawat</option>
							<option value="4">Study Kelayakan</option>
							<option value="5">SOP Kelayakan</option>
							<option value="6">Lintas Pelayanan</option>
							<option value="7">SMKP</option>
						</select>
						<span class="input-group-addon"><i class="fa fa-file"></i></span>
                    </div>
                </div>
				
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-edit"><i class="fa fa-edit"></i> Ke Form Edit</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalcompany" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-file"></i> Badan Usaha</h4>
            </div>
            <div class="modal-body">
                
				<div class="form-group">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="width:70px">Alamat</span>
                        <input type="text" id="edalamat" name="edalamat" class="form-control" />
						<span class="input-group-addon"><i class="fa fa-file"></i></span>
                    </div>
                </div>
				
				<div class="form-group">
					<div class="input-group input-group-sm">
						<span class="input-group-addon" style="width:70px">Pemimpin</span>
                        <input type="text" id="edpemimpin" name="edpemimpin" class="form-control" />
						<span class="input-group-addon"><i class="fa fa-file"></i></span>
                    </div>
                </div>
				
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-upcomp"><i class="fa fa-refresh"></i> Update</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="id-edit" value="" />
<input type="hidden" id="url-doc-ops" value="{{url('/izin/operasi')}}" />
<input type="hidden" id="urlprofile" value="{{ url('/member/getdata') }}" />
<input type="hidden" id="urlizin" value="{{ url('/member/getizin') }}" />
<input type="hidden" id="urlcompany" value="{{ url('/member/getcompany') }}" />
<input type="hidden" id="urlupdater" value="{{ url('/member/update') }}" />
@endsection