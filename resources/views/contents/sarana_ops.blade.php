@extends('layouts.template')

@section('navigation')
	@include('navigations.member')
@endsection

@section('embedscript')
<link rel="stylesheet" href="{{ asset('public/plugins/datepicker/datepicker3.css') }}">

<script type="text/javascript" src="{{ asset('public/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/information.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/handlebars.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/jquery.form.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/myself/operationsaranabak.js') }}"></script>
@endsection

@section('contentheader')
<h1>
    Form Izin
    <small>Sarana Operasi</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-building"></i> Umum</a></li>
   <li><a href="#">Sarana</a></li>
   <li><a href="#">Operasi</a></li>
</ol>
@endsection

@section('content')
<input type="hidden" id="urlsubmit" value="{{url('/izin/operasi/add')}}"/>
<input type="hidden" id="urlupload" value="{{ url('/izin/operasi/surat/upload') }}" />
<input type="hidden" id="removeurl" value="{{ url('/izin/operasi/surat/remove') }}" />
<input type="hidden" id="urllinpel" value="{{ url('/izin/operasi/request/linpel') }}"/>
<div class="row">
	<div class="col-md-12" id="error_msg"></div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header bg-blue">
				<h3 class="box-title">Form Izin Operasi</h3>
			</div>
			<div class="form-horizontal">
				<div class="box-body">
				
					<div class="form-group">
						<label for="perihal" class="col-sm-2 control-label">Perihal</label>
						<div class="col-sm-6">
							<input type="text" id="perihal" class="form-control" placeholder="Perihal Surat">
						</div>
					</div>
					
					<div class="form-group">
						<label for="tanggalsurat" class="col-sm-2 control-label">Tanggal Surat</label>
						<div class="col-sm-6">
							<input type="text" id="tanggalsurat" class="form-control" data-date-format="dd/mm/yyyy" placeholder="Tanggal Surat">
						</div>
					</div>
					
					<div class="form-group">
						<label for="surat" class="col-sm-2 control-label">Nomor Surat</label>
						<div class="col-sm-6">
							<input type="text" id="surat" class="form-control" placeholder="Nomor surat">
						</div>
					</div>
					
					<div class="form-group">
						<label for="izinpilih" class="col-sm-2 control-label">Izin yang diajukan</label>
						<div class="col-sm-6">
							<select id="izinpilih" class="form-control">
								<option value="0" selected="selected">--- Pilih Izin --- </option>
								<option value="1">Penambahan Lintas Pelayanan</option>
								<option value="3">Izin Baru</option>
								<option value="4">Izin Perpanjangan</option>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="form-lintas" style="display: none;">
						<label for="linpel" class="col-sm-2 control-label">Lintas Pelayanan</label>
						<div class="col-sm-6">
							<select id="linpel" class="form-control">
								<option value="0" selected="selected">--- Pilih Satu ---</option>
@if($linpel->count() > 0)
	@foreach($linpel->get() as $option)
								<option value="{{$option->id}}">{{$option->lintas}} (via: {{$option->via}} )</option>
	@endforeach
@endif
								<option value="">Lainnya</option>
							</select>
						</div>
					</div>
					
					<div class="form-group" id="form-kapasitas-request" style="display: none;">
						<label for="kapasitasreq" class="col-sm-2 control-label">Frekuensi Diminta</label>
						<div class="col-sm-6">
							<input type="text" id="kapasitasreq" class="form-control" onkeyup="if(/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
						</div>
					</div>
					
					<div class="form-group">
						<form role="form" action="javascript:uploaddocument();" method="post" enctype="multipart/form-data"  name="uploadarsip" id="uploadarsip">
							<label for="userfile" class="col-sm-2 control-label">&nbsp;</label>
							<div class="btn btn-default btn-file" id="formdoc" style="margin-left: 1%;">
								<i class="fa fa-paperclip"></i> Upload Surat 
								<input id="userfile" name="userfile" type="file" onchange="subform()"> (Type file PDF/JPG max 1MB)
							</div> 
							<a href="javascript:void(0)" id="klikbatal" style="display: none;"><i class="fa fa-close"></i> <label id="hasil">&nbsp;</label><input type="hidden" id="namefile"/></a>
							<button type="submit" class="btn btn-primary" id="btupload" onclick="javascript:uploaddocument();" style="display: none;"><i class="fa fa-upload"></i> Upload</button>
						</form>
					</div>
					
					<div class="form-group" id="prosesform" style="display: none;">
						<label for="inputEmail3" class="col-sm-2 control-label">&nbsp;</label>
						<div class="progress">
							<div id="progress" class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
								<span class="sr-only">50% Complete (success)</span>
							</div>
						</div>					
					</div>
					
					<div class="form-group" id="btn-view">
						<label for="denom" class="col-sm-2 control-label">&nbsp;</label>
						<div class="col-sm-6">
							<button type="submit" class="btn btn-primary" id="btsimpan"><i class="fa fa-save"></i> Simpan</button>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row" id="formlinpel"></div>
@endsection