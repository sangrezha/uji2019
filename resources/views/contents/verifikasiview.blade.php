@extends('layouts.template')

@section('navigation')
	@include('navigations.internal')
@endsection

@section('embedscript')
<script type="text/javascript" src="{{ asset('public/dist/js/information.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/handlebars.js') }}"></script>
@endsection

@section('contentheader')
<h1>
    Verifikasi
    <small>&nbsp;</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-file"></i> Verifikasi</a></li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header bg-blue">
				<h3 class="box-title">Izin Operasi</h3>
				<div class="btn-group pull-right">
					<!--<a class="btn btn-info btn-sm" href="#" data-toggle="modal" data-target="#modalsearch"><i class="fa fa-edit"></i></a>-->&nbsp;
				</div>
			</div>
			<div class="form-horizontal">
				<div class="box-body">
					<table class="table" syle="display: none;" id="table-arsip">
						<tr>
							<th>#</th>
							<th>Badan Usaha</th>
							<th>Perihal</th>
							<th>Jenis</th>
							<th>Detil</th>
						</tr>
						<tr style="display: none;">
							<td>@{{hb_no}}</td>
							<td>@{{hb_bu}}</td>
							<td>@{{hb_perihal}}</td>
							<td>@{{hb_jenis}}</td>
							<td><a href="@{{hb_url}}"><i class="glyphicon glyphicon-eye-open"></i></a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	loaddata();
});
function loaddata()
{
	var url = "{{ url('/verifikasi/izin/operasi/getall') }}";
	
	$.get(url, function(response){
		var data = response.data;
		$("#table-arsip").find("tr:gt(1)").empty();
		if(data != 0){
			var view = Handlebars.compile($("#table-arsip tr").eq(1).html());
			data.forEach(function(row){
				var dataizin = {
					hb_no 		: row.no,
					hb_bu 		: row.bu,
					hb_perihal 	: row.perihal,
					hb_jenis 	: row.jenis,
					hb_url		: row.url
				};
				
				$("#table-arsip tr:last").after("<tr>"+view(dataizin)+"</tr>");
			});
		}
	});
}
</script>
@endsection
