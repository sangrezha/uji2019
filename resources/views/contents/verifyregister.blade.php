@extends('layouts.template')

@section('navigation')
	@include('navigations.internal')
@endsection

@section('embedscript')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript" src="{{ asset('public/dist/js/information.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/handlebars.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/jquery.twbsPagination.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/dumpPagination.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/dist/js/myself/verify.js') }}"></script>
@endsection

@section('contentheader')
<h1>
    <i class="fa fa-user"></i> Verifikasi
    <small>Registrasi</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-user"></i> Verifikasi</a></li>
</ol>
@endsection

@section('content')
<input type="hidden" id="urltable" value="{{url('/verifikasi/alldata')}}"/>
<input type="hidden" id="urlterima" value="{{url('/verifikasi/terima')}}"/>
<input type="hidden" id="urltolak" value="{{url('/verifikasi/tolak')}}"/>
<input type="hidden" id="urldetail" value="{{url('/verifikasi/registrasi/detail')}}"/>
<div class="row">
    <div class="col-md-12" id="error_msg"></div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">&nbsp;</h3>
			</div>
			<div class="box-body">
				<table class="table table-hover table-striped" id="table-verify">
					<tr>
						<th>#</th>
						<th>Nama</th>
						<th>Badan Usaha</th>
						<th>Email</th>
						<th>Detil</th>
					</tr>
					<tr style="display: none;">
						<td>@{{hbno}}</td>
						<td>@{{hbnama}}</td>
						<td>@{{hbbu}}</td>
						<td>@{{hbmail}}</td>
						<td><a href="javascript:void(0)" onclick="accepted('@{{hbid}}','@{{hbnama}}')">Terima</a></td>
						<td><a href="javascript:void(0)" onclick="rejected('@{{hbid}}','@{{hbnama}}')">Tolak</a></td>
						
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection