<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Perizinan Online</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('public/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Theme style -->
    <link href="{{ asset('public/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link href="{{ asset('public/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />
   
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('public/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('public/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>

    <!-- FastClick -->
    <script src="{{ asset('public/plugins/fastclick/fastclick.min.js') }}"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset('public/dist/js/app.min.js') }}" type="text/javascript"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="{{ asset('public/dist/js/pages/dashboard2.js') }}" type="text/javascript"></script>-->

    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('public/dist/js/demo.js') }}" type="text/javascript"></script>
    
    @yield('embedscript')
    
  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">

        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>Iz</b>in</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Perizinan</b> Online</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!--<img src="{{ asset('public/dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image"/>-->
                    <span class="hidden-xs">{!! Auth::user()->name !!}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <!--<img src="{{ asset('public/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image" />-->
                        <!--<p>
                            User - Admin
                            <small>Member since Nov. 2012</small>
                        </p>-->
                    </li>
                    <!-- Menu Body -->
                    <!--<li class="user-body"> 
                        <div class="col-xs-4 text-center">
                            <a href="#">Followers</a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#">Sales</a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#">Friends</a>
                        </div>
                    </li>-->
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{ url('/profile/setting') }}" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{ url('logout') }}" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Keluar</a>
                        </div>
                    </li>
                </ul>
              </li>
            </ul>
          </div>

        </nav>
      </header>
      
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            @yield('navigation')
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          @yield('contentheader')
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          
        </div>
        <strong>Direktorat Jenderal Perkeretaapian - Kementerian Perhubungan </strong> 
      </footer>


    </div><!-- ./wrapper -->

   
  </body>
</html>

<?php
  //set headers to NOT cache a page
  header("Cache-Control: no-cache, no-store, must-revalidate"); //HTTP 1.1
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>