<ul class="sidebar-menu">
@if(Auth::user()->level == 1)
	<li class="header">Menu Admin</li>
@endif
@if(Auth::user()->level == 2)
	<li class="header">Menu KSPU</li>
@endif
@if(Auth::user()->level == 3)
	<li class="header">Menu KASI/KASUBDIT</li>
@endif
@if(Auth::user()->level == 1 || Auth::user()->level == 2)
    <li class="<?php echo isset($menu1) ? 'active' : ''; ?>">
        <a href="{{ url('/verifikasi/registrasi') }}">
            <i class="fa fa-user"></i> <span>Verifikasi User</span>
        </a>
    </li>
	
	<li class="<?php echo isset($menu2) ? 'active' : ''; ?>">
        <a href="{{ url('/verifikasi/izin/operasi') }}">
            <i class="fa fa-file"></i> <span>Verifikasi Izin</span>
        </a>
    </li>
@endif
    
    
</ul>