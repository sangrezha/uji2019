<ul class="sidebar-menu">
    <li class="header">Member Navigasi</li>
    <li class="<?php echo isset($menu1) ? 'active' : ''; ?>">
        <a href="{{url('/member/profile')}}">
            <i class="fa fa-building"></i> <span>Profile</span>
        </a>
    </li>
    
    <li class="treeview <?php echo isset($menu2) ? 'active' : ''; ?>">
        <a href="#">
            <i class="fa fa-building"></i> <span>Umum</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
		<ul class="treeview-menu">
			<li>
				<a href="#"><i class="fa fa-circle-o"></i> Prasarana
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
			
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-circle-o"></i> Usaha</a></li>
					<li><a href="#"><i class="fa fa-circle-o"></i> Operasi</a></li>
					<li><a href="#"><i class="fa fa-circle-o"></i> Pembangunan</a></li>
				</ul>
			</li>
			<li>
				<a href="#"><i class="fa fa-circle-o"></i> Sarana
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
			
				<ul class="treeview-menu">
					<li><a href="#"><i class="fa fa-circle-o"></i> Usaha</a></li>
					<li><a href="{{ url('/izin/operasi') }}"><i class="fa fa-circle-o"></i> Operasi</a></li>
				</ul>
			</li>
		</ul>
    </li>
    
</ul>