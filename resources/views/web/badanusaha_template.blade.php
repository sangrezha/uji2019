@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | Badan Usaha
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('bgnya')
		<div class="wrapall" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/images/badanusaha.jpg')}});">
@endsection

@section('hari_ini')
	{{ $hariini }}
@endsection

@section('script_awal')
@endsection

@section('content')
	<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-5 nopadding">
						<div class="thumbnail judul">
							BADAN USAHA
						</div>
					</div>
					<div class="col-md-7 ">
						<div class="thumbnail isikonten">
							<div class="content-area" style="height:370px">
								<table id="tableDataBrowse" class="table table-bordered table-striped" >
									<thead>
										<tr>
											<td width="80">No.</td>
											<td width="100">Nama</td>
											<td width="340">Alamat</td>
										</tr>
									</thead>
									<tbody>
									@foreach($dataList as $data)
										<tr>
											<td>{{ $ctr++ }}</td>
											<td>{{$data->name}}</td>
											<td>{{$data->alamat}}</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
	<script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
  <script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
  <!-- Regulasi Perizinan script -->
  <script>
    $(function () {
      $("#tableDataBrowse").DataTable();
    });
  </script>
@endsection
