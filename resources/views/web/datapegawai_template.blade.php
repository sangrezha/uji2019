@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | {{ (@$page->title) ? $page->title : 'KOMPOSISI PEGAWAI BALAI PENGUJIAN PERKERETAAPIAN' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-profile.jpg')}})">
  </div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="page-content">
		<div class="container">
			<h1 class="text-uppercase"><span>{{ (@$page->title) ? $page->title : "KOMPOSISI PEGAWAI BALAI PENGUJIAN PERKERETAAPIAN" }}</span></h1>
  <div class="col-md-12">
						{!! @$page->content !!}
        </div>

				<div class="row">

				@foreach ($pegawai as $peg)
					<div class="col-sm-6 col-md-3">

					<div class="thumbnail employee">
						<img src="{{ asset('/public/images/pegawai/'.$peg->picture) }}">
						<div class="caption">
							<h3>{{ $peg->nama }}</h3>
							<table>
								<tr>
									<td valign="top">NIP</td>
									<td width="10" align="center">:</td>
									<td>{{ $peg->nip }}</td>
								</tr>
								<tr>
									<td valign="top">GOL</td>
									<td width="10" align="center">:</td>
									<td>{{ $peg->golongan }}</td>
								</tr>
								<tr>
									<td valign="top">PANGKAT</td>
									<td width="10" align="center">:</td>
									<td>{{ $peg->pangkat }}</td>
								</tr>
								<tr>
									<td valign="top">JABATAN</td>
									<td width="10" align="center">:</td>
									<td>{{ $peg->jabatan }}</td>
								</tr>
								<tr>
									<td valign="top">PENDIDIKAN</td>
									<td width="10" align="center">:</td>
									<td>{{ $peg->pendidikan }}</td>
								</tr>
							</table>
						</div>
					</div>
				</div>

				@endforeach

			</div>
			</div>
    </div>

	
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
