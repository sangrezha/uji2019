@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'Denah Kantor' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="banner">
			<h1><span>{{ (@$page->highlight) ? $page->highlight : "Profil Balai" }}</span></h1>
			<img src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-profile.jpg')}}">
	</div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="content row">
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	        <div class="col-md-10">
	            <h1 class="content-title text-uppercase">@if (@$page->title)
								{!! $page->title !!}
							@else
								Denah Kantor
							@endif</h1>
							<div class="content-area">
							@if (@$page->content)
								{!! $page->content !!}
							@else
							Konten Denah Kantor
              <br><br>
							@endif
	            <br><br>
	        </div>
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	    </div>
		</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
