@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | Kerjasama - {{ $news->title }}
@endsection

@section('metadescription')
	{{ $news->lead }}
@endsection

@section('banner')
<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-news.jpg')}})">
  </div>
  
	<section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>{!! (@$page->title) ? $page->title : "Kerjasama" !!}</h1>     
   </div>
</section>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="page-content">
		{{-- <div class="container"> --}}
	{{-- <div class="content row"> --}}
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	        <div class="col-md-10">
	            <h1 class="content-title text-uppercase"><a href="{{ url()->previous() }}" class="other">Kerjasama Lainnya</a><br><br></h1>
							<div class="content-area">
								<h3 class="news-title">{{ $news->title }}</h3>
	              <br>
	              <br>
	              {!! $news->content !!}

	            <br><br>
							<h1 class="content-title text-uppercase"><a href="{{ url()->previous() }}" class="other">Kerjasama Lainnya</a><br><br></h1>

	        </div>
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	    </div>
		{{-- </div> --}}
	</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
