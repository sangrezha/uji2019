@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | Pengumuman - {{ $news->title }}
@endsection

@section('metadescription')
	{{ $news->lead }}
@endsection

@section('banner')
<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-news.jpg')}})">
  </div>
  <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>{!! (@$page->title) ? $page->title : "Pengumuman" !!} </h1>     
   </div>
</section>
@endsection


@section('script_awal')
@endsection

@section('content')
    <div class="page-content">
        <div class="col-md-12 djka-news-detail">
            <div class="djka-news-detail-header">
                <a href="{{ url()->previous() }}" class="djka-news-detail-other">Pengumuman Lainnya</a>
                <span class="djka-news-detail-date">{{App\Http\Controllers\webController::indonesian_date ($news->publish_date , 'd F Y') }}</span>
                <h1 class="djka-news-detail-title">{{ $news->title }}</h1>
                
            </div><!-- .djka-news-detail-header -->
            <div class="djka-news-detail-body djka-richtext"> 
              <div class="container">{!! preg_replace('/src="\/public\/images/','src="'.url('public/images'),$news->content) !!}</div>
             </div><!-- .djka-news-detail-body -->        
        </div><!-- .col-md-12 -->
    </div><!-- .page-content -->
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
