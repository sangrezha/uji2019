@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | WikiUji - {{ $news->title }}
@endsection

@section('metadescription')
	{{ $news->lead }}
@endsection

@section('banner')
	<div class="page-header">
      <img src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-news.jpg')}}">
      <div class="caption">
          <h1><span>{!! (@$page->title) ? $page->title : "WikiUji" !!}</span></h1>
      </div>
  </div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="page-content">
		<div class="col-md-12 djka-news-detail">
            <div class="djka-news-detail-header">
                <a href="{{ url()->previous() }}" class="djka-news-detail-other">WikiUji Lainnya</a>
                <span class="djka-news-detail-date">{{App\Http\Controllers\webController::indonesian_date ($news->publish_date , 'd F Y') }}</span>
                <h1 class="djka-news-detail-title">{{ $news->title }}</h1>
                
            </div><!-- .djka-news-detail-header -->
            <div class="djka-news-detail-body djka-richtext"> {!! preg_replace('/src="\/public\/images/','src="'.url('public/images'),$news->content) !!} </div><!-- .djka-news-detail-body -->        
        </div><!-- .col-md-12 -->
	</div><!-- .page-content -->
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
