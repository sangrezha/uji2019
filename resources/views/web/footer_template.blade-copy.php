		<div class="footer row">
				<div class="sticky-stopper"></div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10">
									<p>@yield('footer_text')</p>
                </div>
                <div class="col-md-2" align="right"><img class="img-responsive" src="{{asset('/public/assets/images/info-151.png')}}"></div>
            </div>
        </div>
    </div>
<script type="text/javascript">
	window.onload = function () {
			$('body').addClass('page-loaded');
	}
  $('ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(100);
  }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(100);
  });
</script>
