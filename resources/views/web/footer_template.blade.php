
<footer class="djka-footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="djka-footer-logo">
          <a href="#" class=""><img src="{{asset('/public/images/kemenhub.png')}}" alt=""></a>          
        </div>
      </div><!-- .col -->
      <div class="col-md-4">
        <div class="djka-footer-logo">
		  <a href="#"><img src="{{asset('/public/images/info-151.png')}}" alt=""></a>
		  <!-- <a href="#"><img src="{{asset('/public/images/simadu.png')}}" alt=""></a>
		  <div style="text-align: center; color: #440764;"><strong>BANTU KAMI TETAP KOMITMEN MENJADI&nbsp;PROFESIONAL.&nbsp;</strong><strong><span style="color: #b30d0d;">LIHAT. LAWAN. LAPORKAN!</span> SETIAP TINDAKAN KAMI YANG MELANGGAR HUKUM.</strong></div>
         -->
        </div>
      </div><!-- .col -->
      <div class="col-md-4">
        <div class="djka-footer-social-icons">
          <ul>
            <li><a href="https://web.facebook.com/pages/Balai-Pengujian-Perkeretaapian-Kementerian-Perhubungan/234875016879818?_rdc=1&_rdr" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
            <li><a href="https://twitter.com/BalaiUji_KA" target="_blank"><i class="fab fa-twitter"></i></a></li>
            <li><a href="https://www.instagram.com/balaipengujianka/" target="_blank"><i class="fab fa-instagram"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCsSYxPgbCnPbl0eN1oAWN4A" target="_blank"><i class="fab fa-youtube"></i></a></li>
          </ul>
        </div><!-- .djka-footer-social-icons -->
      </div><!-- .col -->
    </div><!-- .row -->
  </div><!-- .container -->
</footer>


<div class="djka-floating-menu" data-djka-floating-menu>
  <div class="djka-floating-menu-toggle" data-djka-floating-menu-toggle>
    <span>MENU</span>
    <i>×</i>
  </div><!-- .djka-floating-menu-toggle -->
  <div class="djka-floating-menu-list">
    <ul>
	@foreach($sidemenu as $key => $val)
      <li><a href="{{$val->link}}" target="_{{$val->window}}">{{$val->title}}</a></li>
	@endforeach
    </ul>
  </div><!-- .djka-floating-menu-list -->
</div><!-- .djka-floating-menu -->
@if ($pref->chat_active)
			@if (str_contains($pref->chat_hari, date('w')))
				@if ((date("H:i")>$pref->chat_jam1) && (date("H:i")<$pref->chat_jam2))
<div class="djka-chat" data-djka-chat>
  <a href="#" class="djka-chat-toggle cheader1" data-djka-chat-toggle>CHAT</a>
  <div class="djka-chat-window">
    <div class="djka-chat-header">
      <i class="fa fa-times djka-chat-close" data-djka-chat-close></i>
    </div>
	<div class="djka-chat-body register">
            <div class="djka-chat-body-scroll chat-register" style="display: none">
                <form>
									{{ csrf_field() }}
									<input type="hidden" name="sesi" id="sesi" value="{{ session()->getId() }}">
									<div class="form-group">
											<select name="subject" id="subject" class="form-control" >
												<option value="">-- Pilih Subject --</option>
												@foreach (config('app.chatSubject') as $k => $v)
													<option value="{{ $k }}">{{ $v }}</option>
												@endforeach
											</select>
									</div>
                    <div class="form-group">
                        <input type="text" class="form-control" aria-describedby="name" name="nama" id="nama" placeholder="Masukkan Nama Anda">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email" placeholder="Masukkan Email Anda">
                        <small id="emailHelp" class="form-text text-muted">Kami tidak akan membagikan email anda ke siapapun.</small>
                    </div>
                </form>
                <button class="btn btn-primary" id="mulaichat">Mulai chat</button>
            </div>
	</div>
	<div class="chat"  style="display:none;">
	<div class="djka-chat-body">
      <div class="djka-chat-body-scroll chat-history">
      </div>
    </div>
    <div class="djka-chat-footer">
      <div class="djka-chat-form">
      <!-- <form action=""> -->
               <input type="hidden" name="sesi" value="{{ session()->getId() }}">
							<input type="text" name="msg" id="msg" class="djka-chat-field" placeholder="Ketik pesan anda disini">
              <button id="kirimchat" type="button"  class="djka-chat-send"><i class="fa fa-chevron-circle-right"></i></button>
      <!-- </form> -->
    </div><!-- .djka-chat-form -->
	</div>
    </div>
  </div>
</div><!-- .djka-chat -->
@endif
			@endif
		@endif
<script src="{{asset('/public/improve-assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('/public/improve-assets/vendor/responsiveslides.min.js')}}"></script>
<script src="{{asset('/public/improve-assets/vendor/bootstrap-4.0.0/js/bootstrap.js')}}"></script>
<script src="{{asset('/public/improve-assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('/public/improve-assets/js/app.js?v=1.1.26')}}"></script>
<script type="text/javascript">
	var msgchat = '';
	var chatid = '';
	var okchat = '';
	var sesiidnya = '';
	// window.onload = function () {
	// 		$('body').addClass('page-loaded');
	// }

	function tambahMessage(){
		$.ajax({
					// url: '{{ route('web.chat3', ['sessid' => session()->getId() ]) }}',
					url: "{{ env('APP_URL') }}/chat3/"+sesiidnya,
					// url: '/ujidjka/chat3/'+sesiidnya,
					method: 'get',
					dataType: 'html',
					success: function(data) {
						if (data){
							$( ".chat-history" ).append(data);
							$('.chat-history').animate({
	   							scrollTop: $('.chat-history').get(0).scrollHeight}, 500);
						}
					}
			});
	}

	$(".cheader1").click(function(){
		console.log($(this).parent().hasClass('is-opened"'));
			if($(this).parent().hasClass('is-opened"')){
					$(this).removeClass('open');
					$(".register .chat-register").hide();
			}else{
					$(this).addClass('open');
					$(".register .chat-register").show();
			}
	})
	function ValidateEmail(mail) 
	{
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
	{
		return (true)
	}
		alert("Email yang anda input tidak sesuai format")
		return (false)
	}
	$("#mulaichat").click(function(e){
		e.preventDefault();
		var subject = $('#subject').val();
		var nama = $('#nama').val();
		var email = $('#email').val();
		var sesi = $('#sesi').val();
		sesiidnya = sesi+Date.now();
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
		{
		var urlnya = "{{ route('web.chat1') }}";
		$.ajaxSetup({
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
		});

		$.ajax({
					url: urlnya,
					method: 'POST',
					data: {subject:subject,nama:nama,email:email,sessionid:sesiidnya},
					dataType: 'json',
					success: function(data) {
						if (data.error < 1){
							$(".register").hide();
							$(".chat").show();
							window.setInterval(function(){
					        tambahMessage(); /// call your function here
							}, 1000);

						} else {
							alert(data.message);
						}
					}
			});
		}else{
			alert("Email yang anda input tidak sesuai format")
		}

	});

	function kirimchatnya(){
		var msg = $('#msg').val();
		var sesi = $('#sesi').val();
		console.log(sesiidnya);

		var urlnya = "{{ route('web.chat2') }}";
		$.ajaxSetup({
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
		});

		$.ajax({
					url: urlnya,
					method: 'POST',
					data: {msg:msg,sessionid:sesiidnya},
					dataType: 'json',
					success: function(data) {
						if (data.error < 1){
							$('#msg').val('');
						} else {
							alert(data.message);
						}
					}
			});
	}

	$("#kirimchat").click(function(){
		kirimchatnya();
	});

	$('#msg').bind("enterKey",function(e){
		kirimchatnya();
	});
	$('#msg').keyup(function(e){
		if(e.keyCode == 13)
		{
		  $(this).trigger("enterKey");
		}
	});

	$(".cheader2").click(function(){
			if($(this).hasClass('open')){
					$(this).removeClass('open');
					$(".chat-history").hide();
					// $(".chat-history").removeClass('open');
					$(".chat-message").hide();
					// $(".chat-message").removeClass('open');
			}else{
					$(this).addClass('open');
					$(".chat-history").show();
					// $(".chat-history").removeClass('open');
					$(".chat-message").show();
					// $(".chat-message").removeClass('open');
			}
	})

	$(".chat").click(function(){
			// $(".chat-history").hide();
			// $(".chat-history").removeClass('open');
			// $(".chat-message").hide();
			// $(".chat-message").removeClass('open');
			// $(".chat").hide();

	});

  // $('ul.nav li.dropdown').hover(function() {
  //     $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(100);
  // }, function() {
  //     $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(100);
  // });
</script>

</body>
</html>
