	<title>@yield('judul')</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"  />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="@yield('metadescription')">
	<meta name="keywords" content="@yield('metakeywords')">
	<link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('/public/improve-assets/vendor/bootstrap-4.0.0/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('/public/improve-assets/vendor/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('/public/improve-assets/css/main.css?v=1.1.181')}}">
    <link rel="stylesheet" href="{{asset('/public/improve-assets/css/content.css?v=1.1.181')}}">

	{{ $pref->konten_head }}
	@yield('head_konten')
