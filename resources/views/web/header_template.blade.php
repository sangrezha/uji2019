
<header class="djka-header">
  <div class="djka-logo-area">
    <a class="djka-logo" href="{{ route('web.home') }}">
       <img
      srcset="{{asset('/public/images/logo_a@2x.png')}} 923w, {{asset('/public/images/logo_a.png')}} 461w"
      sizes="(min-width:900px) 923px, 461px"
      src="{{asset('/public/images/logo_a.png')}}" alt="Logo Kementerian Perhubungan">

    </a>
  </div><!-- .djka-logo-area -->
  <div class="djka-menutoggle" data-djka-menutoggle> <a href="#"><i class="fa fa-bars"></i></a> </div>
  <div class="djka-menu-area" data-djka-menu-area>
    <div class="djka-menu-scroll">
    <div class="djka-searchbox" data-djka-searchbox>
      <a href="#" class="djka-searchbox-toggle" data-djka-searchbox-toggle><i class="fa fa-search"></i></a>
      <div class="djka-searchbox-form" data-djka-searchbox-form>
        <form action="{{ route('web.search') }}" method="post" >
										 {{ csrf_field() }}
            <input type="text" class="djka-searchbox-field" placeholder="Search.." name="keyword" >
            <button class="djka-searchbox-button" type="submit"><i class="fa fa-search"></i></button>
        </form>
      </div>
    </div><!-- .djka-searchbox -->
    @include('web.menu_template')
  </div><!-- .djka-menu-scroll -->
  </div><!-- .djka-menu-area -->  
</header><!-- .djka-header -->
<div class="djka-header-filler"></div>
