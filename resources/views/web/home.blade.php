<title>DIREKTORAT JENDERAL ERKERETAAPIAN | Kementrian Perhubungan Republik Indonesia</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/public/assets/mystyle.css') }}">
	<link rel="shortcut icon" href="{{ asset('/public/assets/images/icon_logo.png')}}"/>
</head>

<body>
<div class="top-header">
	<br>
	<div id="atas">
		<a href="{{url('/homelogin')}}" class="tombol">LOGIN</a>
		<a href="{{url('/registrasi')}}" class="tombol">Registrasi</a>

		<form>
			<input class="search" type="text" placeholder="SEARCH" required>	
			<input class="button"  name="imageField" type="image" src="{{asset('/public/assets/images/search_icon.jpg')}}">	
		</form>
   </div>
</div>

<div class="menu">
 <div id="kanan_menu">
    	<ul>
        	<li id="home"><a href="#"><img src="{{asset('/public/assets/images/home_icon.png')}}"></a></li>
            <li id="profile"><a href="{{url('/profile')}}">PROFIL</a></li>
            <li id="regulasi"><a href="{{ url('/regulasi') }}">REGULASI PERIZINAN</a></li>
            <li id="perizinan"><a href="{{url('/perizinan')}}">PERSYARATAN PERIZINAN</a></li>
            <li id="badanusaha"><a href="#">BADAN USAHA</a></li>
            <li id="kontak"><a href="{{url('/kontak')}}">KONTAK</a></li>
        </ul>
    
    </div>
</div>

<div class="content-index">
  <img src="{{asset('/public/assets/images/index_02.jpg')}}" width="1263" height="433" alt="content_index"> 
</div>

<div class="footer-index">

	© Copyright 2017<br>
Direktorat Jenderal Perkeretaapian  |  Kementerian Perhubungan Republik Indonesia
    
</div>


</body>
</html>
