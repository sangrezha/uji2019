@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | Home
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('script_awal')
@endsection

@section('banner')
	<div class="slide-home">
				@foreach ($highlight as $data)
					<div>
	            <img src="{{asset('/public/images/highlight/'.$data->picture)}}">
	            <div class="caption">
	                <h1>{!! @$data->title !!}</h1>
	                {!! @$data->highlight !!}
	            </div>
	        </div>
				@endforeach
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-1 hidden-xs hidden-sm"></div>
        <div class="col-md-10 compro">
            <div class="row">
                <div class="col-md-7">
									@if (@$page->title)
										{!! $page->title !!}
									@else
										<h1><small>Welcome to </small>balai pengujian perkeretaapian </h1>
									@endif
									@if (@$page->content)
										{!! $page->content !!}
									@else
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrud exercitation ullamco laboris nisi utaliquip ex ea commodo&nbsp;consequat.
												</p>
										<ol>
												<li><span>Lorem ipsum dolor sit amet</span></li>
												<li><span>consectetur adipisicing elit, sed do eiusmod</span></li>
												<li><span>tempor incididunt ut labore et dolore magna aliqua</span></li>
										</ol>
									@endif

                </div>
                <div class="col-md-5"><img class="img-responsive" src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/img-about.jpg')}}"></div>
            </div>
        </div>
        <div class="col-md-1 hidden-xs hidden-sm"></div>
    </div>
    <div class="row service">
        <div class="col-md-1 hidden-xs hidden-sm"></div>
        <div class="col-md-10">
            <div class="row">
                <div class="item col-md-4">
                    <div class="caption">
                        <h1><small>Pengujian</small>SDM</h1>
                        <p>{{ $pref->teks1 }}</p>
                        <a href="{{ route('web.ujisdm1') }}">SELENGKAPNYA</a>
                    </div>
                    <img class="img-responsive" src="{{asset('/public/assets/img/service3.jpg')}}">
                </div>
                <div class="item col-md-4">
                    <div class="caption">
                        <h1><small>Pengujian</small>Prasarana</h1>
                        <p>{{ $pref->teks2 }}</p>
                        <a href="{{ route('web.ujiprasarana1') }}">SELENGKAPNYA</a>
                    </div>
                    <img class="img-responsive" src="{{asset('/public/assets/img/service1.jpg')}}">
                </div>
                <div class="item col-md-4">
                    <div class="caption">
                        <h1><small>Pengujian</small>Sarana</h1>
                        <p>{{ $pref->teks3 }}</p>
                        <a href="{{ route('web.ujisarana1') }}">SELENGKAPNYA</a>
                    </div>
                    <img class="img-responsive" src="{{asset('/public/assets/img/service2.jpg')}}">
                </div>
            </div>
        </div>
        <div class="col-md-1 hidden-xs hidden-sm"></div>
    </div>
    <div class="row news">
        <div class="col-md-1 hidden-xs hidden-sm"></div>
        <div class="col-md-10">
            <a class="hidden-xs hidden-sm hidden-md hidden-lg" href="#"><img class="img-responsive" src="{{asset('/public/assets/img/logo.png')}}"></a>
            <div class="row item">
                <div class="col-md-7">
                    <h1 class="content-title text-uppercase">Berita Terkini</h1>
                    <div class="row">
                        <div class="col-md-6 col-sm-4 hidden-xs">
                            <a href="#"><img class="img-responsive" src="{{asset('/public/images/news/'.$news1->thumbnail)}}"></a>
                        </div>
                        <div class="col-md-6"><strong><a href="#" style="font-size:18px;line-height:20px;">{{ $news1->title }}&nbsp; <small class="visible-lg-block" style="font-size:12px;">{{App\Http\Controllers\webController::indonesian_date ($news1->publish_date , 'd F Y') }}</small></a></strong>
                            <p>{{ $news1->lead }}</p>
                        </div>
                    </div>
                    <div></div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <a href="#" class="other">Berita Lainnya</a>
                    </div>
										@foreach ($news as $berita)
											<div class="row hidden-sm hidden-xs">
	                        <div class="col-md-3">
	                            <a href="#"><img class="img-responsive" src="{{asset('/public/images/news/'.$berita->thumbnail)}}"></a>
	                        </div>
	                        <div class="col-md-9"><strong> <a href="#">{{ $berita->title }} <small class="visible-lg-block">{{App\Http\Controllers\webController::indonesian_date ($berita->publish_date , 'd F Y') }} </small></a></strong></div>
	                    </div>
										@endforeach

                </div>
            </div>
        </div>
        <div class="col-md-1 hidden-xs hidden-sm"></div>
    </div>

@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
		<script type="text/javascript" src="{{asset('/public/assets/js/slick.min.js')}}"></script>
    <script type="text/javascript">
        $('.slide-home').slick({
            dots: true,
            variableWidth: true,
            autoplay: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            centerMode: true
        });
    </script>
@endsection
