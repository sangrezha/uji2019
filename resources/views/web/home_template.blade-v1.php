@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | Home
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('script_awal')
@endsection

@section('banner')
	<div class="slide-home">
			@foreach ($highlight as $data)
				<div>
            <img src="{{asset('/public/images/highlight/'.$data->picture)}}">
            <div class="caption">
                <h1>{!! @$data->title !!}<br/><span>{{ @$data->highlight }}</span></h1>
                {!! @$data->highlight !!}
            </div>
        </div>
			@endforeach
  </div>
	<div class="row update">
    <ul class="nav nav-tabs col-md-1 nopadding">
        <li class="active"><a data-toggle="tab" href="#berita">Berita Terkini</a></li>
        <li><a data-toggle="tab" href="#pengumuman">Pengumuman</a></li>
    </ul>
    <div class="tab-content col-md-11">
        <div id="berita" class="tab-pane in active">
            <div class="carousel slide multi-item-carousel" id="berita-carousel">
                <div class="carousel-inner">
									@foreach ($news as $berita)
                    <div class="item {{ @$active[$berita->uniqid] }}">
                        <div class="col-xs-12 col-md-4">
                            <a href="{!! route('web.detberita', ['slug' => $berita->slug]) !!}">
                                <img src="{{ ($berita->picture) ? asset('/public/images/news/'.$berita->picture) : asset('/public/images/imgthumb.png') }}" class="img-responsive">
                                <h1>{{ $berita->title }}</h1>
                                <span>{{App\Http\Controllers\webController::indonesian_date ($berita->publish_date , 'd F Y') }}</span>
                            </a>
                        </div>
                    </div>
									@endforeach
                </div>
                <a class="left carousel-control" href="#berita-carousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                <a class="right carousel-control" href="#berita-carousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
            </div>
        </div>
        <div id="pengumuman" class="tab-pane">
            <div class="carousel slide multi-item-carousel" id="pengumuman-carousel">
                <div class="carousel-inner">
									@foreach ($pengumuman as $pengumumans)
										<div class="item {{ @$active[$pengumumans->uniqid] }}">
												<div class="col-xs-12 col-md-4">
														<a href="{!! route('web.detpengumuman', ['slug' => $pengumumans->slug]) !!}">
																<img src="{{ ($pengumumans->picture) ? asset('/public/images/pengumuman/'.$pengumumans->picture) : asset('/public/images/imgthumb.png') }}" class="img-responsive">
																<h1>{{ $pengumumans->title }}</h1>
																<span>{{App\Http\Controllers\webController::indonesian_date ($pengumumans->publish_date , 'd F Y') }}</span>
														</a>
												</div>
										</div>
									@endforeach
                </div>
                <a class="left carousel-control" href="#pengumuman-carousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                <a class="right carousel-control" href="#pengumuman-carousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
  	<div class="row compro">
		        <div class="col-md-1 hidden-xs hidden-sm"></div>
		        <div class="col-md-10">
		            <div class="row">
									@if (@$page->title)
										<h1 class="col-md-12">{!! $page->title !!}</h1>
									@else
										<h1 class="col-md-12">Selamat Datang<br/>di Balai Pengujian Perkeretaapian</h1>
									@endif
									@if (@$page->content)
										{!! $page->content !!}
									@else
		                <h2 class="col-md-6">
		                    Dalam rangka melakukan pengendalian dan pengawasan terhadap kualitas sarana dan prasarana perkeretaapian serta sumber daya manusia perkeretaapian
		                </h2>
		                <p class="col-md-6">
		                    kegiatan pengujian merupakan bagian dari kegiatan untuk memberikan jaminan keselamatan operasi kereta api. Pengujian dilakukan pada semua sistem perkeretaapian mteliputi prasarana, sarana dan sumber daya manusia perkeretaapian. Kegiatan pengujian tersebut dimaksudkan untuk meyakinkan bahwa semua aspek kegiatan yang berkaitan dengan operasi kereta api telah laik operasi.
		                </p>
									@endif

		            </div>
		        </div>
		        <div class="col-md-1 hidden-xs hidden-sm"></div>
		    </div>
		    <div class="row service">
		        <div class="col-md-12 nopadding">
		            <div class="item col-md-4">
		                <div class="img">
		                    <h1>Pengujian SDM</h1>
		                    <img class="img-responsive" src="{{asset('/public/assets/img/service3.jpg')}}">
		                </div>
		                <div class="caption">
		                    <p>{{ $pref->teks1 }}</p>
		                    <a href="{{ route('web.ujisdm1') }}">SELENGKAPNYA</a>
		                </div>
		            </div>
		            <div class="item col-md-4">
		                <div class="img">
		                    <h1>Pengujian Prasarana</h1>
		                    <img class="img-responsive" src="{{asset('/public/assets/img/service1.jpg')}}">
		                </div>
		                <div class="caption">
											<p>{{ $pref->teks2 }}</p>
											<a href="{{ route('web.ujiprasarana1') }}">SELENGKAPNYA</a>
		                </div>
		            </div>
		            <div class="item col-md-4">
		                <div class="img">
		                    <h1>Pengujian Sarana</h1>
		                    <img class="img-responsive" src="{{asset('/public/assets/img/service2.jpg')}}">
		                </div>
		                <div class="caption">
											<p>{{ $pref->teks3 }}</p>
											<a href="{{ route('web.ujisarana1') }}">SELENGKAPNYA</a>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="row mitra">
		        <div class="col-md-1 hidden-xs hidden-sm"></div>
		        <div class="col-md-10 nopadding">
		            <h1>KERJASAMA</h1>
								@foreach ($mitra as $mitras)
									<img src="{{asset('/public/images/mitra/'.$mitras->picture)}}" title="{{ $mitras->title}}"/>
								@endforeach
		        </div>
		        <div class="col-md-1 hidden-xs hidden-sm"></div>
		    </div>
		    <div class="row video">
		            <iframe width="100%" height="400" src="https://www.youtube.com/embed/qU5a6JZxpM4?controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		    </div>
		    <div class="row sosmed">
		        <div class="col-md-12">
		            <h1>Akun Resmi Sosial Media Balai Pengujian Perkeretaapian</h1>
		            <p>Ikuti kami untuk mengetahui update terbaru dari kami</p>
		            <br/>
		            <div class="col-md-4 instagram-box" style="padding:0;">
		                <div class="top col-md-12 nopadding">
		                    <div class="col-md-6 nopadding"><img src="{{asset('/public/assets/img/ic-ig.png')}}" width="30px"> @balaipengujianka</div>
		                    <div class="col-md-6 nopadding" align="right"><strong class="ig_follower"><span></span> followers</strong> <a href="https://www.instagram.com/balaipengujianka/" class="follow">follow</a></div>
		                </div>
		                <a href="https://instawidget.net/v/user/balaipengujianka" id="link-f36e5c465ce4c4479278e9c83071103e694f53b2f730672703d20314ca6a27e8">@balaipengujianka</a>
		                <script src="https://instawidget.net/js/instawidget.js?u=f36e5c465ce4c4479278e9c83071103e694f53b2f730672703d20314ca6a27e8&width=100%"></script>
		            </div>
		            <div class="col-md-4 twitter-box">
		                <div class="top col-md-12 nopadding">
		                    <div class="col-md-6 nopadding"><img src="{{asset('/public/assets/img/ic-twitter.png')}}" width="30px"> @BalaiUji_KA </div>
		                    <div class="col-md-6 nopadding" align="right"><strong class="twitter_follower"><span></span> followers</strong> <a href="https://twitter.com/BalaiUji_KA" class="follow">follow</a></div>
		                </div>
		                <div class="timeline-box">
		                    <a class="twitter-timeline" href="https://twitter.com/BalaiUji_KA?ref_src=twsrc%5Etfw" height="340">Tweets by BalaiUji_KA</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
		                </div>
		            </div>
								<div class="col-md-4 youtube-box nopadding">
										<div class="top col-md-12 nopadding">
												<div class="col-md-7 nopadding"><img src="{{asset('/public/assets/img/ic-youtube.png')}}" width="30px"> Balai Pengujian Perkeretaapian</div>
												<div class="col-md-5 nopadding" align="right"><strong class="youtube_follower"><span></span> subscriber</strong> <a href="https://www.youtube.com/channel/UCsSYxPgbCnPbl0eN1oAWN4A" class="follow">subscribe</a></div>
										</div>
									<ul id="playListContainer"></ul>
								</div>
		        </div>
		    </div>

@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
		<script type="text/javascript" src="{{asset('/public/assets/js/slick.min.js')}}"></script>
		<script type="text/javascript">
        window.onload = function () {
            $('body').addClass('page-loaded');
						@if ($pref->popup_active)
						$('#promo-modal').modal('show');
            setTimeout(function(){ $('#promo-modal').modal('hide'); }, 15000);
						@endif
						$(".service .item").hover(function(){
                var titlePosition = $(".service .item:hover .caption").height();
                $(".service .item:hover .img h1").css({'bottom':titlePosition-20});
            },function(){
                $(".service .item .img h1").css({'bottom':'40px'});
            })
        }

        $('.slide-home').slick({
            dots: true,
            variableWidth: true,
            autoplay: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            centerMode: true
        });
        $('.multi-item-carousel').carousel({
            interval: 3000
        });

        $('.multi-item-carousel .item').each(function(){
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length>0) {
            next.next().children(':first-child').clone().appendTo($(this));
        } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
        });

        var token = '5000964138.1677ed0.f98441e2a74c423495cfcc795fb172e0';
        $.ajax({
            url: 'https://api.instagram.com/v1/users/self',
            dataType: 'jsonp',
            type: 'GET',
            data: {access_token: token},
            success: function(data){
                var follows = data['data']['counts']['followed_by'];
                $(".ig_follower span").text(follows);
            },
            error: function(data){
                console.log(data);
            }
        });
        twitterusername = 'BalaiUji_KA';
        $.getJSON('http://twitter.com/users/'+twitterusername+'.json?callback=?', function(data){
            $('.twitter_follower').html('Twitter Followers : ' + data.followers_count + '');
        });

        $(function() {

				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }

				  //Just put your channel name after users
				xmlhttp.open("GET","https://gdata.youtube.com/feeds/api/users/BasmetAmalOfficial",false);
				xmlhttp.send();
				xmlDoc=xmlhttp.responseXML;

				// console.log(xmlDoc.getElementsByTagName("statistics")[0].getAttribute("subscriberCount"));


				});

				$.getJSON("https://www.googleapis.com/youtube/v3/playlistItems", {
            part: "snippet",
            maxResults: 8,
            playlistId: "PLPtB8UCwh2mWxWBkQjlpwFH6wSD8YWtNv",
            key:'AIzaSyDu7UYtbh63FiVADoXDJ6B-DewCzHW5Fhw'},
            function(data) {
                // console.log(JSON.stringify(data));
                var output;
                $.each(data.items, function(i, item) {
                    videoTitle = item.snippet.title;
                    videoThumb = item.snippet.thumbnails.default;
                    output = "<li>"
                                +"<a href='https://www.youtube.com/watch?v="+item.snippet.resourceId.videoId+"' target='_newtab'>"
                                    +"<img src='"+videoThumb.url+"' />"
                                +"</a>"
                            +"</li>";
                    $("#playListContainer").append(output);
                });
            }
        );


    </script>
@endsection
