@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | Home
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('script_awal')
@endsection

@section('banner')
<section class="djka-hero">
  <div class="djka-home-slides" data-djka-home-slides>
 	@foreach ($highlight as $data)
		<div class="djka-home-slide">
		  <div class="djka-home-slide-img" style="background-image: url({{asset('/public/images/highlight/'.$data->picture)}})"></div>
		  <div class="djka-home-slide-title">{!! @$data->title !!}</div>
		  <div class="djka-home-slide-title">{!! @$data->highlight !!}</div>
		</div>
	@endforeach
  </div>
</section>
@endsection

@section('content')
<section class="djka-section-welcome djka-align-center">
  <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
  <div class="container">
  @if (@$page->title)
		<h2 class="djka-title-1 djka-color-white">{!! $page->title !!}</h2>
	@else
		<h2 class="djka-title-1 djka-color-white">SELAMAT DATANG</h2>
		<h3 class="djka-title-2 djka-color-white">DI BALAI PENGUJIAN PERKERETAAPIAN</h3>
	@endif
	@if (@$page->content)
		<div class="djka-richtext">{!! $page->content !!}</div>
	@else
  <div class="djka-fontsize-medium">
	<p>Dalam rangka melakukan pengendalian dan pengawasan terhadap kualitas sarana dan prasarana perkeretaapian serta sumber daya manusia perkeretaapian</p>
	<p>kegiatan pengujian merupakan bagian dari kegiatan untuk memberikan jaminan keselamatan operasi kereta api. Pengujian dilakukan pada semua sistem perkeretaapian mteliputi prasarana, sarana dan sumber daya manusia perkeretaapian. Kegiatan pengujian tersebut dimaksudkan untuk meyakinkan bahwa semua aspek kegiatan yang berkaitan dengan operasi kereta api telah laik operasi.</p>
  </div>
	@endif
  <br>
  <p><a href="http://simadu.dephub.go.id/" target="_blank" rel="noopener"><strong><img style="display: block; margin-left: auto; margin-right: auto;" src="{{asset('/public/images')}}/simadu.png" alt="simadu.dephub.go.id" width="600" height="61"></strong></a><h3 style="text-align: center; color: #440764;"><strong>BANTU KAMI TETAP KOMITMEN MENJADI&nbsp;PROFESIONAL.&nbsp;</strong><strong><span style="color: #b30d0d;">LIHAT. LAWAN. LAPORKAN!</span> SETIAP TINDAKAN KAMI YANG MELANGGAR HUKUM.</strong></h3></p>
  </div><!-- .container -->
</section><!-- .djka-section-welcome -->


<section class="djka-section-highlights">
  <img class="curve" data-top=emboss data-bottom=emboss data-fill=#E2E2E3>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <a href="pengujian-sdm.html" class="djka-highlight">
          <div class="djka-highlight-img"><img src="{{asset('/public/uploads/1a.png')}}" alt=""></div>
          <h2 class="djka-highlight-title"><span>PENGUJIAN SDM</span></h2>
          <div class="djka-highlight-desc">
		  {{ $pref->teks1 }}
          </div>
        </a>
      </div><!-- .col -->
      <div class="col-md-4">
        <a href="pengujian-sdm-2.html" class="djka-highlight">
          <div class="djka-highlight-img"><img src="{{asset('/public/uploads/1b.png')}}" alt=""></div>
          <h2 class="djka-highlight-title"><span>PENGUJIAN</span></h2>
          <div class="djka-highlight-desc">
          {{ $pref->teks2 }}
          </div>
        </a>
      </div><!-- .col -->
      <div class="col-md-4">
        <a href="#" class="djka-highlight">
          <div class="djka-highlight-img"><img src="{{asset('/public/uploads/1c.png')}}" alt=""></div>
          <h2 class="djka-highlight-title"><span>PENGUJIAN SARANA</span></h2>
          <div class="djka-highlight-desc">
          {{ $pref->teks3 }}
          </div>
        </a>
      </div><!-- .col -->
    </div><!-- .row -->
  </div><!-- .container -->
</section><!-- .djka-section-highlights -->

<section class="djka-section-berita">
  <img src="{{asset('/public/images/ribbon-2.png')}}" alt="" class="curve" data-fill=#EBEBEC data-top=emboss data-bottom=emboss>
  <div class="djka-section-inner">
    <h2 class="djka-section-title">BERITA TERKINI</h2>
    <div class="container">
      <div class="djka-berita-carousel" data-carousel-1>
	  @foreach ($news as $berita)
        <a href="{!! route('web.detberita', ['slug' => $berita->slug]) !!}" class="djka-berita-item">
          <div class="djka-berita-item-pic"><img src="{{ ($berita->picture) ? asset('/public/images/news/'.$berita->picture) : asset('/public/images/imgthumb.png') }}" alt=""><span>{{ $berita->title }}</span></div>
          <div class="djka-berita-item-text">
            <div class="djka-berita-item-date">{{App\Http\Controllers\webController::indonesian_date ($berita->publish_date , 'd F Y') }}</div>
          <h3 class="djka-berita-item-title">{{ $berita->title }}</h3>
          <div class="djka-berita-item-desc">{{ $berita->lead }}</div>
          
          </div>
		</a>
	@endforeach
      </div><!-- .djka-berita-carousel -->
    </div><!-- .container -->
  </div><!-- .djka-section-inner -->
</section><!-- .djka-section-berita -->


<section class="djka-section-pengumuman">
  <img src="{{asset('/public/images/ribbon-3.png')}}" data-bottom=emboss class="curve" data-fill=#756aa3 />
  <div class="djka-section-inner">
    <h2 class="djka-section-title">PENGUMUMAN</h2>
    <div class="container">
    <div class="djka-pengumuman-carousel" data-carousel-pengumuman>
      <div class="djka-pengumuman-group">
	<?php $x=0; ?>
	@foreach ($pengumuman as $pengumumans)
	<?php $x++; ?>
	<a class="djka-pengumuman" href="{!! route('web.detpengumuman', ['slug' => $pengumumans->slug]) !!}">
		<div class="djka-pengumuman-pic"><img src="{{ ($pengumumans->picture) ? asset('/public/images/pengumuman/'.$pengumumans->picture) : asset('/public/images/imgthumb.png') }}" alt=""></div>
        <div class="djka-pengumuman-content">
			<h3 class="djka-pengumuman-title">{{ $pengumumans->title }}</h3>
			<div class="djka-pengumuman-date">{{App\Http\Controllers\webController::indonesian_date ($pengumumans->publish_date , 'd F Y') }}</div>
        </div>
	</a>
	<?php if($x == 4){ ?>
	</div>
	<div class="djka-pengumuman-group">
	<?php $x=0;} ?>
	  @endforeach
      </div>	
    </div><!-- .djka-pengumuman-carousel -->
  </div><!-- .container -->
  </div><!-- .djka-section-inner -->
</section><!-- .djka-section-pengumuman -->

<section class="djka-section-social">
  <div class="container">
    <div class="row">
      <div class="col-md-4 djka-socialfeed">
        <div class="djka-socialfeed-inner">
          <div class="djka-socialfeed-title"><i class="fab fa-twitter"></i> @BalaiUji_KA</div>
          <div class="djka-socialfeed-embed"><a class="twitter-timeline" href="https://twitter.com/BalaiUji_KA?ref_src=twsrc%5Etfw">Tweets by BalaiUji_KA</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> </div>
        </div><!-- .djka-socialfeed-inner -->
      </div><!-- .col -->
      <div class="col-md-4 djka-socialfeed">
        <div class="djka-socialfeed-inner">          
          <div class="djka-socialfeed-title"><i class="fab fa-instagram"></i> @balaipengujianka</div>
          <div class="djka-socialfeed-embed">
            <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/B3GmbabJ7sc/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/B3GmbabJ7sc/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div></a> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/B3GmbabJ7sc/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">Pengakuan batik sebagai warisan Dunia berlaku sejak Badan PBB untuk Pendidikan, Keilmuan, dan Kebudayaan atau UNESCO, menetapkan batik sebagai Warisan Kemanusiaan untuk Budaya Lisan dan Nonbendawi (Masterpieces of the Oral and the Intangible Heritage of Humanity) pada 2 Oktober 2009. . Selamat Hari Batik Nasional 2019 #balaipengujianperkeretaapian #DJKA #Kemenhub #PenghubungIndonesia #HariBatikNasional2019 @kemenhub151 @ditjenperkeretaapian @ppidkemenhub</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by <a href="https://www.instagram.com/balaipengujianka/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> Balai Pengujian Perkeretaapian</a> (@balaipengujianka) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2019-10-02T04:54:09+00:00">Oct 1, 2019 at 9:54pm PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
          </div>
          
        </div><!-- .djka-socialfeed-inner -->
      </div><!-- .col -->
      <div class="col-md-4 djka-socialfeed">
        <div class="djka-socialfeed-inner">
          <div class="djka-socialfeed-title"><i class="fab fa-youtube"></i>Balai Pengujian Perkeretaapian </div>
          <div class="djka-socialfeed-embed">
            <iframe width="348" height="390" src="https://www.youtube.com/embed/videoseries?list=PLkIixIwuDO7RPWeMvgYAf9Mut5PPslBM0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        </div><!-- .djka-socialfeed-inner -->
      </div><!-- .col -->
    </div><!-- .row -->
  </div><!-- .container -->
</section><!-- .djka-section-social -->

<section class="djka-section-kerjasama">
  <img class="curve" data-top=bevel data-fill=#EBEBEC>
  <h2 class="djka-section-title">MITRA</h2>
  <div class="djka-hspace-medium"></div>
  <div class="container">
    <div class="djka-partner-carousel" data-djka-partner-carousel>
      @foreach ($mitra as $mitras)
      <div class="djka-partner-logo"><img src="{{asset('/public/images/mitra/'.$mitras->picture)}}"  title="{{ $mitras->title}}" alt=""></div>
      @endforeach
    </div><!-- .djka-partner-carousel -->
  
  </div><!-- .container -->
</section><!-- .djka-kernasama -->
@if ($codepage == 'I01')
		 @if ($pref->popup_active)
			 <div class="modal fade" id="promo-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	       <div class="modal-dialog" role="document">
	           <div class="modal-content" style="margin-top:90px;">
	               <button type="button" class="close" style="position:absolute;top:-35px;right:-35px;background: #f7be11;width:30px;height:30px;opacity:1;border-radius: 30px;" data-dismiss="modal" aria-label="Close">
	                   <span aria-hidden="true">&times;</span>
	               </button>
	               <div class="modal-body" style="padding:0;">
	                   @if ($pref->popup_url)
											 <a href="{{ $pref->popup_url }}" target="_blank"><img class="img-responsive" src="{{ asset('/public/images/popup/'.$pref->popup_image) }}"></a>
										 @else
											 <img class="img-responsive" src="{{ asset('/public/images/popup/'.$pref->popup_image) }}">
										 @endif
	               </div>
	           </div>
	       </div>
	   </div>
	 	@endif
	 @endif
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
<script type="text/javascript">
  window.onload = function () {

  @if ($pref->popup_active)
    $('#promo-modal').modal('show');
    // setTimeout(function(){ $('#promo-modal').modal('hide'); }, 15000);
  @endif
  }
</script>
@endsection
