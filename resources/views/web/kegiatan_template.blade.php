@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | Kegiatan
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="banner">
			<h1><span>{{ (@$page->highlight) ? $page->highlight : "Kegiatan Internal" }}</span></h1>
			<img src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-news.jpg')}}">
	</div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="content row">
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	        <div class="col-md-10">
	            <h1 class="content-title text-uppercase">@if (@$page->title)
								{!! $page->title !!}
							@else
								Kegiatan Internal
							@endif</h1>
							<div class="content-area">
							@if (@$page->content)
								{!! $page->content !!}

							@endif

	            <br><br>
							<div class="row">

							@foreach ($berita as $news)
								<div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                  <img src="{{asset('/public/images/event/'.$news->thumbnail)}}">
                  <div class="caption">
                    <h3>{{ $news->title }}</h3>
										<small class="date">{{App\Http\Controllers\webController::indonesian_date ($news->publish_date , 'd F Y') }}</small>
                    <p>{{ $news->lead }}</p>
                    <a href="{!! route('web.detevent', ['slug' => $news->slug]) !!}" class="btn" role="button">SELENGKAPNYA</a>
                  </div>
                </div>
              </div>

							@endforeach

						</div>
	        </div>
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	    </div>
		</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
