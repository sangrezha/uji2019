@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | Kegiatan - {{ $news->title }}
@endsection

@section('metadescription')
	{{ $news->lead }}
@endsection

@section('banner')
	<div class="banner">
			<h1><span>{{ (@$page->highlight) ? $page->highlight : "Kegiatan Internal" }}</span></h1>
			<img src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-news.jpg')}}">
	</div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="content row">
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	        <div class="col-md-10">
	            <h1 class="content-title text-uppercase">kegiatan internal <a href="{{ url()->previous() }}" class="other">Kegiatan Lainnya</a></h1>
							<div class="content-area">
								<h3 class="news-title">{{ $news->title }}</h3>
	              <small class="date">{{App\Http\Controllers\webController::indonesian_date ($news->publish_date , 'd F Y') }}</small>
	              <br>
	              <br>
	              <img class="img-responsive" src="{{asset('/public/images/event/'.$news->picture)}}">
	              <br>
	              {!! $news->content !!}

	            <br><br>
	        </div>
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	    </div>
		</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
