<title>DIREKTORAT JENDERAL PERKERETAAPIAN | Kementrian Perhubungan Republik Indonesia</title>
	<link href="{{ asset('/public/assets/mystyle.css') }}" rel="stylesheet" type="text/css">
	 <link rel="shortcut icon" href="{{ asset('/public/assets/images/icon_logo.png') }}"/>
    
    <style type="text/css">

.content-kontak { height:433px; width:1263px; margin:auto; position:relative; background-image:url({{asset('/public/assets/images/kontak_02.jpg')}}); }

.isi-kontak { height:340px; width:660px; float:right; position:relative; background:none; font-family: 'Web-Source-Sans'; font-size:14px;  color:#666; padding-right:50px; margin-top:5px; }

.isi-kontak #alamat { font-size:16px; font-weight: normal; line-height:20px; }
.ganjel { height:16px; width:1000px; margin:auto; position:relative; }
.judul { font-family: 'Web-OpenSans-Semibold'; font-size:30px; color:black; margin-left:550px; line-height:10px; }

.top-kontak { height:64px; text-align:right; margin: auto; width:1263px;  position: relative; background:url({{ asset('/public/assets/images/kontak_01.jpg') }}) }

.foot-kontak { height:96px; background:url({{asset('/public/assets/images/foot-kontak.jpg')}}); width:1203px; margin:auto; position:relative; padding-top:0px; padding-left:60px; font-family: 'Web-Source-Sans'; font-size:14px;  color:white;}

</style> 
    
</head>

<body >
<div class="top-kontak">
<br>
<div id="atas">
	<a href="{{url('/homelogin')}}" class="tombol">LOGIN</a>
		<a href="{{url('/registrasi')}}" class="tombol">Registrasi</a>
          
         
            <form>
  <input class="search" type="text" placeholder="SEARCH" required>	
  <input class="button"  name="imageField" type="image" src="{{asset('/public/assets/images/search_icon.jpg')}}">	
</form>
   </div>
</div>

<div class="menu">
 <div id="kanan_menu">
    	<ul>
    	  <li id="home"><a href="{{url('/')}}"><img src="{{asset('/public/assets/images/home_icon.png')}}"></a></li>
    	  <li id="profile"><a href="{{url('/profile')}}">PROFILE</a></li>
            <li id="regulasi"><a href="{{url('/regulasi')}}">REGULASI PERIZINAN</a></li>
            <li id="perizinan"><a href="{{url('/perizinan')}}">SYARAT PERIZINAN</a></li>
            <li id="badanusaha"><a href="#">BADAN USAHA</a></li>
            <li id="kontak"><a href="#">KONTAK</a></li>
        </ul>
    
    </div>
</div>

<div class="content-kontak">
<div class="ganjel"></div>
<h1 class="judul">HUBUNGI KAMI</h1>

<div class="isi-kontak">

<div id="alamat">
Alamat : Jl. Medan Merdeka Barat No. 8 Jakarta Pusat<br>
Telp. (021) 3852649, 3456779 &nbsp;  | &nbsp; Fax.  (021) 3451657<br>
website.	 <a href="http://www.dephub.go.id " target="_parent">http://www.dephub.go.id</a><br>
</div>
<br>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.6735978236024!2d106.82009131446621!3d-6.1744350455298465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5d4f2d3531d%3A0x92487be91791acae!2sDirektorat+Jenderal+Perhubungan+Laut!5e0!3m2!1sen!2sid!4v1495792263931" width="660" height="260" frameborder="0" style="border:0" allowfullscreen></iframe> 


</div>

 </div>

<div class="foot-kontak">
<br>
	© Copyright 2017<br>
Direktorat Jenderal Perkeretaapian  |  Kementerian Perhubungan Republik Indonesia
    
</div>





</body>
</html>
