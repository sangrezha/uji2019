<!doctype html>
<html lang="en">
<head>
	@include('web.head_template')
</head>
<body>
	{{ $pref->konten_body_t }}
	@yield('script_awal')
	@include('web.header_template')
	@yield('banner')

	@yield('content')

	@include('web.footer_template')
	@yield('script_end')
	{{ $pref->konten_body_b }}
</body>
</html>
