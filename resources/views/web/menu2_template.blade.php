<div class="col-lg-5 col-md-6">
		<div class="secondary-mn row" align="right">
				<div class="col-md-8 nopadding">
						<ul>
								<li><a href="{{ route('web.pengumuman') }}" class="{{ (str_contains($codepage, 'I10')) ? 'active': '' }}">PENGUMUMAN </a></li>
								<li>|</li>
								<li><a href="{{ route('web.berita') }}" class="{{ (str_contains($codepage, 'I07')) ? 'active': '' }}">BERITA </a></li>
								<li>|</li>
								<li><a href="{{ route('web.kontak') }}" class="{{ (str_contains($codepage, 'I08')) ? 'active': '' }}">KONTAK </a></li>
						</ul>
				</div>
				<div class="col-md-4">
						<form action="{{ route('web.search') }}" method="post">
							{{ csrf_field() }}
							<div  class="form-group {{ ($errors->has('keyword')) ? 'has-error' : '' }}">
							<input type="text" name="keyword" placeholder="Search">
							<input type="submit" value="">
						</div>
						</form>
				</div>
		</div>
</div>
