<ul class="djka-menu" data-djka-menu>
	<li><a class="{{ (str_contains($codepage, 'I01')) ? 'is-active': '' }}" href="{{ route('web.home') }}">HOME</a></li>
	<li>
	<a class="{{ (str_contains($codepage, 'I02')) ? 'is-active': '' }}" href="#">TENTANG BALAI</a>
	<ul>
		<li><a  class="{{ (str_contains($codepage, 'I021')) ? 'is-active': '' }}" href="{{ route('web.profil1') }}">Profil</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I023')) ? 'is-active': '' }}" href="{{ route('web.profil3') }}">Penjelasan Tupoksi</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I024')) ? 'is-active': '' }}" href="{{ route('web.regulasi') }}">Regulasi</a></li>
	</ul>
	</li>
	<li><a class="{{ (str_contains($codepage, 'I03')) ? 'is-active': '' }}" href="{{ route('web.pembinaan') }}">PEMBINAAN & PENGEMBANGAN</a></li>
	<li>
	<a class="{{ (str_contains($codepage, 'I041') || str_contains($codepage, 'I042') || str_contains($codepage, 'I047')) ? 'is-active': '' }}" href="#">PENGUJIAN SDM</a>
	<ul>
		<li><a  class="{{ (str_contains($codepage, 'I041')) ? 'is-active': '' }}"  href="{{ route('web.ujisdm1') }}">INFO DAN PERSYARATAN</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I042')) ? 'is-active': '' }}"  href="{{ route('web.ujisdm2') }}">SOP</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I047')) ? 'is-active': '' }}"  href="{{ route('web.ujisdm7') }}">FASILITAS PENGUJIAN</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I046')) ? 'is-active': '' }}"  href="{{ route('web.ujisdm6') }}">HASIL PENGUJIAN</a></li>
	</ul>
	</li>
	<li>
	<a class="{{ (str_contains($codepage, 'I061') || str_contains($codepage, 'I064') || str_contains($codepage, 'I066')) ? 'is-active': '' }}" href="#">PENGUJIAN SARANA</a>
	<ul>
		<li><a  class="{{ (str_contains($codepage, 'I061')) ? 'is-active': '' }}"  href="{{ route('web.ujisarana1') }}">INFO DAN PERSYARATAN</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I064')) ? 'is-active': '' }}"  href="{{ route('web.ujisarana4') }}">LOKASI PENGUJIAN</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I066')) ? 'is-active': '' }}"  href="{{ route('web.ujisarana6') }}">ALAT PENGUJIAN</a></li>
	</ul>
	</li>
	<li>
	<a class="{{ (str_contains($codepage, 'I051') || str_contains($codepage, 'I055') || str_contains($codepage, 'I057') || str_contains($codepage, 'I058')) ? 'is-active': '' }}" href="#">PENGUJIAN PRASARANA</a>
	<ul>
		<li><a  class="{{ (str_contains($codepage, 'I051')) ? 'is-active': '' }}"  href="{{ route('web.ujiprasarana1') }}">INFO DAN PERSYARATAN</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I052')) ? 'is-active': '' }}"  href="{{ route('web.ujiprasarana2') }}">SOP</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I057')) ? 'is-active': '' }}"  href="{{ route('web.ujiprasarana7') }}">ALAT PENGUJIAN</a></li>
		<li><a  class="{{ (str_contains($codepage, 'I058')) ? 'is-active': '' }}"  href="{{ route('web.ujiprasarana8') }}">Permohonan Pengujian </a></li>
	</ul>
	</li>
	<li><a class="{{ (str_contains($codepage, 'I09')) ? 'is-active': '' }}"  href="{{ route('web.pengumuman') }}">PENGUMUMAN</a></li>
	<li><a class="{{ (str_contains($codepage, 'I07')) ? 'is-active': '' }}"  href="{{ route('web.berita') }}">BERITA</a></li>
	<li><a class="{{ (str_contains($codepage, 'I12')) ? 'is-active': '' }}"  href="{{ route('web.wikiuji') }}">WIKIUJI</a></li>
	<!-- <li><a class="{{ (str_contains($codepage, 'I024')) ? 'is-active': '' }}"  href="#">WIKIUJI</a></li> -->
	<li><a class="{{ (str_contains($codepage, 'I08')) ? 'is-active': '' }}"  href="{{ route('web.kontak') }}">KONTAK</a></li>
</ul>
