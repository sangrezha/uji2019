@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'INFO PEMBINAAN DAN PENGEMBANGAN' }}

@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-sdm.jpg')}})"></div>
  <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>{!! (@$page->title) ? $page->title : "Pembinaan dan Pengembangan" !!} </h1>     
   </div>
</section>

@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="page-content">
        <div class="djka-hspace-big"></div>
		<div class="container">

        <div class="col-md-12">
						@if (@$page->content)
						{!! @$page->content !!}
						@endif
        </div>
			</div>
    </div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
