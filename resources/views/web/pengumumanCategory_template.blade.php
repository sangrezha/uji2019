	<ul>
		@foreach(config('app.pengumumanCategory') as $k=>$v)
			<!-- <option value="{{ $k }}"  {{ (str_contains(old('category'), $k)) ? "selected" : ''}}>{{ $v }}</option> -->
			<li><a href="{{ route('web.pengumuman') }}?cat={{ $k }}" class="{{ (Input::get('cat') == $k || (Null == Input::get('cat') && $k==1)) ? 'active': '' }}">{{ $v }}</a></li>
		@endforeach
	</ul>
