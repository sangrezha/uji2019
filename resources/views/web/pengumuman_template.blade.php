@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'Pengumuman' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-news.jpg')}})"></div>
  <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>{!! (@$page->title) ? $page->title : "Pengumuman" !!} </h1>     
   </div>
</section>
@endsection


@section('script_awal')
	<link rel="stylesheet" href="{{ asset("/public/plugins/datatables/dataTables.bootstrap.css") }}">
@endsection

@section('content')
<section class="djka-section-grey">
<div class="container-fluid has-large-padding">
    <div class="djka-funky">
        <div class="djka-funky-menu" data-djka-funky-menu>
            <a href="#" class="djka-funky-toggle" data-djka-funky-toggle></a>      
            @include('web.pengumumanCategory_template')
        </div><!-- .djka-funky-menu -->
        <div class="djka-funky-content">
            <!-- <h1 class="text-uppercase"><span>{{ (@$page->title) ? $page->title : "Info Persyaratan" }}</span></h1> -->
            @if (@$page->content)
            <div class="djka-richtext">{!! @$page->content !!}</div>
            @endif
				<div class="djka-news-list">
					@foreach ($berita as $news)
						<a href="{!! route('web.detpengumuman', ['slug' => $news->slug]) !!}" class="djka-news-item">
							<h5 class="djka-news-item-title">{{ $news->title }}</h5>
							<span class="djka-news-item-date">{{App\Http\Controllers\webController::indonesian_date ($news->publish_date , 'd F Y') }}</span>
							<span class="djka-news-item-category" >{{config('app.pengumumanCategory')[$news->category] }}</span>
							<div class="djka-news-item-lead">{{ $news->lead }}</div>
						</a>
					@endforeach										
				</div><!-- .djka-news-list -->
				<div class="djka-news-list-footer">
				{{ $berita->appends(['cat' => Null !== Input::get('cat')?Input::get('cat'):1])->links() }}
				</div>
        </div><!-- .djka-funky-content -->
    </div><!-- .djka-funky -->
</div><!-- .container-fluid -->
</section>

@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
