<title>DIREKTORAT JENDERAL PERKERETAAPIAN | Kementrian Perhubungan Republik Indonesia</title>
<link rel="stylesheet" type="text/css" href="{{ asset('/public/assets/mystyle.css') }}">
<link rel="shortcut icon" href="{{ asset('/public/assets/images/icon_logo.png')}}"/>
    
<style type="text/css">

.content-syarat { height:433px; width:1263px; margin:auto; position:relative; background-image:url( {{asset('/public/assets/images/syarat_02.jpg')}}); }
#isi-syarat { height:340px; width:670px; float:right; position:relative; background:none; font-family: 'Web-Source-Sans'; font-size:14px;  color:#666; padding-right:50px; margin-top:5px; }
.submenu_syarat
		{ width:740px; height:34px; background:#CCC; color:white; font-family: 'Web-OpenSans-Semibold'; 
		font-size:14px;  float:right; position:relative; padding-top:18px; padding-left:20px;  } 
.submenu_syarat a:hover { background:#666; color: #FFF; font-family: 'Web-OpenSans-Semibold'; 
		font-size:14px; padding:18 25 15 25px; }

.submenu_syarat a { color:white; text-decoration:none; font-family: 'Web-OpenSans-Semibold'; 
		font-size:14px; padding:18 25 15 25px;  }

.ganjel { height:16px; width:1000px; margin:auto; position:relative; }
.judul { font-family: 'Web-OpenSans-Semibold'; font-size:30px; color:black; margin-left:550px; line-height:10px; }

   
</style> 
    
</head>

<body >
<div class="top-header2">
	<br>
	<div id="atas">
		<a href="{{url('/homelogin')}}" class="tombol">LOGIN</a>
		<a href="{{url('/registrasi')}}" class="tombol">Registrasi</a>

		<form>
			<input class="search" type="text" placeholder="SEARCH" required>	
			<input class="button"  name="imageField" type="image" src="{{asset('/public/assets/images/search_icon.jpg')}}">	
		</form>
   </div>
</div>

<div class="menu">
 <div id="kanan_menu">
    	<ul>
        	<li id="home"><a href="{{url('/')}}"><img src="{{asset('/public/assets/images/home_icon.png')}}"></a></li>
            <li id="profile"><a href="{{url('/profile')}}">PROFIL</a></li>
            <li id="regulasi"><a href="{{ url('/regulasi') }}">REGULASI PERIZINAN</a></li>
            <li id="perizinan"><a href="#">PERSYARATAN PERIZINAN</a></li>
            <li id="badanusaha"><a href="#">BADAN USAHA</a></li>
            <li id="kontak"><a href="{{ url('/kontak') }}">KONTAK</a></li>
        </ul>
    
    </div>
</div>

<div class="content-syarat">
<div class="ganjel"></div>
<h1 class="judul">PERKERETAAPIAN UMUM</h1>
<div class="submenu_syarat">
<a href="#" target="_parent" class="submenunya">PERIZINAN PRASARANA</a>
<a href="#" target="_parent" class="submenunya">PERIZINAN SARANA</a>
</div>
<div id="isi-syarat">


<div align="justify">
<p><strong>Syarat Izin Operasi sesuai dengan PP 6/2017:</strong></p>
<ul>
	<li>1. Memiliki studi kelayakan</li>
	<li>2. Memiliki paling sedikit 2 (dua) rangkaian kereta api sesuai dengan spesifikasi teknis sarana perkeretaapian;</li>
	<li>3. Sarana perkeretaapian yang akan dioperasikan telah lulus uji pertama yang dinyatakan dengan sertifikat uji pertama</li>
	<li>4. Tersedianya awak sarana perkeretaapian yang memiliki sertifikat kecakapan, serta tenaga perawatan, dan tenaga pemeriksa sarana perkeretaapian yang memiliki sertifikat keahlian</li>
	<li>5. Memiliki sistem dan prosedur pengoperasian, pemeriksaan, dan perawatan sarana perkeretaapian</li>
	<li>6. Menguasai fasilitas perawatan sarana perkeretaapian</li>
	<li>7. Lintas pelayanan telah ditetapkan oleh Menteri, gubernur atau bupati/walikota sesuai dengan kewenangannya</li>
	<li>8. Membuat dan melaksanakan sistem manajemen keselamatan</li>
</ul>

</div>
</div>

 </div>

<div class="foot-syarat">

	© Copyright 2017<br>
Direktorat Jenderal Perkeretaapian  |  Kementerian Perhubungan Republik Indonesia
    
</div>





</body>
</html>
