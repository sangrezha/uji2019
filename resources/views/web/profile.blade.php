<title>DIREKTORAT JENDERAL PERKERETAAPIAN | Kementrian Perhubungan Republik Indonesia</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/public/assets/mystyle.css') }}">
	<link rel="shortcut icon" href="{{ asset('/public/assets/images/icon_logo.png')}}"/>
    
 <style type="text/css">
.content-profil { height:433px; width:1263px; margin:auto; position:relative; background-image:url({{asset('/public/assets/images/profile_02.jpg')}}); }
</style> 
    
</head>

<body >

<div class="top-header2">
	<br>
	<div id="atas">
		<a href="{{url('/homelogin')}}" class="tombol">LOGIN</a>
		<a href="{{url('/registrasi')}}" class="tombol">Registrasi</a>

		<form>
			<input class="search" type="text" placeholder="SEARCH" required>	
			<input class="button"  name="imageField" type="image" src="{{asset('/public/assets/images/search_icon.jpg')}}">	
		</form>
   </div>
</div>








<div class="menu">
 <div id="kanan_menu">
    	<ul>
        	<li id="home"><a href="{{url('/')}}"><img src="{{asset('/public/assets/images/home_icon.png')}}"></a></li>
            <li id="profile"><a href="#">PROFIL</a></li>
            <li id="regulasi"><a href="{{ url('/regulasi') }}">REGULASI PERIZINAN</a></li>
            <li id="perizinan"><a href="{{url('/perizinan')}}">PERSYARATAN PERIZINAN</a></li>
            <li id="badanusaha"><a href="#">BADAN USAHA</a></li>
            <li id="kontak"><a href="{{ url('/kontak') }}">KONTAK</a></li>
        </ul>
    
    </div>
</div>

<div class="content-profil">

<div id="isi">
<h1>SELAMAT DATANG</h1>
<h2>DI SISTEM PERIZINAN ONLINE PERKERETAAPIAN</h2>

<div align="justify">
<p><strong>VISI</strong><br>
Mewujudkan eksistensi sebagai regulator dan penyelenggaraan perkeretaapian multioperator guna terselenggaranya pelayanan angkutan kereta api secara massal yang menjamin keselamatan, aman, nyaman, cepat dan lancar, tertib dan teratur, efisien, terpadu dengan moda transportasi lain, serta menunjang pemerataan, pertumbuhan, stabilitas, pendorong, dan penggerak pembangunan nasional.</p>
<p><strong>MISI</strong>
<ul>
1. Meningkatkan peran Pemerintah sebagai regulator penyelenggaraan perkeretaapian. <br>
2. Mewujudkan penyelenggaraan perkeretaapian multioperator dengan peningkatan peran Pemerintah Daerah dan swasta. <br>
3. Meningkatkan peran Kereta Api sebagai angkutan publik. <br>
4. Meningkatkan peran Kereta Api sebagai tulang punggung angkutan barang. <br>
5. Meningkatkan peran Kereta Api sebagai pelopor terciptanya angkutan terpadu.
</ul>
</p>

<p><strong>TUGAS POKOK</strong><br>
Merumuskan serta melaksanakan kebijakan dan standarisasi teknis di bidang perkeretaapian.</p>


</div>
</div>

 </div>

<div class="foot-prof">

	© Copyright 2017<br>
Direktorat Jenderal Perkeretaapian  |  Kementerian Perhubungan Republik Indonesia
    
</div>





</body>
</html>
