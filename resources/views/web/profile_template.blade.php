@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | Profil
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('bgnya')
		<div class="wrapall" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/images/profile.jpg')}});">
@endsection

@section('hari_ini')
	{{ $hariini }}
@endsection

@section('script_awal')
@endsection

@section('content')
	<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-5 nopadding">
						<div class="thumbnail judul">
							PROFIL
						</div>
					</div>
					<div class="col-md-7">
						<div class="thumbnail isikonten">
							<h1 class="title-page">
								@if (@$page->title)
									{!! $page->title !!}
								@else
									Selamat datang<span>di sistem perizinan online perkeretaapian</span>
								@endif
								</h1>
							<div class="content-area">
								@if (@$page->content)
									{!! $page->content !!}
								@else
									<div align="justify">
										<p><strong>VISI</strong><br>
										Mewujudkan eksistensi sebagai regulator dan penyelenggaraan perkeretaapian multioperator guna terselenggaranya pelayanan angkutan kereta api secara massal yang menjamin keselamatan, aman, nyaman, cepat dan lancar, tertib dan teratur, efisien, terpadu dengan moda transportasi lain, serta menunjang pemerataan, pertumbuhan, stabilitas, pendorong, dan penggerak pembangunan nasional.</p>
										<p><strong>MISI</strong>
										</p>
										1. Meningkatkan peran Pemerintah sebagai regulator penyelenggaraan perkeretaapian.<br>
										2. Mewujudkan penyelenggaraan perkeretaapian multioperator dengan peningkatan peran Pemerintah Daerah dan swasta. <br>
										3. Meningkatkan peran Kereta Api sebagai angkutan publik. <br>
										4. Meningkatkan peran Kereta Api sebagai tulang punggung angkutan barang. <br>
										5. Meningkatkan peran Kereta Api sebagai pelopor terciptanya angkutan terpadu.<br><br>
										<p><strong>TUGAS POKOK</strong><br>
										Merumuskan serta melaksanakan kebijakan dan standarisasi teknis di bidang perkeretaapian.</p>
									</div>

								@endif

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
