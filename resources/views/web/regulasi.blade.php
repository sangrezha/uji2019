<title>DIREKTORAT JENDERAL PERKERETAAPIAN | Kementrian Perhubungan Republik Indonesia</title>
	<link href="{{ asset('/public/assets/mystyle.css') }}" rel="stylesheet" type="text/css">
	 <link rel="shortcut icon" href="{{ asset('/public/assets/images/icon_logo.png')}}"/>
    
    <style type="text/css">


.content-reg { height:433px; width:1263px; margin:auto; position:relative; background-image:url( {{ asset('/public/assets/images/reg_02.jpg') }}); }
.isi-reg { height:340px; width:690px; float:right; position:relative; background:none; font-family: 'Web-Source-Sans'; font-size:14px;  color:#666; padding-right:30px; margin-top:5px; }
.ganjel { height:16px; width:1000px; margin:auto; position:relative; }
.judul { font-family: 'Web-OpenSans-Semibold'; font-size:30px; color:black; margin-left:550px; line-height:10px; }
.top-reg{ height:64px; text-align:right; margin: auto; width:1263px;  position: relative; background:url( {{ asset('/public/assets/images/reg_01.jpg') }} )  }
	
	
.formbox { width:600px; margin-top:30px; border:none; text-align:right;}
.search2 {width:164px; height:39px; background:url({{ asset('/public/assets/images/bg_search_bwh.jpg') }}); padding-left:10px; padding-right:10px; border:none;float:left; position:relative; }
.select { height:36px; width:auto;  padding-left:10px; padding-right:10px; border:1px solid #036; border-bottom-left-radius:8px; border-top-left-radius:8px; border-bottom-right-radius:8px ; border-top-right-radius:8px ; float:left; position:relative;margin-left:10px; }

.submit-but { height:36px; width:auto;  padding-left:10px; padding-right:10px; border:1px solid #036; border-bottom-left-radius:8px;border-top-left-radius:8px; border-bottom-right-radius:8px ; border-top-right-radius:8px ; float:left; position:relative;margin-left:10px; }
.foot-reg { height:96px; background:url({{ asset('/public/assets/images/foot-reg.jpg') }});width:1203px; margin:auto; position:relative; padding-top:0px; padding-left:60px;font-family: 'Web-Source-Sans'; font-size:14px;  color:white;}

</style> 
    
</head>

<body >
<div class="top-reg">
<br>
<div id="atas">
	<a href="{{url('/homelogin')}}" class="tombol">LOGIN</a>
		<a href="{{url('/registrasi')}}" class="tombol">Registrasi</a>
          
         
            <form>
  <input class="search" type="text" placeholder="SEARCH" required>	
  <input class="button"  name="imageField" type="image" src="{{ asset('/public/assets/images/search_icon.jpg') }}">	
</form>
   </div>
</div>

<div class="menu">
 <div id="kanan_menu">
    	<ul>
    	  <li id="home"><a href="{{url('/')}}"><img src="{{ asset('/public/assets/images/home_icon.png') }}"></a></li>
    	  <li id="profile"><a href="{{url('/profile')}}">PROFILE</a></li>
            <li id="regulasi"><a href="#">REGULASI PERIZINAN</a></li>
            <li id="perizinan"><a href="{{url('/perizinan')}}">SYARAT PERIZINAN</a></li>
            <li id="badanusaha"><a href="#">BADAN USAHA</a></li>
            <li id="kontak"><a href="{{ url('/kontak') }}">KONTAK</a></li>
        </ul>
    
    </div>
</div>

<div class="content-reg">
<div class="ganjel"></div>
<h1 class="judul">PERUNDANG - UNDANGAN</h1>

<div class="isi-reg">
<iframe width="690" height="280" scrolling="yes" frameborder="0" src="{{url('/regulasi/table')}}"></iframe>

<!--
<form  class="formbox" >
  <input name="search" type="text" value="SEARCH" size="100" class="search2">
  <select  class="select" >
   <option>SEMUA JENIS PERATURAN</option>
   <option>PERATURAN MENTRI</option>
   <option>PERATURAN MENTRI</option>
</select >
  
  <input name="submit" type="button" value="SUBMIT" class="submit-but">
  
  
</form>
-->


</div>

 </div>

<div class="foot-reg">
<br>
	© Copyright 2017<br>
Direktorat Jenderal Perkeretaapian  |  Kementerian Perhubungan Republik Indonesia
    
</div>





</body>
</html>
