@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'Regulasi' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-profile.jpg')}})"></div>
  <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>{!! (@$page->title) ? $page->title : "Regulasi" !!} </h1>     
   </div>
</section>
@endsection


@section('script_awal')
	<link rel="stylesheet" href="{{ asset("/public/plugins/datatables/dataTables.bootstrap.css") }}">
@endsection

@section('content')
	<div class="page-content">
		<div class="container">
  <div class="col-md-12">
				<div class="djka-richtext">{!! @$page->content !!}</div><!-- .djka-richtext -->
       
				<table id="tableDataBrowse" class="table table-bordered table-striped" >
					<thead>
						<tr class="info">
							<td width="80">TAHUN</td>
							<td width="100">JUDUL</td>
							<td width="200">TENTANG</td>
							<td width="140">JENIS PERATURAN</td>
						</tr>
					</thead>
					<tbody>
					@foreach($dataList as $data)
						<tr>
							<td>{{ $data->tahun }}</td>
							<td><a href="{!! route('web.regulasidown', ['filenya' => $data->attachfile]) !!}" target="_blank">{{$data->title}}</a></td>
							<td>{{$data->tentang}}</td>
							<td>{{$data->name}}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
            
                 </div><!-- .col-md-12 -->
                 <div class="djka-hspace-medium"></div>
			</div><!-- .container -->
    </div><!-- .page-content -->

@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
<script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
  {{-- <script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script> --}}

	
  <!-- Regulasi Perizinan script -->
  <script>
    (function ($) {
    $("#tableDataBrowse").DataTable({
            "order": [[ 0, "desc" ]],
            "paging":   false,
            "info":     false
	});
    })(jQuery);
  </script>
   {{--  --}}
@endsection
