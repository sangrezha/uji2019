<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>REGULASI TABEL</title>

<style type="text/css">
<!-- 

@font-face {
    font-family: 'Web-Source-Sans';
    src: url('fonts/sourcesanspro-regular-webfont.eot');
    src: url('fonts/sourcesanspro-regular-webfontd41d.eot?#iefix') format('embedded-opentype'),
    url('fonts/sourcesanspro-regular-webfont.html') format('woff2'),
    url('fonts/sourcesanspro-regular-webfont.woff') format('woff'),
    url('fonts/sourcesanspro-regular-webfont.ttf') format('truetype'),
    url('fonts/sourcesanspro-regular-webfont.svg#source_sans_proregular') format('svg');
    font-weight: normal;
    font-style: normal;
	}
	
.tabel { width:640px; border:1px solid; }

	.tabel #tahun { width:60px;  background:#A8CFDB; font-family: 'Web-Source-Sans'; font-size:16px; color:#000; height:30px; }
	.tabel #judul { width:150px; background:#A8CFDB; font-family: 'Web-Source-Sans'; font-size:16px; color:#000; height:30px; }
	.tabel #tentang { width:280px; background:#A8CFDB; font-family: 'Web-Source-Sans'; font-size:16px; color:#000; height:30px; }
	.tabel #jenis {  width:150px; background:#A8CFDB; font-family: 'Web-Source-Sans'; font-size:16px; color:#000;  height:30px;}

	.tabel #color01 { background:#E4F0F3;  font-family: 'Web-Source-Sans'; font-size:15px; text-align:center;}
	.tabel #color02 { background:#F2F8F9; font-family: 'Web-Source-Sans'; font-size:15px;  text-align:center;}
-->
</style>
</head>

<body >

<table class="tabel" cellpadding="10">
<tr>
<th id="tahun">TAHUN</th>
<th id="judul">JUDUL</th>
<th id="tentang">TENTANG</th>
<th id="jenis">JENIS PERATURAN</th>

</tr>

<tr id="color01">
<td>2016</td>
<td><a href="{{url('/regulasi/download/KP 434 TAHUN 2016 IZIN OPERASI.pdf')}}" target="_blank">KP. No. 434</a></td>
<td>Izin Operasi Sarana Perkeretaapian Umum</td>
<td> Keputusan Menteri</td>
</tr>

<tr id="color02">
<td>2013</td>
<td><a href="{{url('/regulasi/download/pm_no_66_tahun_2013.pdf')}}" target="_blank">PM. No. 66</a></td>
<td>Perizinan Penyelenggaraan Prasarana Perkeretaapian Umum</td>
<td>Peraturan Menteri</td>
</tr>

<tr id="color01">
<td>2012</td>
<td><a href="{{url('/regulasi/download/PM 31 Tahun 2012 Perizinan Penyelenggaraan Sarana KA Umum.pdf')}}" target="_blank">PM. No. 31</a></td>
<td>Perizinan Penyelenggaraan Sarana Perkeretaapian Umum</td>
<td>Peraturan Menteri</td>
</tr>

<tr id="color02">
<td>2011</td>
<td><a href="{{url('/regulasi/download/pm_no_91_tahun_2011.pdf')}}" target="_blank">PM. No. 91</a></td>
<td>Penyelenggaraan Perkeretaapian Khusus</td>
<td>Peraturan Menteri</td>
</tr>
<!--
<tr id="color01">
<td>2014</td>
<td>KEMENHUB NO. XX1</td>
<td>Tarif Angkutan Lebaran</td>
<td> Keputusan Mentri</td>
</tr>

<tr id="color02">
<td>2014</td>
<td>KEMENHUB NO. XX1</td>
<td>Tarif Angkutan Lebaran</td>
<td> Keputusan Mentri</td>
</tr>

<tr id="color01">
<td>2014</td>
<td>KEMENHUB NO. XX1</td>
<td>Tarif Angkutan Lebaran</td>
<td> Keputusan Mentri</td>
</tr>

<tr id="color02">
<td>2014</td>
<td>KEMENHUB NO. XX1</td>
<td>Tarif Angkutan Lebaran</td>
<td> Keputusan Mentri</td>
</tr> -->
</table>


</body>
</html>
