@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | {{config('app.halamanWeb')[$codepage]['nama']}}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('bgnya')
		<div class="wrapall" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/images/izinumum.jpg')}});">
@endsection

@section('hari_ini')
	{{ $hariini }}
@endsection

@section('script_awal')
@endsection

@section('content')
	<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-5 nopadding">
						<div class="thumbnail judul">
							SYARAT PERIZINAN
						</div>
					</div>
					<div class="col-md-7">
						<div class="thumbnail isikonten">
							<h1 class="title-page">
							@if (@$page->title)
								{!! $page->title !!}
							@else
								PERKERETAAPIAN KHUSUS</span></h1>
							@endif</h1>
							<ul class="submenu">
								<li><a href="#">Izin Operasional</a></li>
								<li>
									<select class="form-control" onChange="window.document.location.href=this.options[this.selectedIndex].value;" >
										@if ($codepage=='I0423')
											<option value="">-- Pilih Izin Operasi ---</option>
										@endif
										<option value="{{route('web.saranakhususops1')}}" {{ ($codepage=='I04231') ? "Selected" : '' }}>Penambahan Lintas Pelayanan</option>
										<option value="{{route('web.saranakhususops2')}}" {{ ($codepage=='I04232') ? "Selected" : '' }}>Penambahan Frekuensi</option>
										<option value="{{route('web.saranakhususops3')}}" {{ ($codepage=='I04233') ? "Selected" : '' }}>Izin Baru</option>
										<option value="{{route('web.saranakhususops4')}}" {{ ($codepage=='I04234') ? "Selected" : '' }}>Izin Perpanjangan</option>
									</select>
								</li>
							</ul>
							<div class="content-area">
								@if (@$page->content)
									{!! $page->content !!}
								@else
									<p align="justify">
									{{config('app.halamanWeb')[$codepage]['nama']}}<b>Lorem ipsum</b> dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
									incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
									exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
									dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
									Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
									mollit anim id est laborum.
								</p>
								<p align="justify">
									<b>Lorem ipsum</b> dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
									incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
									exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
									dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
									Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
									mollit anim id est laborum.
								</p>
								<p align="justify">
									<b>Lorem ipsum</b> dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
									incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
									exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
									dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
									Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
									mollit anim id est laborum.
								</p>
							@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
