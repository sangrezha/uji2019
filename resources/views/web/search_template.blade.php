@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'Cari' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-prasarana.jpg')}})"></div>
     <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>Pencarian</h1>     
   </div>
</section>

@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="page-content">
		<div class="container">
			<div class="row">

	        <div class="col-md-12">

							<div class="content-area">

							@if (@$page->content)
								{!! $page->content !!}
							@endif
							@if (@$pesan)
								<div class="alert alert-info">{{ $pesan }}</div>
							@else
								@if (count($hasilpage))
									<div class="alert alert-info">Hasil pencarian kata kunci: <b><i>{{ $keyword }}</i></b></div>
									<div class="panel panel-default">
									  <!-- Default panel contents -->
									  <div class="panel-heading"><b>Halaman Web</b></div>
									  <div class="panel-body">
									    <p>Ditemukan di <b>{{ count($hasilpage) }}</b> halaman</p>
									  </div>

									  <!-- List group -->
									  <ul class="list-group">
									    @foreach ($hasilpage as $hasil)
									    	<li class="list-group-item"><a href="{{ route(@config('app.halamanWeb')[$hasil->codepage]['route']) }}">Halaman {{ @config('app.halamanWeb')[$hasil->codepage]['nama'] }}</a></li>
									    @endforeach
									  </ul>
									</div>
							@endif
							@if (count($hasilnews))
								<div class="panel panel-default">
								  <!-- Default panel contents -->
								  <div class="panel-heading"><b>Halaman Berita</b></div>
								  <div class="panel-body">
								    <p>Ditemukan di <b>{{ count($hasilnews) }}</b> berita</p>
								  </div>

								  <!-- List group -->
								  <ul class="list-group">
								    @foreach ($hasilnews as $news)
								    	<li class="list-group-item"><a href="{!! route('web.detberita', ['slug' => $news->slug]) !!}">Halaman {{ $news->title }}</a></li>
								    @endforeach
								  </ul>
								</div>
							@endif
							@if (count($hasilevent))
								<div class="panel panel-default">
								  <!-- Default panel contents -->
								  <div class="panel-heading"><b>Halaman Kegiatan</b></div>
								  <div class="panel-body">
								    <p>Ditemukan di <b>{{ count($hasilevent) }}</b> kegiatan</p>
								  </div>

								  <!-- List group -->
								  <ul class="list-group">
								    @foreach ($hasilevent as $event)
								    	<li class="list-group-item"><a href="{!! route('web.detevent', ['slug' => $event->slug]) !!}">Halaman {{ $event->title }}</a></li>
								    @endforeach
								  </ul>
								</div>
							@endif
							@endif
	            <br> <br> <br>
	        </div>
	    </div><!-- .col -->
	    </div><!-- .row -->
		</div><!-- .container -->
		</div><!-- .page-content -->
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
