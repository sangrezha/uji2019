@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'Struktur Organisasi' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="page-header">
      <img src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-profile.jpg')}}">
      <div class="caption">
          <h1><span>{!! (@$page->title) ? $page->title : "Struktur Organisasi" !!}</span></h1>
      </div>
  </div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="page-content">
				<div class="col-md-12">
					<h1 class="text-uppercase"><span>{{ (@$page->title) ? $page->title : "Struktur Organisasi" }}</span></h1>
				</div>
        <div class="col-md-12" style="text-align:center">
            <br>
						<br>
            <div class="tree">
                <ul>
                    <li>
                        <a href="#">
                            <div class="position">
                                KEPALA BALAI / KPA
                            </div>
                            <div class="name">
                                <div>
                                    <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                    <span>AMIRULLOH</span>
                                </div>
                            </div>
                        </a>
                        <ul>
                            <li>
                                <a href="#">
                                    <div class="position">
                                        SUB BAGIAN TATA USAHA
                                    </div>
                                    <div class="name">
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span><strong>KEPALA SUB BAGIAN</strong><br>AGUS SUBANDRIYO</span>
                                        </div>
                                        <div>
                                            <span>Junita Valentina</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="position">
                                        SEKSI PENGUJIAN<br>PRASARANA PERKERETAAPIAN
                                    </div>
                                    <div class="name">
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span><strong>KEPALA SEKSI</strong><br>AGUS SUBANDRIYO</span>
                                        </div>
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span>Junita Valentina</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="position">
                                            SEKSI PENGUJIAN<br>SARANA PERKERETAAPIAN
                                    </div>
                                    <div class="name">
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span><strong>KEPALA SEKSI</strong><br>AGUS SUBANDRIYO</span>
                                        </div>
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span>Junita Valentina</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="position">
                                            SEKSI PENGUJIAN<br>SDM PERKERETAAPIAN
                                    </div>
                                    <div class="name">
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span><strong>KEPALA SEKSI</strong><br>AGUS SUBANDRIYO</span>
                                        </div>
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span>Junita Valentina</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="position">
                                        BENDAHARA PENGELUARAN
                                    </div>
                                    <div class="name">
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span>AGUS SUBANDRIYO</span>
                                        </div>
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span>Junita Valentina</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="position">
                                        PPK
                                    </div>
                                    <div class="name">
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span><strong>KEPALA SUB BAGIAN</strong><br>AGUS SUBANDRIYO</span>
                                        </div>
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span>Junita Valentina</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="position">
                                        PPSPM
                                    </div>
                                    <div class="name">
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span><strong>KEPALA SUB BAGIAN</strong><br>AGUS SUBANDRIYO</span>
                                        </div>
                                        <div>
                                            <img src="{{ asset('/public/images/pegawai/1500098598.jpg') }}">
                                            <span>Junita Valentina</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <br>
						{!! @$page->content !!}
        </div>
    </div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
	<script src="{{asset('/public/assets/js/highcharts.js')}}"></script>
	<script>
	// Build the chart
        Highcharts.chart('compare_pegawai', {
            chart: {
                backgroundColor: 'transparent',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                        distance: -50,
                        filter: {
                            property: 'percentage',
                            operator: '>',
                            value: 4
                        }
                    }
                }
            },
            series: [{
                name: 'Total',
                data: [
                    { name: 'HONORER (27 ORG)', y: 27 , color:'#ff6d00'},
                    { name: 'PNS (24 ORG)', y: 24, color:'#179ce0' }
                ]
            }]
        });

        Highcharts.chart('komposisi_pegawai', {
            chart: {
                backgroundColor:'transparent',
                type: 'column'
            },
            title: {
                text: 'KOMPOSISI TENAGA INSPEKTUR DAN PENGUJI BALAI PENGUJIAN PERKERETAAPIAN'
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">Total: </td>' +
                    '<td style="padding:0"><b>{point.y} Orang</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                data:[
                    {
                        name: 'Inspektur Sarana Perkeretaapian',
                        y: 1,
                        color:'#ff6d00'

                    }, {
                        name: 'Inspektur Prasarana Perkeretaapian',
                        y: 1,
                        color:'#ffd90f'

                    }, {
                        name: 'Penguji Sarana Perkeretaapian',
                        y: 9,
                        color:'#5bd700'

                    }, {
                        name: 'Penguji Jalur & bangunan KA',
                        y: 12,
                        color:'#199bdf'
                    }, {
                        name: 'Penguji Fasilitas Operasi KA',
                        y: 1,
                        color:'#860aac'
                    }, {
                        name: 'Asessor SDM Perkeretaapian',
                        y: 0,
                        color:'#ff0000'
                    }
                ]
            }]
        });
	</script>
@endsection
