@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'Struktur Organisasi' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="page-header">
      <img src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-profile.jpg')}}">
      <div class="caption">
          <h1><span>{!! (@$page->title) ? $page->title : "Struktur Organisasi" !!}</span></h1>
      </div>
  </div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="page-content">
				<div class="col-md-12">
					<h1 class="text-uppercase"><span>{{ (@$page->title) ? $page->title : "Struktur Organisasi" }}</span></h1>
				</div>
				<div class="modal fade detail_struktur" id="detail_struktur">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="margin-top:90px;">
                    <button type="button" class="close" style="position:absolute;top:-35px;right:-35px;background: #f7be11;width:30px;height:30px;opacity:1;border-radius: 30px;" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal-body" style="padding:0;">

                        <div class="detail" id="1">
                            <h1>{!! $jabatan1->title !!}</h1>
                            <div class="item col-md-12">
                                <div class="col-md-2 nopadding">
                                    <div class="thumb">
                                        <img src="{{ asset('/public/images/pegawai/')."/".$pegawai1->picture }}">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <h4>{{ $pegawai1->nama }}</h4><h3>
                                    NIP : {{ $pegawai1->nip }}<br>Golongan : {{ $pegawai1->golongan }}
																	<br>Pangkat : {{ $pegawai1->pangkat }}
																<br>Jabatan : {{ $pegawai1->jabatan }}
															<br>Pendidikan : {{ $pegawai1->pendidikan }}
														</h3>
                                </div>
                            </div>
                        </div>
												@foreach ($jabatan2 as $jabatan)
													<div class="detail" id="{{ $jabatan->id }}">
	                            <h1>{!! $jabatan->title !!}</h1>
																		@foreach ($pegawai[$jabatan->uniqid] as $peg)
																			<div class="item col-md-12">
					                                @if ($peg['picture'])
																						<div class="col-md-2 nopadding">
					                                    <div class="thumb">
					                                        <img src="{{ asset('/public/images/pegawai/')."/".$peg['picture'] }}">
					                                    </div>
					                                	</div>
																					@endif
					                                <div class="col-md-10">
																						<h4>{{ $peg['nama'] }}</h4><h3>
																										NIP : {{ $peg['nip'] }}<br>Golongan : {{ $peg['golongan'] }}
																									<br>Pangkat : {{ $peg['pangkat'] }}
																								<br>Jabatan : {{ $peg['jabatan'] }}
																							<br>Pendidikan : {{ $peg['pendidikan'] }}
																						</h3>
					                                </div>
					                            </div>
																		@endforeach
																	</div>
																@endforeach
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12" style="text-align:center;overflow:scroll;">
            <br>
						<br>
            <div class="tree">
                <ul>
                    <li>
                        <a href="#" class="struktur {{ $jabatan1->id}}" id="{{ $jabatan1->id}}">
                            <div class="position">
                                {{ $jabatan1->title }}
                            </div>
                            <div class="name">
                                <div>
                                    <img src="{{ asset('/public/images/pegawai/')."/".$pegawai1->picture }}">
                                    <span>{{ $pegawai1->nama }}</span>
                                </div>
                            </div>
                        </a>
                        <ul>
													@foreach ($jabatan2 as $jabatan)
                            <li>
                                <a href="#" class="struktur {{ $jabatan->id}}" id="{{ $jabatan->id}}">
                                    <div class="position">
                                        {!! $jabatan->title !!}
                                    </div>
                                    <div class="name">
																			{{-- {{ print_r($pegawai[$jabatan->uniqid]) }} --}}
																			@foreach ($pegawai[$jabatan->uniqid] as $peg)
																				<div>
																					@if ($peg['picture'])
                                            <img src="{{ asset('/public/images/pegawai/')."/".$peg['picture'] }}">
																					@endif
																					@if ($peg['pimpinan'])
                                            <span>{!! ($peg['jabatan']) ? "<strong>".$peg['jabatan']."</strong><br>" : '' !!}{{ $peg['nama'] }}</span>
																					@else
																						<span>{{ $peg['nama'] }}</span>
																					@endif
                                        </div>
																			@endforeach
                                    </div>
                                </a>
                            </li>
														@endforeach
                        </ul>
                    </li>
                </ul>
            </div>
            <br>
						{!! @$page->content !!}
        </div>
    </div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
	<script src="{{asset('/public/assets/js/highcharts.js')}}"></script>
	<script>
	// Build the chart

				$('.struktur').attr('data-toggle','modal');
        $('.struktur').attr('data-target','#detail_struktur');
        $('.struktur').click(function(){
            var iddetail = $(this).attr('class').split(' ');
						console.log(iddetail[1]);
            $('.detail').hide();
            $('#'+iddetail[1]).show();
        });
        Highcharts.chart('compare_pegawai', {
            chart: {
                backgroundColor: 'transparent',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                        distance: -50,
                        filter: {
                            property: 'percentage',
                            operator: '>',
                            value: 4
                        }
                    }
                }
            },
            series: [{
                name: 'Total',
                data: [
                    { name: 'HONORER (27 ORG)', y: 27 , color:'#ff6d00'},
                    { name: 'PNS (24 ORG)', y: 24, color:'#179ce0' }
                ]
            }]
        });

        Highcharts.chart('komposisi_pegawai', {
            chart: {
                backgroundColor:'transparent',
                type: 'column'
            },
            title: {
                text: 'KOMPOSISI TENAGA INSPEKTUR DAN PENGUJI BALAI PENGUJIAN PERKERETAAPIAN'
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">Total: </td>' +
                    '<td style="padding:0"><b>{point.y} Orang</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                data:[
                    {
                        name: 'Inspektur Sarana Perkeretaapian',
                        y: 1,
                        color:'#ff6d00'

                    }, {
                        name: 'Inspektur Prasarana Perkeretaapian',
                        y: 1,
                        color:'#ffd90f'

                    }, {
                        name: 'Penguji Sarana Perkeretaapian',
                        y: 9,
                        color:'#5bd700'

                    }, {
                        name: 'Penguji Jalur & bangunan KA',
                        y: 12,
                        color:'#199bdf'
                    }, {
                        name: 'Penguji Fasilitas Operasi KA',
                        y: 1,
                        color:'#860aac'
                    }, {
                        name: 'Asessor SDM Perkeretaapian',
                        y: 0,
                        color:'#ff0000'
                    }
                ]
            }]
        });
	</script>
@endsection
