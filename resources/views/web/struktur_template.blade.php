@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'Struktur Organisasi' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-profile.jpg')}});"></div>
  <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>{!! (@$page->title) ? $page->title : "Struktur Organisasi" !!} </h1>     
   </div>
</section>
@endsection


@section('script_awal')
	<style>
	.center-block {
   text-align: center;
}
	.card {
	    /* Add shadows to create the "card" effect */
	    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
	    transition: 0.3s;
			min-height: 250px;
			margin-bottom: 10px;
	}

	/* On mouse-over, add a deeper shadow */
	.card:hover {
	    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
	}

	/* Add some padding inside the card container */
	.containers {
	    padding: 2px 16px;
	}

	.title {
		text-align: center;
    padding: 2px 5px;
    background: #f7be11;
		color:#1c0d4c;
		min-height: 55px;
	}
	</style>
@endsection

@section('content')
<section class="djka-section-strukturogranisasi">

  <div class="djka-card2-wrapper">

      <div class="djka-card2" data-djka-card2>
        <div class="djka-card2-pic"><span><img src="{{ asset('/public/images/pegawai/').'/'.$pegawai1->picture }}" alt=""></span></div>
        <div class="djka-card2-content">
          <div class="djka-card2-name">{{ $pegawai1->nama }}</div>
          <div class="djka-card2-title">{{ $pegawai1->jabatan }}</div>
          <div class="djka-card2-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
        </div> <!-- .djka-card2-content -->
      </div><!-- .djka-card2 -->

        <div class="djka-pegawai-list">
 
        @foreach ($pegawai1child as $k=>$child)
            <div class="djka-pegawai-item">
                <div class="djka-pegawai-item-thumb"><img src="{{ ($child['picture'])?asset('/public/images/pegawai/').'/'.$child['picture']:asset('/public/images/propic.png')}}"></div>
                <div class="djka-pegawai-item-name">{{ $child->nama }}</div>
            </div>
        @endforeach
        </div><!-- .djka-pegawai-list -->


     @foreach ($baris as $k=>$barisnya)
            @foreach ($jabatannya[$barisnya->baris] as $key => $jabatan)
            @if(isset($pegawai[$barisnya->baris]))
                <div class="djka-card2" data-djka-card2>
                @foreach ($pegawai[$barisnya->baris][$jabatan['uniqid']] as $peg)
                <div class="djka-card2-pic"><span><img src="{{ asset('/public/images/pegawai/').'/'.$peg['picture'] }}" alt=""></span></div>
                
                <div class="djka-card2-content">
                  <div class="djka-card2-name">{{ $peg['nama'] }}</div>
                  <div class="djka-card2-title">{{ $peg['jabatan'] }}</div>
                  <div class="djka-card2-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                </div> <!-- .djka-card2-content -->
                @endforeach
              </div><!-- .djka-card2 -->
                @endif
            @if(isset($pegawaichild[$barisnya->baris]))
              <div class="djka-pegawai-list">
                @foreach ($pegawaichild[$barisnya->baris][$jabatan['uniqid']] as $peg)
                    <div class="djka-pegawai-item">
                        <div class="djka-pegawai-item-thumb"><img src="{{ ($peg['picture'])?asset('/public/images/pegawai/').'/'.$peg['picture']:asset('/public/images/propic.png')}}"></div>
                        <div class="djka-pegawai-item-name">{{ $peg['nama'] }}</div>
                    </div>
                @endforeach
              </div><!-- .djka-pegawai-list -->
            @endif
            @endforeach
        @endforeach
      </div><!-- .djka-card2-wrapper -->
    </section><!-- .djka-section-strukturogranisasi -->
    
    <section>
             <div class="djka-richtext">{!! @$page->content !!}</div>   						
             </section>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
	<script src="{{asset('/public/assets/js/highcharts.js')}}"></script>
	<script>
	// Build the chart

				$('.struktur').attr('data-toggle','modal');
        $('.struktur').attr('data-target','#detail_struktur');
        $('.struktur').click(function(){
            var iddetail = $(this).attr('class').split(' ');
						console.log(iddetail[1]);
            $('.detail').hide();
            $('#'+iddetail[1]).show();
        });
        Highcharts.chart('compare_pegawai', {
            chart: {
                backgroundColor: 'transparent',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                        distance: -50,
                        filter: {
                            property: 'percentage',
                            operator: '>',
                            value: 4
                        }
                    }
                }
            },
            series: [{
                name: 'Total',
                data: [
                    @foreach ($chartPie as $cp)
                    	{ name: '{{ $cp->title }} ({{$cp->nilai}} ORG)', y: {{ $cp->nilai }} , color:'{{ $cp->warna }}'},
                    @endforeach
                ]
            }]
        });

        Highcharts.chart('komposisi_pegawai', {
            chart: {
                backgroundColor:'transparent',
                type: 'column'
            },
            title: {
                text: 'KOMPOSISI TENAGA INSPEKTUR DAN PENGUJI BALAI PENGUJIAN PERKERETAAPIAN'
            },
            xAxis: {
                type: 'category',
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">Total: </td>' +
                    '<td style="padding:0"><b>{point.y} Orang</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            series: [{
                data:[
									@foreach ($chartBar as $cb)
                    {
                        name: '{{ $cb->title }}',
                        y: {{ $cb->nilai }},
                        color:'{{ $cb->warna }}'

                    },
									@endforeach
                ]
            }]
        });
	</script>
@endsection
