@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'TUGAS DAN FUNGSI BALAI PENGUJIAN PERKERETAAPIAN' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-profile.jpg')}})"></div>
  <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>{!! (@$page->title) ? $page->title : "Penjelasan Tupoksi" !!} </h1>     
   </div>
</section>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="page-content">
        <div class="djka-hspace-big"></div>
		<div class="container">
						@if (@$page->content)
						<div class="djka-richtext">{!! @$page->content !!}</div>
						@else
							<h4>Sesuai dengan Peraturan Menteri Perhubungan Nomor: PM. 64 Tahun 2014 tentang Organisasi dan Tata Kerja Balai Pengujian Perkeretaapian yang ditetapkan di Jakarta pada tanggal 26 Nopember 2014 berlokasi di Bekasi, Jawa Barat memiliki Kedudukan, Tugas dan Fungsi antara lain:</h4><br>
	              <h4 style="color:#ffc412">Pasal 1 :</h4>
	              <ol>
	                <li><span>Balai Pengujian Perkeretaapian merupakan unit Pelaksana Teknis di Lingkungan Kementerian Perhubungan berada di bawah dan bertanggung jawab kepada Direktur Jenderal Perkeretaapian</span></li>
	                <li><span>Balai Pengujian Perkeretaapian dipimpin oleh seorang Kepala.</span></li>
	              </ol>
	              <h4 style="color:#ffc412">Pasal 2 :</h4>
	              <ol>
	                <li><span>Balai Pengujian Perkeretaapian sebagaimana dimaksud dalam pasal 1, mempunyai tugas melaksanakan pengujian prasarana, sarana, dan sumber daya manusia perkeretaapian</span></li>
	              </ol>
	              <h4 style="color:#ffc412">Pasal 3 :</h4>
	              <h4>Fungsi Balai Pengujian Perkeretaapian</h4>
	              <ol>
	                <li><span>Pelaksanaan pengujian pertama dan berkala jalur kereta api, bangunan perkeretaapian dan fasilitas operasi kereta api</span></li>
	                <li><span>Pelaksanaan pengujian pertama dan berkala sarana perkeretaapian berpenggerak dan tanpa penggerak</span></li>
	                <li><span>Pelaksanaan pengujian pertama dan berkala peralatan khusus</span></li>
	                <li><span>Pelaksanaan pengujian kompetensi awak sarana perkeretaapian</span></li>
	                <li><span>Pelaksanaan pengujian kompetensi petugas pengoperasian prasarana perkeretaapian</span></li>
	                <li><span>Pelaksanaan pengujian kompetensi penguji prasarana, penguji sarana, inspektur prasarana, inspektur sarana, dan auditor perkeretaapian</span></li>
	                <li><span>Pengelolaan urusan tata usaha, rumah tangga, kepegawaian, keuangan, hukum, dan hubungan masyarakat</span></li>
	              </ol>
              <div class="djka-hspace-big"></div>
						@endif
			</div><!-- .container -->
    </div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
