@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | {{ config('app.halamanWeb')[$codepage]['nama'] }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-prasarana.jpg')}})"></div>
     <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>Pengujian Prasarana</h1>     
   </div>
</section>
@endsection


@section('script_awal')
	<link rel="stylesheet" href="{{ asset("/public/plugins/datatables/dataTables.bootstrap.css") }}">
@endsection

@section('content')
<section class="djka-section-grey">
<div class="container-fluid has-large-padding">
    <div class="djka-funky">
        <div class="djka-funky-menu" data-djka-funky-menu>
            <a href="#" class="djka-funky-toggle" data-djka-funky-toggle></a>      
            @include('web.ujiprasaranaMenu_template')
        </div><!-- .djka-funky-menu -->
        <div class="djka-funky-content">
           <h1 class="text-uppercase"><span>{{ (@$page->title) ? $page->title : "Jadwal Pengujian" }}</span></h1>
						@if (@$page->content)
						{!! @$page->content !!}
						@endif
						@if ($dat['excel1'])
							<br><p><b>{{ $page->judul1 }}</b></p>
							<table id="tableDataBrowse" class="table table-bordered table-striped" >
								<thead>
									<tr class="info">
										@foreach ($dat['key1'] as $k=>$v)
											<th>{{ucwords(str_replace('_', ' ', $v))}}</th>
										@endforeach
									</tr>
								</thead>
								<tbody>
								@foreach($dat['excel1'] as $datak=>$datav)
									<tr>
										@foreach ($dat['key1'] as $k=>$v)
											@if ($datav[$v])
											<td>{{ $datav[$v] }}</td>
											@endif
										@endforeach
									</tr>
								@endforeach
								</tbody>
							</table>
						@endif
						@if ($dat['excel2'])
							<br><p><b>{{ $page->judul2 }}</b></p>
							<table id="tableDataBrowse2" class="table table-bordered table-striped" >
								<thead>
									<tr class="info">
										@foreach ($dat['key2'] as $k=>$v)
											<th>{{ucwords(str_replace('_', ' ', $v))}}</th>
										@endforeach
									</tr>
								</thead>
								<tbody>
								@foreach($dat['excel2'] as $datak=>$datav)
									<tr>
										@foreach ($dat['key2'] as $k=>$v)
											@if ($datav[$v])
											<td>{{ $datav[$v] }}</td>
										@endif
										@endforeach
									</tr>
								@endforeach
								</tbody>
							</table>
						@endif
        </div><!-- .djka-funky-content -->
    </div><!-- .djka-funky -->
</div><!-- .container-fluid -->
</section>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
	<script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
	<script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>

	<script>
	    $(function () {
	      $("#tableDataBrowse").DataTable({
					"order": [[ 0, "asc" ]],
					"paging":   false,
	        "info":     false
				});

				$("#tableDataBrowse2").DataTable({
					"order": [[ 0, "asc" ]],
					"paging":   false,
	        "info":     false
				});
	    });
	</script>
@endsection
