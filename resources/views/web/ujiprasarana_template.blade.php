@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'Pengujian Prasarana' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="banner">
			<h1><span>{{ (@$page->highlight) ? $page->highlight : "Pengujian PraSarana" }}</span></h1>
			<img src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-sarana.jpg')}}">
	</div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="content row">
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	        <div class="col-md-10">
	            <h1 class="content-title text-uppercase">@if (@$page->title)
								{!! $page->title !!}
							@else
								Info Persyaratan Pengujian dan Bagan Alur Pengujian Prasarana
							@endif</h1>
							<div class="content-area">
							@if (@$page->content)
								<div class="djka-richtext">{!! $page->content !!}</div>
							@else
							<h4>Pelaksanaan Pengujian Jalur dan Bangunan KA dilaksanakan berdasarakan Peraturan Menteri Nomor 64 Tentang Organisasi dan Tata Kerja Balai Pengujian Perkeretaapian  dengan tugas melaksanakan pengujian prasarana, sarana, dan sumber daya manusia perkeretaapian</h4>
              <p>Dalam pelaksanaan tugas pengujian prasarana, dalam struktural diarahkan oleh Kepala Seksi Pengujian Prasarana Perkeretaapian dengan Pembinaan oleh Kepala Balai Pengujian Perkeretaapian. Pelaksanaan dilapangan mengacu pada Peraturan Menteri No. 30 Tahun 2011 Tentang Tata Cara Pengujian dan Pemberian Sertifikat Prasarana Perkeretaapian dengan berpedoman pada persyaratan teknis Peraturan Menteri No. 60 Tahun 2012 Tentang Persyaratan Teknis Kereta Api, Peraturan Menteri No 10 Tahun 2011 Tentang Persyaratan Teknis Peralatan Persinyalan Perkeretaapian, Peraturan Menteri No 11 Tahun 2011 Tentang Persyaratan Teknis Peralatan Telekomunikasi Perkeretaapian, dan Peraturan Menteri No 12 Tentang Persyaratan Teknis Installasi Listrik Perkeretaapian, Berikut bagan alur proses pengujian pertama – berkala prasarana </p>
              <img class="img-responsive" src="{{ asset('/public/assets/img/img-prasarana1.jpg') }}">
              <h4 style="color:#ffc412">Perkeretaapian :</h4>
              <p>Pemohon memberikan surat permohonan pengujian prasarana perkeretaapian kepada Direktorat Jenderal Perkeretaapian untuk melaksanakan pengujian pertama atau berkala prasarana perkeretaapian, kemudian Direktur Jenderal Perkeretaapian memberikan disposisi kepada Balai Pengujian perkeretaapian tentang permohonan pengujian prasarana perkeretaapian, kemudian Balai Pengujian Perkeretaapian memeriksa kelengkapan dokumen pengujian antara lain :</p>
              <ol>
                <li><span class="abc">Detail Enginnering Design yang telah mendapat persetujuan oleh Direktorat Teknis Prasarana Perkeretaapian</span></li>
                <li><span class="abc">Spesifikasi Teknis yang telah mendapat persetujuan oleh Direktorat Teknis Prasarana Perkeretaapian</span></li>
                <li><span class="abc">Gambar Kerja (Shop Drawing) yang telah mendapat persetujuan oleh Direktorat Teknis Prasarana Perkeretaapian</span></li>
                <li><span class="abc">Gambar Hasil Pekerjaan (As Built Drawing).</span></li>
              </ol>
              <br><br>
							@endif
	            <br> <br> <br>
	        </div>
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	    </div>
		</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
