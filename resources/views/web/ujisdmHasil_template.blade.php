@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | {{ config('app.halamanWeb')[$codepage]['nama'] }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-sdm.jpg')}})"></div>
  <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>Pengujian SDM</h1>     
   </div>
</section>
@endsection


@section('script_awal')
	<link rel="stylesheet" href="{{ asset("/public/plugins/datatables/dataTables.bootstrap.css") }}">

@endsection

@section('content')
<section class="djka-page">  
  <div class="djka-page-section">
    <h2 class="djka-section-title">{{ (@$page->title) ? $page->title : "hasil Pengujian" }}</h2>
    <h3 class="djka-title-5 djka-align-center">{{ (@$page->judul1) ? $page->judul1 : "HASIL PENGUJIAN SDM PERKERETAAPIAN" }}</h3>
    <div class="djka-hspace-medium"></div>
    <div class="djka-filterbox">      
        <!-- <form class="form-inline">
          <div class="col-sm-5" style="display: flex;">
          <label class="" style="width: 120px">Sort by</label>
          <select class="custom-select" style="flex-shrink: 1;">
            <option selected>Choose...</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
          </div>
          <div class="col-sm-7">
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text"><i class="fa fa-search"></i></div>
            </div>
            <input type="text" class="form-control" placeholder="Search">
          </div>
          </div>
        </form> -->
    </div><!-- .djka-filterbox -->
    <div class="container">
    <div class="djka-databox">
	<div class="djka-funky-menu" data-djka-funky-menu>
            <a href="#" class="djka-funky-toggle" data-djka-funky-toggle></a>      
            @include('web.ujisdmMenu_template')
        </div>
	@if ($dat['excel1'])
      <div class="djka-databox-content">
        <div class="djka-cards-1">
			@foreach($dat['excel1'] as $datak=>$datav)
			<div class="djka-card-1">
			  <div class="djka-card-1-header">
				<b>{{$datav['nipp']}}</b>
				<span>{{$datav['nama']}}</span>
			  </div>
			  <div class="djka-card-1-body">
				<table>
					@foreach ($dat['key1'] as $k=>$v)
						<tr><td>{{ucwords(str_replace('_', ' ', $v))}}</td> <td>{{ $datav[$v] }}</td></tr>
					@endforeach
				</table>
			  </div>
			</div><!-- .djka-card-1 -->
			@endforeach
			@foreach($dat['excel2'] as $datak=>$datav)
			<div class="djka-card-1">
			  <div class="djka-card-1-header">
				<b>{{$datav['nipp']}}</b>
				<span>{{$datav['nama']}}</span>
			  </div>
			  <div class="djka-card-1-body">
				<table>
					@foreach ($dat['key1'] as $k=>$v)
						<tr><td>{{ucwords(str_replace('_', ' ', $v))}}</td> <td>{{ $datav[$v] }}</td></tr>
					@endforeach
				</table>
			  </div>
			</div><!-- .djka-card-1 -->
			@endforeach
		  

        </div><!-- .djka-cards-1 -->
	  </div><!-- .djka-databox-content -->
	@endif
    </div><!-- .djka-databox -->
  </div><!-- .container -->
  </div><!-- .djka-page-section -->
</section><!-- .djka-page -->


@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
	<script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
	<script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>

	<script>
	    $(function () {
	      $("#tableDataBrowse").DataTable({
					"order": [[ 0, "asc" ]],
					"paging":   false,
	        "info":     false
				});

				$("#tableDataBrowse2").DataTable({
					"order": [[ 0, "asc" ]],
					"paging":   false,
	        "info":     false
				});
	    });
	</script>
@endsection
