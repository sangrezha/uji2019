@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | {{ config('app.halamanWeb')[$codepage]['nama'] }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="djka-page-header" style="background-image:url({{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-sdm.jpg')}})"></div>
  <section class="djka-title-box">
   <img src="{{asset('/public/images/ribbon-1.png')}}" class="curve" data-top=emboss data-bottom=emboss data-fill=#FFC734>
   <div class="container">
      <h1>Pengujian SDM </h1>     
   </div>
</section>
@endsection

@section('head_konten')
	<style>
	.sticky {
      z-index: 9999 !important;
  }
	</style>
@endsection


@section('script_awal')
@endsection

@section('content')
<section class="djka-section-grey">
<div class="container-fluid has-large-padding">
    <div class="djka-funky">
        <div class="djka-funky-menu" data-djka-funky-menu>
            <a href="#" class="djka-funky-toggle" data-djka-funky-toggle></a>      
            @include('web.ujisdmMenu_template')
        </div><!-- .djka-funky-menu -->
        <div class="djka-funky-content">
            <h1 class="text-uppercase"><span>{{ (@$page->title) ? $page->title : "Info Persyaratan" }}</span></h1>
            @if (@$page->content)
            <div class="djka-richtext">{!! @$page->content !!}</div>
            @endif
        </div><!-- .djka-funky-content -->
    </div><!-- .djka-funky -->
</div><!-- .container-fluid -->
</section>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
