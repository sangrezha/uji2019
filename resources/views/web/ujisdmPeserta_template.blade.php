@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | {{ config('app.halamanWeb')[$codepage]['nama'] }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="banner">
			<h1><span>{{ (@$page->highlight) ? $page->highlight : "Pengujian SDM" }}</span></h1>
			<img src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-sdm.jpg')}}">
	</div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="content row">
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	        <div class="col-md-10">
	            <h1 class="content-title text-uppercase">@if (@$page->title)
								{!! $page->title !!}
							@else
								Daftar Peserta
							@endif</h1>
							<div class="content-area">
							@if (@$page->content)
								<div class="djka-richtext">{!! @$page->content !!}</div>
							@endif
	            <br> <br> <br>
	        </div>
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	    </div>
		</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
