@extends('web.master_template')

@section('judul')
	{{ $pref->title }} | 	{{ (@$page->title) ? $page->title : 'Pengujian SDM' }}
@endsection

@section('metadescription')
	{{ $pref->meta_desc }}
@endsection

@section('banner')
	<div class="banner">
			<h1><span>{{ (@$page->highlight) ? $page->highlight : "Pengujian SDM" }}</span></h1>
			<img src="{{ @($page->picture) ? asset('/public/images/page/'.$page->picture) : asset('/public/assets/img/header-sdm.jpg')}}">
	</div>
@endsection


@section('script_awal')
@endsection

@section('content')
	<div class="content row">
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	        <div class="col-md-10">
	            <h1 class="content-title text-uppercase">@if (@$page->title)
								{!! $page->title !!}
							@else
								INFO PERSYARATAN
							@endif</h1>
							<div class="content-area">
							@if (@$page->content)
								<div class="djka-richtext">{!! @$page->content !!}</div>
							@else
								<ol>
	                <li><span class="abc">Permenhub Nomor 19 Tahun 2011 tentang Sertifikat Kecakapan Penjaga Perlintasan</span></li>
	                <li><span class="abc">Permenhub Nomor 4 Tahun 2017 tentang Sertifikasi Kecakapan Awak Sarana Perkeretaapian</span></li>
	                <li><span class="abc">Permenhub Nomor 5 Thaun 2017 tentang Sertifikasi Kecakapan Pengatur dan Pengendali Perjalanan Kereta Api</span></li>
	                <li><span class="abc">Permenhub Nomor 8 Tahun 2017 tentang Sertifikasi Tenaga Pemeriksa Sarana Perkeretaapian</span></li>
	                <li><span class="abc">Permenhub Nomor 9 Tahun 2017 tentang Sertifikasi Tenaga Pemeriksa Prasarana Perkeretaapian</span></li>
	                <li><span class="abc">Permenhub Nomor 16 Tahun 2017 tentang Sertifikasi Tenaga Perawatan Sarana Perkeretaapian</span></li>
	                <li><span class="abc">Permenhub Nomor 17 Tahun 2017 tentang Sertifikasi Tenaga Perawatan Prasarana Perkeretaapian</span></li>
	                <li><span class="abc">Permenhub Nomor 96 Tahun 2010 tentang Sertifikat Keahlian Tenaga Penguji Sarana Perkeretaapian</span></li>
	                <li><span class="abc">Permnehub Nomor 97 Tahn 2010 tentang Sertifikat Tenaga Penguji Prasarana Perkeretaapian</span></li>
	                <li><span class="abc">Permenhub Nomor 22 Tahun 2011 tentang Sertifikat Inspektur Perkeretaapian</span></li>
	                <li><span class="abc">Permenhub Nomor 18 Tahun 2011 tentang Sertifikat Auditor Perkeretaapian</span></li>
	              </ol>
              <br><br>
							@endif
	            <br> <br> <br>
	        </div>
	        <div class="col-md-1 hidden-xs hidden-sm"></div>
	    </div>
		</div>
@endsection

@section('footer_text')
	{!! $pref->footer !!}
@endsection

@section('script_end')
@endsection
