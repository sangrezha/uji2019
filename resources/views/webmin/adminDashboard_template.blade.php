@extends('webmin.admin_template')

@section('judul')
  dashboard
@endsection

@section('page_header')

@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
@endsection

@section('page_name')
  
@endsection

@section('page_description')

@endsection

@section('webmin_content')


  <div class="box-body">

              <div class="callout callout-info">
                <i class="fa fa-bullhorn"></i>
                <h1>Selamat Datang</h1>

                <p>halaman ini untuk mengatur website Balai Pengujian Perkeretaapian</p>
                <p>Silahkan akses menu disebelah kiri untuk melakukan pengaturan.</p>
              </div>
            </div>

@endsection

@section('tambahan_script')
@endsection
