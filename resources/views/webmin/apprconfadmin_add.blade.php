@extends('webmin.admin_template')

@section('judul')
  Approver Configuration Management - Tambah Penyetuju
@endsection

@section('page_header')
  Approver Configuration Management - Tambah Penyetuju
@endsection

@section('page_name')
  Approver Configuration Management - Tambah Penyetuju
@endsection

@section('page_description')
  Menambah Approver
@endsection

@section('tambahan_head')
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('apprconf.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('modul')) ? 'has-error' : '' }}">
                  <label for="modul">Modul</label>
                  <select class="form-control" id="modul" name="modul" placeholder="Pilih Modul">
                    <option value="">Pilih Modul</option>
                    <option value="news">News</option>
                    <option value="pengumuman">Pengumuman</option>
                    <option value="wikiuji">WikiUji</option>
                  </select>
                  @if ($errors->has('modul'))
                    <span class="help-block">{{ $errors->first('modul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('admin')) ? 'has-error' : '' }}">
                  <label for="admin">Admin</label>
                  <select class="form-control" id="admin" name="admin">
                    <option value="">Pilih admin</option>
                    @foreach ($data->admin as $admin)
                    <option value="{{ $admin->id }}">{{ $admin->name }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('admin'))
                    <span class="help-block">{{ $errors->first('admin') }}</span>
                  @endif
                </div>
                
                <div class="form-group {{ ($errors->has('priority')) ? 'has-error' : '' }}">
                  <label for="priority">Priority</label>
                  <input type="number" class="form-control" id="priority" name="priority" placeholder="Priority" value="{{old('priority')}}">
                  @if ($errors->has('priority'))
                    <span class="help-block">{{ $errors->first('priority') }}</span>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Save</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')
@endsection
