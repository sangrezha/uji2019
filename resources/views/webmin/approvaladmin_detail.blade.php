@extends('webmin.admin_template')

@section('judul')
  Approval Management - Detail Module {{ $data->modul }}
@endsection

@section('page_header')
  Approval Management - Detail Module {{ $data->modul }}
@endsection

@section('page_name')
  Approval Management - Detail Module {{ $data->modul }}
@endsection

@section('page_description')
  <!-- Mengubah Berita -->
@endsection

@section('tambahan_head')
@endsection

@section('webmin_content')
  <!-- form start -->
  <form id="formAppr" role="form" enctype="multipart/form-data" action="{{ route('approval.update', ['id' => $data->approvalid]) }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label>Tanggal Publish</label>
                      <i class="fa fa-calendar"></i>  
                    <p>{{ $data->publish_date }}</p>
                </div>
                <div class="form-group">
                  <label for="judul">Judul</label>
                  <p>{{$data->title}}</p>
                </div>
                <div class="form-group">
                  <label for="lead">Teks Lead</label>
                  <p>{{$data->lead}}</p>
                </div>
                <div class="form-group">
                  <label for="content">Konten</label>
                <?php echo preg_replace('/src="\/public\/images/','src="'.url('public/images'),$data->content) ?>
                </div>
                
                <div class="form-group">
                  <label for="note">Catatan</label>
                  <textarea name="note" id="note" class="form-control"  placeholder="Berikan catatan jika konten ini di-Reject"></textarea>
                </div>

              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-success" status="approve" onclick="set_action(this)">Approve</button>
                <button type="button" class="btn btn-danger" status="reject" onclick="set_action(this)">Reject</button>
                <i id="preview" class="btn btn-warning">Preview</i>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

<script>
@if($data->modul == "news")
var slug = "{{ route('web.detberita',['slug' => $data->slug]) }}";
@elseif($data->modul == "pengumuman")
var slug = "{{ route('web.detpengumuman',['slug' => $data->slug]) }}";
@elseif($data->modul == "wikiuji")
var slug = "{{ route('web.detwikiuji',['slug' => $data->slug]) }}";
@endif
    $('#preview').click(function () {
        var redirectWindow = window.open(slug, '_blank');
        redirectWindow.location;
      });
function set_action(_this){
  if($(_this).attr('status') == "reject"){
    if($('#note').val() == ''){
      alert('Catatan reject wajib di isi')
      return false;
    }else{
      $('#formAppr').submit();
    }
  }else{
      $('#formAppr').submit();
  }
}
</script>
@endsection
