@extends('webmin.admin_template')

@section('judul')
  Approval Page Management
@endsection

@section('page_header')
  Approval Page Management
@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
@endsection

@section('page_name')
  Approval Page Management
@endsection

@section('page_description')
  Halaman persetujuan di CMS
@endsection

@section('webmin_content')
  <div class="box">
    <div class="box-header">
      <!-- <button type="button" class="btn btn-primary" onclick="location.href = '{{route('approval.add')}}';">Tambah Approval</button> -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="tableDataBrowse" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th width="175">Aksi</th>
          <th>Module</th>
          <th>Title</th>
          <!-- <th>Admin</th> -->
          <th>Tanggal Posting</th>
          <th>Diposting Oleh</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dataList as $data)
        <tr>
          <td>
            
          <button type="button" class="btn btn-primary" onclick="location.href = '{!! route('approval.detail', ['id' => $data->id]) !!}';">Detail</button>
          </td>
          <td>{{ $data->modul }}</td>
          <td>{{ $data->title }}</td>
          <!-- <td>{{ $data->adminid }}</td> -->
          <td> {{ $data->created_at }} </td>
          <td> {{ $data->created_by }}</td>
        </tr>
      @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection

@section('tambahan_script')
  <script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
  <script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
  <!-- page script -->
  <script>
    $(function () {
      $("#tableDataBrowse").DataTable();
    });
  </script>
@endsection
