@extends('webmin.admin_template')

@section('judul')
  Badan Usaha Management - Add New Badan Usaha
@endsection

@section('page_header')
  Badan Usaha Management - Add New Badan Usaha
@endsection

@section('page_name')
  Badan Usaha Management - Add New Badan Usaha
@endsection

@section('page_description')
  Menambahkan Badan Usaha
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('bu.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                  <label for="name">Name</label>
                  <input type="name" class="form-control" id="name" name="name" placeholder="Badan Usaha Name" value="{{old('name')}}">
                  @if ($errors->has('name'))
                  				<span class="help-block">{{ $errors->first('name') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('address')) ? 'has-error' : '' }}">
                  <label for="address">Address</label>
                  <input type="address" class="form-control" id="address" name="address" placeholder="Badan Usaha Address" value="{{old('address')}}">
                  @if ($errors->has('address'))
                  				<span class="help-block">{{ $errors->first('address') }}</span>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
