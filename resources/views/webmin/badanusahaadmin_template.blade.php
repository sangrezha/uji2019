@extends('webmin.admin_template')

@section('judul')
  Badan Usaha Management
@endsection

@section('page_header')
  Badan Usaha Management
@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
@endsection

@section('page_name')
  Badan Usaha Management
@endsection

@section('page_description')
  Halaman untuk menambah Badan Usaha
@endsection

@section('webmin_content')
  <div class="box">
    <div class="box-header">
      <button type="button" class="btn btn-primary" onclick="location.href = '{{route('bu.add')}}';">Add New Badan Usaha</button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="tableDataBrowse" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th> - </th>
          <th>Name</th>
          <th>Address</th>
          <th>Post Date</th>
          <th>Post By</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dataList as $data)
        <tr>
          <td>
            <button type="button" class="btn btn-primary" onclick="location.href = '{!! route('bu.edit', ['id' => $data->id]) !!}';">Edit</button>
            <button type="button" class="btn btn-danger" onclick="location.href = '{!! route('bu.delete', ['id' => $data->id]) !!}';">Delete</button>
          </td>
          <td>{{ $data->name }}</td>
          <td>{{ $data->address }}</td>
          <td> {{ $data->created_at->toDayDateTimeString()}} </td>
          <td> {{ $data->created_by }}</td>
        </tr>
      @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th> - </th>
            <th>Page</th>
            <th>Title</th>
            <th>Post By</th>
            <th>Post Date</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection

@section('tambahan_script')
  <script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
  <script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
  <!-- page script -->
  <script>
    $(function () {
      $("#tableDataBrowse").DataTable();
    });
  </script>
@endsection
