@extends('webmin.admin_template')

@section('judul')
  Bar Chart Management - Tambah Bar Chart baru
@endsection

@section('page_header')
  Bar Chart Management - Tambah Bar Chart baru
@endsection

@section('page_name')
  Bar Chart Management - Tambah Bar Chart baru
@endsection

@section('page_description')
  Menambahkan Bar Chart baru di Halaman Profil
@endsection

@section('tambahan_head')
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('chartbar.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul" value="{{old('judul')}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('nilai')) ? 'has-error' : '' }}">
                  <label for="nilai">Nilai</label>
                  <input type="number" class="form-control" id="nilai" name="nilai" placeholder="Nilai Chart" value="{{old('nilai')}}">
                  @if ($errors->has('nilai'))
                  				<span class="help-block">{{ $errors->first('nilai') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('warna')) ? 'has-error' : '' }}">
                  <label for="warna">Warna</label>
                  <input type="color" class="form-control" id="warna" name="warna" value="{{old('nilai')}}">
                  @if ($errors->has('warna'))
                  				<span class="help-block">{{ $errors->first('warna') }}</span>
                  @endif
                </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
