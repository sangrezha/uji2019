@extends('webmin.admin_template')

@section('judul')
  Bar Chart Management - Ubah Bar Chart
@endsection

@section('page_header')
  Bar Chart Management - Ubah Bar Chart
@endsection

@section('page_name')
  Bar Chart Management - Ubah Bar Chart
@endsection

@section('page_description')
  Merubah Bar Chart di Halaman Profil
@endsection

@section('tambahan_head')
@endsection

@section('webmin_content')
  <!-- form start -->
  <form role="form" enctype="multipart/form-data" action="{{ route('chartbar.update', ['id' => $data->id]) }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul" value="{{$data->title}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('nilai')) ? 'has-error' : '' }}">
                  <label for="nilai">Nilai</label>
                  <input type="number" class="form-control" id="nilai" name="nilai" placeholder="Nilai Chart" value="{{$data->nilai}}">
                  @if ($errors->has('nilai'))
                  				<span class="help-block">{{ $errors->first('nilai') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('warna')) ? 'has-error' : '' }}">
                  <label for="warna">Warna</label>
                  <input type="color" class="form-control" id="warna" name="warna" value="{{$data->warna}}">
                  @if ($errors->has('warna'))
                  				<span class="help-block">{{ $errors->first('warna') }}</span>
                  @endif
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
