@extends('webmin.admin_template')

@section('judul')
  Bar Chart Management - Mengurutkan Bar Chart
@endsection

@section('page_header')
  Bar Chart Management - Mengurutkan Bar Chart
@endsection

@section('page_name')
  Bar Chart Management - Mengurutkan Bar Chart
@endsection

@section('page_description')
  Mengurutkan Bar Chart
@endsection

@section('tambahan_head')
  <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('webmin_content')
  <div class="box-header">
    <button type="button" class="btn btn-primary" onclick="location.href = '{{route('chartbar.dashboard')}}';">Kembali ke Bar Chart</button>
  </div>
  <div class="box-body">
             <ul class="todo-list" id="sortable">
               @foreach($dataList as $data)
                 <li id="item-{{ $data->id }}">
                 <!-- drag handle -->
                     <span class="handle">
                       <i class="fa fa-ellipsis-v"></i>
                       <i class="fa fa-ellipsis-v"></i>
                     </span>
                     <span class="text">{!! $data->title !!}</span>
               </li>
             @endforeach
             </ul>
           </div>

@endsection

@section('tambahan_script')
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
  </script>
  <script>
  $(document).ready(function () {
    $('ul').sortable({
        axis: 'y',
        stop: function (event, ui) {
	        var data = $(this).sortable('serialize');
            // $('span').text(data);
            console.log(data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                    data: data,
                type: 'POST',
                url: '{{route('chartbar.sortsave')}}'
            });
  	       }
      });
  });
  </script>
@endsection
