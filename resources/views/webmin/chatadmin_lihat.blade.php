@extends('webmin.admin_template')

@section('judul')
  Chat Management
@endsection

@section('page_header')
  Chat Management
@endsection

@section('page_name')
  Chat Management
@endsection

@section('page_description')
  Menambahkan Chat baru
@endsection

@section('tambahan_head')
  <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('webmin_content')
  <div class="box box-primary direct-chat direct-chat-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Chat dengan {{ $data->nama }}</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- Conversations are loaded here -->
      <div class="direct-chat-messages">
        <!-- Message. Default to the left -->
        @foreach ($listchat as $list)
          @if (!$list->userid)
            <div class="direct-chat-msg">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left">{{ $list->nama }}</span>
                <span class="direct-chat-timestamp pull-right">{{ $list->created_at->format("d F Y H:i:s")}}</span>
              </div>
              <div class="direct-chat-text">
                {{ $list->message }}
              </div>
            </div>
          @else
            <div class="direct-chat-msg right">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-right">{{ @$listadmin[$list->userid]->name }}</span>
                <span class="direct-chat-timestamp pull-left">{{ $list->created_at->format("d F Y H:i:s")}}</span>
              </div>
              <div class="direct-chat-text">
                {{ $list->message }}
              </div>
            </div>
          @endif
        @endforeach

      </div>
      <!-- /.direct-chat-pane -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <div class="input-group">
          <input type="text" name="msg" id="msg" placeholder="Ketik pesan disini..." class="form-control">
              <span class="input-group-btn">
                <button type="button" class="btn btn-primary btn-flat" id="kirimchat">Kirim</button>
              </span>
        </div>
    </div>
    <!-- /.box-footer-->
  </div>

          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')
<script>
  function tambahMessage(){
		$.ajax({
					url: '{{ route('chat.listchat', ['sessid' => $data->appsessionid ]) }}',
					method: 'get',
					dataType: 'html',
					success: function(data) {
            if (data){
              $( ".direct-chat-messages" ).append(data);
  						$('.direct-chat-messages').animate({
     							scrollTop: $('.direct-chat-messages').get(0).scrollHeight}, 500);
            }

					}
			});
	}

  $('.direct-chat-messages').animate({
      scrollTop: $('.direct-chat-messages').get(0).scrollHeight}, 500);

  window.setInterval(function(){
      tambahMessage(); /// call your function here
  }, 1000);

  function kirimchatnya(){
		var msg = $('#msg').val();
		var sesi = '{{ $data->appsessionid }}';

		var urlnya = "{{ route('chat.postchat') }}";
		$.ajaxSetup({
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
		});

		$.ajax({
					url: urlnya,
					method: 'POST',
					data: {msg:msg,sessionid:sesi},
					dataType: 'json',
					success: function(data) {
						if (data.error < 1){
							$('#msg').val('');
						} else {
							alert(data.message);
						}
					}
			});
	}

  $("#kirimchat").click(function(){
		kirimchatnya();
	});

	$('#msg').bind("enterKey",function(e){
		kirimchatnya();
	});
	$('#msg').keyup(function(e){
		if(e.keyCode == 13)
		{
		  $(this).trigger("enterKey");
		}
	});
</script>
@endsection
