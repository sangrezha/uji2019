@extends('webmin.admin_template')

@section('judul')
  Chat Management
@endsection

@section('page_header')
  Chat Management
@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
@endsection

@section('page_name')
  Chat Management
@endsection

@section('page_description')
  Halaman untuk Chat CMS
@endsection

@section('webmin_content')
  <div class="box">
    {{-- <div class="box-header">
      <button type="button" class="btn btn-primary" onclick="location.href = '{{route('pegawai.add')}}';">Tambah Data Pegawai</button>
      <button type="button" class="btn btn-success" onclick="location.href = '{{route('pegawai.sort')}}';">Urutkan Data Pegawai</button>
    </div> --}}
    <!-- /.box-header -->
    <div class="box-body">
      <table id="tableDataBrowse" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th width="175">Aksi</th>
          <th>Nama</th>
          <th>Email</th>
          <th>Subject</th>
          <th>Tanggal Posting</th>
          <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dataList as $data)
        <tr {!! (!$data->status) ? "class='danger'" : '' !!}>
          <td>
            <a class="btn btn-primary" href="{!! route('chat.lihat', ['id' => $data->id]) !!}" role="button" target="_blank">Lihat/Jawab Chat</a>
            <button type="button" class="btn btn-danger" onclick="confirmDelete(' Chat {{{ $data->nama }}}','{!! route('chat.delete', ['id' => $data->id]) !!}');">Hapus</button>
          </td>
          <td>{{{ $data->nama }}}</td>
          <td>{{ $data->email }}</td>
          <td> {{ config('app.chatSubject')[$data->subject] }}</td>
          <td> {{ $data->created_at}} </td>
          <td> {{ config('app.chatStatus')[$data->status] }}</td>
        </tr>
      @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection

@section('tambahan_script')
  <script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
  <script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
  <!-- page script -->
  <script>
    $(function () {
      $("#tableDataBrowse").DataTable();
    });
  </script>
@endsection
