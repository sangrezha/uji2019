<p>Dear {{ $name }},</p>
<br><br>
<p>Anda mendapat permintaan untuk menyetujui konten Balai Pengujian Perkeretaapian .</p><br>
<p>Link: <a href="{{ url('webadmin/approval') }}" target="_blank">Klik tautan ini untuk menyetujui</a></p>
<br><br>
<p>Hormat Kami,</p>
<p>Tim Support Balai Pengujian Perkeretaapian</p>
