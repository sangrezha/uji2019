@extends('webmin.admin_template')

@section('judul')
  Event Management - Tambah Kegiatan baru
@endsection

@section('page_header')
  Event Management - Tambah Kegiatan baru
@endsection

@section('page_name')
  Event Management - Tambah Kegiatan baru
@endsection

@section('page_description')
  Menambah Kegiatan baru
@endsection

@section('tambahan_head')
  <!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('/public/plugins/daterangepicker/daterangepicker.css') }}">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('/public/plugins/datepicker/datepicker3.css') }}">
  <script type="text/javascript" src="{{ asset('/public/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>
  <script type="text/javascript">
tinymce.init({
            selector : "textarea",
   plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],

   toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",

   image_advtab: true ,
   height: 250,
     relative_urls: false,
   external_filemanager_path:"{!! str_finish(asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/'),'/') !!}",
  filemanager_title        :"File Manager" , // bisa diganti terserah anda
  external_plugins         : { "filemanager" : "{{ asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/plugin.min.js') }}"},
});

</script>
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('event.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('codepage')) ? 'has-error' : '' }}">
                  <label>Tanggal Publish</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" name="publish_date" id="publish_date"  value="{{old('publish_date')}}">
                  </div>
                  @if ($errors->has('publish_date'))
                  				<span class="help-block">{{ $errors->first('publish_date') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
                  <label for="title">Judul</label>
                  <input type="text" class="form-control" id="title" name="title" placeholder="Judul Berita" value="{{old('title')}}">
                  @if ($errors->has('title'))
                  				<span class="help-block">{{ $errors->first('title') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('lead')) ? 'has-error' : '' }}">
                  <label for="lead">Teks Lead</label>
                  <input type="text" class="form-control" id="lead" name="lead" placeholder="Teks Lead" value="{{old('lead')}}">
                  @if ($errors->has('lead'))
                  				<span class="help-block">{{ $errors->first('lead') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('content')) ? 'has-error' : '' }}">
                  <label for="content">Konten Berita</label>
                  <textarea class="form-control" name="content" rows="3" placeholder="Isi konten berita disini">{{old('content')}}</textarea>
                  @if ($errors->has('content'))
                  				<span class="help-block">{{ $errors->first('content') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('thumbnail')) ? 'has-error' : '' }}">
                  <label for="thumbnail">Thumbnail (260 x 193 px)</label>
                  <input type="file" id="thumbnail" name="thumbnail">
                  @if ($errors->has('thumbnail'))
                  				<span class="help-block">{{ $errors->first('thumbnail') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('picture')) ? 'has-error' : '' }}">
                  <label for="picture">Gambar (max 1200 x 353 px)</label>
                  <input type="file" id="picture" name="picture">
                  @if ($errors->has('picture'))
                  				<span class="help-block">{{ $errors->first('picture') }}</span>
                  @endif
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')
<script src="{{ asset('/public/plugins/datepicker/bootstrap-datepicker.js') }}""></script>
<script>
  $('#publish_date').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'    });
</script>
@endsection
