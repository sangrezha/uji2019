@extends('webmin.admin_template')

@section('judul')
  Event Management
@endsection

@section('page_header')
  Event Management
@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
@endsection

@section('page_name')
  Event Management
@endsection

@section('page_description')
  Halaman Kegiatan
@endsection

@section('webmin_content')
  <div class="box">
    <div class="box-header">
      <button type="button" class="btn btn-primary" onclick="location.href = '{{route('event.add')}}';">Tambah Kegiatan Baru</button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="tableDataBrowse" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th width="175">Aksi</th>
          <th>Judul</th>
          <th>Lead</th>
          <th>Tanggal Publish</th>
          <th>Dilihat</th>
          <th>Tanggal Posting</th>
          <th>Diposting Oleh</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dataList as $data)
        <tr>
          <td>
            <button type="button" class="btn btn-primary" onclick="location.href = '{!! route('event.edit', ['id' => $data->id]) !!}';">Ubah</button>
            <button type="button" class="btn btn-danger" onclick="confirmDelete('{{{ $data->title }}}','{!! route('event.delete', ['id' => $data->id]) !!}');">Hapus</button>
          </td>
          <td>{{{ $data->title }}}</td>
          <td>{{ $data->lead }}</td>
          <td>{{ $data->publish_date }}</td>
          <td>{{ $data->counter }} x</td>
          <td> {{ $data->created_at }} </td>
          <td> {{ $data->created_by }}</td>
        </tr>
      @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection

@section('tambahan_script')
  <script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
  <script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
  <!-- page script -->
  <script>
    $(function () {
      $("#tableDataBrowse").DataTable();
    });
  </script>
@endsection
