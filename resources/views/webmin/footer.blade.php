
<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Kementerian Perhubungan Republik Indonesia
    </div>
    <!-- Default to the left -->
    Copyright &copy; 2019 Direktorat Jenderal Perkeretaapian, All rights reserved.
  </footer>
  <audio id="myAudio">
  <source src="{{asset('/public/assets/sfx/notif.ogg')}}" type="audio/ogg">
  <source src="{{asset('/public/assets/sfx/notif.mp3')}}" type="audio/mpeg">
</audio>
  <script type="text/javascript">
  var x = document.getElementById("myAudio");
  function checkNotif(){
    $.ajax({
          url: '{{ route('chat.chatnotif') }}',
          method: 'get',
          dataType: 'html',
          success: function(data) {
            if (data){
              $( "#notifinfo" ).html(data);
              // x.play();
            }
          }
      });
  }

  function countNotif(){
    $.ajax({
          url: '{{ route('chat.chatcount') }}',
          method: 'get',
          dataType: 'html',
          success: function(data) {
            if (data){
              $( "#jumnotif" ).html(data);
            }
          }
      });
  }

  window.setInterval(function(){
      checkNotif(); /// call your function here
      countNotif();
  }, 1000);
  </script>
