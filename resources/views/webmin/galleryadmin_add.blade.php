@extends('webmin.admin_template')

@section('judul')
  Gallery Management - Tambah Galeri baru
@endsection

@section('page_header')
  Gallery Management - Tambah Galeri baru
@endsection

@section('page_name')
  Gallery Management - Tambah Galeri baru
@endsection

@section('page_description')
  Menambah Galeri baru
@endsection

@section('tambahan_head')
  <link rel="stylesheet" href="{{ asset("/public/assets/js/croppie/croppie.css") }}">
  <style>

    #upload-demo
    {
      position: relative;
    }

    .image-upload > input {
      visibility:hidden;
      width:0;
      height:0;
      background:transparent;
  }


  </style>
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('gal.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('codepage')) ? 'has-error' : '' }}">
                  <label for="halaman">Halaman</label>
                  <select class="form-control select2" name="halaman" style="width: 100%;">
                    @foreach(config('app.halamanGallery') as $k=>$v)
                      <option value="{{ $k }}" {{($k==old('halaman')) ? 'Selected' : ''}}>{{ $v }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('halaman'))
                  				<span class="help-block">{{ $errors->first('halaman') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul" value="{{old('judul')}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('thumbnail')) ? 'has-error' : '' }}">
                  <label for="thumbnail">Thumbnail (188 x 140 px)</label>
                  <input type="file" id="upload" name="thumbnail" data-target="#myModal" data-toggle="modal" >
                  <div id="upload-demo-i" style="cursor: pointer" onclick="editImage()"></div>
                  @if ($errors->has('thumbnail'))
                  				<span class="help-block">{{ $errors->first('thumbnail') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('picture')) ? 'has-error' : '' }}">
                  <label for="picture">Gambar</label>
                  <input type="file" id="picture" name="picture">
                  @if ($errors->has('picture'))
                          <span class="help-block">{{ $errors->first('picture') }}</span>
                  @endif
                </div>

              </div>
              <!-- /.box-body -->


              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
          <!-- /.box -->
          <div id="myModal" class="modal fade " role="dialog">
            <div class="modal-dialog"  style="width:500px">

              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button"   style=" "class="close" data-dismiss="modal">&times;</button>

                  </div>
                  <div class="modal-body">

                    <div id="upload-demo" style="width:500px"></div>

                    <button class="btn btn-success upload-result" data-dismiss="modal">Upload Image</button>

                </div>
              </div>

            </div>
          </div>

@endsection

@section('tambahan_script')
  <script src="{{ asset("/public/assets/js/croppie/croppie.min.js") }}"></script>

  <script type="text/javascript">
  $uploadCrop = $('#upload-demo').croppie({
      enableExif: true,
      viewport: {
          width: 188,
          height: 140,
          type: 'rectangle'
      },
      boundary: {
          width: 400,
          height: 400
      },
      enableResize: false
  });

  $('#upload').on('change', function () {
    var reader = new FileReader();
      reader.onload = function (e) {
        $uploadCrop.croppie('bind', {
          url: e.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });

      }
      reader.readAsDataURL(this.files[0]);
  });

  $('.upload-result').on('click', function (ev) {
    $uploadCrop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function (resp) {

    $.ajax({
        url: "{{ route('gal.postimage') }}",
        type: "POST",
        data: {
          "_token": "{{ csrf_token() }}",
          "image":resp
        },
        success: function (data) {//alert(data);
          html = '<img src="' + resp + '" />';
          $("#upload-demo-i").html(html);
          // console.log(data);
        }
      });
    });
  });
  /*This function is added for Image Reupload Facility: Start*/
  function editImage() {
    //alert("hiiiiiii");

    // location.reload(true);
    editImage2();


  }

  function editImage() {
    $("#upload").click();
  }

  /*This function is added for Image Reupload Facility: End*/


  </script>
@endsection
