  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Balai Pengujian Perkeretaapian CMS | @yield('judul')</title>
  <meta name="robots" content="noindex,nofollow" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset("/public/bootstrap/css/bootstrap.min.css") }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{ asset("/public/plugins/datatables/dataTables.bootstrap.css") }}">


  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset("/public/dist/css/AdminLTE.min.css") }}">

  <link rel="stylesheet" href="{{ asset("/public/plugins/iCheck/square/blue.css") }}">

  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="{{ asset("/public/dist/css/skins/_all-skins.min.css") }}">

  <script>
  function set_action(param)
  {
    var stats = $(param).attr('status');
    $('form input[name=status][type=hidden]').remove();
    if(typeof stats != 'undefined')
    {
      $(param).parent().append('<input type="hidden" name="status" value="'+stats+'" />');
    }    
    // console.log(param.form);
     document.body.style.cursor = 'wait';
     param.form.target = '_self';
     param.form.submit();
  }

  function confirmDelete(pesan, alamat)
  {
    var x = confirm("Anda yakin ingin menghapus "+pesan+"?");
    if (x) {
         window.location.href=alamat;
        return true;
      }
    else {
      return false;}
  }
  </script>

  @yield('tambahan_head')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
