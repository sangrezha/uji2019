@extends('webmin.admin_template')

@section('judul')
  Highlight Management - Ubah Highlight
@endsection

@section('page_header')
  Highlight Management - Ubah Highlight
@endsection

@section('page_name')
  Highlight Management - Ubah Highlight
@endsection

@section('page_description')
  Merubah Highlight baru di home
@endsection

@section('tambahan_head')
<link rel="stylesheet" href="{{ asset("/public/assets/js/croppie/croppie.css") }}">
@endsection

@section('webmin_content')
  <!-- form start -->
  <form role="form" enctype="multipart/form-data" action="{{ route('highlight.update', ['id' => $data->id]) }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul" value="{{$data->title}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('highlight')) ? 'has-error' : '' }}">
                  <label for="highlight">Teks Highlight</label>
                  <input type="text" class="form-control" id="highlight" name="highlight" placeholder="Teks Highlight" value="{{$data->highlight}}">
                  @if ($errors->has('highlight'))
                  				<span class="help-block">{{ $errors->first('highlight') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('gambar')) ? 'has-error' : '' }}">
                  <label for="gambar">Gambar (1366 x 584 px, Home tanpa gambar)</label>
                  <input type="file" id="gambar" name="gambar" data-target="#myModal" data-toggle="modal" >
                  <div id="upload-demo-i" style="cursor: pointer" onclick="editImage()"></div>
                  @if ($errors->has('gambar'))
                          <span class="help-block">{{ $errors->first('gambar') }}</span>
                  @endif
                </div>
                @if ($data->picture)
                <div class="form-group">
                  <img src="{{asset('public/images/highlight/'.$data->picture)}}" width="100" />
                  <br /><br />
                  <input type='checkbox' name="picture_del" /> Centang untuk hapus gambar
                </div>
                @endif

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
          <!-- /.box -->
          <div id="myModal" class="modal fade " role="dialog">
            <div class="modal-dialog"  style="width:1450px">

              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button"   style=" "class="close" data-dismiss="modal">&times;</button>

                  </div>
                  <div class="modal-body">

                    <div id="upload-demo" style="width:1450px"></div>

                    <button class="btn btn-success upload-result" data-dismiss="modal">Upload Image</button>

                </div>
              </div>

            </div>
          </div>

@endsection

@section('tambahan_script')

<script src="{{ asset("/public/assets/js/croppie/croppie.min.js") }}"></script>
<script type="text/javascript">
  $uploadCrop = $('#upload-demo').croppie({
      enableExif: true,
      viewport: {
          width: 1366,
          height: 584,
          type: 'rectangle'
      },
      boundary: {
          width: 1410,
          height: 400
      },
      enableResize: false
  });

  $('#gambar').on('change', function () {
    var reader = new FileReader();
      reader.onload = function (e) {
        $uploadCrop.croppie('bind', {
          url: e.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });

      }
      reader.readAsDataURL(this.files[0]);
  });

  $('.upload-result').on('click', function (ev) {
    $uploadCrop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function (resp) {

    $.ajax({
        url: "{{ route('highlight.postimage') }}",
        type: "POST",
        data: {
          "_token": "{{ csrf_token() }}",
          "image":resp
        },
        success: function (data) {//alert(data);
          html = '<img src="' + resp + '" />';
          $("#upload-demo-i").html(html);
          // console.log(data);
        }
      });
    });
  });
  /*This function is added for Image Reupload Facility: Start*/
  function editImage() {
    //alert("hiiiiiii");

    // location.reload(true);
    editImage2();


  }

  function editImage() {
    $("#gambar").click();
  }

  /*This function is added for Image Reupload Facility: End*/


  </script>
@endsection
