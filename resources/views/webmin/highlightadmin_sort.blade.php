@extends('webmin.admin_template')

@section('judul')
  Highlight Management - Mengurutkan Highlight
@endsection

@section('page_header')
  Highlight Management - Mengurutkan Highlight
@endsection

@section('page_name')
  Highlight Management - Mengurutkan Highlight
@endsection

@section('page_description')
  Mengurutkan Highlight home
@endsection

@section('tambahan_head')
  <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('webmin_content')
  <div class="box-header">
    <button type="button" class="btn btn-primary" onclick="location.href = '{{route('highlight.dashboard')}}';">Kembali ke Highlight</button>
  </div>
  <div class="box-body">
             <ul class="todo-list" id="sortable">
               @foreach($dataList as $data)
                 <li id="item-{{ $data->id }}">
                 <!-- drag handle -->
                     <span class="handle">
                       <i class="fa fa-ellipsis-v"></i>
                       <i class="fa fa-ellipsis-v"></i>
                     </span>
                 <img src="{{asset('public/images/highlight/'.$data->picture)}}" height="50" /><span class="text">{!! $data->title !!}</span>
               </li>
             @endforeach
             </ul>
           </div>

@endsection

@section('tambahan_script')
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
  } );
  </script>
  <script>
  $(document).ready(function () {
    $('ul').sortable({
        axis: 'y',
        stop: function (event, ui) {
	        var data = $(this).sortable('serialize');
            // $('span').text(data);
            console.log(data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                    data: data,
                type: 'POST',
                url: '{{route('highlight.sortsave')}}'
            });
  	       }
      });
  });
  </script>
@endsection
