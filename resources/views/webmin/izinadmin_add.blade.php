@extends('webmin.admin_template')

@section('judul')
  Regulasi Management - Tambah
@endsection

@section('page_header')
  Regulasi Management - Tambah
@endsection

@section('page_name')
  Regulasi Management - Tambah
@endsection

@section('page_description')
  Menambah CMS Regulasi Pengujian
@endsection

@section('tambahan_head')
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('izin.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('kategori')) ? 'has-error' : '' }}">
                  <label for="kategori">Kategori</label>
                  <select class="form-control select2" name="kategori" style="width: 100%;">
                    @foreach($dataList as $data)
                      <option value="{{ $data->id }}" {{($data->id==old('category')) ? 'Selected' : ''}}>{{$data->name}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('kategori'))
                  				<span class="help-block">{{ $errors->first('kategori') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul" value="{{old('judul')}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('tahun')) ? 'has-error' : '' }}">
                  <label for="tahun">Tahun</label>
                  <input type="tahun" class="form-control" id="tahun" name="tahun" placeholder="Tahun" value="{{old('tahun')}}">
                  @if ($errors->has('tahun'))
                  				<span class="help-block">{{ $errors->first('tahun') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('tentang')) ? 'has-error' : '' }}">
                  <label for="tentang">Tentang</label>
                  <textarea class="form-control" name="tentang" rows="3" placeholder="Isi Tentang apa disini">{{old('tentang')}}</textarea>
                  @if ($errors->has('tentang'))
                  				<span class="help-block">{{ $errors->first('tentang') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('attachfile')) ? 'has-error' : '' }}">
                  <label for="attachfile">File Attachment</label>
                  <input type="file" id="attachfile" name="attachfile">
                  @if ($errors->has('attachfile'))
                  				<span class="help-block">{{ $errors->first('attachfile') }}</span>
                  @endif
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

<script>
$(function () {
    $('#attachfile').change(function () {
        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(pdf)$");

        if (!(regex.test(val))) {
            $(this).val('');
            alert('Hanya file PDF');
        }
    });
});
</script>
@endsection
