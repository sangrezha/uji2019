@extends('webmin.admin_template')

@section('judul')
   Regulasi Management - Ubah
@endsection

@section('page_header')
   Regulasi Management - Ubah
@endsection

@section('page_name')
   Regulasi Management - Ubah
@endsection

@section('page_description')
  Mengubah CMS Regulasi
@endsection

@section('tambahan_head')

@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('izin.update', ['id' => $data->id]) }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('kategori')) ? 'has-error' : '' }}">
                  <label for="kategori">Kategori</label>
                  <select class="form-control select2" name="kategori" style="width: 100%;">
                    @foreach($dataList as $datas)
                      <option value="{{ $datas->id }}" {{($datas->id == $data->category_id) ? 'Selected' : ''}}>{{$datas->name}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('category'))
                  				<span class="help-block">{{ $errors->first('category') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Judul</label>
                  <input type="title" class="form-control" id="judul" name="judul" placeholder="Judul" value="{{$data->title}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('tahun')) ? 'has-error' : '' }}">
                  <label for="tahun">Tahun</label>
                  <input type="tahun" class="form-control" id="tahun" name="tahun" placeholder="Tahun" value="{{$data->tahun}}">
                  @if ($errors->has('tahun'))
                  				<span class="help-block">{{ $errors->first('tahun') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('tentang')) ? 'has-error' : '' }}">
                  <label for="tentan">Tentang</label>
                  <textarea class="form-control" name="tentang" rows="3" placeholder="Isi Tentang apa disini">{{$data->tentang}}</textarea>
                  @if ($errors->has('tentang'))
                  				<span class="help-block">{{ $errors->first('tentang') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('picture')) ? 'has-error' : '' }}">
                  <label for="attachfile">File Attachment</label>
                  <input type="file" id="attachfile" name="attachfile">
                  @if ($errors->has('attachfile'))
                  				<span class="help-block">{{ $errors->first('attachfile') }}</span>
                  @endif
                </div>
                @if ($data->attachfile)
                <div class="form-group">
                  <a href="{{asset('/storage/app/public/website/'.$data->attachfile)}}" target="_blank">{{$data->attachfile}}</a>
                  <br /><br />
                  <input type='checkbox' name="attachfile_del" /> Check to remove file attachment
                </div>
                @endif

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
