@extends('webmin.admin_template')

@section('judul')
  Jabatan Management - Tambah Jabatan baru
@endsection

@section('page_header')
  Jabatan Management - Tambah Jabatan baru
@endsection

@section('page_name')
  Jabatan Management - Tambah Jabatan baru
@endsection

@section('page_description')
  Menambahkan Jabatan baru
@endsection

@section('tambahan_head')
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('jabatan.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('nama')) ? 'has-error' : '' }}">
                  <label for="nama">Nama Jabatan</label>
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Jabatan" value="{{old('nama')}}">
                  @if ($errors->has('nama'))
                  				<span class="help-block">{{ $errors->first('nama') }}</span>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
