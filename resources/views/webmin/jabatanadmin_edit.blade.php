@extends('webmin.admin_template')

@section('judul')
  Jabatan Management - Ubah Jabatan
@endsection

@section('page_header')
  Jabatan Management - Ubah Jabatan
@endsection

@section('page_name')
  Jabatan Management - Ubah Jabatan
@endsection

@section('page_description')
  Merubah Jabatan baru di home
@endsection

@section('tambahan_head')
@endsection

@section('webmin_content')
  <!-- form start -->
  <form role="form" enctype="multipart/form-data" action="{{ route('jabatan.update', ['id' => $data->id]) }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('nama')) ? 'has-error' : '' }}">
                  <label for="nama">Nama Jabatan</label>
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Jabatan" value="{{$data->title}}">
                  @if ($errors->has('nama'))
                  				<span class="help-block">{{ $errors->first('nama') }}</span>
                  @endif
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
