@extends('webmin.admin_template')

@section('judul')
  Mitra Management - Ubah Mitra
@endsection

@section('page_header')
  Mitra Management - Ubah Mitra
@endsection

@section('page_name')
  Mitra Management - Ubah Mitra
@endsection

@section('page_description')
  Merubah Mitra baru di home
@endsection

@section('tambahan_head')
  <!-- <script type="text/javascript" src="{{ asset('/public/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script> -->
  <script type="text/javascript">
// tinymce.init({
//             selector : "textarea",
//             content_css : '{{ asset('/public/assets/css/editor-style.css') }},{{asset('/public/assets/bootstrap/css/bootstrap.min.css')}},{{ asset('/public/assets/css/editor-style.css') }}',
//    plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],

//    toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",

//    image_advtab: true ,
//    height: 250,
//      relative_urls: false,
//    external_filemanager_path:"{!! str_finish(asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/'),'/') !!}",
//   filemanager_title        :"File Manager" , // bisa diganti terserah anda
//   external_plugins         : { "filemanager" : "{{ asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/plugin.min.js') }}"},
// });
</script>
@endsection

@section('webmin_content')
  <!-- form start -->
  <form role="form" enctype="multipart/form-data" action="{{ route('mitra.update', ['id' => $data->id]) }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Nama Mitra</label>
                  <input type="text" class="form-control" id="judul" name="judul" placeholder="Nama Mitra" value="{{$data->title}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <!-- <div class="form-group {{ ($errors->has('konten')) ? 'has-error' : '' }}">
                  <label for="konten">Konten</label>
                  <textarea class="form-control" name="konten" rows="3" placeholder="Isi konten penjelasan mitra disini">{{$data->content}}</textarea>
                  @if ($errors->has('konten'))
                  				<span class="help-block">{{ $errors->first('konten') }}</span>
                  @endif
                </div> -->
                <div class="form-group {{ ($errors->has('gambar')) ? 'has-error' : '' }}">
                  <label for="gambar">Gambar Logo Mitra (1366 x 584 px, Home tanpa gambar)</label>
                  <input type="file" id="gambar" name="gambar">
                  @if ($errors->has('gambar'))
                          <span class="help-block">{{ $errors->first('gambar') }}</span>
                  @endif
                </div>
                @if ($data->picture)
                <div class="form-group">
                  <img src="{{asset('public/images/mitra/'.$data->picture)}}" width="100" />
                  <br /><br />
                  <input type='checkbox' name="picture_del" /> Centang untuk hapus gambar
                </div>
                @endif

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
