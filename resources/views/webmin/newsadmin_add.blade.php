@extends('webmin.admin_template')

@section('judul')
  News Management - Tambah Berita baru
@endsection

@section('page_header')
  News Management - Tambah Berita baru
@endsection

@section('page_name')
  News Management - Tambah Berita baru
@endsection

@section('page_description')
  Menambah Berita baru
@endsection

@section('tambahan_head')
  <!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('/public/plugins/daterangepicker/daterangepicker.css') }}">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('/public/plugins/datepicker/datepicker3.css') }}">
  <script type="text/javascript" src="{{ asset('/public/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>
  <script type="text/javascript">
tinymce.init({
            selector : "textarea",
           content_css : 'https://fonts.googleapis.com/css?family=Roboto, {{asset('/public/assets/bootstrap/css/bootstrap.min.css')}},{{ asset('/public/assets/css/editor-style.css') }}',
   plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],

   toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",

   image_advtab: true ,
   height: 250,
     relative_urls: false,
   external_filemanager_path:"{!! str_finish(asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/'),'/') !!}",
  filemanager_title        :"File Manager" , // bisa diganti terserah anda
  external_plugins         : { "filemanager" : "{{ asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/plugin.min.js') }}"},
});

</script>
  <link rel="stylesheet" href="{{ asset("/public/assets/js/croppie/croppie.css") }}">
  <style>

    #upload-demo
    {
      position: relative;
    }

    .image-upload > input {
      visibility:hidden;
      width:0;
      height:0;
      background:transparent;
  }


  </style>
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('news.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('tanggal_publish')) ? 'has-error' : '' }}">
                  <label>Tanggal Publish</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" name="tanggal_publish" id="tanggal_publish"  value="{{old('tanggal_publish')}}">
                  </div>
                  @if ($errors->has('tanggal_publish'))
                  				<span class="help-block">{{ $errors->first('tanggal_publish') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Berita" value="{{old('judul')}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('lead')) ? 'has-error' : '' }}">
                  <label for="lead">Teks Lead</label>
                  <input type="text" class="form-control" id="lead" name="lead" placeholder="Teks Lead" value="{{old('lead')}}">
                  @if ($errors->has('lead'))
                  				<span class="help-block">{{ $errors->first('lead') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('konten')) ? 'has-error' : '' }}">
                  <label for="konten">Konten Berita</label>
                  <textarea class="form-control" name="konten"  id="konten" rows="3" placeholder="Isi konten berita disini">{{old('konten')}}</textarea>
                  @if ($errors->has('konten'))
                  				<span class="help-block">{{ $errors->first('konten') }}</span>
                  @endif
                </div>

                <div class="form-group {{ ($errors->has('thumbnail')) ? 'has-error' : '' }}">
                  <label for="thumbnail">Thumbnail (282 x 210 px)</label>
                  <input type="file" id="upload" name="thumbnail" data-target="#myModal" data-toggle="modal" >
                  <div id="upload-demo-i" style="cursor: pointer" onclick="editImage()"></div>
                  @if ($errors->has('thumbnail'))
                  				<span class="help-block">{{ $errors->first('thumbnail') }}</span>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Save</button>
                <button type="button" class="btn btn-default" status="draft" onclick="set_action(this)">Save Draft</button>
                <i id="preview" class="btn btn-warning">Preview</i>
              </div>
            </form>
          </div>
          <!-- /.box -->
          <!-- /.box -->
          <div id="myModal" class="modal fade " role="dialog">
            <div class="modal-dialog"  style="width:500px">

              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button"   style=" "class="close" data-dismiss="modal">&times;</button>

                  </div>
                  <div class="modal-body">

                    <div id="upload-demo" style="width:500px"></div>

                    <button class="btn btn-success upload-result" data-dismiss="modal">Upload Image</button>

                </div>
              </div>

            </div>
          </div>

@endsection

@section('tambahan_script')
<script src="{{ asset('/public/plugins/datepicker/bootstrap-datepicker.js') }}""></script>
<script>
  $('#tanggal_publish').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'    });
    $('#preview').click(function () {
        var redirectWindow = window.open('{{ route('web.preview',['modul' => 'news']) }}&tanggal_publish='+encodeURIComponent($("#tanggal_publish").val())+'&judul='+encodeURIComponent($("#judul").val())+'&lead='+encodeURIComponent($("#lead").val())+'&konten='+encodeURIComponent(tinyMCE.editors[$('#konten').attr('id')].getContent()), '_blank');
        redirectWindow.location;
      });
</script>
<script src="{{ asset("/public/assets/js/croppie/croppie.min.js") }}"></script>

<script type="text/javascript">
$uploadCrop = $('#upload-demo').croppie({
    enableExif: true,
    viewport: {
        width: 282,
        height: 210,
        type: 'rectangle'
    },
    boundary: {
        width: 400,
        height: 400
    },
    enableResize: false
});

$('#upload').on('change', function () {
  var reader = new FileReader();
    reader.onload = function (e) {
      $uploadCrop.croppie('bind', {
        url: e.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });

    }
    reader.readAsDataURL(this.files[0]);
});

$('.upload-result').on('click', function (ev) {
  $uploadCrop.croppie('result', {
    type: 'canvas',
    size: 'viewport'
  }).then(function (resp) {

  $.ajax({
      url: "{{ route('news.postimage') }}",
      type: "POST",
      data: {
        "_token": "{{ csrf_token() }}",
        "image":resp
      },
      success: function (data) {//alert(data);
        html = '<img src="' + resp + '" />';
        $("#upload-demo-i").html(html);
        // console.log(data);
      }
    });
  });
});
/*This function is added for Image Reupload Facility: Start*/
function editImage() {
  //alert("hiiiiiii");

  // location.reload(true);
  editImage2();


}

function editImage() {
  $("#upload").click();
}

/*This function is added for Image Reupload Facility: End*/


</script>
@endsection
