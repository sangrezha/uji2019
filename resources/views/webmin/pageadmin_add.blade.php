@extends('webmin.admin_template')

@section('judul')
  Page Management - Add New Page
@endsection

@section('page_header')
  Page Management - Add New Page
@endsection

@section('page_name')
  Page Management - Add New Page
@endsection

@section('page_description')
  Menambah CMS Page
@endsection

@section('tambahan_head')
<link rel="stylesheet" href="{{ asset("/public/assets/js/croppie/croppie.css") }}">
  <style>
  body.wait, body.wait *{
   cursor: wait !important;
  }
  </style>
  <script type="text/javascript" src="{{ asset('/public/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>
  <script type="text/javascript">
tinymce.init({
            selector : "textarea",
            content_css : 'https://fonts.googleapis.com/css?family=Roboto, {{asset('/public/assets/bootstrap/css/bootstrap.min.css')}},{{ asset('/public/assets/css/editor-style.css') }}',
   plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],

   toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",

   image_advtab: true ,
   height: 250,
     relative_urls: false,
   external_filemanager_path:"{!! str_finish(asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/'),'/') !!}",
  filemanager_title        :"File Manager" , // bisa diganti terserah anda
  external_plugins         : { "filemanager" : "{{ asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/plugin.min.js') }}"},
});

</script>
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('page.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('codepage')) ? 'has-error' : '' }}">
                  <label for="halaman">Halaman</label>
                  <select class="form-control select2" name="halaman" style="width: 100%;">
                    @foreach(config('app.halamanWeb') as $k=>$v)
                      <option value="{{ $k }}" {{($k==old('codepage')) ? 'Selected' : ''}}>{{config('app.halamanWeb')[$k]['nama']}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('halaman'))
                  				<span class="help-block">{{ $errors->first('halaman') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Halaman" value="{{old('judul')}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('highlight')) ? 'has-error' : '' }}">
                  <label for="highlight">Teks Highlight</label>
                  <input type="text" class="form-control" id="highlight" name="highlight" placeholder="Teks Highlight" value="{{old('highlight')}}">
                  @if ($errors->has('highlight'))
                  				<span class="help-block">{{ $errors->first('highlight') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('konten')) ? 'has-error' : '' }}">
                  <label for="konten">Konten</label>
                  <textarea class="form-control" name="konten" rows="3" placeholder="Isi konten disini">{{old('konten')}}</textarea>
                  @if ($errors->has('konten'))
                  				<span class="help-block">{{ $errors->first('konten') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('gambar')) ? 'has-error' : '' }}">
                  <label for="gambar">Gambar (1366 x 376 px, Home tanpa gambar)</label>
                  <input type="file" id="gambar" name="gambar"  data-target="#myModal" data-toggle="modal" >
                  <div id="upload-demo-i" style="cursor: pointer" onclick="editImage()"></div>
                  @if ($errors->has('gambar'))
                  				<span class="help-block">{{ $errors->first('gambar') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('judul1')) ? 'has-error' : '' }}">
                  <label for="judul1">Judul Excel 1 (Jika Ada)</label>
                  <input type="text" class="form-control" id="judul1" name="judul1" placeholder="Judul Excel 1 (Jika Ada)" value="{{old('judul1')}}">
                  @if ($errors->has('judul1'))
                  				<span class="help-block">{{ $errors->first('judul1') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('excel1')) ? 'has-error' : '' }}">
                  <label for="excel1">File Excel</label>
                  <input type="file" id="excel1" name="excel1">
                  @if ($errors->has('excel1'))
                  				<span class="help-block">{{ $errors->first('excel1') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('judul2')) ? 'has-error' : '' }}">
                  <label for="judul2">Judul Excel 2 (Jika Ada)</label>
                  <input type="text" class="form-control" id="judul2" name="judul2" placeholder="Judul Excel 2 (Jika Ada)" value="{{old('judul1')}}">
                  @if ($errors->has('judul2'))
                  				<span class="help-block">{{ $errors->first('judul2') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('excel2')) ? 'has-error' : '' }}">
                  <label for="excel2">File Excel</label>
                  <input type="file" id="excel2" name="excel2">
                  @if ($errors->has('excel2'))
                  				<span class="help-block">{{ $errors->first('excel2') }}</span>
                  @endif
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
          <!-- /.box -->
          <div id="myModal" class="modal fade " role="dialog">
            <div class="modal-dialog"  style="width:1450px">

              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button"   style=" "class="close" data-dismiss="modal">&times;</button>

                  </div>
                  <div class="modal-body">

                    <div id="upload-demo" style="width:1450px"></div>

                    <button class="btn btn-success upload-result" data-dismiss="modal">Upload Image</button>

                </div>
              </div>

            </div>
          </div>


@endsection

@section('tambahan_script')
<script src="{{ asset("/public/assets/js/croppie/croppie.min.js") }}"></script>
<script type="text/javascript">
  $uploadCrop = $('#upload-demo').croppie({
      enableExif: true,
      viewport: {
          width: 1366,
          height: 376,
          type: 'rectangle'
      },
      boundary: {
          width: 1410,
          height: 400
      },
      enableResize: false
  });

  $('#gambar').on('change', function () {
    var reader = new FileReader();
      reader.onload = function (e) {
        $uploadCrop.croppie('bind', {
          url: e.target.result
        }).then(function(){
          console.log('jQuery bind complete');
        });

      }
      reader.readAsDataURL(this.files[0]);
  });

  $('.upload-result').on('click', function (ev) {
    $uploadCrop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function (resp) {

    $.ajax({
        url: "{{ route('page.postimage') }}",
        type: "POST",
        data: {
          "_token": "{{ csrf_token() }}",
          "image":resp
        },
        success: function (data) {//alert(data);
          html = '<img src="' + resp + '" />';
          $("#upload-demo-i").html(html);
          // console.log(data);
        }
      });
    });
  });
  /*This function is added for Image Reupload Facility: Start*/
  function editImage() {
    //alert("hiiiiiii");

    // location.reload(true);
    editImage2();


  }

  function editImage() {
    $("#gambar").click();
  }

  /*This function is added for Image Reupload Facility: End*/


  </script>
@endsection
