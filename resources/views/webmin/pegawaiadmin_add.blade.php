@extends('webmin.admin_template')

@section('judul')
  Data Pegawai Management
@endsection

@section('page_header')
  Data Pegawai Management
@endsection

@section('page_name')
  Data Pegawai Management
@endsection

@section('page_description')
  Menambahkan Data Pegawai baru
@endsection

@section('tambahan_head')
  <script type="text/javascript" src="{{ asset('/public/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>
  <script type="text/javascript">
tinymce.init({
            selector : "textarea",
            content_css : 'https://fonts.googleapis.com/css?family=Roboto, {{asset('/public/assets/bootstrap/css/bootstrap.min.css')}},{{ asset('/public/assets/css/editor-style.css') }}',
   plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],

   toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",

   image_advtab: true ,
   height: 250,
     relative_urls: false,
   external_filemanager_path:"{!! str_finish(asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/'),'/') !!}",
  filemanager_title        :"File Manager" , // bisa diganti terserah anda
  external_plugins         : { "filemanager" : "{{ asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/plugin.min.js') }}"},
});
</script>
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('pegawai.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('nama')) ? 'has-error' : '' }}">
                  <label for="nama">Nama *</label>
                  <input type="tex" class="form-control" id="nama" name="nama" placeholder="Nama Pegawai" value="{{old('nama')}}">
                  @if ($errors->has('nama'))
                  				<span class="help-block">{{ $errors->first('nama') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <input type='checkbox' value="1" name="pimpinan" {{ (old('pimpinan')) ? "checked" : "" }} /> Sebagai Pimpinan (Jabatan Akan Ditulis dan Berada di atas)
                </div>
                <div class="form-group {{ ($errors->has('divisi')) ? 'has-error' : '' }}">
                  <label for="divisi_jabatan">Divisi Jabatan *</label>
                  <select class="form-control select2" name="divisi_jabatan" style="width: 100%;">
                    @foreach($dataList as $datas)
                      <option value="{{ $datas->uniqid }}" {{($datas->uniqid == old('divisi_jabatan')) ? 'Selected' : ''}}>{{$datas->title}}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('divisi_jabatan'))
                  				<span class="help-block">{{ $errors->first('divisi_jabatan') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('nip')) ? 'has-error' : '' }}">
                  <label for="nip">NIP</label>
                  <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP" value="{{old('nip')}}">
                  @if ($errors->has('nip'))
                  				<span class="help-block">{{ $errors->first('nip') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('golongan')) ? 'has-error' : '' }}">
                  <label for="golongan">Golongan</label>
                  <input type="text" class="form-control" id="golongan" name="golongan" placeholder="Golongan" value="{{old('golongan')}}">
                  @if ($errors->has('golongan'))
                  				<span class="help-block">{{ $errors->first('golongan') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('pangkat')) ? 'has-error' : '' }}">
                  <label for="pangkat">Pangkat</label>
                  <input type="text" class="form-control" id="pangkat" name="pangkat" placeholder="Pangkat" value="{{old('pangkat')}}">
                  @if ($errors->has('pangkat'))
                  				<span class="help-block">{{ $errors->first('pangkat') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('jabatan')) ? 'has-error' : '' }}">
                  <label for="jabatan">Jabatan</label>
                  <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan" value="{{old('jabatan')}}">
                  @if ($errors->has('jabatan'))
                  				<span class="help-block">{{ $errors->first('jabatan') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('pendidikan')) ? 'has-error' : '' }}">
                  <label for="pendidikan">Pendidikan</label>
                  <input type="text" class="form-control" id="pendidikan" name="pendidikan" placeholder="Pendidikan" value="{{old('pendidikan')}}">
                  @if ($errors->has('pendidikan'))
                  				<span class="help-block">{{ $errors->first('pendidikan') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('staff')) ? 'has-error' : '' }}">
                  <label for="staff">List Staff</label>
                  <textarea class="form-control" name="staff" rows="3" placeholder="Para Staff">{{old('staff')}}</textarea>
                  @if ($errors->has('staff'))
                  				<span class="help-block">{{ $errors->first('staff') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('picture')) ? 'has-error' : '' }}">
                  <label for="picture">Photo (286 x 208 px) *</label>
                  <input type="file" id="picture" name="picture">
                  @if ($errors->has('picture'))
                  				<span class="help-block">{{ $errors->first('picture') }}</span>
                  @endif
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
