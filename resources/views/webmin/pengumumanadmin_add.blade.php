@extends('webmin.admin_template')

@section('judul')
  Pengumuman Management - Tambah Pengumuman baru
@endsection

@section('page_header')
  Pengumuman Management - Tambah Pengumuman baru
@endsection

@section('page_name')
  Pengumuman Management - Tambah Pengumuman baru
@endsection

@section('page_description')
  Menambah Pengumuman baru
@endsection

@section('tambahan_head')
  <!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('/public/plugins/daterangepicker/daterangepicker.css') }}">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('/public/plugins/datepicker/datepicker3.css') }}">
  <script type="text/javascript" src="{{ asset('/public/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>
  <script type="text/javascript">
tinymce.init({
            selector : "textarea",
            content_css : '{{ asset('/public/assets/css/styles.css') }},{{asset('/public/assets/bootstrap/css/bootstrap.min.css')}},{{ asset('/public/assets/css/editor-style.css') }}',
   plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],

   toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",

   image_advtab: true ,
   height: 250,
     relative_urls: false,
   external_filemanager_path:"{!! str_finish(asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/'),'/') !!}",
  filemanager_title        :"File Manager" , // bisa diganti terserah anda
  external_plugins         : { "filemanager" : "{{ asset('/public/plugins/tinymce/js/tinymce/plugins/filemanager/plugin.min.js') }}"},
});

</script>
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('pengumuman.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('tanggal_publish')) ? 'has-error' : '' }}">
                  <label>Tanggal Publish</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" name="tanggal_publish" id="tanggal_publish"  value="{{old('tanggal_publish')}}">
                  </div>
                  @if ($errors->has('tanggal_publish'))
                  				<span class="help-block">{{ $errors->first('tanggal_publish') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('category')) ? 'has-error' : '' }}">
                  <label for="category">Category</label>
                  <select class="form-control" id="category" name="category" >
                      <option value="">Pilih Category</option>
                    @foreach(config('app.pengumumanCategory') as $k=>$v)
                      <option value="{{ $k }}"  {{ (str_contains(old('category'), $k)) ? "selected" : ''}}>{{ $v }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('category'))
                  				<span class="help-block">{{ $errors->first('category') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('judul')) ? 'has-error' : '' }}">
                  <label for="judul">Judul</label>
                  <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Berita" value="{{old('judul')}}">
                  @if ($errors->has('judul'))
                  				<span class="help-block">{{ $errors->first('judul') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('lead')) ? 'has-error' : '' }}">
                  <label for="lead">Teks Lead</label>
                  <input type="text" class="form-control" id="lead" name="lead" placeholder="Teks Lead" value="{{old('lead')}}">
                  @if ($errors->has('lead'))
                  				<span class="help-block">{{ $errors->first('lead') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('konten')) ? 'has-error' : '' }}">
                  <label for="konten">Konten Pengumuman</label>
                  <textarea class="form-control" name="konten" id="konten" rows="3" placeholder="Isi konten pengumuman disini">{{old('konten')}}</textarea>
                  @if ($errors->has('konten'))
                  				<span class="help-block">{{ $errors->first('konten') }}</span>
                  @endif
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Save</button>
                <button type="button" class="btn btn-default" status="draft" onclick="set_action(this)">Save Draft</button>
                <i id="preview" class="btn btn-warning">Preview</i>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')
<script src="{{ asset('/public/plugins/datepicker/bootstrap-datepicker.js') }}""></script>
<script>
  $('#tanggal_publish').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'    });
    
    $('#preview').click(function () {
        var redirectWindow = window.open('{{ route('web.preview',['modul' => 'pengumuman']) }}&tanggal_publish='+encodeURIComponent($("#tanggal_publish").val())+'&judul='+encodeURIComponent($("#judul").val())+'&lead='+encodeURIComponent($("#lead").val())+'&konten='+encodeURIComponent(tinyMCE.editors[$('#konten').attr('id')].getContent()), '_blank');
        redirectWindow.location;
      });
</script>
@endsection
