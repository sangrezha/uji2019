@extends('webmin.admin_template')

@section('judul')
  Jenis Peraturan Management - Menambah Jenis Peraturan
@endsection

@section('page_header')
  Jenis Peraturan Management - Menambah Jenis Peraturan
@endsection

@section('page_name')
  Jenis Peraturan Management - Menambah Jenis Peraturan
@endsection

@section('page_description')
  Menambahkan Jenis Peraturan
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('rc.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('nama')) ? 'has-error' : '' }}">
                  <label for="nama">Nama</label>
                  <input type="name" class="form-control" id="nama" name="nama" placeholder="Jenis Peraturan" value="{{old('nama')}}">
                  @if ($errors->has('nama'))
                  				<span class="help-block">{{ $errors->first('nama') }}</span>
                  @endif
                </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
