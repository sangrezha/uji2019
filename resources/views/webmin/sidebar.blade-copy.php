<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset("/public/images/user/".Auth::guard('admin')->user()->picture) }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::guard('admin')->user()->name }}</p>

        </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">CMS MENU</li>
        <!-- Optionally, you can add icons to the links -->
        <li{!! ($codepage == 'P00') ? " class='active'" : '' !!}><a href="#"><i class="fa fa-circle-o"></i> <span>Settings</span></a></li>
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P01'))
          <li{!! ($codepage == 'P01') ? " class='active'" : '' !!}><a href="{{ route('useradmin.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Admin Management</span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P02'))
        <li{!! ($codepage == 'P02') ? " class='active'" : '' !!}><a href="{{ route('page.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Page Management</span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P03'))
        <li class="treeview">
          <a href="#"><i class="fa fa-folder"></i> <span>Regulasi Perizinan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li{!! ($codepage == 'P031') ? " class='active'" : '' !!}><a href="{{ route('rc.dashboard')}}"><i class="fa fa-circle-o"></i>Jenis Peraturan</a></li>
            <li{!! ($codepage == 'P032') ? " class='active'" : '' !!}><a href="{{ route('izin.dashboard')}}"><i class="fa fa-circle-o"></i>Regulasi Perizinan</a></li>
          </ul>
        </li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P04'))
        <li{!! ($codepage == 'P04') ? " class='active'" : '' !!}><a href="{{ route('bu.dashboard')}}"><i class="fa fa-circle-o"></i> <span>Badan Usaha Management</span></a></li>
        @endif
        <li><a href="{{ route('admin.logout') }}"><i class="fa fa-circle-o"></i> <span>Sign out</span></a></li>

      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
