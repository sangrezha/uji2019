<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset("/public/images/user/".Auth::guard('admin')->user()->picture) }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::guard('admin')->user()->name }}</p>

        </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">CMS MENU</li>
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Home</span></a></li>

        <!-- Optionally, you can add icons to the links -->
        <li{!! ($codepage == 'P1') ? " class='active'" : '' !!}><a href="{{ route('user.chgpasswd') }}"><i class="fa fa-circle-o"></i> <span>Change Password</span></a></li>
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P00'))
          <li{!! ($codepage == 'P00') ? " class='active'" : '' !!}><a href="{{ route('admin.setting') }}"><i class="fa fa-circle-o"></i> <span>Web Settings</span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P01'))
          <li{!! ($codepage == 'P01') ? " class='active'" : '' !!}><a href="{{ route('useradmin.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Admin Management</span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P02'))
        <li{!! ($codepage == 'P02') ? " class='active'" : '' !!}><a href="{{ route('page.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Page Management</span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P03'))
        <li{!! ($codepage == 'P03') ? " class='active'" : '' !!}><a href="{{ route('highlight.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Highlight Home Management</span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P04'))
        <li{!! ($codepage == 'P04') ? " class='active'" : '' !!}><a href="{{ route('news.dashboard') }}"><i class="fa fa-circle-o"></i> <span>News Management</span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P11'))
        <li{!! ($codepage == 'P11') ? " class='active'" : '' !!}><a href="{{ route('gal.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Gallery Management</span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P05'))
        <li{!! ($codepage == 'P05') ? " class='active'" : '' !!}><a href="{{ route('mitra.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Mitra Management</span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P06'))
        <li class="treeview">
          <a href="#" {!! ($codepage == 'P06') ? " class='active'" : '' !!}><i class="fa fa-folder"></i> <span>Regulasi Pengujian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li{!! ($codepage == 'P061') ? " class='active'" : '' !!}><a href="{{ route('rc.dashboard')}}"><i class="fa fa-circle-o"></i>Jenis Peraturan</a></li>
            <li{!! ($codepage == 'P062') ? " class='active'" : '' !!}><a href="{{ route('izin.dashboard')}}"><i class="fa fa-circle-o"></i>Regulasi Pengujian</a></li>
          </ul>
        </li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P12'))
        <li class="treeview">
          <a href="#" {!! ($codepage == 'P12') ? " class='active'" : '' !!}><i class="fa fa-folder"></i> <span>Chart Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li{!! ($codepage == 'P121') ? " class='active'" : '' !!}><a href="{{ route('chartbar.dashboard')}}"><i class="fa fa-circle-o"></i>Bar</a></li>
            <li{!! ($codepage == 'P122') ? " class='active'" : '' !!}><a href="{{ route('chartpie.dashboard')}}"><i class="fa fa-circle-o"></i>Pie</a></li>
          </ul>
        </li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P08'))
        <li{!! ($codepage == 'P08') ? " class='active'" : '' !!}><a href="{{ route('pengumuman.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Pengumuman </span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P09'))
        <li{!! ($codepage == 'P091') ? " class='active'" : '' !!}><a href="{{ route('jabatan.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Jabatan </span></a></li>
        <li{!! ($codepage == 'P092') ? " class='active'" : '' !!}><a href="{{ route('pegawai.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Pegawai </span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P10'))
        <li{!! ($codepage == 'P10') ? " class='active'" : '' !!}><a href="{{ route('chat.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Chat </span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P13'))
        <li{!! ($codepage == 'P13') ? " class='active'" : '' !!}><a href="{{ route('apprconf.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Approver Config </span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P14'))
        <li{!! ($codepage == 'P14') ? " class='active'" : '' !!}><a href="{{ route('approval.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Approval </span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P15'))
        <li{!! ($codepage == 'P15') ? " class='active'" : '' !!}><a href="{{ route('wikiuji.dashboard') }}"><i class="fa fa-circle-o"></i> <span>WikiUji </span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P16'))
        <li{!! ($codepage == 'P16') ? " class='active'" : '' !!}><a href="{{ route('sidemenu.dashboard') }}"><i class="fa fa-circle-o"></i> <span>Menu Samping </span></a></li>
        @endif
        @if (str_contains(Auth::guard('admin')->user()->privilege, 'P07'))
        <li{!! ($codepage == 'P07') ? " class='active'" : '' !!}><a href="{{ route('page.stats') }}"><i class="fa fa-circle-o"></i> <span>Page Statistic </span></a></li>
        @endif
        <li><a href="{{ route('admin.logout') }}"><i class="fa fa-circle-o"></i> <span>Sign out</span></a></li>

      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>
