@extends('webmin.admin_template')

@section('title')
  menu Management - Tambah Menu Samping baru
@endsection

@section('page_header')
  menu Management - Tambah Menu Samping baru
@endsection

@section('page_name')
  menu Management - Tambah Menu Samping baru
@endsection

@section('page_description')
  Menambah Menu Samping baru
@endsection

@section('tambahan_head')
 
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('sidemenu.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" id="title" name="title" placeholder="Title Menu Samping" value="{{old('title')}}">
                  @if ($errors->has('title'))
                  				<span class="help-block">{{ $errors->first('title') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('link')) ? 'has-error' : '' }}">
                  <label for="link">Link</label>
                  <input type="text" class="form-control" id="link" name="link" placeholder="Link" value="{{old('link')}}">
                  @if ($errors->has('link'))
                  				<span class="help-block">{{ $errors->first('link') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('window')) ? 'has-error' : '' }}">
                  <label for="window">window</label><br/>
                  <input type="radio" id="self" name="window" placeholder="self" value="self" {{ (old('window') == 'self'?'checked':'') }}> <span for="self">Self</span><br />
                  <input type="radio" id="blank" name="window" placeholder="blank" value="blank" {{ (old('window') == 'blank'?'checked':'') }}> <span for="blank">Blank</span>
                  @if ($errors->has('window'))
                  				<span class="help-block">{{ $errors->first('window') }}</span>
                  @endif
                </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Save</button>
                <!-- <button type="button" class="btn btn-default" status="draft" onclick="set_action(this)">Save Draft</button>
                <i id="preview" class="btn btn-warning">Preview</i> -->
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')
@endsection
