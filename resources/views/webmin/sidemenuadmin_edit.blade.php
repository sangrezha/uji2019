@extends('webmin.admin_template')

@section('title')
  News Management - Tambah Menu Samping baru
@endsection

@section('page_header')
  News Management - Tambah Menu Samping baru
@endsection

@section('page_name')
  News Management - Tambah Menu Samping baru
@endsection

@section('page_description')
  Menambah Menu Samping baru
@endsection

@section('tambahan_head')
 
@endsection

@section('webmin_content')
  <!-- form start -->
  <form role="form" enctype="multipart/form-data" action="{{ route('sidemenu.update', ['id' => $data->id]) }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{$data->title}}">
                  @if ($errors->has('title'))
                  				<span class="help-block">{{ $errors->first('title') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('link')) ? 'has-error' : '' }}">
                  <label for="link"> Link</label>
                  <input type="text" class="form-control" id="link" name="link" placeholder=" Link" value="{{$data->link}}">
                  @if ($errors->has('link'))
                  				<span class="help-block">{{ $errors->first('link') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('window')) ? 'has-error' : '' }}">
                  <label for="window">window</label><br/>
                  <input type="radio" id="self" name="window" placeholder="self" value="self" {{ ($data->window == 'self'?'checked':'') }}> Self<br />
                  <input type="radio" id="blank" name="window" placeholder="blank" value="blank" {{ ($data->window == 'blank'?'checked':'') }}> Blank
                  @if ($errors->has('window'))
                  				<span class="help-block">{{ $errors->first('window') }}</span>
                  @endif
                </div>

              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Save</button>
                <!-- <button type="button" class="btn btn-default" status="draft" onclick="set_action(this)">Save Draft</button>
                <i id="preview" class="btn btn-warning">Preview</i> -->
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')
@endsection
