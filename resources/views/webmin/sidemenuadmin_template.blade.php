@extends('webmin.admin_template')

@section('judul')
  Menu Samping Management
@endsection

@section('page_header')
  Menu Samping Management
@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
  
  @if ($errors->has('approver'))
    <div class="alert alert-danger">{{ $errors->first('approver') }}</div>
  @endif
@endsection

@section('page_name')
  Menu Samping Management
@endsection

@section('page_description')
  Halaman untuk menu samping di CMS
@endsection

@section('webmin_content')
  <div class="box">
    <div class="box-header">
      <button type="button" class="btn btn-primary" onclick="location.href = '{{route('sidemenu.add')}}';">Tambah Menu Baru</button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="tableDataBrowse" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th width="175">Aksi</th>
          <th>Title</th>
          <th>Link</th>
          <th>Window</th>
          <th>Status</th>
          <!-- <th>Dilihat</th> -->
          <th>Tanggal Posting</th>
          <th>Diposting Oleh</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dataList as $data)
        <tr>
          <td>
            <button type="button" class="btn btn-primary" onclick="location.href = '{!! route('sidemenu.edit', ['id' => $data->id]) !!}';">Ubah</button>
            <button type="button" class="btn btn-danger" onclick="confirmDelete('{{{ $data->title }}}','{!! route('sidemenu.delete', ['id' => $data->id]) !!}');">Hapus</button>
          </td>
          <td>{{{ $data->title }}}</td>
          <td>{{ $data->link }}</td>
          <td>{{ $data->window }}</td>
          <td><a href="{{ route('sidemenu.setstatus',['id'=>$data->id, 'status'=>$data->status]) }}"><i class="fa fa-{{ ($data->status == 'active'?'check text-success':'times text-danger') }}"></i></a></td>
          <!-- <td>{{ $data->counter }} x</td> -->
          <td> {{ $data->created_at }} </td>
          <td> {{ $data->created_by }}</td>
        </tr>
      @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection

@section('tambahan_script')
  <script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
  <script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
  <!-- page script -->
  <script>
    $(function () {
      $("#tableDataBrowse").DataTable();
    });
  </script>
@endsection
