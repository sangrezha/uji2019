@extends('webmin.admin_template')

@section('judul')
  Halaman Statistik
@endsection

@section('page_header')
  Halaman Statistik
@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
@endsection

@section('page_name')
  Halaman Statistik
@endsection

@section('page_description')
  Halaman Statistik
@endsection

@section('tambahan_head')
  <link rel="stylesheet" href="{{ asset("/public/plugins/daterangepicker/daterangepicker.css") }}">


@endsection

@section('webmin_content')

          <div class="col-md-12">
              <div class="col-lg-3 col-xs-6">
             <!-- small box -->
             <div class="small-box bg-aqua">
               <div class="inner">
                 <h3>{{ number_format($statsHariIni->total,0, '','.') }}<sup style="font-size: 20px"> view</sup></h3>

                 <p>Hari ini</p>
               </div>
               <div class="icon">
                 <i class="ion ion-stats-bars"></i>
               </div>
             </div>
            </div>
            <div class="col-lg-3 col-xs-6">
             <!-- small box -->
             <div class="small-box bg-green">
               <div class="inner">
                 <h3>{{ number_format(intval($statsMingguIni->total),0, '','.') }}<sup style="font-size: 20px"> view</sup></h3>

                 <p>Minggu ini</p>
               </div>
               <div class="icon">
                 <i class="ion ion-stats-bars"></i>
               </div>
             </div>
            </div>
            <div class="col-lg-3 col-xs-6">
             <!-- small box -->
             <div class="small-box bg-red">
               <div class="inner">
                 <h3>{{ number_format(intval($statsBulanIni->total),0, '','.')}}<sup style="font-size: 20px"> view</sup></h3>

                 <p>Bulan ini</p>
               </div>
               <div class="icon">
                 <i class="ion ion-stats-bars"></i>
               </div>
             </div>
            </div>

            <div class="col-lg-3 col-xs-6">
             <!-- small box -->
             <div class="small-box bg-yellow">
               <div class="inner">
                 <h3>{{ number_format(intval($statsTahunIni->total),0, '','.')}}<sup style="font-size: 20px"> view</sup></h3>

                 <p>Tahun ini</p>
               </div>
               <div class="icon">
                 <i class="ion ion-stats-bars"></i>
               </div>
             </div>
            </div>
          </div>


          <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">OS yang paling banyak digunakan untuk mengakses website</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="box-body chart-responsive">
                <div class="chart" id="bar2-chart" style="height: 300px;"></div>
              </div>
              <!-- /.box-body -->
            </div>
          </div>

          <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Browser paling banyak mengakses website</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="box-body chart-responsive">
                <div class="chart" id="bar-chart" style="height: 300px;"></div>
              </div>
              <!-- /.box-body -->
            </div>
          </div>

          <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Halaman paling banyak diakses</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              @foreach ($statsPage as $pagestat)
                <div>{{ @config('app.halamanWeb')[$pagestat->codepage]['nama'] }}</div>
                <div class="progress">
                  <div class="progress-bar progress-bar-aqua" role="progressbar" aria-valuenow="{{ ($pagestat->total) ? $pagestat->total : 0 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ ($pagestat->total / $statsTot->total)*100 }}%">
                    <span>{{ ($pagestat->total) ? $pagestat->total : 0 }} akses</span>
                  </div>
                </div>
              @endforeach
            </div>
            <!-- /.box-body -->
          </div>
        </div>

@endsection

@section('tambahan_script')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="{{ asset("/public/plugins/morris/morris.min.js") }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
  <script src="{{ asset("/public/plugins/daterangepicker/daterangepicker.js") }}"></script>
  <script>
  $(function () {
   "use strict";



    var bar = new Morris.Bar({
       element: 'bar-chart',
       resize: true,
       data: [
       @foreach ($statsBrowser as $data2)
         {y: '{{$data2->browser}}', item1: {{$data2->total}}},
       @endforeach
       ],
       xkey: 'y',
       ykeys: ['item1'],
       labels: ['Hit'],
       lineColors: ['#3c8dbc'],
       hideHover: 'auto'
     });

     var bar2 = new Morris.Bar({
        element: 'bar2-chart',
        resize: true,
        data: [
        @foreach ($statsOS as $data3)
          {y: '{{$data3->os}}', item1: {{$data3->total}}},
        @endforeach
        ],
        xkey: 'y',
        ykeys: ['item1'],
        labels: ['Hit'],
        lineColors: ['#3c8dbc'],
        hideHover: 'auto'
      });


 });
  $('#range_tgl').daterangepicker({
    locale: {
      format: 'YYYY-MM-DD'
    },
  });
  </script>

@endsection
