@extends('webmin.admin_template')

@section('judul')
  Admin Management - Add New Admin User
@endsection

@section('page_header')
  Admin Management - Add New Admin User
@endsection

@section('page_name')
  Admin Management - Add New Admin User
@endsection

@section('page_description')
  Menambah CMS Admin User
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('user.add.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                  <label for="name">Nama</label>
                  <input type="name" class="form-control" id="name" name="name" placeholder="Nama" value="{{old('name')}}">
                  @if ($errors->has('name'))
                  				<span class="help-block">{{ $errors->first('name') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('username')) ? 'has-error' : '' }}">
                  <label for="username">Username</label>
                  <input type="username" class="form-control" id="username" name="username" placeholder="Username" value="{{old('username')}}">
                  @if ($errors->has('username'))
                  				<span class="help-block">{{ $errors->first('username') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="{{old('email')}}">
                  @if ($errors->has('email'))
                  				<span class="help-block">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
                  <label for="Password">Password</label>
                  <input type="password" class="form-control" id="Password" name="password" placeholder="Password">
                  @if ($errors->has('password'))
                  				<span class="help-block">{{ $errors->first('password') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('gambar')) ? 'has-error' : '' }}">
                  <label for="gambar">Gambar/Avatar (160x160 px)</label>
                  <input type="file" id="gambar" name="gambar">
                  @if ($errors->has('gambar'))
                  				<span class="help-block">{{ $errors->first('gambar') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('chatkategori')) ? 'has-error' : '' }}">
                  <label>Kategori Chat<br /></label>
                      @foreach(config('app.chatSubject') as $k=>$v)
                        <br /><input type="checkbox" name="chatkategori[]" value="{{ $k }}" {{ (str_contains(old('chatkategori'), $k)) ? "checked" : ''}}> {{ $v }}
                      @endforeach
                      @if ($errors->has('chatkategori'))
                      				<span class="help-block">{{ $errors->first('chatkategori') }}</span>
                      @endif
                </div>
                <div class="form-group {{ ($errors->has('privilege')) ? 'has-error' : '' }}">
                  <label>Hak Akses<br /></label>
                      @foreach(config('app.admPriv') as $k=>$v)
                        <br /><input type="checkbox" name="privilege[]" value="{{ $k }}"> {{ $v }}
                      @endforeach
                      @if ($errors->has('privilege'))
                      				<span class="help-block">{{ $errors->first('privilege') }}</span>
                      @endif
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
