@extends('webmin.admin_template')

@section('judul')
  Ganti Password
@endsection

@section('page_header')
  Ganti Password
@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
@endsection

@section('page_name')
  Ganti Password
@endsection

@section('page_description')
  Ganti Password
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('user.chgpasswdsave', ['id' => Auth::guard('admin')->user()->id]) }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('password_lama')) ? 'has-error' : '' }}">
                  <label for="password_lama">Password Lama</label>
                  <input type="password" class="form-control" id="password_lama" name="password_lama" placeholder="Password Lama">
                  @if ($errors->has('password_lama'))
                  				<span class="help-block">{{ $errors->first('password_lama') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('password_baru')) ? 'has-error' : '' }}">
                  <label for="password_baru">Password Baru (Min 6 Char)</label>
                  <input type="password" class="form-control" id="password_baru" name="password_baru" placeholder="Password Baru (Min 6 char)">
                  @if ($errors->has('password_baru'))
                          <span class="help-block">{{ $errors->first('password_baru') }}</span>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
