@extends('webmin.admin_template')

@section('judul')
  Admin Management - Ubah Admin User {{ $data->name }}
@endsection

@section('page_header')
  Admin Management - Ubah Admin User {{ $data->name }}
@endsection

@section('page_name')
  Admin Management - Ubah Admin User
@endsection

@section('page_description')
  Merubah CMS Admin User
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('user.update', ['id' => $data->id]) }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                  <label for="name">Nama</label>
                  <input type="name" class="form-control" id="name" name="name" placeholder="Nama" value="{{$data->name}}">
                  @if ($errors->has('name'))
                  				<span class="help-block">{{ $errors->first('name') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('username')) ? 'has-error' : '' }}">
                  <label for="username">Username</label>
                  <input type="username" class="form-control" id="username" name="username" placeholder="Username" value="{{$data->username}}">
                  @if ($errors->has('username'))
                  				<span class="help-block">{{ $errors->first('username') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" value="{{$data->email}}">
                  @if ($errors->has('email'))
                  				<span class="help-block">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
                  <label for="Password">Password</label>
                  <input type="password" class="form-control" id="Password" name="password" placeholder="Kosongi jika tidak merubah password ">
                  @if ($errors->has('password'))
                  				<span class="help-block">{{ $errors->first('password') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('gambar')) ? 'has-error' : '' }}">
                  <label for="gambar">Gambar/Avatar (160x160 px)</label>
                  <input type="file" id="gambar" name="gambar">
                  @if ($errors->has('gambar'))
                  				<span class="help-block">{{ $errors->first('gambar') }}</span>
                  @endif
                </div>
                @if ($data->picture)
                <div class="form-group">
                  <img src="{{asset('public/images/user/'.$data->picture)}}" width="160" />
                  <br /><br />
                  <input type='checkbox' name="picture_del" /> Centang untuk hapus gambar/avatar
                </div>
                @endif
                <div class="form-group {{ ($errors->has('chatkategori')) ? 'has-error' : '' }}">
                  <label>Kategori Chat<br /></label>
                      @foreach(config('app.chatSubject') as $k=>$v)
                        <br /><input type="checkbox" name="chatkategori[]" value="{{ $k }}" {{ (str_contains($data->chatkategori, $k)) ? "checked" : ''}}> {{ $v }}
                      @endforeach
                      @if ($errors->has('chatkategori'))
                      				<span class="help-block">{{ $errors->first('chatkategori') }}</span>
                      @endif
                </div>
                <div class="form-group {{ ($errors->has('privilege')) ? 'has-error' : '' }}">
                  <label>Hak Akses<br /></label>
                      @foreach(config('app.admPriv') as $k=>$v)
                        <br /><input type="checkbox" name="privilege[]" value="{{ $k }}" {{ (str_contains($data->privilege, $k)) ? "checked" : ''}}> {{ $v }}
                      @endforeach
                      @if ($errors->has('privilege'))
                      				<span class="help-block">{{ $errors->first('privilege') }}</span>
                      @endif
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
