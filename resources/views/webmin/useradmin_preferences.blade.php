@extends('webmin.admin_template')

@section('judul')
  Website Setting
@endsection

@section('page_header')
  Website Setting
@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
@endsection

@section('page_name')
  Website Setting
@endsection

@section('page_description')
  Website Setting
@endsection

@section('webmin_content')
  <!-- form start -->
            <form role="form" enctype="multipart/form-data" action="{{ route('admin.setting.submit') }}" method="post">
              {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
                  <label for="title">Judul Website (tampil di tab browser)</label>
                  <input type="text" class="form-control" id="title" name="title" placeholder="Judul Website" value="{{ $data->title }}">
                  @if ($errors->has('title'))
                  				<span class="help-block">{{ $errors->first('title') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('footer')) ? 'has-error' : '' }}">
                  <label for="footer">Footer Website</label>
                  <input type="text" class="form-control" id="footer" name="footer" placeholder="Footer Website" value="{{ $data->footer }}">
                  @if ($errors->has('footer'))
                  				<span class="help-block">{{ $errors->first('footer') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('meta_description')) ? 'has-error' : '' }}">
                  <label for="meta_description">Meta Description (max 255 karakter agar optimal)</label>
                  <input type="text" class="form-control" id="meta_description" name="meta_description" placeholder="Meta Description" value="{{ $data->meta_desc }}">
                  @if ($errors->has('meta_description'))
                  				<span class="help-block">{{ $errors->first('meta_description') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('meta_keyword')) ? 'has-error' : '' }}">
                  <label for="meta_keyword">Meta Keyword (separasi pake ; )</label>
                  <input type="text" class="form-control" id="meta_keyword" name="meta_keyword" placeholder="Meta Keyword"  value="{{ $data->meta_keyword }}">
                  @if ($errors->has('meta_keyword'))
                  				<span class="help-block">{{ $errors->first('meta_keyword') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('konten_head')) ? 'has-error' : '' }}">
                  <label for="konten_head">Tambahan HEAD konten. ex. script alexa, ga, track, dll</label>
                  <textarea class="form-control" id="konten_head" name="konten_head" placeholder="Tambahan di <HEAD>">{{ $data->konten_head }}</textarea>
                  @if ($errors->has('konten_head'))
                  				<span class="help-block">{{ $errors->first('konten_head') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('konten_body_atas')) ? 'has-error' : '' }}">
                  <label for="konten_body_atas">Tambahan konten di area BODY atas. ex. script track, dll</label>
                  <textarea class="form-control" id="konten_body_atas" name="konten_body_atas" placeholder="Tambahan di <Body> atas">{{ $data->konten_body_t }}</textarea>
                  @if ($errors->has('konten_body_atas'))
                  				<span class="help-block">{{ $errors->first('konten_body_atas') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('konten_body_bawah')) ? 'has-error' : '' }}">
                  <label for="konten_body_bawah">Tambahan konten di area BODY bawah. ex. script track, ga, fitur jquery, dll</label>
                  <textarea class="form-control" id="konten_body_bawah" name="konten_body_bawah" placeholder="Tambahan di <Body> bawah">{{ $data->konten_body_b }}</textarea>
                  @if ($errors->has('konten_body_bawah'))
                  				<span class="help-block">{{ $errors->first('konten_body_bawah') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('teks_sdm')) ? 'has-error' : '' }}">
                  <label for="teks_sdm">Teks shortcut link Pengujian SDM di home</label>
                  <textarea class="form-control" id="teks_sdm" name="teks_sdm" placeholder="Teks shortcut link Pengujian SDM di home">{{ $data->teks1 }}</textarea>
                  @if ($errors->has('teks_sdm'))
                  				<span class="help-block">{{ $errors->first('teks_sdm') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('teks_prasarana')) ? 'has-error' : '' }}">
                  <label for="teks_prasarana">Teks shortcut link Pengujian Prasarana di home</label>
                  <textarea class="form-control" id="teks_prasarana" name="teks_prasarana" placeholder="Teks shortcut link Pengujian Prasarana di home">{{ $data->teks2 }}</textarea>
                  @if ($errors->has('teks_prasarana'))
                  				<span class="help-block">{{ $errors->first('teks_prasarana') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('teks_sarana')) ? 'has-error' : '' }}">
                  <label for="teks_sarana">Teks shortcut link Pengujian Sarana di home</label>
                  <textarea class="form-control" id="teks_sarana" name="teks_sarana" placeholder="Teks shortcut link Pengujian Sarana di home">{{ $data->teks3 }}</textarea>
                  @if ($errors->has('teks_sarana'))
                  				<span class="help-block">{{ $errors->first('teks_sarana') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <input type='checkbox' value="1" name="chat_active" {{ ($data->chat_active) ? "checked" : "" }} />Chat Aktif
                </div>
                <div class="form-group {{ ($errors->has('chat_hari')) ? 'has-error' : '' }}">
                  <label>Hari Chat Aktif<br /></label>
                      @foreach(config('app.chatHari') as $k=>$v)
                        <br /><input type="checkbox" name="chat_hari[]" value="{{ $k }}" {{ (str_contains($data->chat_hari, $k)) ? "checked" : ''}}> {{ $v }}
                      @endforeach
                      @if ($errors->has('chat_hari'))
                      				<span class="help-block">{{ $errors->first('chat_hari') }}</span>
                      @endif
                </div>
                <div class="form-group {{ ($errors->has('chat_jam1')) ? 'has-error' : '' }}">
                  <label for="chat_jam1">Jam Mulai Chat</label>
                  <input type="time" class="form-control" id="chat_jam1" name="chat_jam1" value="{{ $data->chat_jam1 }}">
                  @if ($errors->has('chat_jam1'))
                  				<span class="help-block">{{ $errors->first('chat_jam1') }}</span>
                  @endif
                </div>
                <div class="form-group {{ ($errors->has('chat_jam2')) ? 'has-error' : '' }}">
                  <label for="chat_jam2">Jam Selesai Chat</label>
                  <input type="time" class="form-control" id="chat_jam2" name="chat_jam2" value="{{ $data->chat_jam2 }}">
                  @if ($errors->has('chat_jam2'))
                  				<span class="help-block">{{ $errors->first('chat_jam2') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <input type='checkbox' value="1" name="popup_active" {{ ($data->popup_active) ? "checked" : "" }} /> Pop Up Image Aktif
                </div>
                <div class="form-group {{ ($errors->has('gambar')) ? 'has-error' : '' }}">
                  <label for="gambar">Gambar Pop Up</label>
                  <input type="file" id="gambar" name="gambar">
                  @if ($errors->has('gambar'))
                          <span class="help-block">{{ $errors->first('gambar') }}</span>
                  @endif
                </div>
                @if ($data->popup_image)
                <div class="form-group">
                  <img src="{{asset('public/images/popup/'.$data->popup_image)}}" width="160" />
                  <br /><br />
                  <input type='checkbox' name="picture_del" /> Centang untuk hapus gambar/avatar
                </div>
                @endif
                <div class="form-group {{ ($errors->has('popup_url')) ? 'has-error' : '' }}">
                  <label for="popup_url">PopUp URL (ex. https://www.dephub.go.id)</label>
                  <input type="text" class="form-control" id="popup_url" name="popup_url" placeholder="PopUp URL (ex. https://www.dephub.go.id)"  value="{{ $data->popup_url }}">
                </div>

            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="set_action(this)">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

@endsection

@section('tambahan_script')

@endsection
