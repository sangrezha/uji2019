@extends('webmin.admin_template')

@section('judul')
  Admin Management
@endsection

@section('page_header')
  Admin Management
@endsection

@section('pesan')
  @if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
  @endif
@endsection

@section('page_name')
  Admin Management
@endsection

@section('page_description')
  Halaman untuk menambahkan user akses di CMS
@endsection

@section('webmin_content')
  <div class="box">
    <div class="box-header">
      <button type="button" class="btn btn-primary" onclick="location.href = '{{route('user.add')}}';">Tambah User Baru</button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="tableDataBrowse" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th width="175">Aksi</th>
          <th>Username</th>
          <th>Hak Akses</th>
          <th>Status</th>
          <th>Tanggal Posting</th>
          <th>Diposting Oleh</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dataList as $data)
        <tr>
          <td>
            <button type="button" class="btn btn-primary" onclick="location.href = '{!! route('user.edit', ['id' => $data->id]) !!}';">Ubah</button>
            <button type="button" class="btn btn-danger" onclick="confirmDelete('{{{ $data->username }}}','{!! route('user.delete', ['id' => $data->id]) !!}');">Hapus</button>
          </td>
          <td>{{ $data->username }}
          </td>
          <td>
            <ul>
            @if ($data->privilege != "")
              @foreach(explode('|', $data->privilege) as $info)
                <li>{{ @config('app.admPriv')[$info]}}</li>
              @endforeach
            @endif
            </ul>
          </td>
          <td>{{config('app.admStatus')[$data->active]}} - <button type="button" class="btn btn-info" onclick="location.href = '{!! route('user.ubahstatus', ['id' => $data->id]) !!}';">Set to {{ ($data->active) ? 'non ' : ''}}Active</button></td>
          <td> {{ $data->created_at }} </td>
          <td> {{ $data->created_by }}</td>
        </tr>
      @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@endsection

@section('tambahan_script')
  <script src="{{ asset("/public/plugins/datatables/jquery.dataTables.min.js") }}"></script>
  <script src="{{ asset("/public/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>
  <!-- page script -->
  <script>
    $(function () {
      $("#tableDataBrowse").DataTable();
    });
  </script>
@endsection
