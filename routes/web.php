<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'webController@index')->name('web.home');

Route::get('/profil/struktur-organisasi', 'webController@struktur')->name('web.profil1');
Route::get('/profil/data-pegawai-balai-pengujian', 'webController@datapegawai')->name('web.profil2');
Route::get('/profil/penjelasan-tupoksi', 'webController@tupoksi')->name('web.profil3');
Route::get('/profil/denah-kantor', 'webController@denah')->name('web.profil5');

Route::get('/profil/regulasi/download/{filenya}', 'webController@regulasidown')->name('web.regulasidown');
Route::get('/profil/regulasi', 'webController@regulasi')->name('web.regulasi');

// Route::get('/pengumuman/download/{filenya}', 'webController@pengumumandown')->name('web.pengumumandown');
Route::get('/pengumuman/{slug}', 'webController@detilpengumuman')->name('web.detpengumuman');
Route::get('/pengumuman', 'webController@pengumuman')->name('web.pengumuman');



Route::get('/pembinaan-dan-pengembangan', 'webController@pembinaan')->name('web.pembinaan');

Route::get('/pengujian-sdm/info-umum', 'webController@ujisdmUmum')->name('web.ujisdm0');
Route::get('/pengujian-sdm/info-persyaratan', 'webController@ujisdmInfo')->name('web.ujisdm1');
Route::get('/pengujian-sdm/sop', 'webController@ujisdmSOP')->name('web.ujisdm2');
Route::get('/pengujian-sdm/jadwal', 'webController@ujisdmJadwal')->name('web.ujisdm3');
Route::get('/pengujian-sdm/daftar-peserta', 'webController@ujisdmPeserta')->name('web.ujisdm4');
Route::get('/pengujian-sdm/lokasi-pengujian', 'webController@ujisdmLokasi')->name('web.ujisdm5');
Route::get('/pengujian-sdm/hasil-pengujian', 'webController@ujisdmHasil')->name('web.ujisdm6');
Route::get('/pengujian-sdm/sarana-pengujian', 'webController@ujisdmSarana')->name('web.ujisdm7');

Route::get('/pengujian-prasarana/info-umum', 'webController@ujiprasaranaUmum')->name('web.ujiprasarana0');
Route::get('/pengujian-prasarana/info-persyaratan', 'webController@ujiprasaranaInfo')->name('web.ujiprasarana1');
Route::get('/pengujian-prasarana/sop', 'webController@ujiprasaranaSOP')->name('web.ujiprasarana2');
Route::get('/pengujian-prasarana/jadwal', 'webController@ujiprasaranaJadwal')->name('web.ujiprasarana3');
Route::get('/pengujian-prasarana/daftar-peserta', 'webController@ujiprasaranaPeserta')->name('web.ujiprasarana4');
Route::get('/pengujian-prasarana/lokasi-pengujian', 'webController@ujiprasaranaLokasi')->name('web.ujiprasarana5');
Route::get('/pengujian-prasarana/hasil-pengujian', 'webController@ujiprasaranaHasil')->name('web.ujiprasarana6');
Route::get('/pengujian-prasarana/peralatan-pengujian', 'webController@ujiprasaranaData')->name('web.ujiprasarana7');
Route::get('/pengujian-prasarana/permohonan-pengujian', 'webController@ujiprasaranaPermohonan')->name('web.ujiprasarana8');

Route::get('/pengujian-sarana/info-umum', 'webController@ujisaranaUmum')->name('web.ujisarana0');
Route::get('/pengujian-sarana/info-persyaratan', 'webController@ujisaranaInfo')->name('web.ujisarana1');
Route::get('/pengujian-sarana/sop', 'webController@ujisaranaSOP')->name('web.ujisarana2');
Route::get('/pengujian-sarana/jadwal', 'webController@ujisaranaJadwal')->name('web.ujisarana3');
Route::get('/pengujian-sarana/lokasi-pengujian', 'webController@ujisaranaLokasi')->name('web.ujisarana4');
Route::get('/pengujian-sarana/hasil-pengujian', 'webController@ujisaranaHasil')->name('web.ujisarana5');
Route::get('/pengujian-sarana/peralatan-pengujian', 'webController@ujisaranaPeralatan')->name('web.ujisarana6');

Route::get('/pengujian-sarana', 'webController@ujisarana')->name('web.ujisarana');

Route::get('/kegiatan/{slug}', 'webController@detilevent')->name('web.detevent');
Route::get('/kegiatan', 'webController@event')->name('web.event');

Route::get('/berita/{slug}', 'webController@detilberita')->name('web.detberita');
Route::get('/berita', 'webController@berita')->name('web.berita');

Route::get('/wikiuji/{slug}', 'webController@detilwikiuji')->name('web.detwikiuji');
Route::get('/wikiuji', 'webController@wikiuji')->name('web.wikiuji');

Route::get('/mitrakerja/{slug}', 'webController@detilmitrakerja')->name('web.detmitrakerja');
Route::get('/mitrakerja', 'webController@mitrakerja')->name('web.mitrakerja');

Route::get('/kontak', 'webController@kontak')->name('web.kontak');

Route::post('/cari', 'webController@cari')->name('web.search');

Route::post('sendmessage', 'webController@sendMessage')->name('web.sendMessage');

Route::post('chat1', 'webController@chat1')->name('web.chat1');
Route::post('chat2', 'webController@chat2')->name('web.chat2');
Route::get('chat3/{sessid}', 'webController@chat3')->name('web.chat3');


Route::get('preview', 'webController@preview')->name('web.preview');

Route::get('webadmin', 'AdminController@index')->name('admin.dashboard');

Route::get('webadmin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('webadmin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

Route::get('webadmin/forgot-password', 'Auth\AdminLoginController@showForgotPassword')->name('admin.forgot');
Route::post('webadmin/forgot-password', 'Auth\AdminLoginController@forgotSent')->name('admin.forgot.submit');


Route::get('webadmin/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
Route::get('webadmin/home', 'Auth\AdminLoginController@home')->name('admin.home');

Route::get('webadmin/setting', 'adminUserController@webSetting')->name('admin.setting');
Route::post('webadmin/setting', 'adminUserController@webSettingSave')->name('admin.setting.submit');


Route::get('webadmin/user/add', 'adminUserController@create')->name('user.add');
Route::post('webadmin/user/add', 'adminUserController@store')->name('user.add.submit');
Route::get('webadmin/user/changepassword', 'adminUserController@changePassword')->name('user.chgpasswd');
Route::post('webadmin/user/{id}/changepassword', 'adminUserController@changePasswordSave')->name('user.chgpasswdsave');

Route::get('webadmin/user/{id}/edit', 'adminUserController@edit')->name('user.edit');
Route::post('webadmin/user/{id}/edit', 'adminUserController@update')->name('user.update');
Route::get('webadmin/user/{id}/delete', 'adminUserController@destroy')->name('user.delete');
Route::get('webadmin/user/{id}/status', 'adminUserController@status')->name('user.ubahstatus');
Route::get('webadmin/user', 'adminUserController@index')->name('useradmin.dashboard');

Route::get('webadmin/page/statistic', 'adminPageController@stats')->name('page.stats');

Route::get('webadmin/page/add', 'adminPageController@create')->name('page.add');
Route::post('webadmin/page/add', 'adminPageController@store')->name('page.add.submit');
Route::get('webadmin/page/{id}/edit', 'adminPageController@edit')->name('page.edit');
Route::post('webadmin/page/{id}/edit', 'adminPageController@update')->name('page.update');
Route::get('webadmin/page/{id}/delete', 'adminPageController@destroy')->name('page.delete');
Route::get('webadmin/page', 'adminPageController@index')->name('page.dashboard');
Route::post('webadmin/page/postImage', 'adminPageController@postImage')->name('page.postimage');

Route::get('webadmin/highlight/add', 'adminHighlightController@create')->name('highlight.add');
Route::get('webadmin/highlight/sort', 'adminHighlightController@sort')->name('highlight.sort');
Route::post('webadmin/highlight/sort', 'adminHighlightController@sortsave')->name('highlight.sortsave');
Route::post('webadmin/highlight/add', 'adminHighlightController@store')->name('highlight.add.submit');
Route::get('webadmin/highlight/{id}/edit', 'adminHighlightController@edit')->name('highlight.edit');
Route::post('webadmin/highlight/{id}/edit', 'adminHighlightController@update')->name('highlight.update');
Route::get('webadmin/highlight/{id}/delete', 'adminHighlightController@destroy')->name('highlight.delete');
Route::get('webadmin/highlight', 'adminHighlightController@index')->name('highlight.dashboard');
Route::post('webadmin/highlight/postImage', 'adminHighlightController@postImage')->name('highlight.postimage');

Route::get('webadmin/chart_bar/add', 'adminChartBarController@create')->name('chartbar.add');
Route::get('webadmin/chart_bar/sort', 'adminChartBarController@sort')->name('chartbar.sort');
Route::post('webadmin/chart_bar/sort', 'adminChartBarController@sortsave')->name('chartbar.sortsave');
Route::post('webadmin/chart_bar/add', 'adminChartBarController@store')->name('chartbar.add.submit');
Route::get('webadmin/chart_bar/{id}/edit', 'adminChartBarController@edit')->name('chartbar.edit');
Route::post('webadmin/chart_bar/{id}/edit', 'adminChartBarController@update')->name('chartbar.update');
Route::get('webadmin/chart_bar/{id}/delete', 'adminChartBarController@destroy')->name('chartbar.delete');
Route::get('webadmin/chart_bar', 'adminChartBarController@index')->name('chartbar.dashboard');

Route::get('webadmin/chart_pie/add', 'adminChartPieController@create')->name('chartpie.add');
Route::get('webadmin/chart_pie/sort', 'adminChartPieController@sort')->name('chartpie.sort');
Route::post('webadmin/chart_pie/sort', 'adminChartPieController@sortsave')->name('chartpie.sortsave');
Route::post('webadmin/chart_pie/add', 'adminChartPieController@store')->name('chartpie.add.submit');
Route::get('webadmin/chart_pie/{id}/edit', 'adminChartPieController@edit')->name('chartpie.edit');
Route::post('webadmin/chart_pie/{id}/edit', 'adminChartPieController@update')->name('chartpie.update');
Route::get('webadmin/chart_pie/{id}/delete', 'adminChartPieController@destroy')->name('chartpie.delete');
Route::get('webadmin/chart_pie', 'adminChartPieController@index')->name('chartpie.dashboard');

Route::get('webadmin/jabatan/add', 'adminJabatanController@create')->name('jabatan.add');
Route::get('webadmin/jabatan/sort', 'adminJabatanController@sort')->name('jabatan.sort');
Route::post('webadmin/jabatan/sort', 'adminJabatanController@sortsave')->name('jabatan.sortsave');
Route::post('webadmin/jabatan/add', 'adminJabatanController@store')->name('jabatan.add.submit');
Route::get('webadmin/jabatan/{id}/edit', 'adminJabatanController@edit')->name('jabatan.edit');
Route::post('webadmin/jabatan/{id}/edit', 'adminJabatanController@update')->name('jabatan.update');
Route::get('webadmin/jabatan/{id}/delete', 'adminJabatanController@destroy')->name('jabatan.delete');
Route::get('webadmin/jabatan', 'adminJabatanController@index')->name('jabatan.dashboard');

Route::get('webadmin/mitra/add', 'adminMitraController@create')->name('mitra.add');
Route::get('webadmin/mitra/sort', 'adminMitraController@sort')->name('mitra.sort');
Route::post('webadmin/mitra/sort', 'adminMitraController@sortsave')->name('mitra.sortsave');
Route::post('webadmin/mitra/add', 'adminMitraController@store')->name('mitra.add.submit');
Route::get('webadmin/mitra/{id}/edit', 'adminMitraController@edit')->name('mitra.edit');
Route::post('webadmin/mitra/{id}/edit', 'adminMitraController@update')->name('mitra.update');
Route::get('webadmin/mitra/{id}/delete', 'adminMitraController@destroy')->name('mitra.delete');
Route::get('webadmin/mitra', 'adminMitraController@index')->name('mitra.dashboard');

Route::get('webadmin/pegawai/add', 'adminPegawaiController@create')->name('pegawai.add');
Route::get('webadmin/pegawai/sort', 'adminPegawaiController@sort')->name('pegawai.sort');
Route::post('webadmin/pegawai/sort', 'adminPegawaiController@sortsave')->name('pegawai.sortsave');
Route::post('webadmin/pegawai/add', 'adminPegawaiController@store')->name('pegawai.add.submit');
Route::get('webadmin/pegawai/{id}/edit', 'adminPegawaiController@edit')->name('pegawai.edit');
Route::post('webadmin/pegawai/{id}/edit', 'adminPegawaiController@update')->name('pegawai.update');
Route::get('webadmin/pegawai/{id}/delete', 'adminPegawaiController@destroy')->name('pegawai.delete');
Route::get('webadmin/pegawai', 'adminPegawaiController@index')->name('pegawai.dashboard');

Route::get('webadmin/regulasi_cat/add', 'adminRegCatController@create')->name('rc.add');
Route::post('webadmin/regulasi_cat/add', 'adminRegCatController@store')->name('rc.add.submit');
Route::get('webadmin/regulasi_cat/{id}/edit', 'adminRegCatController@edit')->name('rc.edit');
Route::post('webadmin/regulasi_cat/{id}/edit', 'adminRegCatController@update')->name('rc.update');
Route::get('webadmin/regulasi_cat/{id}/delete', 'adminRegCatController@destroy')->name('rc.delete');
Route::get('webadmin/regulasi_cat', 'adminRegCatController@index')->name('rc.dashboard');

Route::get('webadmin/regulasi/add', 'adminIzinController@create')->name('izin.add');
Route::post('webadmin/regulasi/add', 'adminIzinController@store')->name('izin.add.submit');
Route::get('webadmin/regulasi/{id}/edit', 'adminIzinController@edit')->name('izin.edit');
Route::post('webadmin/regulasi/{id}/edit', 'adminIzinController@update')->name('izin.update');
Route::get('webadmin/regulasi/{id}/delete', 'adminIzinController@destroy')->name('izin.delete');
Route::get('webadmin/regulasi', 'adminIzinController@index')->name('izin.dashboard');

Route::post('webadmin/pengumuman/upload', 'adminPengumumanController@upload')->name('pengumuman.upload');
Route::get('webadmin/pengumuman/add', 'adminPengumumanController@create')->name('pengumuman.add');
Route::post('webadmin/pengumuman/add', 'adminPengumumanController@store')->name('pengumuman.add.submit');
Route::get('webadmin/pengumuman/{id}/edit', 'adminPengumumanController@edit')->name('pengumuman.edit');
Route::post('webadmin/pengumuman/{id}/edit', 'adminPengumumanController@update')->name('pengumuman.update');
Route::get('webadmin/pengumuman/{id}/delete', 'adminPengumumanController@destroy')->name('pengumuman.delete');
// Route::post('webadmin/pengumuman/upload_progress', 'adminPengumumanController@upload_progress')->name('pengumuman.upload');

Route::get('webadmin/pengumuman', 'adminPengumumanController@index')->name('pengumuman.dashboard');

Route::get('webadmin/chat/{id}/lihat', 'adminChatController@lihat')->name('chat.lihat');
Route::get('webadmin/chat/{sessid}/list', 'adminChatController@listchat')->name('chat.listchat');
Route::post('webadmin/chat/post', 'adminChatController@postchat')->name('chat.postchat');
Route::get('webadmin/chat/notif', 'adminChatController@chatnotif')->name('chat.chatnotif');
Route::get('webadmin/chat/notifcount', 'adminChatController@chatcount')->name('chat.chatcount');
Route::get('webadmin/chat/{id}/delete', 'adminChatController@destroy')->name('chat.delete');
Route::get('webadmin/chat', 'adminChatController@index')->name('chat.dashboard');


Route::get('webadmin/sidemenu/add', 'adminSideMenuController@create')->name('sidemenu.add');
Route::post('webadmin/sidemenu/add', 'adminSideMenuController@store')->name('sidemenu.add.submit');
Route::get('webadmin/sidemenu/{id}/edit', 'adminSideMenuController@edit')->name('sidemenu.edit');
Route::post('webadmin/sidemenu/{id}/edit', 'adminSideMenuController@update')->name('sidemenu.update');
Route::get('webadmin/sidemenu/{id}/delete', 'adminSideMenuController@destroy')->name('sidemenu.delete');
Route::get('webadmin/sidemenu', 'adminSideMenuController@index')->name('sidemenu.dashboard');
Route::get('webadmin/sidemenu/{id}/{status}/setstatus', 'adminSideMenuController@setstatus')->name('sidemenu.setstatus');

Route::get('webadmin/news/add', 'adminNewsController@create')->name('news.add');
Route::post('webadmin/news/add', 'adminNewsController@store')->name('news.add.submit');
Route::get('webadmin/news/{id}/edit', 'adminNewsController@edit')->name('news.edit');
Route::post('webadmin/news/{id}/edit', 'adminNewsController@update')->name('news.update');
Route::get('webadmin/news/{id}/delete', 'adminNewsController@destroy')->name('news.delete');
Route::get('webadmin/news', 'adminNewsController@index')->name('news.dashboard');
Route::post('webadmin/postimage', 'adminNewsController@postimage')->name('news.postimage');
Route::get('webadmin/chat', 'adminChatController@index')->name('chat.dashboard');

Route::get('webadmin/wikiuji/add', 'adminWikiUjiController@create')->name('wikiuji.add');
Route::post('webadmin/wikiuji/add', 'adminWikiUjiController@store')->name('wikiuji.add.submit');
Route::get('webadmin/wikiuji/{id}/edit', 'adminWikiUjiController@edit')->name('wikiuji.edit');
Route::post('webadmin/wikiuji/{id}/edit', 'adminWikiUjiController@update')->name('wikiuji.update');
Route::get('webadmin/wikiuji/{id}/delete', 'adminWikiUjiController@destroy')->name('wikiuji.delete');
Route::get('webadmin/wikiuji', 'adminWikiUjiController@index')->name('wikiuji.dashboard');

Route::get('webadmin/gallery/add', 'adminGalleryController@create')->name('gal.add');
Route::post('webadmin/gallery/add', 'adminGalleryController@store')->name('gal.add.submit');
Route::get('webadmin/gallery/{id}/edit', 'adminGalleryController@edit')->name('gal.edit');
Route::post('webadmin/gallery/{id}/edit', 'adminGalleryController@update')->name('gal.update');
Route::get('webadmin/gallery/{id}/delete', 'adminGalleryController@destroy')->name('gal.delete');
Route::post('webadmin/gallery/postImage', 'adminGalleryController@postImage')->name('gal.postimage');
Route::get('webadmin/gallery', 'adminGalleryController@index')->name('gal.dashboard');

Route::get('webadmin/event/add', 'adminEventController@create')->name('event.add');
Route::post('webadmin/event/add', 'adminEventController@store')->name('event.add.submit');
Route::get('webadmin/event/{id}/edit', 'adminEventController@edit')->name('event.edit');
Route::post('webadmin/event/{id}/edit', 'adminEventController@update')->name('event.update');
Route::get('webadmin/event/{id}/delete', 'adminEventController@destroy')->name('event.delete');
Route::get('webadmin/event', 'adminEventController@index')->name('event.dashboard');

Route::get('webadmin/apprconf/add', 'adminApprconfController@create')->name('apprconf.add');
Route::post('webadmin/apprconf/add', 'adminApprconfController@store')->name('apprconf.add.submit');
// Route::get('webadmin/apprconf/{id}/edit', 'adminApprconfController@edit')->name('apprconf.edit');
// Route::post('webadmin/apprconf/{id}/edit', 'adminApprconfController@update')->name('apprconf.update');
Route::get('webadmin/apprconf/{id}/delete', 'adminApprconfController@destroy')->name('apprconf.delete');
Route::get('webadmin/apprconf', 'adminApprconfController@index')->name('apprconf.dashboard');

Route::get('webadmin/approval/add', 'adminApprovalController@create')->name('approval.add');
Route::post('webadmin/approval/add', 'adminApprovalController@store')->name('approval.add.submit');
Route::get('webadmin/approval/{id}/detail', 'adminApprovalController@detail')->name('approval.detail');
// Route::get('webadmin/approval/{id}/edit', 'adminApprovalController@edit')->name('approval.edit');
Route::post('webadmin/approval/{id}/edit', 'adminApprovalController@update')->name('approval.update');
Route::get('webadmin/approval/{id}/delete', 'adminApprovalController@destroy')->name('approval.delete');
Route::get('webadmin/approval', 'adminApprovalController@index')->name('approval.dashboard');
